#include "libViewer/ViewerViewModel.h"

#include <iostream>

#include "libViewer/MetalRoughMaterials.h"
#include "libViewer/ObjectEnabledListViewItem.h"
#include "libViewer/ObjectVisibilityListViewItem.h"
#include "libViewer/PoseCalculator.h"
#include "libViewer/ViewportsManager.h"

namespace viewer
{
void ViewerViewModel::spatialObjectPicked(QString id, QVector3D world_position, QVector3D local_position)
{
    std::cout << "Spatial object with id " << id.toStdString() << " picked." << std::endl << "World position: " << world_position.x() << "," << world_position.y() << "," << world_position.z() << std::endl << "Local position: " << local_position.x() << "," << local_position.y() << "," << local_position.z() << std::endl;
    emit spatial_object_or_image_picked(id, world_position, local_position);
}

void ViewerViewModel::spatialObjectHover(QString id, QVector3D world_position, QVector3D local_position)
{
    std::cout << "Spatial object with id " << id.toStdString() << " hovered on." << std::endl << "World position: " << world_position.x() << "," << world_position.y() << "," << world_position.z() << std::endl << "Local position: " << local_position.x() << "," << local_position.y() << "," << local_position.z() << std::endl;
    emit spatial_object_or_image_hovered(id, world_position, local_position);
}

ViewerViewModel::ViewerViewModel(QObject* parent):QObject(parent)
{
    m_visibility_list.clear();
    m_viewer_config = new ViewerConfiguration(this);

    m_spatial_objects_manager = new SpatialObjectsManager(this);
    m_images_manager = new ImagesManager(this);
    m_viewports_manager = new ViewportsManager(m_viewer_config,this);

    auto axial_id_and_name = m_viewer_config->get_slice_id_and_name(SliceType::AXIAL);
    auto sagittal_id_and_name = m_viewer_config->get_slice_id_and_name(SliceType::SAGITTAL);
    auto coronal_id_and_name = m_viewer_config->get_slice_id_and_name(SliceType::CORONAL);
    add_to_visibility_list(
        QVariant::fromValue(new ObjectVisibilityListViewItem(axial_id_and_name.second, axial_id_and_name.first, VisibilityClass::VISIBLE)));
    add_to_visibility_list(
        QVariant::fromValue(new ObjectVisibilityListViewItem(sagittal_id_and_name.second, sagittal_id_and_name.first, VisibilityClass::VISIBLE)));
    add_to_visibility_list(
        QVariant::fromValue(new ObjectVisibilityListViewItem(coronal_id_and_name.second, coronal_id_and_name.first, VisibilityClass::VISIBLE)));
    add_to_enabled_list(QVariant::fromValue(new ObjectEnabledListViewItem(
        axial_id_and_name.second, axial_id_and_name.first, true)));
    add_to_enabled_list(QVariant::fromValue(new ObjectEnabledListViewItem(
        sagittal_id_and_name.second, sagittal_id_and_name.first, true)));
    add_to_enabled_list(QVariant::fromValue(new ObjectEnabledListViewItem(
        coronal_id_and_name.second, coronal_id_and_name.first, true)));
}

ViewerViewModel::~ViewerViewModel()
{
    std::cout << "~ViewerViewModel" << std::endl;
}

ViewerConfiguration* ViewerViewModel::get_viewer_configuration() const
{
    return m_viewports_manager->get_viewer_configuration();
}

void ViewerViewModel::add_to_visibility_list(QVariant item)
{
    m_visibility_list.append(item);
    emit visibility_list_model_changed();
}

void ViewerViewModel::remove_from_visibility_list(const QString& id)
{
    QList<QVariant>::iterator i;
    for (i = m_visibility_list.begin(); i != m_visibility_list.end(); ++i)
    {
        if (i->canConvert<ObjectVisibilityListViewItem*>())
        {
            auto item = i->value<ObjectVisibilityListViewItem*>();
            auto item_id = item->get_id();
            if (item_id == id)
            {
                m_visibility_list.erase(i);
                break;
            }
        }
        else
        {
            throw "Cannot convert to ObjectVisibilityListViewItem*";
        }
    }
}

QVariantList ViewerViewModel::get_enabled_list_model() const
{
    return m_enabled_list;
}

void ViewerViewModel::add_to_enabled_list(QVariant item)
{
    m_enabled_list.append(item);
    emit enabled_list_model_changed();
}

void ViewerViewModel::remove_from_enabled_list(const QString& id)
{
    QList<QVariant>::iterator i;
    for (i = m_enabled_list.begin(); i != m_enabled_list.end(); ++i)
    {
        if (i->canConvert<ObjectEnabledListViewItem*>())
        {
            auto item = i->value<ObjectEnabledListViewItem*>();
            auto item_id = item->get_id();
            if (item_id == id)
            {
                m_enabled_list.erase(i);
                break;
            }
        }
        else
        {
            throw "Cannot convert to ObjectEnabledListViewItem*";
        }
    }
}

void ViewerViewModel::add_image(const std::string& id, const VolumeImageData& image)
{
    m_images_manager->add_image(QString::fromStdString(id), image);
    emit images_manager_changed();
}

void ViewerViewModel::remove_image(const std::string& id)
{
    m_images_manager->remove_image(QString::fromStdString(id));
}

void ViewerViewModel::remove_all_images()
{
    for(const auto& id:m_images_manager->get_image_ids_in_viewer())
    {
        m_images_manager->remove_image(id);
    }
}

bool ViewerViewModel::contains_image_with_id(const std::string& id) const
{
    return m_images_manager->contains_image_with_id(QString::fromStdString(id));
}

void ViewerViewModel::set_ruler_enabled(bool enabled)
{
}

void ViewerViewModel::set_side(const Side& side)
{
    m_side = side;
}

void ViewerViewModel::set_slices_pose(const QVector3D& position, const QVector3D& x_axis, const QVector3D& normal)
{
    /* Input: Pose of axial plane in world COS (z direction towards camera, x direction to the right):
     * Z-direction (normal) of pose points towards negative z-axis of world/image COS, x-direction (x_axis) points towards x-axis of world/image COS.
     * Pose of other planes is then calculated (orthogonal to axial plane). */
    auto pose = PoseCalculator::get_transform_from(position, x_axis, normal);
    emit slice_pose_changed(pose);
}

void ViewerViewModel::set_object_pose(const std::string& id, const QVector3D& position, const QVector3D& x_axis,
                                          const QVector3D& normal)
{
    /* Input: Pose of axial plane in world COS (z direction towards camera, x direction to the right):
     * Z-direction (normal) of pose points towards negative z-axis of world/image COS, x-direction (x_axis) points towards x-axis of world/image COS.
     * Pose of other planes is then calculated (orthogonal to axial plane). */
    if(contains_object_with_id(id))
    {
        m_spatial_objects_manager->set_object_pose(id, position, x_axis, normal);
    }
}

void ViewerViewModel::set_threeD_viewer_camera_pose(const QVector3D& camera_position, const QVector3D& look_at_position,
    const QVector3D& up_vector)
{
    m_viewports_manager->set_threeD_viewer_camera_pose(camera_position, look_at_position, up_vector);
}

void ViewerViewModel::threeD_viewer_camera_view_all()
{
    m_viewports_manager->threeD_viewer_camera_view_all();
    emit viewports_manager_changed();
}

QVariantList ViewerViewModel::get_visibility_list_model() const
{
    return m_visibility_list;
}

void ViewerViewModel::set_object_visibility(const std::string& id, const VisibilityClass::Visibility& visibility)
{
    if(contains_object_with_id(id) || is_id_a_slice(id))
    {
        for (int i = 0;i<m_visibility_list.length(); i++)
        {
            if (m_visibility_list.at(i).value<ObjectVisibilityListViewItem*>()->get_id().compare(QString::fromStdString(id)) == 0)
            {
                m_visibility_list.at(i).value<ObjectVisibilityListViewItem*>()->set_visibility(visibility);
                //emit visibility_list_model_changed();
            }
        }
    }
}

void ViewerViewModel::set_object_is_enabled(const std::string& id, bool is_enabled)
{
    if (contains_object_with_id(id))
    {
        for (int i = 0;i< m_enabled_list.length(); i++)
        {
            if (m_enabled_list.at(i).value<ObjectEnabledListViewItem*>()->get_id().compare(QString::fromStdString(id)) == 0)
            {
                m_enabled_list.at(i).value<ObjectEnabledListViewItem*>()->set_is_enabled(is_enabled);
            }
        }
    }
}

bool ViewerViewModel::get_object_is_enabled(const std::string& id)
{
    if (contains_object_with_id(id))
    {
        for (int i = 0; i < m_enabled_list.length(); i++)
        {
            if (m_enabled_list.at(i).value<ObjectEnabledListViewItem*>()->get_id().compare(QString::fromStdString(id)) == 0)
            {
                return m_enabled_list.at(i).value<ObjectEnabledListViewItem*>()->get_is_enabled();
            }
        }
    } else
    {
        auto msg = "Object with id " + id + " is not in viewer";
        throw msg;
    }

}

void ViewerViewModel::hide_all()
{
    for (int i = 0; i < m_visibility_list.length(); i++)
    {
        m_visibility_list.at(i).value<ObjectVisibilityListViewItem*>()->set_visibility(VisibilityClass::Visibility::HIDDEN);
    }
}


void ViewerViewModel::show_all()
{
    for (int i = 0; i < m_visibility_list.length(); i++)
    {
        m_visibility_list.at(i).value<ObjectVisibilityListViewItem*>()->set_visibility(VisibilityClass::Visibility::VISIBLE);
    }
}

void ViewerViewModel::show_slice_in_threeD(const SliceType& type)
{
    auto id_name = m_viewer_config->get_slice_id_and_name(type);
    set_object_visibility(id_name.first.toStdString(),VisibilityClass::VISIBLE);
}

void ViewerViewModel::show_slices_in_threeD()
{
    std::vector<SliceType> types{ SliceType::AXIAL,SliceType::SAGITTAL,SliceType::CORONAL };
    for(const auto t:types)
    {
        auto id_name = m_viewer_config->get_slice_id_and_name(t);
        set_object_visibility(id_name.first.toStdString(), VisibilityClass::VISIBLE);
    }
}

void ViewerViewModel::hide_slice_in_threeD(const SliceType& type)
{
    auto id_name = m_viewer_config->get_slice_id_and_name(type);
    set_object_visibility(id_name.first.toStdString(), VisibilityClass::HIDDEN);
}

void ViewerViewModel::hide_slices_in_threeD()
{
    std::vector<SliceType> types{ SliceType::AXIAL,SliceType::SAGITTAL,SliceType::CORONAL };
    for (const auto t : types)
    {
        auto id_name = m_viewer_config->get_slice_id_and_name(t);
        set_object_visibility(id_name.first.toStdString(), VisibilityClass::HIDDEN);
    }
}

void ViewerViewModel::set_spatial_object_viewport_visibility(const std::string& id,
                                                             const viewer::ViewportVisibility& viewport_visibility)
{
    m_spatial_objects_manager->set_viewport_visibility(id, viewport_visibility);
}

ViewportVisibility* ViewerViewModel::get_spatial_object_viewport_visibility(const std::string& id) const
{
    return m_spatial_objects_manager->get_viewport_visibility(id);
}

void ViewerViewModel::set_object_material(const std::string& id, const MaterialSettings& material_settings)
{
    m_spatial_objects_manager->set_object_material(id, material_settings);
}

MaterialSettings ViewerViewModel::get_object_material(const std::string& id) const
{
    return m_spatial_objects_manager->get_object_material(id);
}

void ViewerViewModel::set_image_visibility(const std::string& id,
                                           const viewer::ViewportVisibility& viewport_visibility)
{
    m_images_manager->set_viewport_visibility(QString::fromStdString(id), viewport_visibility);
}

void ViewerViewModel::set_object_picking_enabled(const std::string& id, bool picking_enabled)
{
    m_spatial_objects_manager->set_picking_enabled(id, picking_enabled);
}

void ViewerViewModel::set_all_object_picking_enabled(bool picking_enabled)
{
    auto ids = m_spatial_objects_manager->get_object_ids_in_viewer();
    for (const auto& id : ids)
    {
        try
        {
            std::cout << "Set object picking enabled for object with id " << id << std::endl;
            set_object_picking_enabled(id, picking_enabled);
        }
        catch (std::exception& ex)
        {
            std::cerr << "Could not enable picking for object with id: " << id << ". Exception: " << ex.what() << std::endl;
        }
    }
}

void ViewerViewModel::set_all_picking_enabled(bool picking_enabled)
{
    auto ids = m_spatial_objects_manager->get_object_ids_in_viewer();
    for(const auto& id:ids)
    {
        try
        {
            std::cout << "Set object picking enabled for object with id " << id << std::endl;
            set_object_picking_enabled(id, picking_enabled);
        } catch(std::exception& ex)
        {
            std::cerr << "Could not enable picking for object with id: " << id << ". Exception: " << ex.what() << std::endl;
        }
    }
    set_slice_picking_enabled(picking_enabled);
}

void ViewerViewModel::set_slice_picking_enabled(bool picking_enabled)
{
    m_images_manager->set_slice_picking_enabled(picking_enabled);
}

void ViewerViewModel::add_object(const std::string& id, const std::string& file_path_name, const Pose& pose, const std::string& human_readable_name,
                                 const MaterialSettings& material_settings, const ViewportVisibility& viewport_visibility, bool picking_enabled)
{
    if (!contains_object_with_id(id))
    {
        add_to_visibility_list(QVariant::fromValue(new ObjectVisibilityListViewItem(
            QString::fromStdString(human_readable_name), QString::fromStdString(id), VisibilityClass::VISIBLE)));
        add_to_enabled_list(QVariant::fromValue(new ObjectEnabledListViewItem(
            QString::fromStdString(human_readable_name), QString::fromStdString(id),true)));
        m_spatial_objects_manager->add_object_from_file(id, file_path_name, pose, material_settings,viewport_visibility,picking_enabled);
    }
    else
    {
        throw std::runtime_error("Viewer already contains object with id " + id);
    }
}

void ViewerViewModel::add_object(const std::string& id, Qt3DRender::QGeometryRenderer* primitive_geometry,
                                             const Pose& pose, const std::string& human_readable_name, const MaterialSettings& material_settings, const ViewportVisibility& viewport_visibility, bool picking_enabled)
{
    if (!contains_object_with_id(id))
    {
        add_to_visibility_list(QVariant::fromValue(new ObjectVisibilityListViewItem(
            QString::fromStdString(human_readable_name), QString::fromStdString(id), VisibilityClass::VISIBLE)));
        add_to_enabled_list(QVariant::fromValue(new ObjectEnabledListViewItem(
            QString::fromStdString(human_readable_name), QString::fromStdString(id), true)));
        m_spatial_objects_manager->add_primitive_geometry(id, primitive_geometry, pose, material_settings,viewport_visibility, picking_enabled);
    }
    else
    {
        throw std::runtime_error("Viewer already contains object with id " + id);
    }
}

void ViewerViewModel::add_object(const std::string& id, QMLSpatialObjectType::Type type, QMLSpatialObjectParameters* params, const Pose& pose,
    const std::string& human_readable_name, const MaterialSettings& material_settings, const ViewportVisibility& viewport_visibility)
{
    if (!contains_object_with_id(id))
    {
        add_to_visibility_list(QVariant::fromValue(new ObjectVisibilityListViewItem(
            QString::fromStdString(human_readable_name), QString::fromStdString(id), VisibilityClass::VISIBLE)));
        add_to_enabled_list(QVariant::fromValue(new ObjectEnabledListViewItem(
            QString::fromStdString(human_readable_name), QString::fromStdString(id), true)));
        m_spatial_objects_manager->add_object_by_type(id, type,params, pose, material_settings,viewport_visibility);
    }
    else
    {
        throw std::runtime_error("Viewer already contains object with id " + id);
    }

}

void ViewerViewModel::remove_object(const std::string& id)
{
    if (contains_object_with_id(id))
    {
        m_spatial_objects_manager->remove_object(id);
        remove_from_visibility_list(QString::fromStdString(id));
        remove_from_enabled_list(QString::fromStdString(id));
        emit visibility_list_model_changed();
    }
    else
    {
        throw std::runtime_error("Cannot remove object that is not in viewer. " + id);
    }

}

void ViewerViewModel::remove_all_objects()
{
    auto ids = get_object_ids_in_viewer();
    for(const auto& id:ids)
    {
        remove_object(id);
    }
}

void ViewerViewModel::add_animation(const std::string& id, const AnimationProperty& animation_property,
                                    const AnimationParameters& animation_parameters)
{
    if (contains_object_with_id(id))
    {
        m_spatial_objects_manager->add_animation(id, animation_property,animation_parameters);
    }
    else
    {
        throw std::runtime_error("Viewer can only add animation to object already part of viewer: " + id);
    }
}

void ViewerViewModel::remove_animations(const std::string& id)
{
    if (contains_object_with_id(id))
    {
        m_spatial_objects_manager->remove_animations(id);
    }
    else
    {
        throw std::runtime_error("Viewer can only remove animations from object already in the viewer: " + id);
    }
}

void ViewerViewModel::remove_animation(const std::string& id, const AnimationProperty& animation_property)
{
    if (contains_object_with_id(id))
    {
        m_spatial_objects_manager->remove_animation(id,animation_property);
    }
    else
    {
        throw std::runtime_error("Viewer can only remove animations from object already in the viewer: " + id);
    }
}

void ViewerViewModel::set_slice_viewer_behaviour(ViewerBehaviour::Behaviour viewer_behaviour)
{
    m_viewports_manager->set_slice_viewer_behaviour(viewer_behaviour);
    emit viewports_manager_changed();
}

void ViewerViewModel::set_threeD_viewer_behaviour(ViewerBehaviour::Behaviour viewer_behaviour)
{
    m_viewports_manager->set_threeD_viewer_behaviour(viewer_behaviour);
    emit viewports_manager_changed();
}

Side ViewerViewModel::get_side() const
{
    return m_side;
}

bool ViewerViewModel::is_id_a_slice(const std::string& id) const
{
    bool is_a_slice = false;
    auto slice_ids = m_viewer_config->get_slice_ids();
    if (std::find(slice_ids.begin(), slice_ids.end(), QString::fromStdString(id)) != slice_ids.end())
    {
        is_a_slice = true;
    }
    return is_a_slice;
}

bool ViewerViewModel::contains_object_with_id(const std::string& id) const
{
    auto ids_in_viewer = get_object_ids_in_viewer();
    if (std::find(ids_in_viewer.begin(), ids_in_viewer.end(), id) != ids_in_viewer.end())
    {
        return true;
    }
    return false;
}

void ViewerViewModel::set_axial_slice_pose(const QMatrix4x4& mat)
{
    if(m_axial_slice_pose!=mat)
    {
        std::lock_guard lg(m_mutex);
        m_axial_slice_pose = mat;
    }
}

void ViewerViewModel::move_to_next_slice(int viewport_idx, int cranial_or_caudal)
{
    move_to_next_slice_requested(viewport_idx, cranial_or_caudal);
}

void ViewerViewModel::reset_viewport(int viewport_type)
{
    reset_viewport_requested(viewport_type);
}

bool ViewerViewModel::get_tracked_model_pose_indicator_visibility() const
{
    return m_tracked_model_pose_indicator_visibility;
}

void ViewerViewModel::set_tracked_model_pose_indicator_visibility(bool visibility)
{
    if (m_tracked_model_pose_indicator_visibility != visibility)
    {
        m_tracked_model_pose_indicator_visibility = visibility;
        emit tracked_model_pose_indicator_visibility_changed();
    }
}

void ViewerViewModel::expand_viewport(int viewport)
{
    m_viewports_manager->get_viewer_configuration()->expand_viewport(viewport);
    //emit viewports_manager_changed();
}

int ViewerViewModel::get_expanded_viewport() const
{
    return m_viewports_manager->get_viewer_configuration()->get_expanded_viewport();
}

void ViewerViewModel::set_viewer_configuration(ViewerConfiguration* viewer_configuration)
{
    m_viewer_config = viewer_configuration;
    m_viewports_manager->set_viewer_configuration(viewer_configuration);
    emit viewports_manager_changed();
}

QMatrix4x4 ViewerViewModel::get_axial_slice_pose() const
{
    std::lock_guard lg(m_mutex);
    return m_axial_slice_pose;
}

QMLSpatialObjectParameters* ViewerViewModel::get_spatial_object_parameters(const std::string& id) const
{
    return m_spatial_objects_manager->get_spatial_object_parameters(id);
}

std::vector<std::string> ViewerViewModel::get_object_ids_in_viewer() const
{
    return m_spatial_objects_manager->get_object_ids_in_viewer();
}

std::vector<std::string> ViewerViewModel::get_image_ids_in_viewer() const
{
    std::vector<std::string> ids;
    for(const auto& id:m_images_manager->get_image_ids_in_viewer())
    {
        ids.push_back(id.toStdString());
    }
    return ids;
}

std::vector<std::string> ViewerViewModel::get_primitive_spatial_object_ids_in_viewer() const
{
    return m_spatial_objects_manager->get_primitive_object_ids_in_viewer();
}

std::vector<std::string> ViewerViewModel::get_spatial_object_from_file_ids_in_viewer() const
{
    return m_spatial_objects_manager->get_object_from_file_ids_in_viewer();
}

std::vector<std::string> ViewerViewModel::get_qml_spatial_object_ids_in_viewer() const
{
    return m_spatial_objects_manager->get_qml_object_ids_in_viewer();
}

ImageIntensityRangeHighlightSettings* ViewerViewModel::get_image_intensity_range_highlight_settings() const
{
    return m_images_manager->get_image_intensity_range_highlight_settings();
}
}
