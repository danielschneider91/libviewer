#include "libViewer/ObjectEnabledListViewItem.h"

namespace viewer {
ObjectEnabledListViewItem::ObjectEnabledListViewItem(): QObject(),m_name(""),m_id(""),m_is_enabled(true)
{
}

ObjectEnabledListViewItem::ObjectEnabledListViewItem(const QString& name, const QString& id,
	const bool& is_enabled) : QObject(), m_name(name), m_id(id), m_is_enabled(is_enabled)
{
}

QString ObjectEnabledListViewItem::get_name() const
{
	return m_name;
}

void ObjectEnabledListViewItem::set_name(const QString& name)
{
	if (m_name != name)
	{
		m_name = name;
		emit name_changed();
	}
}

QString ObjectEnabledListViewItem::get_id() const
{
	return m_id;
}

void ObjectEnabledListViewItem::set_id(const QString& id)
{
	if(m_id!=id)
	{
		m_id = id;
		emit id_changed();
	}
}

bool ObjectEnabledListViewItem::get_is_enabled() const
{
	return m_is_enabled;
}

void ObjectEnabledListViewItem::set_is_enabled(bool is_enabled)
{
	if (m_is_enabled != is_enabled)
	{
		m_is_enabled = is_enabled;
		emit is_enabled_changed();
	}
}
}
