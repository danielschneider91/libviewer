#include "libViewer/AnimationContainer.h"


#include <iostream>
#include <qpropertyanimation.h>
#include <qsequentialanimationgroup.h>
#include <sstream>
#include <Qt3DCore/QTransform>

namespace viewer {
void AnimationContainer::add_animation(const std::string& id, const AnimationProperty& animation_property, const AnimationParameters& animation_params, QObject* target)
{
    switch (animation_property)
    {
	case AnimationProperty::SCALE:
	{
		try {
			auto params = dynamic_cast<const NumberAnimationParameters&>(animation_params);
			auto animation = create_scale_animation(target, params);
			add_animation_private(id, animation_property, animation);
		} catch (std::exception& ex)
		{
			std::stringstream ss;
			ss << "In combination with AnimationProperty::Scale use NumberAnimationParameters. Exception: " << ex.what();
			std::cerr << ss.str() << std::endl;
			throw ss.str();
		}
		break;
	}
    case AnimationProperty::COLOR: 
	{
		throw "AnimationProperty::COLOR is not implemented yet.";
		break;
	}
    default: ;
    }
}

void AnimationContainer::remove_animation(const std::string& id, const AnimationProperty& animation_property)
{
	if (m_objects_id_to_animation_map.find(id) != m_objects_id_to_animation_map.end())
	{
		auto animations = m_objects_id_to_animation_map.at(id);
		if (animations.find(animation_property) != animations.end())
		{
			auto animation_group = animations.at(animation_property);
			// There are 2 in a group creating a "loop", both targeting the same object. Thus it is possible to pick any (index 0 or 1), however need to adjust property_animation->start/endValue())
			auto property_animation = qobject_cast<QPropertyAnimation*>(animation_group->animationAt(0));
			property_animation->targetObject()->setProperty(property_animation->propertyName(),
				property_animation->startValue());
			animation_group->stop();
			delete animation_group;
			animations.erase(animation_property);
		}
		if (animations.size() == 0)
		{
			m_objects_id_to_animation_map.erase(id);
		}
	}
}

void AnimationContainer::remove_animations(const std::string& id)
{
	if (m_objects_id_to_animation_map.find(id) != m_objects_id_to_animation_map.end())
	{
		auto animations = m_objects_id_to_animation_map.at(id);
		std::map<AnimationProperty, QSequentialAnimationGroup*>::iterator it;

		for (it = animations.begin(); it != animations.end(); it++)
		{
			auto animation_group = it->second;
			// There are 2 in a group creating a "loop", both targeting the same object. Thus it is possible to pick any (index 0 or 1), however need to adjust property_animation->start/endValue())
			auto property_animation = qobject_cast<QPropertyAnimation*>(animation_group->animationAt(0));
			property_animation->targetObject()->setProperty(property_animation->propertyName(),
				property_animation->startValue());
			animation_group->stop();
			delete it->second;
			animations.erase(it->first);
		}
		assert(animations.size() == 0);
		m_objects_id_to_animation_map.erase(id);
	}
}

void AnimationContainer::add_animation_private(const std::string& id,const AnimationProperty& animation_property, QSequentialAnimationGroup* animation)
{
	if (m_objects_id_to_animation_map.find(id) != m_objects_id_to_animation_map.end())
	{
		auto animations = m_objects_id_to_animation_map.at(id);
		if (animations.find(animation_property) != animations.end())
		{
			std::stringstream ss;
			ss << "Object with id " << id << " already contains animation of property " << static_cast<int>(animation_property) << ". Remove the existing first.";
			std::cerr << ss.str() << std::endl;
			throw ss.str();
		}
		animations.insert(std::make_pair(animation_property, animation));
		return;
	}

	std::map<AnimationProperty, QSequentialAnimationGroup*> map;
	map.insert(std::make_pair(animation_property, animation));
	m_objects_id_to_animation_map.insert(std::make_pair(id, map));
}

QSequentialAnimationGroup* AnimationContainer::create_scale_animation(QObject* target,
    const NumberAnimationParameters& animation_parameters) const
{
	QSequentialAnimationGroup* group = new QSequentialAnimationGroup;
	QPropertyAnimation* from_to_anim = new QPropertyAnimation(target, "scale");
	from_to_anim->setStartValue(animation_parameters.get_from());
	from_to_anim->setEndValue(animation_parameters.get_to());
	from_to_anim->setDuration(static_cast<int>(std::round(animation_parameters.get_duration() / 2.0f)));
	from_to_anim->setEasingCurve(QEasingCurve::Type::InCubic);
	from_to_anim->setLoopCount(1);

	QPropertyAnimation* to_from_anim = new QPropertyAnimation(target, "scale");
	to_from_anim->setStartValue(animation_parameters.get_to());
	to_from_anim->setEndValue(animation_parameters.get_from());
	to_from_anim->setDuration(static_cast<int>(std::round(animation_parameters.get_duration() / 2.0f)));
	to_from_anim->setEasingCurve(QEasingCurve::Type::OutQuad);
	to_from_anim->setLoopCount(1);

	group->addAnimation(from_to_anim);
	group->addAnimation(to_from_anim);
	group->setLoopCount(-1);
	group->start(QAbstractAnimation::DeleteWhenStopped);

	return group;
}
}
