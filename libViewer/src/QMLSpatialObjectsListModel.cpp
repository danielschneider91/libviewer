#include "libViewer/QMLSpatialObjectsListModel.h"

#include <iostream>

namespace viewer
{
QMLSpatialObjectsListModel::QMLSpatialObjectsListModel(QObject* parent): QAbstractListModel(parent)
{
}

QMLSpatialObjectsListModel::~QMLSpatialObjectsListModel()
{
}

QHash<int, QByteArray> QMLSpatialObjectsListModel::roleNames() const {
    QHash<int, QByteArray> roles;
    roles[Type] = "type";
    roles[Parameters] = "parameters";
    roles[Transform] = "transform";
    roles[Material] = "material";
    roles[TransparentMaterial] = "transparentMaterial";
    roles[Enabled] = "enabled";
    roles[Visible] = "visible";
    roles[Name] = "name";
    roles[Id] = "id";
    roles[PickingEnabled] = "pickingEnabled";
    return roles;
}

void QMLSpatialObjectsListModel::set_picking_enabled(const std::string& id, const bool picking_enabled)
{
    auto index = m_id_index_map.at(QString::fromStdString(id));
    m_data[index]->set_is_picking_enabled(picking_enabled);
    QModelIndex top = createIndex(index, 0);
    emit dataChanged(top, top);
}

QVariant QMLSpatialObjectsListModel::data(const QModelIndex& index, int role) const
{
    QVariant result = QVariant();
    int row = index.row();
    int column = index.column();

    if (!index.isValid() || row >= rowCount())
    {
        return result;
    }

    switch (role)
    {
    case Type:
    {
        return m_data.at(row)->get_type()->get_type();
    }
    case Parameters:
    {
        return QVariant::fromValue(m_data.at(row)->get_parameters());
    }
    case Transform:
    {
        return QVariant::fromValue(m_data.at(row)->get_transform());
    }
    case Material:
    {
        return QVariant::fromValue(m_data.at(row)->get_material());
    }
    case TransparentMaterial:
    {
        return QVariant::fromValue(m_data.at(row)->get_transparent_material());
    }
    case Enabled:
    {
        return QVariant::fromValue(m_data.at(row)->get_is_enabled());
    }
    case Visible:
    {
        return QVariant::fromValue(m_data.at(row)->get_is_visible());
    }
    case Name:
    {
        return QVariant::fromValue(m_data.at(row)->get_name());
    }
    case Id:
    {
        return QVariant::fromValue(m_data.at(row)->get_id());
    }
    case PickingEnabled:
    {
        return QVariant::fromValue(m_data.at(row)->get_is_picking_enabled());
    }
    default: break;
    }


    return result;
}

int QMLSpatialObjectsListModel::rowCount(const QModelIndex& parent) const
{
    if(parent.isValid())
    {
        return 0;
    }
    return m_data.size();
}

Qt::ItemFlags QMLSpatialObjectsListModel::flags(const QModelIndex& index) const
{
    Qt::ItemFlags result = Qt::ItemIsEditable | QAbstractItemModel::flags(index);
    return result;
}

void QMLSpatialObjectsListModel::add_object(QMLSpatialObject* data)
{
    beginInsertRows(QModelIndex(), m_data.size(), m_data.size());
    m_id_index_map[data->get_id()] = m_data.size();
    m_data.push_back(data);
    endInsertRows();
}

void QMLSpatialObjectsListModel::remove_object(const QString& id)
{
    auto index = m_id_index_map.at(id);
    std::cout << "Removing object with id " << id.toStdString() << " at index " << index << std::endl;
    beginRemoveRows(QModelIndex(), index, index); // Specify the first and last row numbers for the span of rows you want to remove from an item in a model.
    delete m_data.takeAt(index);
    endRemoveRows();

    // rebuild id_index_map
    m_id_index_map.clear();
    for (int i = 0; i < m_data.count(); ++i)
    {
        m_id_index_map[m_data[i]->get_id()] = i;
    }
}

QList<QMLSpatialObject*> QMLSpatialObjectsListModel::get_objects() const
{
    return m_data;
}

void QMLSpatialObjectsListModel::set_pose(const std::string& id, const viewer::Pose& pose)
{
    auto index = m_id_index_map[QString::fromStdString(id)];
    m_data[index]->set_pose((pose.get_position()), (pose.get_x_axis()), (pose.get_z_axis()));
    QModelIndex top = createIndex(index, 0);
    emit dataChanged(top, top);
}

void QMLSpatialObjectsListModel::set_material(const std::string& id, const viewer::MaterialSettings& material_settings)
{
    auto index = m_id_index_map[QString::fromStdString(id)];
    m_data[index]->set_material(material_settings.get_metal_rough_material_properties().get_base_color(),
        material_settings.get_metal_rough_material_properties().get_metalness(),
        material_settings.get_metal_rough_material_properties().get_roughness(),
        material_settings.get_metal_rough_material_properties().get_normal(),
        material_settings.get_metal_rough_material_properties().get_ambient_occlusion(),
        material_settings.get_metal_rough_material_properties().get_texture_scale(),
        material_settings.get_transparent_color());
    QModelIndex top = createIndex(index, 0);
    emit dataChanged(top, top);

}

MaterialSettings QMLSpatialObjectsListModel::get_material(const std::string& id) const
{
    auto index = m_id_index_map.at(QString::fromStdString(id));
    return m_data[index]->get_material();
}

QMLSpatialObjectParameters* QMLSpatialObjectsListModel::get_parameters(const std::string& id) const
{
    auto index = m_id_index_map.at(QString::fromStdString(id));
    return m_data[index]->get_parameters();
}
}
