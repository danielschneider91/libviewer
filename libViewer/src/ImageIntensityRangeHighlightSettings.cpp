#include "libViewer/ImageIntensityRangeHighlightSettings.h"

namespace viewer
{
ImageIntensityRangeHighlightSettings::ImageIntensityRangeHighlightSettings(QObject* parent) :QObject(parent),m_lower_threshold(300),m_upper_threshold(3000),m_color(0,0,255,255)
{
}

bool ImageIntensityRangeHighlightSettings::is_highlight() const
{
    return m_highlight;
}

void ImageIntensityRangeHighlightSettings::set_highlight(const bool highlight)
{
    if(m_highlight!=highlight)
    {
        m_highlight = highlight;
        emit highlight_changed();
    }
}

float ImageIntensityRangeHighlightSettings::get_lower_threshold() const
{
    return m_lower_threshold;
}

void ImageIntensityRangeHighlightSettings::set_lower_threshold(const float lower_threshold)
{
    if (m_lower_threshold != lower_threshold)
    {
        m_lower_threshold = lower_threshold;
        emit lower_threshold_changed();
    }
}

float ImageIntensityRangeHighlightSettings::get_upper_threshold() const
{
    return m_upper_threshold;
}

void ImageIntensityRangeHighlightSettings::set_upper_threshold(const float upper_threshold)
{
    if (m_upper_threshold != upper_threshold)
    {
        m_upper_threshold = upper_threshold;
        emit upper_threshold_changed();
    }
}

QColor ImageIntensityRangeHighlightSettings::get_color() const
{
    return m_color;
}

void ImageIntensityRangeHighlightSettings::set_color(const QColor& color)
{
    if (m_color != color)
    {
        m_color = color;
        emit color_changed();
    }
}
}
