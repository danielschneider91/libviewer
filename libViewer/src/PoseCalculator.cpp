#include "libViewer/PoseCalculator.h"

#include <iostream>

namespace viewer {
	QMatrix4x4 PoseCalculator::get_transform_from(const QVector3D& position, const QVector3D& x_axis, const QVector3D& z_axis)
	{
		/* If position, x_axis, and z_axis are the position, x-axis, and z-axis vectors of an object represented in the world COS, then:
		 * This function returns an Affin3d matrix containing the pose of the object at position (represented in the world COS)
		 * with its x-axis and z-axis corresponding to x_axis and z_axis represented in the world COS (world_T_object).
		 * The Affine3d matrix at the same time describes how to transform a vector represented in the object COS into its representation in the world COS (T_world<-object).
		 * If Affine3d = [R,t;0,1], then the rotational part R contains the x_axis (in world COS) in its first column and the z_axis (in world COS) in its last column.
		 * The translational part t contains position (in world COS).
		 *
	     * QMatrix4x4, by default stores values in column-major format. 
		 */
		QMatrix4x4 T_w_object;
		auto y_axis = QVector3D::crossProduct(z_axis, x_axis);
		T_w_object.setColumn(0, QVector4D(x_axis.x(), x_axis.y(), x_axis.z(), 0));
		T_w_object.setColumn(1, QVector4D(y_axis.x(), y_axis.y(), y_axis.z(), 0));
		T_w_object.setColumn(2, QVector4D(z_axis.x(), z_axis.y(), z_axis.z(), 0));
		T_w_object.setColumn(3,QVector4D(position.x(), position.y(), position.z(), 1));

		return T_w_object;

	}

    Pose PoseCalculator::get_transform_from(const QMatrix4x4& pose)
    {
		auto x_axis = pose.column(0).toVector3D();
		auto z_axis = pose.column(2).toVector3D();
		auto position = pose.column(3).toVector3D();
		return Pose(position, x_axis, z_axis);
    };

}
