#include "libViewer/VolumeImageDataQml.h"

#include <iostream>

namespace viewer
{

VolumeImageDataQml::VolumeImageDataQml(QObject *parent):QObject(parent), m_textureData(createTexture(m_dimension[0], m_dimension[1], m_dimension[2],m_channels)),
m_textureDataGradient(createTexture(m_dimension[0], m_dimension[1], m_dimension[2], 3))
{
    m_orientation.setToIdentity();
}

QVector3D VolumeImageDataQml::get_origin() const
{
    return m_origin;
}

QVector3D VolumeImageDataQml::get_bounding_box_origin() const
{
    return m_origin - get_spacing() / 2.0;
}

void VolumeImageDataQml::set_origin(const QVector3D& origin)
{
    m_origin = origin;
}

QMatrix4x4 VolumeImageDataQml::get_orientation() const
{
    return m_orientation;
}

void VolumeImageDataQml::set_orientation(const QMatrix4x4& orientation)
{
    m_orientation = orientation;
}

QVector3D VolumeImageDataQml::get_spacing() const
{
    return m_spacing;
}

void VolumeImageDataQml::set_spacing(const QVector3D& spacing)
{
    m_spacing = spacing;
}

QVector3D VolumeImageDataQml::get_dimension() const
{
    return m_dimension;
}

QVector3D VolumeImageDataQml::get_dimension_times_spacing() const
{
    return m_dimension * m_spacing;
}

short VolumeImageDataQml::get_minimum_value() const
{
    return m_minimum_value;
}

short VolumeImageDataQml::get_maximum_value() const
{
    return m_maximum_value;
}

void VolumeImageDataQml::set_dimension(const QVector3D& dimension)
{
    m_dimension = dimension;
}
int VolumeImageDataQml::get_channels() const
{
    return m_channels;
}

void VolumeImageDataQml::set_texture_data(const QByteArray &data)
{
    if(m_textureData!=data) {
        m_textureData.clear();
        m_textureData = data;
        emit textureDataChanged();
    }
}

void VolumeImageDataQml::set_texture_data(const std::vector<short>& data)
{
    auto byte_array = vectorShortToByteArray(data);

    auto min = std::min_element(std::begin(data), std::end(data)); // c++11
    auto max = std::max_element(std::begin(data), std::end(data)); // c++11
    m_minimum_value = *min;
    m_maximum_value = *max;

    set_texture_data(byte_array);
}

void VolumeImageDataQml::set_texture_data_gradient(const std::vector<short>& data)
{
    int counter = 0;
    auto byte_array = vectorShortToByteArray(data);
    if (m_textureDataGradient != byte_array) {
        m_textureDataGradient.clear();
        m_textureDataGradient = byte_array;
    }
    m_textureGradientPrecomputed = true;
}

void VolumeImageDataQml::set_channels(int c)
{
    m_channels = c;
}

QByteArray VolumeImageDataQml::get_texture_data() const
{
    return m_textureData;
}

QByteArray VolumeImageDataQml::get_texture_gradient() const
{
    return m_textureDataGradient;
}

bool VolumeImageDataQml::get_texture_gradient_precomputed() const
{
    return m_textureGradientPrecomputed;
}

QByteArray VolumeImageDataQml::createTexture(int maxX, int maxY, int maxZ, int numberOfChannels) const
{
    QByteArray ba;
    ba.resize(maxX*maxY*maxZ*numberOfChannels);

    int bacounter = 0;
    int val = 0;
    for(int i=0;i<maxX;i++) {
        for(int j=0;j<maxY;j++) {
            for(int k=0;k<maxZ;k++) {
                if(val>=255) {
                    val = 0;
                }
                for(int c =0;c<numberOfChannels;c++) {
                    if(c==2) {
                        ba[bacounter] = (char)val;//'z';
                    } else {
                        ba[bacounter]='0';
                    }
                    bacounter++;
                }
                val++;

            }
        }
    }
    return ba;
}

QByteArray VolumeImageDataQml::vectorShortToByteArray(const std::vector<short>& data)
{
    QByteArray byte_array;

    int counter = 0;
    for (const auto& val : data)
    {
        char aa[sizeof(short)];
        memcpy(&aa, &val, sizeof(short)); 

        byte_array.push_back(aa[0]);
        counter++;
        byte_array.push_back(aa[1]);
        counter++;
    }

    return byte_array;
}

float VolumeImageDataQml::get_maximum_extent() const {
    const auto dimenstion_times_spacing = get_dimension_times_spacing();
    return sqrt(dimenstion_times_spacing.x()*dimenstion_times_spacing.x()+
                 dimenstion_times_spacing.y()*dimenstion_times_spacing.y()+
                 dimenstion_times_spacing.z()*dimenstion_times_spacing.z());
}
}
