#include "libViewer/TextureImageGradientGenerator3D.h"

#include <iostream>
#include <QTextureImageData>
#include <QTextureImageDataPtr>
namespace viewer {
Qt3DRender::QTextureImageDataPtr TextureImageGradientGenerator3D::operator()() {
    Qt3DRender::QTextureImageDataPtr data = Qt3DRender::QTextureImageDataPtr::create();

    int maxX = m_dimension[0];
    int maxY = m_dimension[1];
    int maxZ = m_dimension[2];
    int numberOfChannels = m_channels;

    data->setLayers(1);
    data->setMipLevels(1);
    data->setTarget(QOpenGLTexture::Target3D);

    if (maxX == 0 || maxY== 0 || maxZ== 0) {

        data->setPixelFormat(QOpenGLTexture::PixelFormat::RGB_Integer);
        data->setPixelType(QOpenGLTexture::PixelType::Int16);
        data->setFormat(QOpenGLTexture::TextureFormat::RGB16_SNorm);

        data->setWidth(16);
        data->setHeight(16);
        data->setDepth(32);

        auto texture = createTexture(16, 16, 32, numberOfChannels);
        data->setData(texture, 3);
    }
    else {
        data->setWidth(maxX);
        data->setHeight(maxY);
        data->setDepth(maxZ);

        data->setPixelFormat(QOpenGLTexture::PixelFormat::RGB);
        data->setPixelType(QOpenGLTexture::PixelType::Int16);
        data->setFormat(QOpenGLTexture::TextureFormat::RGB16_SNorm);
        //data->setPixelFormat(QOpenGLTexture::PixelFormat::Red_Integer);
        //data->setPixelType(QOpenGLTexture::PixelType::Int16);
        //data->setFormat(QOpenGLTexture::TextureFormat::R16I);

        //auto tex = createTextureBytes(maxX, maxY, maxZ, 3, 2);
        //print_byte_array_values(tex,2);
        data->setData(m_textureData, 3,false);
    }

    return data;
}

TextureImageGradientGenerator3D::TextureImageGradientGenerator3D() : QTextureImageDataGenerator() {
}

bool TextureImageGradientGenerator3D::operator==(const Qt3DRender::QTextureImageDataGenerator& other) const {
    const TextureImageGradientGenerator3D* otherFunctor = functor_cast<TextureImageGradientGenerator3D>(&other);
    // if its the same URL, but different modification times, its not the same image.
    return false;//(otherFunctor != nullptr);
}

void TextureImageGradientGenerator3D::set_texture_information(const QByteArray& data, QVector3D dimension, int numberOfChannels)
{
    m_textureData = data;
    m_dimension = dimension;
    m_channels = numberOfChannels;
}

void TextureImageGradientGenerator3D::print_byte_array_values(const QByteArray& cs, int numberOfBytes)
{
    for(int i=0;i<cs.size();i=i+numberOfBytes)
    {
        unsigned char a = (cs[i] & 255);          // Ou: (num & 0xFF)
        unsigned char b = ((cs[i+1] >> 8) & 255);          // Ou: (num & 0xFF)

        short uvalBack;

        char aa[sizeof(short)];
        aa[0] = cs[i];
        aa[1] = cs[i + 1];
        memcpy(&uvalBack, aa, sizeof(short)); // and back to s again

        int16_t number = a | b << 8;
    }
}

QByteArray TextureImageGradientGenerator3D::createTexture(int maxX, int maxY, int maxZ, int numberOfChannels) const
{

    QByteArray ba2;
    //ba2.resize(maxX * maxY * maxZ * numberOfChannels);

    int bacounter = 0;
    short val = 0;
    for (int i = 0; i < maxX; i++) {
        for (int j = 0; j < maxY; j++) {
            for (int k = 0; k < maxZ; k++) {
                if (val >= 255) {
                    val = 0;
                }
                for (int c = 0; c < numberOfChannels; c++) {
                    if (c == 0) {
                        ba2.append(reinterpret_cast<const char*>(&val), sizeof(val));
                    }
                    else {
                        short v = 0;
                        ba2.append(reinterpret_cast<const char*>(&v), sizeof(v));
                    }
                    bacounter++;
                }
                val++;
            }
        }
    }

    return ba2;
}

QByteArray TextureImageGradientGenerator3D::createTextureBytes(int maxX, int maxY, int maxZ, int numberOfChannels, int numberOfBytes) const
{

    QByteArray ba2;
    //ba2.resize(maxX * maxY * maxZ * numberOfChannels);

    short val = 0;
    for (int i = 0; i < maxX; i++) {
        for (int j = 0; j < maxY; j++) {
            for (int k = 0; k < maxZ; k++) {
                if (val >= 255) {
                    val = 0;
                }
                short sval = val - 100;
                char aa[sizeof(short)];
                memcpy(&aa, &sval, sizeof(short)); // copy a to s

                for (int c = 0; c < numberOfChannels; c++) {
                    if (c == 0) {
                        for(int b=0; b< numberOfBytes;b++)
                        {
                            ba2.append(aa[b]);
                        }
                    }
                    else {
                        for (int b = 0; b < numberOfBytes; b++)
                        {
                            ba2.append(aa[b]);
                        }
                    }
                }
                val++;
            }
        }
    }

    return ba2;
}
}
