#include "libViewer/MetalRoughMaterialProperties.h"

namespace viewer
{
MetalRoughMaterialProperties::MetalRoughMaterialProperties(const QColor& base_color, const float& metalness,
    const float& roughness, const float& texture_scale):
	m_ambient_occlusion(0.0f), m_base_color(QVariant::fromValue(base_color)), m_metalness(QVariant::fromValue(metalness)), m_normal(0.0f),
	m_roughness(QVariant::fromValue(roughness)), m_texture_scale(texture_scale)
{
}

MetalRoughMaterialProperties::MetalRoughMaterialProperties(const QVariant& base_color, const QVariant& metalness, const QVariant& roughness,
                                                           const QVariant& normal, const QVariant& ambient_occlusion, 
                                                           const float& texture_scale):
	m_ambient_occlusion(ambient_occlusion), m_base_color(base_color), m_metalness(metalness), m_normal(normal),
	m_roughness(roughness),m_texture_scale(texture_scale)
{
}

void MetalRoughMaterialProperties::set_ambient_occlusion(const QVariant& ambient_occlusion)
{
	m_ambient_occlusion = ambient_occlusion;
}

void MetalRoughMaterialProperties::set_base_color(const QVariant& base_color)
{
	m_base_color = base_color;
}

void MetalRoughMaterialProperties::set_metalness(const QVariant& metalness)
{
	m_metalness = metalness;
}

void MetalRoughMaterialProperties::set_normal(const QVariant& normal)
{
	m_normal = normal;
}

void MetalRoughMaterialProperties::set_roughness(const QVariant& roughness)
{
	m_roughness = roughness;
}

void MetalRoughMaterialProperties::set_texture_scale(const float& texture_scale)
{
	m_texture_scale = texture_scale;
}

QVariant MetalRoughMaterialProperties::get_base_color() const
{
	return m_base_color;
}

QVariant MetalRoughMaterialProperties::get_metalness() const
{
	return m_metalness;
}

QVariant MetalRoughMaterialProperties::get_normal() const
{
	return m_normal;
}

QVariant MetalRoughMaterialProperties::get_roughness() const
{
	return m_roughness;
}

float MetalRoughMaterialProperties::get_texture_scale() const
{
	return m_texture_scale;
}

QVariant MetalRoughMaterialProperties::get_ambient_occlusion() const
{
	return m_ambient_occlusion;
}
}
