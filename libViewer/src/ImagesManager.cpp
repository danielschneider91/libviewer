#include "libViewer/ImagesManager.h"

#include <iostream>

namespace viewer
{
ImagesManager::ImagesManager(QObject* parent):QObject(parent), m_intensity_range_highlight_settings(new ImageIntensityRangeHighlightSettings(this))
{
}

void ImagesManager::add_image(const QString& id, const viewer::VolumeImageData& image,
    const ViewportVisibility& viewport_visibility)
{
    if (!contains_image_with_id(id))
    {
        auto image_volume_data = new VolumeImageDataQml(this);
        image_volume_data->set_texture_data(image.getTextureData());
        image_volume_data->set_dimension((image.get_dimension()));
        image_volume_data->set_spacing((image.get_spacing()));
        image_volume_data->set_origin((image.get_origin()));
        auto o = image.get_orientation();
        QMatrix4x4 orientation;
        orientation.setToIdentity();
        std::cout << "--------------- image orientation: " << std::endl;
        std::cout << o(0, 0) << ", " << o(0, 1) << ", " << o(0, 2) << std::endl;
        std::cout << o(1, 0) << ", " << o(1, 1) << ", " << o(1, 2) << std::endl;
        std::cout << o(2, 0) << ", " << o(2, 1) << ", " << o(2, 2) << std::endl;

        orientation.setColumn(0, QVector4D(o(0, 0), o(1, 0), o(2, 0), 0));
        orientation.setColumn(1, QVector4D(o(0, 1), o(1, 1), o(2, 1), 0));
        orientation.setColumn(2, QVector4D(o(0, 2), o(1, 2), o(2, 2), 0));
        orientation.setColumn(3, QVector4D(0, 0, 0, 1));
        image_volume_data->set_orientation(orientation);
        //m_image_volume_data_map.insert(std::make_pair(id, image_volume_data));

        m_id_index_map[id] = m_image_volume_data_list.size();
        auto viewport_vis = new ViewportVisibility(viewport_visibility.get_three_d_viewports_visibility(), viewport_visibility.get_slice_viewports_visibility(), viewport_visibility.get_slice_viewport_rendering_mode(), viewport_visibility.get_clipping_distance(),this);
        m_image_viewport_visibility.append(viewport_vis);
        m_image_volume_data_list.append(image_volume_data);
        emit volume_images_changed();
        emit images_viewport_visibility_changed();
    }
    else
    {
        throw std::runtime_error("Viewer already contains image with id " + id.toStdString());
    }
}

void ImagesManager::remove_image(const QString& id)
{
    if (contains_image_with_id(id))
    {
        auto index = m_id_index_map[id];
        delete m_image_volume_data_list.takeAt(index);
        delete m_image_viewport_visibility.takeAt(index);
        m_id_index_map.erase(id);

        // update id_index_map
        for (auto& [key, val] : m_id_index_map)
        {
            if (key == id)
                assert(true && "This cannot happen, object with key == id should have been erased.");
            if (val > index)
                val = val - 1;
        }
        emit volume_images_changed();
        emit images_viewport_visibility_changed();
    }
    else
    {
        throw "Cannot remove image with id which is not in viewer. " + id;
    }

    //TODO
    emit volume_images_changed();
}

void ImagesManager::set_slice_picking_enabled(bool picking_enabled)
{
    if(get_is_slice_picking_enabled()!=picking_enabled)
    {
        m_slice_picking_enabled = picking_enabled;
        emit slice_picking_enabled_changed();
    }
}

void ImagesManager::set_viewport_visibility(const QString& id, const viewer::ViewportVisibility& viewport_visibility)
{
    if(contains_image_with_id(id))
    {
        int index = m_id_index_map.at(id);
        try
        {
            m_image_viewport_visibility[index]->set_three_d_viewports_visibility(viewport_visibility.get_three_d_viewports_visibility());
            m_image_viewport_visibility[index]->set_slice_viewports_visibility(viewport_visibility.get_slice_viewports_visibility());
            m_image_viewport_visibility[index]->set_slice_viewport_rendering_mode(viewport_visibility.get_slice_viewport_rendering_mode());
            m_image_viewport_visibility[index]->set_clipping_distance(viewport_visibility.get_clipping_distance());
            m_image_viewport_visibility[index]->set_depth_testing(viewport_visibility.get_depth_testing());
            emit images_viewport_visibility_changed();
        }
        catch (std::exception& ex)
        {
            std::cerr << "set_viewport_visibility, m_image_viewport_visibility: this cannot happen. Exception: " << ex.what() << std::endl;
            assert(true);
        }
    }
}

std::vector<QString> ImagesManager::get_image_ids_in_viewer() const
{
    std::vector<QString> ids;
    for(const auto& [id,index]: m_id_index_map)
    {
        ids.push_back(id);
    }
    return ids;
}

bool ImagesManager::contains_image_with_id(const QString& id) const
{
    if (m_id_index_map.find(id) == m_id_index_map.end()) {
        return false;
    }
    return true;
}

QList<VolumeImageDataQml*> ImagesManager::get_volume_images() const
{
    return m_image_volume_data_list;
}

ImageIntensityRangeHighlightSettings* ImagesManager::get_image_intensity_range_highlight_settings() const
{
    return m_intensity_range_highlight_settings;
}

bool ImagesManager::get_is_slice_picking_enabled() const
{
    return m_slice_picking_enabled;
}
}
