#include "libViewer/AnimationParametersContainer.h"


#include <iostream>
#include <qpropertyanimation.h>
#include <sstream>
#include <Qt3DCore/QTransform>

namespace viewer {
AnimationParametersContainer::AnimationParametersContainer(QObject* parent):QObject(parent)
{
}

void AnimationParametersContainer::add_animation_parameters(const std::string& id, const AnimationProperty& animation_property, const AnimationParameters& animation_parameters)
{
    std::cout << "m_animation_params.size(): " << m_animation_params.size() << std::endl;
    std::cout << "m_animated_object_ids.size(): " << m_animated_object_ids.size() << std::endl;
    std::cout << "m_animated_object_id_to_animation_property_and_index_map.size(): " << m_animated_object_id_to_animation_property_and_index_map.size() << std::endl;

    int counter = 0;
    for (const auto& [key, val] : m_animated_object_id_to_animation_property_and_index_map)
    {
        for (const auto& [key2, val2] : val)
        {
            counter++;
        }
    }
    std::cout << "total m_animated_object_id_to_animation_property_and_index_map count: " << counter << std::endl;
    switch (animation_property)
    {
    case AnimationProperty::SCALE: {

        if (m_animated_object_id_to_animation_property_and_index_map.find(id) != m_animated_object_id_to_animation_property_and_index_map.end() // an animation for object with id already exists
            )
        {
            auto animations = m_animated_object_id_to_animation_property_and_index_map.at(id);
            if (animations.find(animation_property) != animations.end())// animation of animation_property for object with id already exists
            {
                // update existing animation property
                std::stringstream ss;
                ss << "Object with id " << id << " already contains animation of property " << static_cast<int>(animation_property) << ". Replacing the existing.";
                std::cerr << ss.str() << std::endl;
                auto idx = animations.at(animation_property);
                m_animation_params[idx]->set_running(animation_parameters.is_running());
                m_animation_params[idx]->set_property(animation_parameters.get_property());
                m_animation_params[idx]->set_duration(animation_parameters.get_duration());
                m_animation_params[idx]->set_from(animation_parameters.get_from());
                m_animation_params[idx]->set_to(animation_parameters.get_to());
                //assert(m_animation_params[idx].toStdString() == id && "Id in variable is inconsistent with id provided as function argument.");
                //assert(m_animation_params.size() == m_animated_object_id_to_animation_property_and_index_map.size() && "The number of parameters and indexes must match.");
            } else // animation of animation_property for object with id does not yet exists
            {
                auto ret = animations.insert(std::make_pair(animation_property, m_animation_params.size()));
                //assert(ret.second == true && "Since this animation_property does not yet exists, it must always be possible to insert.");
                m_animation_params.push_back(new AnimationParameters(animation_parameters.get_duration(), animation_parameters.get_from(), animation_parameters.get_to(), animation_parameters.is_running(), animation_parameters.get_property()));
                m_animated_object_ids.push_back(QString::fromStdString(id));
                //assert(m_animation_params.size() == m_animated_object_id_to_animation_property_and_index_map.size() && "The number of parameters and indexes must match.");
            } 

            //auto ret = animations.insert(std::make_pair(animation_property, m_animation_params.size()));
            //if (ret.second == false) {
            //    std::stringstream ss;
            //    ss << "Object with id " << id << " already contains animation of property " << static_cast<int>(animation_property) << ". Not inserting anything.";
            //    std::cerr << ss.str() << std::endl;
            //}
            //m_animated_object_ids.push_back(index);
            //m_animation_params.push_back(new AnimationParameters(animation_parameters.get_duration(), animation_parameters.get_from(), animation_parameters.get_to(),animation_parameters.is_running(),animation_parameters.get_property()));
            //assert(m_animation_params.size() == m_animated_object_id_to_animation_property_and_index_map.size() && "The number of parameters and indexes must match.");
        }
        else
        {
            std::map<AnimationProperty, int> map;
            map.insert(std::make_pair(animation_property, m_animation_params.size()));
            auto res = m_animated_object_id_to_animation_property_and_index_map.insert(std::make_pair(id, map));
            //assert(ret.second == true && "Since no animation exists for object with that id, it must always be possible to insert.");

            m_animation_params.push_back(new AnimationParameters(animation_parameters.get_duration(), animation_parameters.get_from(), animation_parameters.get_to(), animation_parameters.is_running(), animation_parameters.get_property()));
            m_animated_object_ids.push_back(QString::fromStdString(id));
            //assert(m_animation_params.size() == m_animated_object_id_to_animation_property_and_index_map.size() && "The number of parameters and indexes must match.");
        }
        emit animation_params_changed();
        emit animated_object_ids_changed();
        break;
    }
    case AnimationProperty::COLOR: {
        throw "AnimationProperty::COLOR is not implemented yet.";
        break;
    }
    }

    assert(are_sizes_consistent() && "Size consistency check unsuccessful after void AnimationManager::add_animation(const std::string& id, const AnimationProperty& animation_property,const AnimationParameters & animation_parameters, const int& index)");
}

void AnimationParametersContainer::remove_animation_parameters(const std::string& id, const AnimationProperty& animation_property)
{

    if (m_animated_object_id_to_animation_property_and_index_map.find(id) != m_animated_object_id_to_animation_property_and_index_map.end())
    {
        auto map = m_animated_object_id_to_animation_property_and_index_map.at(id);
        if (map.find(animation_property) != map.end()) // found
        {
            int idx = map.at(animation_property);
            auto params = m_animation_params.at(idx);
            params->set_running(false);
            emit animation_params_changed();

            delete m_animation_params.takeAt(idx);
            m_animated_object_ids.takeAt(idx);
            map.erase(animation_property);
            if (map.size() <= 0)
            {
                m_animated_object_id_to_animation_property_and_index_map.erase(id);
            }
            
            // update id_index_map
            for (auto& [key, val] : m_animated_object_id_to_animation_property_and_index_map)
            {
                for (auto& [key2, val2] : val)
                {
                    if (key == id && key2 == animation_property)
                        assert(true && "This cannot happen, object with key == id and animation property == animation_property should have been erased.");
            
                    if (val2 > idx)
                    {
                        val2 = val2 - 1;
                    }
                }
            }
        }
    }

    emit animation_params_changed();
    emit animated_object_ids_changed();

    assert(are_sizes_consistent() && "Size consistency check unsuccessful after void AnimationManager::remove_animation(const std::string& id, const AnimationProperty& animation_property)");
}

void AnimationParametersContainer::remove_animations_parameters(const std::string& id)
{
    std::vector<AnimationProperty> animation_properties;
    if (m_animated_object_id_to_animation_property_and_index_map.find(id) != m_animated_object_id_to_animation_property_and_index_map.end())
    {
        std::map<AnimationProperty, int> map = m_animated_object_id_to_animation_property_and_index_map.at(id);
        for (auto const& v : map)
        {
            animation_properties.push_back(v.first);
        }
    }
    for (const auto& a : animation_properties)
    {
        remove_animation_parameters(id, a); 
    }

    assert(are_sizes_consistent() && "Size consistency check unsuccessful after void AnimationManager::remove_animations(const std::string& id)");

}

bool AnimationParametersContainer::are_sizes_consistent()
{
    bool result = true;
    if (m_animation_params.size() != m_animated_object_ids.size())
    {
        std::cerr << "m_animation_params.size()!=m_animated_object_ids.size()" << std::endl;
        result = false;
    }
    int counter = 0;
    for (const auto& [key, val] : m_animated_object_id_to_animation_property_and_index_map)
    {
        for (const auto& [key2, val2] : val)
        {
            counter++;
        }
    }
    if (counter != m_animated_object_ids.size())
    {
        std::cerr << "counter!=m_animated_object_ids.size()" << std::endl;
        result = false;
    }

    return result;
}

}
