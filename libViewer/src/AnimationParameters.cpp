#include "libViewer/AnimationParameters.h"


namespace viewer
{
AnimationParameters::AnimationParameters(const int duration, const QVariant& from, const QVariant& to, const bool running, const QString& prop)
    : m_running(running),
    m_property(prop),
    m_duration(duration),
    m_from(from),
    m_to(to)
{
}

AnimationParameters::AnimationParameters() : m_running(true),m_property(""), m_duration(250),
m_from(QVariant::fromValue(1.0)),
m_to(QVariant::fromValue(0.0))
{}

AnimationParameters::AnimationParameters(const AnimationParameters& other)
{
    m_running = other.m_running;
    m_property = other.m_property;
    m_duration = other.m_duration;
    m_from = other.m_from;
    m_to = other.m_to;
}

int AnimationParameters::get_duration() const
{
    return m_duration;
}

QVariant AnimationParameters::get_from() const
{
    return m_from;
}

QVariant AnimationParameters::get_to() const
{
    return m_to;
}

void AnimationParameters::set_duration(int duration)
{
    if(m_duration!=duration)
    {
        m_duration = duration;
        emit duration_changed();
    }
}

void AnimationParameters::set_from(const QVariant& from)
{
    if (m_from != from)
    {
        m_from = from;
        emit from_changed();
    }
}

void AnimationParameters::set_to(const QVariant& to)
{
    if (m_to != to)
    {
        m_to = to;
        emit to_changed();
    }
}

bool AnimationParameters::is_running() const
{
    return m_running;
}

void AnimationParameters::set_running(const bool running)
{
    if(m_running!=running)
    {
        m_running = running;
        emit running_changed();
    }
}

QString AnimationParameters::get_property() const
{
    return m_property;
}

void AnimationParameters::set_property(const QString& prop)
{
    if(m_property!=prop)
    {
        m_property = prop;
        emit prop_changed();
    }
}
}
