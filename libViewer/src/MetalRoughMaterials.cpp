#include "libViewer/MetalRoughMaterials.h"

#include <QTexture>
#include <QTextureImage>

namespace viewer
{
    MetalRoughMaterialProperties MetalRoughMaterials::get_material(viewer::MetalRoughMaterialType metal_rough_material_type, float roughness)
    {
        switch (metal_rough_material_type)
        {
        case MetalRoughMaterialType::ALUMINIUM:
        {
            MetalRoughMaterialProperties material(QVariant::fromValue(QColor(0.913 * 255.0, 0.921 * 255.0, 0.925 * 255.0)), QVariant::fromValue(0.9), QVariant::fromValue(roughness));
            return material;
        }
        case MetalRoughMaterialType::ALUMINIUM_TEXTURE:
        {
            MetalRoughMaterialProperties material(get_texture("qrc:/libViewer/ui/resources/materials/aluminium/Metal009_1K_Color.jpg"),
                                                  QVariant::fromValue(0.9), get_texture("qrc:/libViewer/ui/resources/materials/aluminium/Metal009_1K_Roughness.jpg"),
                                                  get_texture("qrc:/libViewer/ui/resources/materials/aluminium/Metal009_1K_Normal.jpg"));
            return material;
        }
        case MetalRoughMaterialType::ARTERY:
        {
            MetalRoughMaterialProperties material(QVariant::fromValue(QColor(128, 3, 3)), QVariant::fromValue(0), QVariant::fromValue(roughness));
            return material;
        }
        case MetalRoughMaterialType::BONE:
        {
            MetalRoughMaterialProperties material(QVariant::fromValue(QColor(0.89 * 255.0, 0.855 * 255.0, 0.788 * 255.0)), QVariant::fromValue(0.0), QVariant::fromValue(roughness));
            return material;
        }
        case MetalRoughMaterialType::BONE_TEXTURE:
        {
            MetalRoughMaterialProperties material(get_texture("qrc:/libViewer/ui/resources/materials/bone/512-diffuse.jpg"),
                                                  QVariant::fromValue(0.0), QVariant::fromValue(0.5), // get_texture("qrc:/libViewer/ui/resources/materials/bone/512-ro.jpg"),
                                                  get_texture("qrc:/libViewer/ui/resources/materials/bone/512-normal.jpg"),
                                                  get_texture("qrc:/libViewer/ui/resources/materials/bone/512-ao.jpg"));
            return material;
        }
        case MetalRoughMaterialType::GOLD:
        {
            MetalRoughMaterialProperties material(QVariant::fromValue(QColor(1 * 255.0, 0.766 * 255.0, 0.336 * 255.0)), QVariant::fromValue(0.7), QVariant::fromValue(roughness));
            return material;
        }
        case MetalRoughMaterialType::GOLD_TEXTURE:
        {
            MetalRoughMaterialProperties material(get_texture("qrc:/libViewer/ui/resources/materials/gold/Metal034_1K_Color.jpg"),
                                                  QVariant::fromValue(1), QVariant::fromValue(1),
                                                  get_texture("qrc:/libViewer/ui/resources/materials/gold/Metal034_1K_Normal.jpg"));
            return material;
        }
        case MetalRoughMaterialType::NERVE:
        {
            MetalRoughMaterialProperties material(QVariant::fromValue(QColor(1.0 * 255.0, 1.0 * 255.0, 0.6 * 255.0)), QVariant::fromValue(0.0), QVariant::fromValue(roughness));
            return material;
        }
        case MetalRoughMaterialType::PLASTIC:
        {
            MetalRoughMaterialProperties material(QVariant::fromValue(QColor(0.0 * 255.0, 0.0 * 255.0, 1 * 255.0)), QVariant::fromValue(0.0), QVariant::fromValue(roughness));
            return material;
        }
        case MetalRoughMaterialType::SKIN:
        {
            MetalRoughMaterialProperties material(QVariant::fromValue(QColor(236.0, 188, 180)), QVariant::fromValue(0.0), QVariant::fromValue(roughness));
            return material;
        }
        case MetalRoughMaterialType::STEEL:
        {
            MetalRoughMaterialProperties material(QVariant::fromValue(QColor(0.560 * 255.0, 0.570 * 255.0, 0.580 * 255.0)), QVariant::fromValue(0.4), QVariant::fromValue(roughness));
            return material;
        }
        case MetalRoughMaterialType::STEEL_TEXTURE:
        {
            MetalRoughMaterialProperties material(get_texture("qrc:/libViewer/ui/resources/materials/steel/Metal003_1K_Color.jpg"),
                                                  get_texture("qrc:/libViewer/ui/resources/materials/steel/Metal003_1K_Metalness.jpg"),
                                                  get_texture("qrc:/libViewer/ui/resources/materials/steel/Metal003_1K_Roughness.jpg"),
                                                  get_texture("qrc:/libViewer/ui/resources/materials/steel/Metal003_1K_Normal.jpg"));
            return material;
        }
        case MetalRoughMaterialType::TITANIUM:
        {
            MetalRoughMaterialProperties material(QVariant::fromValue(QColor(0.542 * 255.0, 0.497 * 255.0, 0.449 * 255.0)), QVariant::fromValue(0.6), QVariant::fromValue(roughness));
            return material;
        }
        case MetalRoughMaterialType::TUMOR:
        {
            MetalRoughMaterialProperties material(QVariant::fromValue(QColor(153, 138, 106)), QVariant::fromValue(0.0), QVariant::fromValue(roughness));
            return material;
        }
        case MetalRoughMaterialType::TUNGSTEN:
        {
            MetalRoughMaterialProperties material(QVariant::fromValue(QColor(0.8913 * 255.0, 0.901 * 255.0, 0.905 * 255.0)), QVariant::fromValue(0.5), QVariant::fromValue(roughness));
            return material;
        }
        case MetalRoughMaterialType::VEIN:
        {
            MetalRoughMaterialProperties material(QVariant::fromValue(QColor(128, 3, 30)), QVariant::fromValue(0.0), QVariant::fromValue(roughness));
            return material;
        }
        default:
            MetalRoughMaterialProperties material(QVariant::fromValue(QColor(255, 255, 255)), QVariant::fromValue(0.0), QVariant::fromValue(roughness));
            return material;
        }
    }

    MetalRoughMaterialProperties MetalRoughMaterials::get_material(std::string metal_rough_material_type, float roughness)
    {
        MetalRoughMaterialType material = get_material_as_enum(metal_rough_material_type);
        return get_material(material);
    }

    QVariant MetalRoughMaterials::get_texture(const std::string &file)
    {
        Qt3DRender::QTextureImage *texture_image = new Qt3DRender::QTextureImage;
        texture_image->setSource(QString::fromStdString(file));
        Qt3DRender::QTexture2D *texture = new Qt3DRender::QTexture2D;
        texture->addTextureImage(texture_image);
        return QVariant::fromValue(texture);
    }
}
