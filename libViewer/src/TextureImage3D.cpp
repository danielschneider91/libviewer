#include "libViewer/TextureImage3D.h"

#include <iostream>
#include <QTextureImageDataGenerator>

#include "libViewer/TextureImageDataGenerator3D.h"

namespace viewer {

void TextureImage3D::setTextureInformation(const QByteArray& data, QVector3D dimension, QVector3D spacing, QVector3D origin, QMatrix4x4 orientation,
    int numberOfChannels)
{
    m_textureData = data;
    m_dimension = dimension;
    m_spacing = spacing;
    m_dimensionTimesSpacing = m_dimension * m_spacing;
    m_origin = origin;
    m_orientation = orientation;
    m_channels = numberOfChannels;
}

void TextureImage3D::setTextureData(const QByteArray& data)
{
    //if(data!=m_texture_data) {
    if (true) {
        m_textureData = data;
        m_defaultTextureData = false;
        m_seed++;
        if (m_seed >= 3) {
            m_seed = 0;
        }
        emit textureDataChanged();
        notifyDataGeneratorChanged();
    }
}

QByteArray TextureImage3D::getTextureData() const
{
    return m_textureData;
}

QVector3D TextureImage3D::getDimensionTimesSpacing() const
{
    return m_dimension * m_spacing;
}

Qt3DRender::QTextureImageDataGeneratorPtr TextureImage3D::dataGenerator() const {
    auto imgGen = new TextureImageDataGenerator3D();

    if (m_defaultTextureData) {
        auto integer = static_cast<int>(m_seed);
        auto generated_texture = createTexture(integer * m_dimension[0], integer * m_dimension[1], integer * m_dimension[2], m_channels);
        imgGen->set_texture_information(generated_texture, m_dimension, m_spacing, m_origin, m_orientation, m_channels);
    }
    else {
        auto integer = 1;
        imgGen->set_texture_information(m_textureData, m_dimension, m_spacing, m_origin, m_orientation, m_channels);
    }

    imgGen->setInteger(static_cast<int>(m_seed));

    return Qt3DRender::QTextureImageDataGeneratorPtr(imgGen);
}

QByteArray TextureImage3D::createTexture(int maxX, int maxY, int maxZ, int numberOfChannels) const
{
    QByteArray ba;
    ba.resize(maxX * maxY * maxZ * numberOfChannels);

    int bacounter = 0;
    int val = 0;
    for (int i = 0; i < maxX; i++) {
        for (int j = 0; j < maxY; j++) {
            for (int k = 0; k < maxZ; k++) {
                if (val >= 255) {
                    val = 0;
                }
                for (int c = 0; c < numberOfChannels; c++) {
                    if (c == 1) {
                        ba[bacounter] = (char)val;//'z';
                    }
                    else {
                        ba[bacounter] = '0';
                    }
                    bacounter++;
                }
                val++;

            }
        }
    }
    return ba;
}
}