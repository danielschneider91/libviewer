#include "libViewer/MeasurementsViewModel.h"

#include <iostream>

#include "libViewer/QMLCrosshairsObjectParameters.h"
#include "libViewer/QMLLineObjectParameters.h"
#include "libViewer/QMLPointsObjectParameters.h"
#include "libViewer/IntegerIdGenerator.h"

namespace viewer
{
MeasurementsViewModel::MeasurementsViewModel(std::shared_ptr<ViewerViewModel> viewer_vm, QObject* parent): QObject(parent),
    m_viewer_vm(viewer_vm)
{
    m_id_generator = std::make_unique<IntegerIdGenerator>();
    auto existing_ids = m_viewer_vm->get_object_ids_in_viewer();
    auto id_point_1 = m_id_generator->create_unique_id(existing_ids, "measurement_point_");
    existing_ids.push_back(id_point_1);
    auto id_point_2 = m_id_generator->create_unique_id(existing_ids, "measurement_point_");
    existing_ids.push_back(id_point_2);
    auto id_line = m_id_generator->create_unique_id(existing_ids, "measurement_line_");
    m_measurement = new Measurement(std::tuple<QString, QString, QString>{QString::fromStdString(id_point_1) , QString::fromStdString(id_point_2), QString::fromStdString(id_line)}, this);
}

void MeasurementsViewModel::resetMeasurement()
{
    reset_measurement();
}

bool MeasurementsViewModel::get_ruler_enabled() const
{
    return m_ruler_enabled;
}

void MeasurementsViewModel::set_ruler_enabled(const bool ruler_enabled)
{
    reset_measurement();

    if (ruler_enabled)
    {
        wireup_events();
        add_empty_measurement();
        m_viewer_vm->set_all_picking_enabled(true);
    }
    else
    {
        remove_measurement();
        m_viewer_vm->set_all_picking_enabled(false);
        unwire_events();
    }

    if (m_ruler_enabled != ruler_enabled)
    {
        m_ruler_enabled = ruler_enabled;
        emit ruler_enabled_changed();
    }
}

int MeasurementsViewModel::get_position_counter() const
{
    return m_position_counter;
}

void MeasurementsViewModel::set_position_counter(const int position_counter)
{
    m_position_counter = position_counter;
}

float MeasurementsViewModel::get_measurement_visual_size() const
{
    return m_measurement_visual_size;
}

void MeasurementsViewModel::wireup_events()
{
    m_connections.push_back(connect(m_viewer_vm.get(), &ViewerViewModel::spatial_object_or_image_hovered,
        [this](QString id, QVector3D world_position, QVector3D local_position)
        {
            std::cout << "Spatial object with id " << id.toStdString() << " was hovered." << std::endl;
            print_position(world_position, "World: ");
            print_position(local_position, "Local: ");

            if (get_position_counter() == 0)
            {
                m_measurement->set_from(world_position);
                try
                {
                    m_viewer_vm->set_object_pose(m_measurement->get_from_id().toStdString(), m_measurement->get_from(), QVector3D{ 1,0,0 }, QVector3D{ 0,0,1 });
                } catch(std::exception& ex)
                {
                    std::cerr << "Could not set object pose of measurement from point (id: " << m_measurement->get_from_id().toStdString() << "). Exception: " << ex.what() << std::endl;
                } 
                update_line(m_measurement->get_from(), m_measurement->get_from());
            }
            if (get_position_counter() == 1)
            {
                m_measurement->set_to(world_position);
                try
                {
                    m_viewer_vm->set_object_pose(m_measurement->get_to_id().toStdString(), m_measurement->get_to(), QVector3D{ 1,0,0 }, QVector3D{ 0,0,1 });
                }
                catch (std::exception& ex)
                {
                    std::cerr << "Could not set object pose of measurement to point (id: " << m_measurement->get_to_id().toStdString() << "). Exception: " << ex.what() << std::endl;
                }
                update_line(m_measurement->get_from(), m_measurement->get_to());
            }

        }));

    m_connections.push_back(connect(m_viewer_vm.get(), &ViewerViewModel::spatial_object_or_image_picked,
        [this](QString id, QVector3D world_position, QVector3D local_position)
        {
            std::cout << "Spatial object with id " << id.toStdString() << " was picked." << std::endl;
            print_position(world_position, "World: ");
            print_position(local_position, "Local: ");

            set_position_counter(get_position_counter() + 1);

            if (get_position_counter() == 1)
            {
                // update line
            }
        }));
}

void MeasurementsViewModel::unwire_events()
{
    for (auto& c : m_connections)
    {
        disconnect(c);
    }
    m_connections.clear();
}

void MeasurementsViewModel::update_line(const QVector3D& from, const QVector3D& to)
{
    try
    {
        auto p = m_viewer_vm->get_spatial_object_parameters(m_measurement->get_line_id().toStdString());
        auto params = dynamic_cast<QMLLineObjectParameters*>(p);
        if (params == nullptr)
        {
            std::cerr << "void MeasurementsViewModel::update_line(const QVector3D& from, const QVector3D& to): This cannot happen" << std::endl;
            assert(false);
        }
        params->set_from(from);
        params->set_to(to);
    } catch(std::exception& ex)
    {
        std::cerr << "Could not update measurement line (id: "<< m_measurement->get_line_id().toStdString() << "). Exception: " << ex.what() << std::endl;
    }
}

void MeasurementsViewModel::remove_measurement()
{
    try
    {
        m_viewer_vm->remove_object(m_measurement->get_from_id().toStdString());
    }
    catch (std::exception& ex)
    {
        std::cerr << "Could not remove from viewer object with id " << m_measurement->get_from_id().toStdString() << ". Exception: "
            << ex.what() << std::endl;
    }
    try
    {
        m_viewer_vm->remove_object(m_measurement->get_to_id().toStdString());
    }
    catch (std::exception& ex)
    {
        std::cerr << "Could not remove from viewer object with id " << m_measurement->get_to_id().toStdString() << ". Exception: "
            << ex.what() << std::endl;
    }
    try
    {
        m_viewer_vm->remove_object(m_measurement->get_line_id().toStdString());
    }
    catch (std::exception& ex)
    {
        std::cerr << "Could not remove from viewer object with id " << m_measurement->get_line_id().toStdString() << ". Exception: "
            << ex.what() << std::endl;
    }
    reset_measurement();
}

void MeasurementsViewModel::reset_measurement()
{
    set_position_counter(0);
    m_measurement->reset_measurement();
    m_viewer_vm->set_object_pose(m_measurement->get_from_id().toStdString(), m_measurement->get_from(), QVector3D{ 1,0,0 }, QVector3D{ 0,0,1 });
    m_viewer_vm->set_object_pose(m_measurement->get_to_id().toStdString(), m_measurement->get_to(), QVector3D{ 1,0,0 }, QVector3D{ 0,0,1 });
}

void MeasurementsViewModel::add_empty_measurement()
{
    QColor color{ 255,255,255 };
    QColor transparent_color{ 122,122,122,122 };
    SliceViewportRenderingModeClass::SliceViewportRenderingMode slice_viewport_rendering_mode =
        SliceViewportRenderingModeClass::SliceViewportRenderingMode::MODEL;
    auto params = new QMLCrosshairsObjectParameters{ get_measurement_visual_size() };
    Pose p_1{ QVector3D{0,0,0}, QVector3D{1, 0, 0}, QVector3D{0, 0, 1} };
    m_measurement->set_from(p_1.get_position());
    MaterialSettings material_settings_1{
        false,
        MetalRoughMaterialProperties{
            QVariant::fromValue(color), QVariant::fromValue(0.3),
            QVariant::fromValue(0.5)
        },
        transparent_color,
    };

    try
    {
        m_viewer_vm->add_object(m_measurement->get_from_id().toStdString(), QMLSpatialObjectType::CROSSHAIRS, params,
            p_1, m_measurement->get_from_id().toStdString(), material_settings_1,
            ViewportVisibility{
                true, true, slice_viewport_rendering_mode,10,true
            });
    }
    catch (std::exception& ex)
    {
        std::cerr << "Could not add measurement first point. Exception: " << ex.what() << std::endl;
    }

    Pose p_2{ QVector3D{0, 0, 0}, QVector3D{1, 0, 0}, QVector3D{0, 0, 1} };
    m_measurement->set_to(p_1.get_position());
    MaterialSettings material_settings_2{
        false,
        MetalRoughMaterialProperties{
            QVariant::fromValue(color), QVariant::fromValue(0.3),
            QVariant::fromValue(0.5)
        },
        transparent_color,
    };

    try
    {
        m_viewer_vm->add_object(m_measurement->get_to_id().toStdString(), QMLSpatialObjectType::CROSSHAIRS, params,
            p_2, m_measurement->get_to_id().toStdString(), material_settings_2,
            ViewportVisibility{
                true, true,slice_viewport_rendering_mode,10,true
            });
    }
    catch (std::exception& ex)
    {
        std::cerr << "Could not add measurement second point. Exception: " << ex.what() << std::endl;
    }

    try {

        auto params_line = new QMLLineObjectParameters{ m_measurement->get_from(), m_measurement->get_to() };
        m_viewer_vm->add_object(m_measurement->get_line_id().toStdString(), QMLSpatialObjectType::LINE, params_line, Pose{},
            m_measurement->get_line_id().toStdString(), material_settings_2,
            ViewportVisibility{
                true, true,slice_viewport_rendering_mode,10,true
            });
    }
    catch (std::exception& ex)
    {
        std::cerr << "Could not add measurement line. Exception: " << ex.what() << std::endl;
    }
}

void MeasurementsViewModel::print_position(const QVector3D& position, const QString& msg) const
{
    std::cout << msg.toStdString() << position.x() << "," << position.y() << "," << position.
        z()
        << std::endl;
}

}
