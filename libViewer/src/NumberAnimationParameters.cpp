#include "libViewer/NumberAnimationParameters.h"


namespace viewer
{
NumberAnimationParameters::NumberAnimationParameters(const int duration, const float& from, const float& to, const bool running, const QString& prop) : AnimationParameters(duration,QVariant::fromValue(from),QVariant::fromValue(to),running,prop)
{
}

NumberAnimationParameters::NumberAnimationParameters() : AnimationParameters(250, QVariant::fromValue(1.0), QVariant::fromValue(0.0),true,"")
{
}

NumberAnimationParameters::NumberAnimationParameters(const NumberAnimationParameters& other)
{
	m_running = other.m_running;
	m_property = other.m_property;
	m_duration = other.m_duration;
	m_from= other.m_from;
	m_to = other.m_to;
}

float NumberAnimationParameters::get_from() const
{
	assert(m_from.canConvert<float>());
	return m_from.toFloat();
}

float NumberAnimationParameters::get_to() const
{
	assert(m_to.canConvert<float>());
	return m_to.toFloat();
}
}
