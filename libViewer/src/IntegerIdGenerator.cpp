#include "libViewer/IntegerIdGenerator.h"

#include <stdexcept>

namespace viewer
{
std::string IntegerIdGenerator::create_unique_id(const std::vector<std::string>& existing_ids,
    const std::string& id_root) const
{
    bool found_unique = false;
    std::string unique_id = "";
    int counter = 0;
    while(!found_unique && counter<10000)
    {
        std::string potentially_new_id = id_root + std::to_string(counter);
        if (std::find(existing_ids.begin(), existing_ids.end(), potentially_new_id) != existing_ids.end())
        {
            // Found element: do nothing, try another id
        } else
        {
            unique_id = potentially_new_id;
            found_unique = true;
        }
        counter++;
    }

    if(found_unique)
    {
        return unique_id;
    } else
    {
        throw std::runtime_error("IntegerIdGenerator could not create a unique id.");

    }
}
}
