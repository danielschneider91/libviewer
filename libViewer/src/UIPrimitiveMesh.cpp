#include "libViewer/UIPrimitiveMesh.h"

#include <iostream>

namespace viewer
{
UIPrimitiveMesh::UIPrimitiveMesh(QObject* parent): UISpatialObject(parent),
                                                               m_mesh(new Qt3DRender::QGeometryRenderer)
                                                               , m_material(new Qt3DExtras::QMetalRoughMaterial)
                                                               , m_transparent_material(new Qt3DExtras::QDiffuseSpecularMaterial)
{
}

Qt3DRender::QGeometryRenderer* UIPrimitiveMesh::get_mesh() const
{
	return m_mesh;
}

void UIPrimitiveMesh::set_mesh(Qt3DRender::QGeometryRenderer* mesh)
{
	m_mesh = mesh;
	emit mesh_changed();
}

Qt3DExtras::QMetalRoughMaterial* UIPrimitiveMesh::get_material() const
{
	return m_material;
}

Qt3DExtras::QDiffuseSpecularMaterial* UIPrimitiveMesh::get_transparent_material() const
{
	return m_transparent_material;
}

void UIPrimitiveMesh::set_material(const QVariant& baseColor, const QVariant& metalness,
                                         const QVariant& roughness, const QVariant& normal,
                                         const QVariant& ambientOcclusion, float textureScale, const QColor& transparent_color)
{
	if (!baseColor.isNull())
	{
		m_material->setBaseColor(baseColor);
		if (QString(baseColor.typeName()).compare(QString("QColor")) == 0)
		{
			auto color = baseColor.value<QColor>();
			color.setAlpha(122);
			m_transparent_material->setDiffuse(QVariant::fromValue(color));
			m_transparent_material->setTextureScale(1);
			m_transparent_material->setAlphaBlendingEnabled(true);
		}
		else
		{
			m_transparent_material->setDiffuse(QVariant::fromValue(transparent_color));
			m_transparent_material->setAlphaBlendingEnabled(true);
		}
	}
	if (!metalness.isNull())
	{
		m_material->setMetalness(metalness);
	}
	if (!roughness.isNull())
	{
		m_material->setRoughness(roughness);
	}
	if (!normal.isNull())
	{
		m_material->setNormal(normal);
	}
	if (!ambientOcclusion.isNull())
	{
		m_material->setAmbientOcclusion(ambientOcclusion);
	}
	m_material->setTextureScale(textureScale);

	emit material_changed();
	emit transparent_material_changed();
}

bool UIPrimitiveMesh::get_is_picking_enabled() const
{
    return m_picking_enabled;
}

void UIPrimitiveMesh::set_is_picking_enabled(const bool picking_enabled)
{
	if(get_is_picking_enabled()!=picking_enabled)
	{
		m_picking_enabled = picking_enabled;
		emit picking_enabled_changed();
	}
}
}
