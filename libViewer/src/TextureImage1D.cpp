#include "libViewer/TextureImage1D.h"

#include <iostream>
#include <QTextureImageDataGenerator>

#include "libViewer/TextureImageDataGenerator1D.h"

namespace viewer {

void TextureImage1D::setTextureInformation(const QByteArray& data, int dimension, float spacing, QVector3D origin, QMatrix4x4 orientation,
    int numberOfChannels, QVector<QColor> colors)
{
    m_texture_data = data;
    m_dimension = dimension;
    m_spacing = spacing;
    m_dimension_times_spacing = (float)m_dimension * m_spacing;
    m_origin = origin;
    m_orientation = orientation;
    m_channels = numberOfChannels;
    m_colors = colors;
}

void TextureImage1D::set_texture_data(const QByteArray& data)
{
    m_texture_data = data;
    m_default_texture_data = false;

    emit textureDataChanged();
    notifyDataGeneratorChanged();
}

QByteArray TextureImage1D::get_texture_data() const
{
    return m_texture_data;
}

QVector<QColor> TextureImage1D::get_colors() const
{
    return m_colors;
}

void TextureImage1D::set_colors(const QVector<QColor>& colors)
{
    if(m_colors!=colors)
    {
        m_colors = colors;
        emit colorsChanged();
        notifyDataGeneratorChanged();
    }
}

Qt3DRender::QTextureImageDataGeneratorPtr TextureImage1D::dataGenerator() const {
    auto imgGen = new TextureImageDataGenerator1D();

    if (m_default_texture_data) {
        auto generated_texture = make_color_spectrum(m_dimension, m_colors);
        imgGen->set_texture_information(generated_texture, m_dimension, m_spacing, m_origin, m_orientation, m_channels);
    }
    else {
        auto integer = 1;
        auto generated_texture = make_color_spectrum(m_dimension, m_colors);
        imgGen->set_texture_information(generated_texture, m_dimension, m_spacing, m_origin, m_orientation, m_channels);
    }

    return Qt3DRender::QTextureImageDataGeneratorPtr(imgGen);
}

QByteArray TextureImage1D::make_color_spectrum(size_t width, const QVector<QColor>& colors) const
{
    QByteArray ba;
    ba.resize(static_cast<int>(width) * 4);
    int index = (int)colors.size() - 1;
    for (int i = 0; i < width; ++i)
    {
        int   coli = (int)(index * (float)i / width);
        float colt = (index * (float)i / width) - coli;
        //std::cout << "rgba: "<< (255.0 * (colors[coli].red() / 255.0 * (1.0f - colt) + colors[coli + 1].red() / 255.0 * colt)) << ", " <<
        //    (255.0 * (colors[coli].green() / 255.0 * (1.0f - colt) + colors[coli + 1].green() / 255.0 * colt)) << ", " <<
        //    (255.0 * (colors[coli].blue() / 255.0 * (1.0f - colt) + colors[coli + 1].blue() / 255.0 * colt)) << ", " <<
        //     (255.0 * (colors[coli].alpha() / 255.0 * (1.0f - colt) + colors[coli + 1].alpha() / 255.0 * colt)) <<std::endl;
        ba[i * 4 + 0] = (unsigned char)(255.0 * (colors[coli].red()/255.0 * (1.0f - colt) + colors[coli + 1].red() / 255.0 * colt));
        ba[i * 4 + 1] = (unsigned char)(255.0 * (colors[coli].green() / 255.0 * (1.0f - colt) + colors[coli + 1].green() / 255.0 * colt));
        ba[i * 4 + 2] = (unsigned char)(255.0 * (colors[coli].blue() / 255.0 * (1.0f - colt) + colors[coli + 1].blue() / 255.0 * colt));
        ba[i * 4 + 3] = (unsigned char)(255.0 * (colors[coli].alpha() / 255.0 * (1.0f - colt) + colors[coli + 1].alpha() / 255.0 * colt));
    }
    return ba;
}

}