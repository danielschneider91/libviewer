#include "libViewer/ViewportVisibility.h"

namespace viewer
{
ViewportVisibility::ViewportVisibility(const bool three_d_viewports_visibility, const bool slice_viewports_visibility, const SliceViewportRenderingModeClass::SliceViewportRenderingMode& slice_viewport_rendering_mode, const float& clipping_distance, const bool& depth_testing, QObject* parent)
    : QObject(parent), m_threeD_viewports_visibility(three_d_viewports_visibility),
    m_slice_viewports_visibility(slice_viewports_visibility), m_slice_viewport_rendering_mode(slice_viewport_rendering_mode),
m_clipping_distance(clipping_distance),m_depth_testing(depth_testing)
{
}

bool ViewportVisibility::get_three_d_viewports_visibility() const
{
    return m_threeD_viewports_visibility;
}

void ViewportVisibility::set_three_d_viewports_visibility(const bool three_d_viewports_visibility)
{
    if (m_threeD_viewports_visibility != three_d_viewports_visibility)
    {
        m_threeD_viewports_visibility = three_d_viewports_visibility;
        emit threeD_viewports_visibility_changed();
    }
}

bool ViewportVisibility::get_slice_viewports_visibility() const
{
    return m_slice_viewports_visibility;
}

void ViewportVisibility::set_slice_viewports_visibility(const bool slice_viewports_visibility)
{
    if (m_slice_viewports_visibility != slice_viewports_visibility)
    {
        m_slice_viewports_visibility = slice_viewports_visibility;
        emit slice_viewports_visibility_changed();
    }
}

//SliceViewportRenderingModeClass* ViewportVisibility::get_slice_viewport_rendering_mode_class() const
//{
//    return m_slice_viewport_rendering_mode;
//}
//
//void ViewportVisibility::set_slice_viewport_rendering_mode_class(
//    const SliceViewportRenderingModeClass* slice_viewport_rendering_mode_class)
//{
//    m_slice_viewport_rendering_mode->set_mode(slice_viewport_rendering_mode_class->get_mode());
//    emit slice_viewport_rendering_mode_changed();
//}

SliceViewportRenderingModeClass::SliceViewportRenderingMode ViewportVisibility::
get_slice_viewport_rendering_mode() const
{
    return static_cast<SliceViewportRenderingModeClass::SliceViewportRenderingMode>(m_slice_viewport_rendering_mode);
}

void ViewportVisibility::set_slice_viewport_rendering_mode(
    const SliceViewportRenderingModeClass::SliceViewportRenderingMode& slice_viewport_rendering_mode)
{
    if(m_slice_viewport_rendering_mode!=slice_viewport_rendering_mode)
    {
        m_slice_viewport_rendering_mode = slice_viewport_rendering_mode;
        emit slice_viewport_rendering_mode_changed();
    }
}

float ViewportVisibility::get_clipping_distance() const
{
    return m_clipping_distance;
}

void ViewportVisibility::set_clipping_distance(const float& d)
{
    if(m_clipping_distance!=d)
    {
        m_clipping_distance = d;
        emit clipping_distance_changed();
    }
}

bool ViewportVisibility::get_depth_testing() const
{
    return m_depth_testing;
}

void ViewportVisibility::set_depth_testing(const bool depth_testing)
{
    m_depth_testing = depth_testing;
}
}
