#include "libViewer/ExpandShrinkAlgorithm.h"

namespace viewer
{
void ExpandShrinkAlgorithm::reset(const QList<QRectF>& initial_layout, QList<QRectF>& actual_layout,
                                  QList<bool>& enabled)
{
    assert(are_sizes_consistent(initial_layout, actual_layout, enabled));
    for (int i = 0; i < initial_layout.size(); i++)
    {
        actual_layout[i] = initial_layout[i];
        enabled[i] = true;
    }
}

QRectF ExpandShrinkAlgorithm::minimize(int viewportIndex, int viewerIndexToBeMaximized, int numberOfColumns,
                                       int numberOfRows,
                                       int behaviour
                                       /*0: completely disappear, 1: to the right, 99: top left corner small*/)
{
    float x = 0;
    float y = 0;
    float width = 0;
    float height = 0;

    switch (behaviour)
    {
    case 0:
        {
            float proportion0 = 0.001f;
            x = (viewportIndex % numberOfColumns) * 1 / numberOfColumns * proportion0;
            y = std::floor(viewportIndex / numberOfColumns) * 1.0f / numberOfRows * proportion0;
            width = 1.0f / numberOfColumns * proportion0;
            height = 1.0f / numberOfRows * proportion0;
            break;
        }
    case 1:
        {
            height = 1.0f / (numberOfRows * numberOfColumns - 1);
            width = height;
            x = 1 - width;
            if (viewportIndex < viewerIndexToBeMaximized) { y = viewportIndex * height; }
            else if (viewportIndex == viewerIndexToBeMaximized)
            {
                throw "Viewport to be minimized and viewport to be maximized cannot be the same";
            }
            else { y = (viewportIndex - 1.0f) * height; }
            break;
        }
    case 99:
        {
            float proportion = 0.1f;
            x = (viewportIndex % numberOfColumns) * 1 / numberOfColumns * proportion;
            y = std::floor(viewportIndex / numberOfColumns) * 1.0f / numberOfRows * proportion;
            width = 1.0f / numberOfColumns * proportion;
            height = 1.0f / numberOfRows * proportion;
            break;
        }
    }

    return QRectF(x, y, width, height);
}

void ExpandShrinkAlgorithm::toggle_expand(int viewport_idx, bool expand, const QList<QRectF>& initial_layout,
                                          QList<QRectF>& actual_layout, QList<bool>& enabled, int number_of_rows,
                                          int number_of_columns)
{
    assert(are_sizes_consistent(initial_layout, actual_layout, enabled));
    assert(viewport_idx < actual_layout.size());
    if (expand == false ||
        viewport_idx < 0 ||
        (actual_layout.at(viewport_idx).width() >= 1.0 && actual_layout.at(viewport_idx).height() >=
            1.0))
    {
        return reset(initial_layout, actual_layout, enabled);
    }

    for (int i = 0; i < initial_layout.size(); i++)
    {
        assert(expand);
        if (viewport_idx == i) // expand
        {
            actual_layout[i] = QRectF(0.0, 0.0, 1.0, 1.0);
            enabled[i] = true;
        }
        else // minimize
        {
            int behaviour = 0;
            actual_layout[i] = minimize(i, viewport_idx, number_of_columns, number_of_rows, behaviour);
            if (behaviour == 0)
            {
                enabled[i] = false;
            }
        }
    }
}

bool ExpandShrinkAlgorithm::are_sizes_consistent(const QList<QRectF>& initial_layout, QList<QRectF>& actual_layout,
                                                 QList<bool>& enabled)
{
    return initial_layout.size() == actual_layout.size() &&
        initial_layout.size() == enabled.size();
}
}
