#include "libViewer/MaterialSettings.h"


namespace viewer
{
MaterialSettings::MaterialSettings(const bool has_material, const MetalRoughMaterialProperties& metal_rough_material_properties,
    const QColor& transparent_color)
    : m_has_material(has_material),
    m_metal_rough_material_properties(metal_rough_material_properties),
    m_transparent_color(transparent_color)
{
}

bool MaterialSettings::get_has_material() const
{
    return m_has_material;
}

void MaterialSettings::set_has_material(const bool has_material)
{
    m_has_material = has_material;
}

MetalRoughMaterialProperties MaterialSettings::get_metal_rough_material_properties() const
{
    return m_metal_rough_material_properties;
}

void MaterialSettings::set_metal_rough_material_properties(
    const MetalRoughMaterialProperties& metal_rough_material_properties)
{
    m_metal_rough_material_properties = metal_rough_material_properties;
}

QColor MaterialSettings::get_transparent_color() const
{
    return m_transparent_color;
}

void MaterialSettings::set_transparent_color(const QColor& transparent_color)
{
    m_transparent_color = transparent_color;
}
}
