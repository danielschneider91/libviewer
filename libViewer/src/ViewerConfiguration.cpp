#include "libViewer/ViewerConfiguration.h"

#include <QtGlobal>

#include "libViewer/ExpandShrinkAlgorithm.h"

namespace viewer
{
ViewerConfiguration::ViewerConfiguration(QObject* parent) : QObject(parent),
                                                            m_initial_viewports_layout(m_viewports_layout)
{
}

ViewerConfiguration::~ViewerConfiguration()
{
}

std::pair<QString, QString> ViewerConfiguration::get_slice_id_and_name(const SliceType& type) const
{
    switch (type)
    {
    case SliceType::AXIAL:
        {
            return m_axial_slice_id_and_name;
            break;
        }
    case SliceType::SAGITTAL:
        {
            return m_sagittal_slice_id_and_name;
            break;
        }
    case SliceType::CORONAL:
        {
            return m_coronal_slice_id_and_name;
            break;
        }
    default: ;
        throw std::exception("Unknown slice type.");
    }
}

std::vector<QString> ViewerConfiguration::get_slice_ids() const
{
    std::vector<QString> ids;
    ids.push_back(m_axial_slice_id_and_name.first);
    ids.push_back(m_sagittal_slice_id_and_name.first);
    ids.push_back(m_coronal_slice_id_and_name.first);
    return ids;
}

int ViewerConfiguration::getNumberOfSlices()
{
    int counter = 0;
    QMapIterator<QString, QVariant> i(m_viewport_to_slicetype_map);
    while (i.hasNext())
    {
        i.next();
        //key:0,1,2
        //value: Axial, Coronal, Sagittal, 3D
        auto val = i.value();
        auto comparison_result = QString::compare(val.toString(), "3D");
        if (comparison_result != 0)
        {
            counter++;
        }
    }
    return counter;
}

int ViewerConfiguration::getThreeDViewportId()
{
    QMapIterator<QString, QVariant> i(m_viewport_to_slicetype_map);
    while (i.hasNext())
    {
        i.next();
        //key:0,1,2
        //value: Axial, Coronal, Sagittal, 3D
        auto key = i.key();
        auto key_as_int = key.toInt();
        auto val = i.value();
        auto comparison_result = QString::compare(val.toString(), "3D");
        if (comparison_result == 0)
        {
            return key_as_int;
        }
    }
    return -1;
}

// slices numbered from 0 to m
// 3d viewport at position p
// slices after position p are on viewport i+1
int ViewerConfiguration::getViewportIndexFromSliceIndex(int index)
{
    int counter = 0;
    for (int i = 0; i <= index; i++)
    {
        if (QString::compare(m_viewport_to_slicetype_map[QString::number(i)].toString(), "3D") == 0)
        {
            counter++;
        }
    }
    return index + counter;
}

int ViewerConfiguration::getSliceIndexFromViewportIndex(int index)
{
    int counter = 0;
    for (int i = 0; i <= index; i++)
    {
        if (QString::compare(m_viewport_to_slicetype_map[QString::number(i)].toString(), "3D") == 0)
        {
            counter++;
        }
    }
    return index - counter;
}

QList<QRectF> ViewerConfiguration::get_viewports_layout() const
{
    return m_viewports_layout;
}

QList<bool> ViewerConfiguration::get_viewports_enabled() const
{
    return m_viewports_enabled;
}

int ViewerConfiguration::get_number_of_rows() const
{
    return m_number_of_rows;
}

int ViewerConfiguration::get_number_of_columns() const
{
    return m_number_of_columns;
}

QList<QString> ViewerConfiguration::get_horizontal_line_colors() const
{
    return m_horizontal_line_colors;
}

QList<QString> ViewerConfiguration::get_vertical_line_colors() const
{
    return m_vertical_lne_colors;
}

QList<QString> ViewerConfiguration::get_slice_colors() const
{
    return m_slice_colors;
}

QVariantMap ViewerConfiguration::get_viewport_to_slicetype_map() const
{
    return m_viewport_to_slicetype_map;
}

QVariantMap ViewerConfiguration::get_viewport_to_sliceid_map() const
{
    return m_viewport_to_slice_id_map;
}

QVariantMap ViewerConfiguration::get_viewport_to_viewport_name_map() const
{
    return m_viewport_to_viewport_name_map;
}

QVariantMap ViewerConfiguration::get_slice_id_to_normal_shift_map() const
{
    return m_slice_id_to_normal_shift_map;
}

void ViewerConfiguration::set_viewports_layout(const QList<QRectF>& i)
{
    if (m_viewports_layout != i)
    {
        m_viewports_layout = i;
        m_initial_viewports_layout = i;
        emit viewports_layout_changed();
    }
}

void ViewerConfiguration::set_viewports_enabled(const QList<bool>& e)
{
    if (m_viewports_enabled != e)
    {
        m_viewports_enabled = e;
        emit viewports_enabled_changed();
    }
}

void ViewerConfiguration::set_number_of_rows(int r)
{
    if (m_number_of_rows != r)
    {
        m_number_of_rows = r;
        emit number_of_rows_changed();
    }
}

void ViewerConfiguration::set_number_of_columns(int c)
{
    if (m_number_of_columns != c)
    {
        m_number_of_columns = c;
        emit number_of_columns_changed();
    }
}

void ViewerConfiguration::set_horizontal_line_colors(const QList<QString>& clrs)
{
    if (m_horizontal_line_colors != clrs)
    {
        m_horizontal_line_colors = clrs;
        emit horizontal_line_colors_changed();
    }
}

void ViewerConfiguration::set_vertical_line_colors(const QList<QString>& clrs)
{
    if (m_vertical_lne_colors != clrs)
    {
        m_vertical_lne_colors = clrs;
        emit vertical_line_colors_changed();
    }
}

void ViewerConfiguration::set_slice_colors(const QList<QString>& clrs)
{
    if (m_slice_colors != clrs)
    {
        m_slice_colors = clrs;
        emit slice_colors_changed();
    }
}

void ViewerConfiguration::set_viewport_to_slicetype_map(const QVariantMap& m)
{
    if (m_viewport_to_slicetype_map != m)
    {
        m_viewport_to_slicetype_map = m;
        emit viewport_to_slicetype_map_changed();
    }
}

void ViewerConfiguration::set_viewport_to_sliceid_map(const QVariantMap& m)
{
    if (m_viewport_to_slice_id_map != m)
    {
        m_viewport_to_slice_id_map = m;
        emit viewport_to_slice_id_map_changed();
    }
}

void ViewerConfiguration::set_viewport_to_viewport_name_map(const QVariantMap& m)
{
    if (m_viewport_to_viewport_name_map != m)
    {
        m_viewport_to_viewport_name_map = m;
        emit viewport_to_viewport_name_map_changed();
    }
}

void ViewerConfiguration::set_slice_id_to_normal_shift_map(const QVariantMap& m)
{
    if (m_slice_id_to_normal_shift_map != m)
    {
        m_slice_id_to_normal_shift_map = m;
        emit slice_id_to_normal_shift_map_changed();
    }
}

QColor ViewerConfiguration::get_background_color() const
{
    return m_background_color;
}

void ViewerConfiguration::set_background_color(const QColor& background_color)
{
    if(background_color!=m_background_color)
    {
        m_background_color = background_color;
        emit background_color_changed();
    }
}

void ViewerConfiguration::expand_viewport(int viewport)
{
    if (m_expanded_viewport != viewport)
    {
        bool expand_or_reset = viewport >= 0;
        ExpandShrinkAlgorithm a;
        a.toggle_expand(viewport, expand_or_reset, m_initial_viewports_layout, m_viewports_layout, m_viewports_enabled,
                        m_number_of_rows, m_number_of_columns);
        viewports_layout_changed();
        viewports_enabled_changed();
        m_expanded_viewport = viewport;
        emit expanded_viewport_changed();
    }
}

void ViewerConfiguration::reset_viewports_layout()
{
    expand_viewport(-1);
}

int ViewerConfiguration::get_expanded_viewport() const
{
    return m_expanded_viewport;
}
}
