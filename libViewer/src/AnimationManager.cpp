#include "libViewer/AnimationManager.h"

#include "libViewer/QMLCrosshairsObjectParameters.h"
#include "libViewer/QMLSpatialObject.h"

namespace viewer
{
AnimationManager::AnimationManager(QObject* parent):QObject(parent)
{
    m_animation_parameters_container = new AnimationParametersContainer(this);
}

void AnimationManager::add_animation(const std::string& id, const AnimationProperty& animation_property,
                                          const AnimationParameters& animation_parameters, QObject* target)
{
    m_animation_container.add_animation(id, animation_property, animation_parameters, target);
}

void AnimationManager::add_animation(const std::string& id, const AnimationProperty& animation_property,
    const AnimationParameters& animation_parameters)
{
    m_animation_parameters_container->add_animation_parameters(id, animation_property, animation_parameters);
}

void AnimationManager::remove_animation(const std::string& id, const AnimationProperty& animation_property)
{
    m_animation_container.remove_animation(id, animation_property);
    m_animation_parameters_container->remove_animation_parameters(id, animation_property);
}

void AnimationManager::remove_animations(const std::string& id)
{
    m_animation_container.remove_animations(id);
    m_animation_parameters_container->remove_animations_parameters(id);
}

}
