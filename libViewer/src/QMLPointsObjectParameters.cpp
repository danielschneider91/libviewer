#include "libViewer/QMLPointsObjectParameters.h"


namespace viewer
{
QMLPointsObjectParameters::QMLPointsObjectParameters(const QList<QVector3D>& positions, const QList<float>& sizes)
    : m_positions(positions),m_sizes(sizes)
{
}

QMLPointsObjectParameters::QMLPointsObjectParameters(const QMLPointsObjectParameters& other)
{
    m_positions = other.m_positions;
    m_sizes = other.m_sizes;
}

QList<QVector3D> QMLPointsObjectParameters::get_positions() const
{
    return m_positions;
}

QList<float> QMLPointsObjectParameters::get_sizes() const
{
    return m_sizes;
}
}
