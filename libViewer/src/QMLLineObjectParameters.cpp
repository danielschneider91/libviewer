#include "libViewer/QMLLineObjectParameters.h"


namespace viewer
{
QMLLineObjectParameters::QMLLineObjectParameters(const QVector3D& from, const QVector3D& to)
    : m_from(from),
    m_to(to)
{
}

QMLLineObjectParameters::QMLLineObjectParameters() :
m_from(QVector3D(0,0,0)),
m_to(QVector3D(0, 0, 0))
{}

QMLLineObjectParameters::QMLLineObjectParameters(const QMLLineObjectParameters& other)
{
    m_from = other.m_from;
    m_to = other.m_to;
}

QVector3D QMLLineObjectParameters::get_from() const
{
    return m_from;
}

QVector3D QMLLineObjectParameters::get_to() const
{
    return m_to;
}

void QMLLineObjectParameters::set_from(const QVector3D& from)
{
    if(m_from!=from)
    {
        m_from = from;
        emit from_changed();
    }
}

void QMLLineObjectParameters::set_to(const QVector3D& to)
{
    if(m_to!=to)
    {
        m_to = to;
        emit to_changed();
    }
}
}
