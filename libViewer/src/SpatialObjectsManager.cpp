#include "libViewer/SpatialObjectsManager.h"

#include <iostream>
#include <sstream>

#include "libViewer/QMLCrosshairsObjectParameters.h"
#include "libViewer/QMLLineObjectParameters.h"
#include "libViewer/QMLPointsObjectParameters.h"
#include "libViewer/QMLSpatialObject.h"
#include "libViewer/UISpatialMeshFromFileDiffuseSpecular.h"

namespace viewer
{

SpatialObjectsManager::SpatialObjectsManager(QObject* parent):QObject(parent)
{
    m_spatial_objects_from_file = new SpatialObjectsListModel(this);
    m_primitive_objects = new PrimitiveSpatialObjectsListModel(this);
    m_qml_spatial_objects = new QMLSpatialObjectsListModel(this);
    m_animation_manager = new AnimationManager(this);
}

int SpatialObjectsManager::get_index_from_id(QString id)
{
    auto id_str = id.toStdString();
    if (m_spatial_objects_from_file_id_to_index_map.find(id_str) != m_spatial_objects_from_file_id_to_index_map.end())
    {
        return m_spatial_objects_from_file_id_to_index_map.at(id_str);
    }
    if (m_primitive_objects_id_to_index_map.find(id_str) != m_primitive_objects_id_to_index_map.end())
    {
        return m_primitive_objects_id_to_index_map.at(id_str);
    }
    if (m_qml_spatial_objects_id_to_index_map.find(id_str) != m_qml_spatial_objects_id_to_index_map.end())
    {
        return m_qml_spatial_objects_id_to_index_map.at(id_str);
    }
    return -1;
}

void SpatialObjectsManager::add_primitive_geometry(const std::string& id, Qt3DRender::QGeometryRenderer* primitive_geometry,
    const Pose& pose, const MaterialSettings& material_settings, const ViewportVisibility& viewport_visibility,bool picking_enabled)
{
    auto position = pose.get_position();
    auto x_axis = pose.get_x_axis();
    auto normal_vector = pose.get_z_axis();
    auto mesh = new UIPrimitiveMesh();
    mesh->set_id(QString::fromStdString(id));
    mesh->set_name(QString::fromStdString(id));
    mesh->set_pose((position),
        (x_axis),
        (normal_vector));
    mesh->set_is_picking_enabled(picking_enabled);
    mesh->set_mesh(primitive_geometry);
    auto material_properties = material_settings.get_metal_rough_material_properties();
    mesh->set_material(material_properties.get_base_color(), material_properties.get_metalness(), material_properties.get_roughness(),
        material_properties.get_normal(), material_properties.get_ambient_occlusion(), material_properties.get_texture_scale(), material_settings.get_transparent_color());
    m_primitive_objects_id_to_index_map[id] = m_primitive_objects->get_objects().size();
    auto viewport_vis = new ViewportVisibility(viewport_visibility.get_three_d_viewports_visibility(), viewport_visibility.get_slice_viewports_visibility(), viewport_visibility.get_slice_viewport_rendering_mode(), viewport_visibility.get_clipping_distance(),
        viewport_visibility.get_depth_testing());
    m_primitive_spatial_objects_viewport_visibility.append(viewport_vis);
    m_primitive_objects->add_object(mesh);
    emit primitive_objects_changed();
    emit primitive_spatial_objects_viewport_visibility_changed();
}

void SpatialObjectsManager::add_object_by_type(const std::string& id, QMLSpatialObjectType::Type type, QMLSpatialObjectParameters* params, const Pose& pose,
    const MaterialSettings& material_settings, const ViewportVisibility& viewport_visibility, bool picking_enabled)
{
    auto position = pose.get_position();
    auto x_axis = pose.get_x_axis();
    auto normal_vector = pose.get_z_axis();
    auto obj = new QMLSpatialObject();
    switch (type)
    {
    case QMLSpatialObjectType::POINTS: {
        if (dynamic_cast<QMLPointsObjectParameters*>(params))
        {
            obj->set_parameters(params);
        }
        else
        {
            std::cerr << "Use QMLPointsObjectParameters for QMLSpatialObjectType::POINTS. Using default parameters." << std::endl;
            obj->set_parameters(new QMLPointsObjectParameters{ QList<QVector3D>{QVector3D(0,0,0)},QList<float>{10.0f} });
        }
        break;
    }
    case QMLSpatialObjectType::LINE: {
        if (dynamic_cast<QMLLineObjectParameters*>(params))
        {
            obj->set_parameters(params);
        }
        else
        {
            std::cerr << "Use QMLLineObjectParameters for QMLSpatialObjectType::LINE. Using default parameters." << std::endl;
            obj->set_parameters(new QMLLineObjectParameters{ QVector3D(0,0,0),QVector3D(50,50,50) });
        }
        break;
    }
    case QMLSpatialObjectType::CROSSHAIRS: {
        if(dynamic_cast<QMLCrosshairsObjectParameters*>(params))
        {
            obj->set_parameters(params);
        } else
        {
            std::cerr << "Use QMLCrosshairsObjectParameters for QMLSpatialObjectType::CROSSHAIRS. Using default parameters." << std::endl;
            obj->set_parameters(new QMLCrosshairsObjectParameters{100.0});
        }
        break;
    }
    default:
    {
        std::cerr << "This cannot happen (unknown QMLSpatialObjectType, in default case." << std::endl;
        assert(true);
        break;
    }

    }
    obj->set_type(type);
    obj->set_id(QString::fromStdString(id));
    obj->set_name(QString::fromStdString(id));
    obj->set_pose((position),
        (x_axis),
        (normal_vector));
    auto material_properties = material_settings.get_metal_rough_material_properties();
    obj->set_material(material_properties.get_base_color(), material_properties.get_metalness(), material_properties.get_roughness(),
        material_properties.get_normal(), material_properties.get_ambient_occlusion(), material_properties.get_texture_scale(), material_settings.get_transparent_color());

    m_qml_spatial_objects_id_to_index_map[id] = m_qml_spatial_objects->get_objects().size();
    auto viewport_vis = new ViewportVisibility(viewport_visibility.get_three_d_viewports_visibility(), viewport_visibility.get_slice_viewports_visibility(), viewport_visibility.get_slice_viewport_rendering_mode(),viewport_visibility.get_clipping_distance(),
        viewport_visibility.get_depth_testing());
    m_qml_spatial_objects_viewport_visibility.append(viewport_vis);
    emit qml_spatial_objects_viewport_visibility_changed();
    m_qml_spatial_objects->add_object(obj);
    emit qml_spatial_objects_changed();
}

void SpatialObjectsManager::remove_object(const std::string& id)
{
    if(contains_object_with_id(id))
    {
        auto objs_from_file = get_object_from_file_ids_in_viewer();
        if (std::find(objs_from_file.begin(), objs_from_file.end(), id) != objs_from_file.end())
        {
            remove_spatial_object_from_file(QString::fromStdString(id));
        }

        auto objs_primitive = get_primitive_object_ids_in_viewer();
        if (std::find(objs_primitive.begin(), objs_primitive.end(), id) != objs_primitive.end())
        {
            remove_primitive_spatial_object(QString::fromStdString(id));
        }

        auto objs_qml = get_qml_object_ids_in_viewer();
        if (std::find(objs_qml.begin(), objs_qml.end(), id) != objs_qml.end())
        {
            remove_qml_spatial_object(QString::fromStdString(id));
        }
    } else
    {
        throw "Cannot remove object with id that is not in viewer. " + id;
    }
}

void SpatialObjectsManager::set_object_material(const std::string& id, const MaterialSettings& material_settings)
{
    if (contains_object_with_id(id))
    {
        auto objs_from_file = get_object_from_file_ids_in_viewer();
        if (std::find(objs_from_file.begin(), objs_from_file.end(), id) != objs_from_file.end())
        {
            m_spatial_objects_from_file->set_material(id, material_settings);
        }

        auto objs_primitive = get_primitive_object_ids_in_viewer();
        if (std::find(objs_primitive.begin(), objs_primitive.end(), id) != objs_primitive.end())
        {
            m_primitive_objects->set_material(id, material_settings);
        }

        auto objs_qml = get_qml_object_ids_in_viewer();
        if (std::find(objs_qml.begin(), objs_qml.end(), id) != objs_qml.end())
        {
            m_qml_spatial_objects->set_material(id, material_settings);
        }
    }
    else
    {
        throw "Cannot set material of object with id that is not in viewer. " + id;
    }
}

MaterialSettings SpatialObjectsManager::get_object_material(const std::string& id) const
{
    if (contains_object_with_id(id))
    {
        auto objs_from_file = get_object_from_file_ids_in_viewer();
        if (std::find(objs_from_file.begin(), objs_from_file.end(), id) != objs_from_file.end())
        {
            return m_spatial_objects_from_file->get_material(id);
        }

        auto objs_primitive = get_primitive_object_ids_in_viewer();
        if (std::find(objs_primitive.begin(), objs_primitive.end(), id) != objs_primitive.end())
        {
            return m_primitive_objects->get_material(id);
        }

        auto objs_qml = get_qml_object_ids_in_viewer();
        if (std::find(objs_qml.begin(), objs_qml.end(), id) != objs_qml.end())
        {
            return m_qml_spatial_objects->get_material(id);
        }
    }
    throw "Cannot get material of object with id that is not in viewer. " + id;
}

void SpatialObjectsManager::add_animation(const std::string& id, const AnimationProperty& animation_property,
                                          const AnimationParameters& animation_parameters)
{
    if (m_primitive_objects_id_to_index_map.find(id) != m_primitive_objects_id_to_index_map.end())
    {
        int index = m_primitive_objects_id_to_index_map[id];
        UIPrimitiveMesh* obj = m_primitive_objects->get_objects().at(index);
        switch (animation_property)
        {
        case AnimationProperty::SCALE: {
            m_animation_manager->add_animation(id, animation_property, animation_parameters, obj->get_transform());
            break;
        }
        case AnimationProperty::COLOR: {
            throw "AnimationProperty::COLOR is not implemented yet.";
            break;
        }
        }
    }

    if (m_qml_spatial_objects_id_to_index_map.find(id) != m_qml_spatial_objects_id_to_index_map.end())
    {
        //int index = m_qml_spatial_objects_id_to_index_map[id];
        switch (animation_property)
        {
        case AnimationProperty::SCALE: {
            m_animation_manager->add_animation(id, animation_property, animation_parameters);
            break;
        }
        case AnimationProperty::COLOR: {
            throw "AnimationProperty::COLOR is not implemented yet.";
            break;
        }
        }
    }
}

void SpatialObjectsManager::remove_animation(const std::string& id, const AnimationProperty& animation_property)
{
    m_animation_manager->remove_animation(id, animation_property);
}

void SpatialObjectsManager::remove_animations(const std::string& id)
{
    m_animation_manager->remove_animations(id);
}

void SpatialObjectsManager::add_object_from_file(const std::string& id, const std::string& file_path_name,
                                               const Pose& pose, const MaterialSettings& material_settings, const ViewportVisibility& viewport_visibility,
    bool picking_enabled)
{
    auto position = pose.get_position();
    auto x_axis = pose.get_x_axis();
    auto normal_vector = pose.get_z_axis();
    auto mesh = new UISpatialMeshFromFile(this);
    mesh->set_id(QString::fromStdString(id));
    mesh->set_name(QString::fromStdString(id));
    mesh->set_pose((position),
        (x_axis),
        (normal_vector));
    mesh->set_mesh_file_path_name(QString::fromStdString(file_path_name));
    mesh->set_has_material(material_settings.get_has_material());
    mesh->set_is_picking_enabled(picking_enabled);
    if (!material_settings.get_has_material())
    {
        auto material_properties = material_settings.get_metal_rough_material_properties();
        mesh->set_material(material_properties.get_base_color(), material_properties.get_metalness(), material_properties.get_roughness(),
            material_properties.get_normal(), material_properties.get_ambient_occlusion(), material_properties.get_texture_scale(), material_settings.get_transparent_color());
    }
    m_spatial_objects_from_file_id_to_index_map[id] = m_spatial_objects_from_file->get_objects().size();
    auto viewport_vis = new ViewportVisibility(viewport_visibility.get_three_d_viewports_visibility(), viewport_visibility.get_slice_viewports_visibility(), viewport_visibility.get_slice_viewport_rendering_mode(),viewport_visibility.get_clipping_distance(),
        viewport_visibility.get_depth_testing());
    m_spatial_objects_from_file_viewport_visibility.append(viewport_vis);
    m_spatial_objects_from_file->add_object(mesh);

    emit spatial_objects_from_file_viewport_visibility_changed();
    emit objects_from_file_changed();
}

void SpatialObjectsManager::set_object_pose(const std::string& id, const QVector3D& position, const QVector3D& x_axis,
                                            const QVector3D& normal)
{

    try
    {
        if (m_spatial_objects_from_file_id_to_index_map.find(id) != m_spatial_objects_from_file_id_to_index_map.end())
        {
            m_spatial_objects_from_file->set_pose(id, Pose(position, x_axis, normal));
            //emit objects_from_file_changed();
        }
        if (m_primitive_objects_id_to_index_map.find(id) != m_primitive_objects_id_to_index_map.end())
        {
            m_primitive_objects->set_pose(id, Pose(position, x_axis, normal));
        }
        if (m_qml_spatial_objects_id_to_index_map.find(id) != m_qml_spatial_objects_id_to_index_map.end())
        {
            m_qml_spatial_objects->set_pose(id, Pose(position, x_axis, normal));
        }
    } catch (std::out_of_range ex)
    {
            std::cerr << "SpatialObjectsManager::set_tracked_object_pose: Did not find tracked object with id " << id << " in tracked objects list." << std::endl;
    }
}

void SpatialObjectsManager::set_viewport_visibility(const std::string& id,
    const viewer::ViewportVisibility& viewport_visibility)
{
    if(contains_object_with_id((id)))
    {
        if(m_spatial_objects_from_file_id_to_index_map.find(id)!=m_spatial_objects_from_file_id_to_index_map.end())
        {
            int index = m_spatial_objects_from_file_id_to_index_map.at(id);
            try
            {
                m_spatial_objects_from_file_viewport_visibility[index]->set_three_d_viewports_visibility(viewport_visibility.get_three_d_viewports_visibility());
                m_spatial_objects_from_file_viewport_visibility[index]->set_slice_viewports_visibility(viewport_visibility.get_slice_viewports_visibility());
                m_spatial_objects_from_file_viewport_visibility[index]->set_slice_viewport_rendering_mode(viewport_visibility.get_slice_viewport_rendering_mode());
                m_spatial_objects_from_file_viewport_visibility[index]->set_clipping_distance(viewport_visibility.get_clipping_distance());
                m_spatial_objects_from_file_viewport_visibility[index]->set_depth_testing(viewport_visibility.get_depth_testing());
                emit spatial_objects_from_file_viewport_visibility_changed();
            } catch(std::exception& ex)
            {
                std::cerr << "set_viewport_visibility, m_spatial_objects_from_file_viewport_visibility: this cannot happen. Exception: " << ex.what() << std::endl;
                assert(true);
            }
        }
        if (m_primitive_objects_id_to_index_map.find(id) != m_primitive_objects_id_to_index_map.end())
        {
            int index = m_primitive_objects_id_to_index_map.at(id);
            try
            {
                m_primitive_spatial_objects_viewport_visibility[index]->set_three_d_viewports_visibility(viewport_visibility.get_three_d_viewports_visibility());
                m_primitive_spatial_objects_viewport_visibility[index]->set_slice_viewports_visibility(viewport_visibility.get_slice_viewports_visibility());
                m_primitive_spatial_objects_viewport_visibility[index]->set_slice_viewport_rendering_mode(viewport_visibility.get_slice_viewport_rendering_mode());
                m_primitive_spatial_objects_viewport_visibility[index]->set_clipping_distance(viewport_visibility.get_clipping_distance());
                m_primitive_spatial_objects_viewport_visibility[index]->set_depth_testing(viewport_visibility.get_depth_testing());
                emit primitive_spatial_objects_viewport_visibility_changed();
            }
            catch (std::exception& ex)
            {
                std::cerr << "set_viewport_visibility, m_primitive_objects_id_to_index_map: this cannot happen. Exception: " << ex.what() << std::endl;
                assert(true);
            }
        }
        if (m_qml_spatial_objects_id_to_index_map.find(id) != m_qml_spatial_objects_id_to_index_map.end())
        {
            int index = m_qml_spatial_objects_id_to_index_map.at(id);
            try
            {
                m_qml_spatial_objects_viewport_visibility[index]->set_three_d_viewports_visibility(viewport_visibility.get_three_d_viewports_visibility());
                m_qml_spatial_objects_viewport_visibility[index]->set_slice_viewports_visibility(viewport_visibility.get_slice_viewports_visibility());
                m_qml_spatial_objects_viewport_visibility[index]->set_slice_viewport_rendering_mode(viewport_visibility.get_slice_viewport_rendering_mode());
                m_qml_spatial_objects_viewport_visibility[index]->set_clipping_distance(viewport_visibility.get_clipping_distance());
                m_qml_spatial_objects_viewport_visibility[index]->set_depth_testing(viewport_visibility.get_depth_testing());
                emit qml_spatial_objects_viewport_visibility_changed();
            }
            catch (std::exception& ex)
            {
                std::cerr << "set_viewport_visibility, m_qml_spatial_objects_viewport_visibility: this cannot happen. Exception: " << ex.what() << std::endl;
                assert(true);
            }
        }
    }
}

ViewportVisibility* SpatialObjectsManager::get_viewport_visibility(const std::string& id) const
{
    if (contains_object_with_id((id)))
    {
        if (m_spatial_objects_from_file_id_to_index_map.find(id) != m_spatial_objects_from_file_id_to_index_map.end())
        {
            int index = m_spatial_objects_from_file_id_to_index_map.at(id);
            try
            {
                return m_spatial_objects_from_file_viewport_visibility[index];
            }
            catch (std::exception& ex)
            {
                std::cerr << "get_viewport_visibility, m_spatial_objects_from_file_viewport_visibility: this cannot happen. Exception: " << ex.what() << std::endl;
                assert(true);
            }
        }
        if (m_primitive_objects_id_to_index_map.find(id) != m_primitive_objects_id_to_index_map.end())
        {
            int index = m_primitive_objects_id_to_index_map.at(id);
            try
            {
                return m_primitive_spatial_objects_viewport_visibility[index];
            }
            catch (std::exception& ex)
            {
                std::cerr << "get_viewport_visibility, m_primitive_spatial_objects_viewport_visibility: this cannot happen. Exception: " << ex.what() << std::endl;
                assert(true);
            }
        }
        if (m_qml_spatial_objects_id_to_index_map.find(id) != m_qml_spatial_objects_id_to_index_map.end())
        {
            int index = m_qml_spatial_objects_id_to_index_map.at(id);
            try
            {
                return m_qml_spatial_objects_viewport_visibility[index];
            }
            catch (std::exception& ex)
            {
                std::cerr << "get_viewport_visibility, m_qml_spatial_objects_viewport_visibility: this cannot happen. Exception: " << ex.what() << std::endl;
                assert(true);
            }
        }
    }
}

void SpatialObjectsManager::set_picking_enabled(const std::string& id, bool picking_enabled)
{
    if (contains_object_with_id((id)))
    {
        if (m_spatial_objects_from_file_id_to_index_map.find(id) != m_spatial_objects_from_file_id_to_index_map.end())
        {
            m_spatial_objects_from_file->set_picking_enabled(id, picking_enabled);
        }
        if (m_primitive_objects_id_to_index_map.find(id) != m_primitive_objects_id_to_index_map.end())
        {
            m_primitive_objects->set_picking_enabled(id, picking_enabled);
        }
        if (m_qml_spatial_objects_id_to_index_map.find(id) != m_qml_spatial_objects_id_to_index_map.end())
        {
            m_qml_spatial_objects->set_picking_enabled(id, picking_enabled);
        }
    }
}

std::vector<std::string> SpatialObjectsManager::get_object_ids_in_viewer() const
{
    std::vector<std::string> objs;
    auto objs_from_file = get_object_from_file_ids_in_viewer();
    objs.insert(objs.end(), objs_from_file.begin(), objs_from_file.end());

    auto objs_primitive = get_primitive_object_ids_in_viewer();
    objs.insert(objs.end(), objs_primitive.begin(), objs_primitive.end());

    auto objs_qml = get_qml_object_ids_in_viewer();
    objs.insert(objs.end(), objs_qml.begin(), objs_qml.end());

    return objs;
}

std::vector<std::string> SpatialObjectsManager::get_primitive_object_ids_in_viewer() const
{
    std::vector<std::string> objs;
    for (const auto& obj : m_primitive_objects_id_to_index_map)
    {
        objs.push_back(obj.first);
    }
    return objs;

}

std::vector<std::string> SpatialObjectsManager::get_object_from_file_ids_in_viewer() const
{
    std::vector<std::string> objs;
    for (const auto& obj : m_spatial_objects_from_file_id_to_index_map)
    {
        objs.push_back(obj.first);
    }
    return objs;
}

std::vector<std::string> SpatialObjectsManager::get_qml_object_ids_in_viewer() const
{
    std::vector<std::string> objs;
    for (const auto& obj : m_qml_spatial_objects_id_to_index_map)
    {
        objs.push_back(obj.first);
    }
    return objs;
}

Side SpatialObjectsManager::get_side() const
{
	return m_side;
}

void SpatialObjectsManager::set_side(const Side& side)
{
	m_side = side;
}

viewer::SpatialObjectsListModel* SpatialObjectsManager::get_spatial_objects_from_file()
{
    return m_spatial_objects_from_file;
}

QMLSpatialObjectParameters* SpatialObjectsManager::get_spatial_object_parameters(const std::string& id) const
{
    if (contains_object_with_id((id)))
    {
        if (m_spatial_objects_from_file_id_to_index_map.find(id) != m_spatial_objects_from_file_id_to_index_map.end())
        {
            throw std::runtime_error("Not implemented.");
        }
        if (m_primitive_objects_id_to_index_map.find(id) != m_primitive_objects_id_to_index_map.end())
        {
            throw std::runtime_error("Not implemented.");
        }
        if (m_qml_spatial_objects_id_to_index_map.find(id) != m_qml_spatial_objects_id_to_index_map.end())
        {
            return m_qml_spatial_objects->get_parameters(id);
        }
    }
    std::stringstream ss;
    ss << "No spatial object with id " << id << " in viewer.";
    throw std::runtime_error(ss.str());
}

bool SpatialObjectsManager::contains_object_with_id(const std::string& id) const
{
    auto ids_in_viewer = get_object_ids_in_viewer();
    if (std::find(ids_in_viewer.begin(), ids_in_viewer.end(), id) != ids_in_viewer.end())
    {
        return true;
    }
    return false;
}

void SpatialObjectsManager::remove_spatial_object_from_file(const QString& id)
{
    // Element in vector.
    m_spatial_objects_from_file->remove_object(id);
    emit objects_from_file_changed();
    auto index = m_spatial_objects_from_file_id_to_index_map.at(id.toStdString()); //it is necessary to rebuild id_to_index map when removing item from somewhere...
    delete m_spatial_objects_from_file_viewport_visibility.takeAt(index);
    m_spatial_objects_from_file_id_to_index_map.erase(id.toStdString());

    // update id_index_map
    for (auto& [key, val] : m_spatial_objects_from_file_id_to_index_map)
    {
        if (key == id.toStdString())
            assert(true && "This cannot happen, object with key == id should have been erased.");
        if (val > index)
            val = val - 1;
    }
    emit spatial_objects_from_file_viewport_visibility_changed();
}

void SpatialObjectsManager::remove_qml_spatial_object(const QString& id)
{
    // remove animation if available
    remove_animations(id.toStdString());

    // Element in vector.
    m_qml_spatial_objects->remove_object(id);
    emit qml_spatial_objects_changed();
    auto index = m_qml_spatial_objects_id_to_index_map.at(id.toStdString()); //it is necessary to rebuild id_to_index map when removing item from somewhere...
    delete m_qml_spatial_objects_viewport_visibility.takeAt(index);
    m_qml_spatial_objects_id_to_index_map.erase(id.toStdString());

    // update id_index_map
    for (auto& [key, val] : m_qml_spatial_objects_id_to_index_map)
    {
        if (key == id.toStdString())
            assert(true && "This cannot happen, object with key == id should have been erased.");
        if (val > index)
            val = val - 1;
    }
    emit qml_spatial_objects_viewport_visibility_changed();
}

void SpatialObjectsManager::remove_primitive_spatial_object(const QString& id)
{
    // remove animation if available
    remove_animations(id.toStdString());

    // Element in vector.
    m_primitive_objects->remove_object(id);
    emit primitive_objects_changed();
    auto index = m_primitive_objects_id_to_index_map.at(id.toStdString()); //it is necessary to rebuild id_to_index map when removing item from somewhere...
    delete m_primitive_spatial_objects_viewport_visibility.takeAt(index);
    m_primitive_objects_id_to_index_map.erase(id.toStdString());

    // update id_index_map
    for (auto& [key, val] : m_primitive_objects_id_to_index_map)
    {
        if (key == id.toStdString())
            assert(true && "This cannot happen, object with key == id should have been erased.");
        if (val > index)
            val = val - 1;
    }
    emit primitive_spatial_objects_viewport_visibility_changed();
}
}
