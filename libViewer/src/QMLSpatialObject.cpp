#include "libViewer/QMLSpatialObject.h"

#include <iostream>

namespace viewer
{
QMLSpatialObject::QMLSpatialObject(QObject* parent): UISpatialObject(parent), m_type(new QMLSpatialObjectType())
                                                               , m_material(new Qt3DExtras::QMetalRoughMaterial)
                                                               , m_transparent_material(new Qt3DExtras::QDiffuseSpecularMaterial)
{
}

QMLSpatialObjectType* QMLSpatialObject::get_type() const
{
	return m_type;
}

void QMLSpatialObject::set_type(const QMLSpatialObjectType::Type& type)
{
	if(get_type()->get_type()!=type)
	{
		m_type->set_type(type);
		emit type_changed();
	}
}

void QMLSpatialObject::set_parameters(QMLSpatialObjectParameters* parameters)
{
	m_parameters = parameters;
	emit parameters_changed();
}

QMLSpatialObjectParameters* QMLSpatialObject::get_parameters() const
{
	return m_parameters;
}

Qt3DExtras::QMetalRoughMaterial* QMLSpatialObject::get_material() const
{
	return m_material;
}

Qt3DExtras::QDiffuseSpecularMaterial* QMLSpatialObject::get_transparent_material() const
{
	return m_transparent_material;
}

void QMLSpatialObject::set_material(const QVariant& baseColor, const QVariant& metalness,
                                         const QVariant& roughness, const QVariant& normal,
                                         const QVariant& ambientOcclusion, float textureScale, const QColor& transparent_color)
{
	if (!baseColor.isNull())
	{
		m_material->setBaseColor(baseColor);
		if (QString(baseColor.typeName()).compare(QString("QColor")) == 0)
		{
			auto color = baseColor.value<QColor>();
            color.setAlpha(122);
			m_transparent_material->setDiffuse(QVariant::fromValue(color));
			m_transparent_material->setTextureScale(1);
			m_transparent_material->setAlphaBlendingEnabled(true);
		}
		else
		{
			m_transparent_material->setDiffuse(QVariant::fromValue(transparent_color));
			m_transparent_material->setAlphaBlendingEnabled(true);
		}
	}
	if (!metalness.isNull())
	{
		m_material->setMetalness(metalness);
	}
	if (!roughness.isNull())
	{
		m_material->setRoughness(roughness);
	}
	if (!normal.isNull())
	{
		m_material->setNormal(normal);
	}
	if (!ambientOcclusion.isNull())
	{
		m_material->setAmbientOcclusion(ambientOcclusion);
	}
	m_material->setTextureScale(textureScale);

	emit material_changed();
	emit transparent_material_changed();
}

bool QMLSpatialObject::get_is_picking_enabled() const
{
	return m_picking_enabled;
}

void QMLSpatialObject::set_is_picking_enabled(const bool picking_enabled)
{
	if (get_is_picking_enabled() != picking_enabled)
	{
		m_picking_enabled = picking_enabled;
		emit picking_enabled_changed();
	}
}
}
