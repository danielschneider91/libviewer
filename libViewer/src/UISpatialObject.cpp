#include "libViewer/UISpatialObject.h"


#include "libViewer/PoseCalculator.h"

namespace viewer
{
UISpatialObject::UISpatialObject(QObject* parent): QObject(parent),m_transform(new Qt3DCore::QTransform)
{
}

UISpatialObject::~UISpatialObject()
{
}

const QString& UISpatialObject::get_id() const
{
	return m_id;
}

void UISpatialObject::set_id(const QString& id)
{
	if (m_id != id)
	{
		m_id = id;
		emit idChanged();
	}
}

const QString& UISpatialObject::get_name() const
{
	return m_name;
}

void UISpatialObject::set_name(const QString& name)
{
	m_name = name;
}

bool UISpatialObject::get_is_enabled() const
{
	return m_enabled;
}

void UISpatialObject::set_is_enabled(bool enabled)
{
	m_enabled = enabled;
}

bool UISpatialObject::get_is_visible() const
{
	return m_visible;
}

void UISpatialObject::set_is_visible(bool visible)
{
	if (m_visible != visible)
	{
		m_visible = visible;
		emit visibleChanged();
	}
}

const QMatrix4x4& UISpatialObject::get_pose() const
{
	return m_pose;
}

Qt3DCore::QTransform* UISpatialObject::get_transform() const
{
	return m_transform;
}

void UISpatialObject::set_transform(Qt3DCore::QTransform* transform)
{
	if(m_transform!=transform)
	{
		m_transform = transform;
		emit transformChanged();
	}
}

void UISpatialObject::set_pose(const QMatrix4x4& pose)
{
	//if (m_pose != pose || m_transform->matrix() != pose)
	//{
		m_transform->setMatrix(pose);
		emit transformChanged();
		m_pose = pose;
		emit poseChanged();
	//}
}

void UISpatialObject::set_pose(const QVector3D& position, const QVector3D& x_axis, const QVector3D& z_axis)
{
	auto pose = PoseCalculator::get_transform_from((position),
	                                               (x_axis),
	                                               (z_axis));

	//QMatrix4x4 q_pose(pose(0,0), pose(0, 1), pose(0, 2), pose(0, 3),
	//    pose(1, 0), pose(1, 1), pose(1, 2), pose(1, 3),
	//    pose(2, 0), pose(2, 1), pose(2, 2), pose(2, 3),
	//    0,0,0,1);
	set_pose(pose);
}

void UISpatialObject::set_position(const QVector3D& position)
{
	m_pose(0, 3) = position.x();
	m_pose(1, 3) = position.y();
	m_pose(2, 3) = position.z();
	emit poseChanged();
}
}
