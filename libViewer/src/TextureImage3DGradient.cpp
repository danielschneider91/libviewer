#include "libViewer/TextureImage3DGradient.h"

#include <iostream>
#include <QTextureImageDataGenerator>

#include "libViewer/TextureImageGradientGenerator3D.h"

namespace viewer {
    
void TextureImage3DGradient::setTextureData(const QByteArray& data)
{
    //if(data!=m_texture_data) {
    if (true) {
        m_textureData = data;
        m_defaultTextureData = false;
        emit textureDataChanged();
        notifyDataGeneratorChanged();
    }
}

QByteArray TextureImage3DGradient::getTextureData() const
{
    return m_textureData;
}

Qt3DRender::QTextureImageDataGeneratorPtr TextureImage3DGradient::dataGenerator() const {
    auto imgGen = new TextureImageGradientGenerator3D();

    if (m_defaultTextureData) {
        auto generated_texture = createTexture(m_dimension[0], m_dimension[1], m_dimension[2],3,2);
        imgGen->set_texture_information(generated_texture, m_dimension, m_channels);
    }
    else {
        auto integer = 1;
        imgGen->set_texture_information(m_textureData, m_dimension, m_channels);
    }

    return Qt3DRender::QTextureImageDataGeneratorPtr(imgGen);
}

QByteArray TextureImage3DGradient::createTexture(int maxX, int maxY, int maxZ, int numberOfChannels, int numberOfBytes) const
{
    QByteArray ba;
    //ba.resize(maxX * maxY * maxZ * numberOfChannels);

    int bacounter = 0;
    int val = 0;
    for (int i = 0; i < maxX; i++) {
        for (int j = 0; j < maxY; j++) {
            for (int k = 0; k < maxZ; k++) {
                if (val >= 255) {
                    val = 0;
                }

                short sval = val - 100;
                char aa[sizeof(short)];
                memcpy(&aa, &sval, sizeof(short)); // copy a to s


                for (int c = 0; c < numberOfChannels; c++) {
                    if (c == 1) {
                        for (int b = 0; b < numberOfBytes; b++)
                        {
                            ba.append(aa[b]);
                        }
                        //ba[bacounter] = (char)val;//'z';
                    }
                    else {
                        for (int b = 0; b < numberOfBytes; b++)
                        {
                            ba.append(aa[b]);
                        }
                        //ba[bacounter] = '0';
                    }
                    bacounter++;
                }
                val++;

            }
        }
    }
    return ba;
}
}