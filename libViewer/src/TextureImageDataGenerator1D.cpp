#include "libViewer/TextureImageDataGenerator1D.h"

#include <iostream>
#include <QTextureImageData>
#include <QTextureImageDataPtr>
namespace viewer {
Qt3DRender::QTextureImageDataPtr TextureImageDataGenerator1D::operator()() {
    Qt3DRender::QTextureImageDataPtr data = Qt3DRender::QTextureImageDataPtr::create();

    int maxX = m_dimension;
    int numberOfChannels = m_channels;

    data->setLayers(1);
    data->setTarget(QOpenGLTexture::Target1D);

    if (maxX == 0) {

        data->setPixelFormat(QOpenGLTexture::PixelFormat::Red);
        data->setPixelType(QOpenGLTexture::PixelType::Float32);
        data->setFormat(QOpenGLTexture::TextureFormat::R32F);

        data->setWidth(m_dimension);
        data->setHeight(1);
        data->setDepth(1);

        auto texture = create_texture(16, numberOfChannels);
        data->setData(texture, 1);
    }
    else {
        data->setWidth(maxX);
        data->setMipLevels(1);
        data->setLayers(1);
        data->setHeight(1);
        data->setDepth(1);
        
        data->setPixelFormat(QOpenGLTexture::PixelFormat::RGBA); // Format of the input texture data
        data->setPixelType(QOpenGLTexture::PixelType::UInt8); // Format of the input texture data 
        data->setFormat(QOpenGLTexture::TextureFormat::RGBA8_UNorm); // OpenGL internal format

        //print_byte_array_values_rgba(m_texture_data,4);
        data->setData(m_texture_data, 4, false);
    }

    return data;
}

TextureImageDataGenerator1D::TextureImageDataGenerator1D() : QTextureImageDataGenerator() {

}

bool TextureImageDataGenerator1D::operator==(const Qt3DRender::QTextureImageDataGenerator& other) const {
    const TextureImageDataGenerator1D* otherFunctor = functor_cast<TextureImageDataGenerator1D>(&other);
    // if its the same URL, but different modification times, its not the same image.
    return false;//(otherFunctor != nullptr);
}

void TextureImageDataGenerator1D::set_texture_information(const QByteArray& data, int dimension, float spacing, QVector3D origin,
    QMatrix4x4 orientation, int numberOfChannels)
{
    m_texture_data = data;
    m_dimension = dimension;
    m_spacing = spacing;
    m_origin = origin;
    m_orientation = orientation;
    m_channels = numberOfChannels;
}

void TextureImageDataGenerator1D::print_byte_array_values(const QByteArray& cs, int numberOfBytes)
{
    for(int i=0;i<cs.size();i=i+numberOfBytes)
    {
        unsigned char a = (cs[i] & 255);          // Ou: (num & 0xFF)
        unsigned char b = ((cs[i+1] >> 8) & 255);          // Ou: (num & 0xFF)

        short uvalBack;

        char aa[sizeof(short)];
        aa[0] = cs[i];
        aa[1] = cs[i + 1];
        memcpy(&uvalBack, aa, sizeof(short)); // and back to s again

        int16_t number = a | b << 8;
    }
}

void TextureImageDataGenerator1D::print_byte_array_values_rgba(const QByteArray& cs, int numberOfBytes)
{
    for (int i = 0; i < cs.size(); i = i + numberOfBytes)
    {
        std::cout << "rgba: " << (int)(uint8_t)cs[i] << ", " << (int)(uint8_t)cs[i + 1] << ", " << (int)(uint8_t)cs[i + 2] << ", " << (int)(uint8_t)cs[i + 3] << std::endl;
    }
}

QByteArray TextureImageDataGenerator1D::create_texture(int maxX, int numberOfChannels) const
{

    QByteArray ba2;
    //ba2.resize(maxX * maxY * maxZ * numberOfChannels);

    int bacounter = 0;
    short val = 0;
    for (int i = 0; i < maxX; i++) {
        if (val >= 255) {
            val = 0;
        }
        for (int c = 0; c < numberOfChannels; c++) {

            short v = 0;
            ba2.append(reinterpret_cast<const char*>(&v), sizeof(v));
            bacounter++;
        }
        val++;
    }

    return ba2;
}

}
