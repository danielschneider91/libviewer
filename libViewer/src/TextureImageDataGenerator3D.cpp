#include "libViewer/TextureImageDataGenerator3D.h"

#include <iostream>
#include <QTextureImageData>
#include <QTextureImageDataPtr>
namespace viewer {
Qt3DRender::QTextureImageDataPtr TextureImageDataGenerator3D::operator()() {
    Qt3DRender::QTextureImageDataPtr data = Qt3DRender::QTextureImageDataPtr::create();

    int maxX = m_dimension[0];
    int maxY = m_dimension[1];
    int maxZ = m_dimension[2];
    int numberOfChannels = m_channels;

    //data->setPixelFormat(QOpenGLTexture::PixelFormat::RGB);
    //data->setPixelType(QOpenGLTexture::PixelType::Int8);
    //data->setFormat(QOpenGLTexture::TextureFormat::RGB8_UNorm);

    data->setLayers(1);
    data->setTarget(QOpenGLTexture::Target3D);

    if (maxX == 0 || maxY== 0 || maxZ== 0) {

        data->setPixelFormat(QOpenGLTexture::PixelFormat::Red_Integer);
        data->setPixelType(QOpenGLTexture::PixelType::Int16);
        data->setFormat(QOpenGLTexture::TextureFormat::R16I);

        data->setWidth(16);
        data->setHeight(16);
        data->setDepth(32);

        auto texture = createTexture(16, 16, 32, numberOfChannels);
        data->setData(texture, 1);
    }
    else {
        data->setWidth(maxX);
        data->setHeight(maxY);
        data->setDepth(maxZ);

        // works with isampler3d in frag shader (see VolumeRenderingRayTracingShader.frag and VolumeShaderSlicePlane.frag), yields int value in range: [-32768, 32767] (https://stackoverflow.com/questions/41713631/what-is-the-correct-way-to-sample-a-16-bit-height-map-in-opengl)
        //data->setPixelFormat(QOpenGLTexture::PixelFormat::Red_Integer); // specifies type of input data (entries of m_textureData, see https://www.khronos.org/registry/OpenGL-Refpages/gl4/html/glTexImage3D.xhtml)
        //data->setPixelType(QOpenGLTexture::PixelType::Int16); // specifies type of input data (entries of m_textureData, see https://www.khronos.org/registry/OpenGL-Refpages/gl4/html/glTexImage3D.xhtml)
        //data->setFormat(QOpenGLTexture::TextureFormat::R16I); // specifies the internal format (see https://www.khronos.org/registry/OpenGL-Refpages/gl4/html/glTexImage3D.xhtml)

        // works with sampler3d in frag shader (see VolumeRenderingRayTracingShader.frag and VolumeShaderSlicePlane.frag), yields float value in range: [-1, 1] (https://stackoverflow.com/questions/41713631/what-is-the-correct-way-to-sample-a-16-bit-height-map-in-opengl)
        // glTexImage3D(
        //    target(GL_TEXTURE_3D),
        //    level (0),
        //    internalformat (Specifies the number of color components in the texture. E.g., GL_RED, GL_RG, GL_RGB, GL_RGBA, GL_R8, GL_R8_SNORM, ...),
        //    width (Specifies the width of the texture image),
        //    height (Specifies the height of the texture image. All implementations support 3D texture images that are at least 256 texels high.),
        //    depth (Specifies the depth of the texture image, or the number of layers in a texture array. All implementations support 3D texture images that are at least 256 texels deep, and texture arrays that are at least 256 layers deep.),
        //    border (This value must be 0),
        //    format (In Qt this is "PixeFormat", Specifies the format of the pixel data. The following symbolic values are accepted: GL_RED (Qt -> QOpenGLTexture::Red), GL_RG (Qt -> QOpenGLTexture::RG), GL_RGB, GL_BGR, GL_RGBA, GL_BGRA, GL_RED_INTEGER (Qt -> QOpenGLTexture::Red_Integer), GL_RG_INTEGER, GL_RGB_INTEGER, GL_BGR_INTEGER, GL_RGBA_INTEGER, GL_BGRA_INTEGER, GL_STENCIL_INDEX, GL_DEPTH_COMPONENT, GL_DEPTH_STENCIL.),
        //    type : In Qt this is PixelType. Specifies the data type of the pixel data. The following symbolic values are accepted: GL_UNSIGNED_BYTE (Qt -> QOpenGLTexture::UInt8), GL_BYTE (Qt -> QOpenGLTexture::Int8), GL_UNSIGNED_SHORT (Qt -> QOpenGLTexture::UInt16), GL_SHORT (Qt -> QOpenGLTexture::Int16), GL_UNSIGNED_INT, GL_INT (Qt -> QOpenGLTexture::Int32), GL_HALF_FLOAT, GL_FLOAT, GL_UNSIGNED_BYTE_3_3_2,...,
        //    data (Specifies a pointer to the image data in memory.)



        // TODO restore this
        data->setPixelFormat(QOpenGLTexture::PixelFormat::Red); // specifies type of input data (entries of m_textureData, see https://www.khronos.org/registry/OpenGL-Refpages/gl4/html/glTexImage3D.xhtml)
        data->setPixelType(QOpenGLTexture::PixelType::Int16); // specifies type of input data (entries of m_textureData, see https://www.khronos.org/registry/OpenGL-Refpages/gl4/html/glTexImage3D.xhtml)
        data->setFormat(QOpenGLTexture::TextureFormat::R16_SNorm); // specifies the opengl internal image/texture format (see https://www.khronos.org/registry/OpenGL-Refpages/gl4/html/glTexImage3D.xhtml)




        //data->setPixelFormat(QOpenGLTexture::PixelFormat::Red_Integer); // specifies type of input data (entries of m_textureData, see https://www.khronos.org/registry/OpenGL-Refpages/gl4/html/glTexImage3D.xhtml)
        //data->setPixelType(QOpenGLTexture::PixelType::Int16); // specifies type of input data (entries of m_textureData, see https://www.khronos.org/registry/OpenGL-Refpages/gl4/html/glTexImage3D.xhtml)
        //data->setFormat(QOpenGLTexture::TextureFormat::R16F); // specifies the opengl internal image/texture format (see https://www.khronos.org/registry/OpenGL-Refpages/gl4/html/glTexImage3D.xhtml)

        m_integer++;
        if (m_integer > 2) { m_integer = 0; }
        //auto genTex = createTexture2(maxX, maxY, maxZ, 1);
        //data->setData(genTex, 2);
        //print_byte_array_values(m_texture_data,2);
        data->setData(m_textureData, 1);
    }

    return data;
}

TextureImageDataGenerator3D::TextureImageDataGenerator3D() : QTextureImageDataGenerator() {

}

bool TextureImageDataGenerator3D::operator==(const Qt3DRender::QTextureImageDataGenerator& other) const {
    const TextureImageDataGenerator3D* otherFunctor = functor_cast<TextureImageDataGenerator3D>(&other);
    // if its the same URL, but different modification times, its not the same image.
    return false;//(otherFunctor != nullptr);
}

void TextureImageDataGenerator3D::setInteger(int integer)
{
    m_integer = integer;
}

void TextureImageDataGenerator3D::set_texture_information(const QByteArray& data, QVector3D dimension, QVector3D spacing, QVector3D origin,
    QMatrix4x4 orientation, int numberOfChannels)
{
    m_textureData = data;
    m_dimension = dimension;
    m_spacing = spacing;
    m_origin = origin;
    m_orientation = orientation;
    m_channels = numberOfChannels;
}

void TextureImageDataGenerator3D::print_byte_array_values(const QByteArray& cs, int numberOfBytes)
{
    for(int i=0;i<cs.size();i=i+numberOfBytes)
    {
        unsigned char a = (cs[i] & 255);          // Ou: (num & 0xFF)
        unsigned char b = ((cs[i+1] >> 8) & 255);          // Ou: (num & 0xFF)

        short uvalBack;

        char aa[sizeof(short)];
        aa[0] = cs[i];
        aa[1] = cs[i + 1];
        memcpy(&uvalBack, aa, sizeof(short)); // and back to s again

        int16_t number = a | b << 8;
    }
}

QByteArray TextureImageDataGenerator3D::createTexture(int maxX, int maxY, int maxZ, int numberOfChannels) const
{

    QByteArray ba2;
    //ba2.resize(maxX * maxY * maxZ * numberOfChannels);

    int bacounter = 0;
    short val = 0;
    for (int i = 0; i < maxX; i++) {
        for (int j = 0; j < maxY; j++) {
            for (int k = 0; k < maxZ; k++) {
                if (val >= 255) {
                    val = 0;
                }
                for (int c = 0; c < numberOfChannels; c++) {
                    if (c == m_integer) {
                        ba2.append(reinterpret_cast<const char*>(&val), sizeof(val));
                    }
                    else {
                        short v = 0;
                        ba2.append(reinterpret_cast<const char*>(&v), sizeof(v));
                    }
                    bacounter++;
                }
                val++;
            }
        }
    }

    return ba2;
}

QByteArray TextureImageDataGenerator3D::createTexture2(int maxX, int maxY, int maxZ, int numberOfChannels) const
{
    std::vector<unsigned short> vec(maxX * maxY * maxZ * numberOfChannels);
    QByteArray ba2;
    int blockSize = 2;
    //ba2.resize(maxX * maxY * maxZ * numberOfChannels*blockSize);
    //ba[0] = 0x3c;
    int bacounter = 0;
    int shortCounter = 0;
    int val = 0;
    for (int i = 0; i < maxX; i++) {
        for (int j = 0; j < maxY; j++) {
            for (int k = 0; k < maxZ; k++) {
                if (val >= 255) {
                    val = 0;
                }
                for (int c = 0; c < numberOfChannels; c++) {
                    vec[shortCounter] = shortCounter;


                    //if (c == m_integer) {

                    unsigned char a = (vec[shortCounter] & 255);          // Ou: (num & 0xFF)
                    unsigned char b = ((vec[shortCounter] >> 8) & 255);          // Ou: (num & 0xFF)

                        //QByteArray tmpba;
                        //tmpba.setNum(12);
                        //ba2.append(tmpba);
                    ba2.append(a);
                    ba2.append(b);
                        int number = a | b << 8;
                    if(shortCounter%100000==0)
                    {
                        //std::cout << "number: " << number << std::endl;
                    }
                        //std::cout << "uval: " << uval << std::endl;
                        //std::cout << "number: " << number << std::endl;
                        //ba2[bacounter] = (vec[shortCounter] & 255);          // Ou: (num & 0xFF)
                        //bacounter++;
                        //ba2[bacounter] = ((vec[shortCounter] >> 8) & 255);          // Ou: (num & 0xFF)
                        //ba2[bacounter] = (char)val;//'z';
                        //bacounter++;
                        //ba2[bacounter] = (char)val;//'z';
                    //}
                    //else {
                    //    ba2[bacounter] = (vec[shortCounter] & 255);          // Ou: (num & 0xFF)
                    //    bacounter++;
                    //    ba2[bacounter] = ((vec[shortCounter] >> 8) & 255);          // Ou: (num & 0xFF)
                    //    //ba2[bacounter] = '0';
                    //    //bacounter++;
                    //    //ba2[bacounter] = '0';
                    //    //ba2[bacounter] = static_cast<char>(200);
                    //}
                    bacounter++;
                    shortCounter++;
                }
                val++;
            }
        }
    }

    return ba2;
}

QByteArray TextureImageDataGenerator3D::createTexture3(int maxX, int maxY, int maxZ, int numberOfChannels) const
{
    QByteArray ba2;
    int blockSize = 1;
    ba2.resize(maxX * maxY * maxZ * numberOfChannels * blockSize);
    //ba[0] = 0x3c;
    int bacounter = 0;
    int val = 0;
    for (int i = 0; i < maxX; i++) {
        for (int j = 0; j < maxY; j++) {
            for (int k = 0; k < maxZ; k++) {
                if (val >= 255) {
                    val = 0;
                }
                for (int c = 0; c < numberOfChannels; c++) {

                    if (c == m_integer) {
                        ba2[bacounter] = (val);          // Ou: (num & 0xFF)
                    }
                    else {
                        ba2[bacounter] = (val);          // Ou: (num & 0xFF)
                    }
                    bacounter++;
                }
                val++;
            }
        }
    }

    return ba2;
}
}
