#include "libViewer/Measurement.h"

#include <iostream>

#include "libViewer/QMLCrosshairsObjectParameters.h"
#include "libViewer/QMLLineObjectParameters.h"
#include "libViewer/QMLPointsObjectParameters.h"

namespace viewer
{
Measurement::Measurement(std::tuple<QString, QString, QString> ids, QObject* parent): QObject(parent),
    m_ids(ids)
{
}

QVector3D Measurement::get_from() const
{
    return m_positions.first;
}

void Measurement::set_from(const QVector3D& p)
{
    if(p!=m_positions.first)
    {
        m_positions.first = p;
        emit distance_changed();
    }
}

QVector3D Measurement::get_to() const
{
    return m_positions.second;
}

void Measurement::set_to(const QVector3D& p)
{
    if (p != m_positions.second)
    {
        m_positions.second = p;
        emit distance_changed();
    }
}

QString Measurement::get_from_id() const
{
    return std::get<0>(m_ids);
}

QString Measurement::get_to_id() const
{
    return std::get<1>(m_ids);
}

QString Measurement::get_line_id() const
{
    return std::get<2>(m_ids);
}

void Measurement::reset_measurement()
{
    set_from(QVector3D{ 0,0,0 });
    set_to(QVector3D{ 0,0,0 });
}

void Measurement::print_position(const QVector3D& position, const QString& msg) const
{
    std::cout << msg.toStdString() << position.x() << "," << position.y() << "," << position.
        z()
        << std::endl;
}

float Measurement::get_distance() const
{
    return (get_to() - get_from()).length();
}
}
