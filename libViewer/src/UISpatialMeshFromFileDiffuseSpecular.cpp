#include "libViewer/UISpatialMeshFromFileDiffuseSpecular.h"

namespace viewer {
UISpatialMeshFromFileDiffuseSpecular::UISpatialMeshFromFileDiffuseSpecular(QObject* parent): UISpatialObject(parent)
{
    m_material = new Qt3DExtras::QDiffuseSpecularMaterial();
}

QString UISpatialMeshFromFileDiffuseSpecular::get_mesh_file_path_name() const
{
    return m_mesh_file_path_name;
}

void UISpatialMeshFromFileDiffuseSpecular::set_mesh_file_path_name(const QString& mesh_file_path_name)
{
    if (m_mesh_file_path_name != mesh_file_path_name)
    {
        m_mesh_file_path_name = mesh_file_path_name;
        emit mesh_file_path_changed();
    }
}

bool UISpatialMeshFromFileDiffuseSpecular::get_has_material() const
{
    return m_has_material;
}

void UISpatialMeshFromFileDiffuseSpecular::set_has_material(const bool has_material)
{
    if(m_has_material!=has_material)
    {
        m_has_material = has_material;
        emit has_material_changed();
    }
}

Qt3DExtras::QDiffuseSpecularMaterial* UISpatialMeshFromFileDiffuseSpecular::get_material() const
{
    return m_material;
}

void UISpatialMeshFromFileDiffuseSpecular::set_material(const QVariant& diffuse, const QVariant& specular, float shininess, const QColor& ambient,
    const QVariant& normal, bool alphaBlending, float textureScale)
{
    if (!diffuse.isNull())
    {
        m_material->setDiffuse(diffuse);
    }
    if (!specular.isNull())
    {
        m_material->setSpecular(specular);
    }
    if (!normal.isNull())
    {
        m_material->setNormal(normal);
    }

    m_material->setShininess(shininess);
    m_material->setAmbient(ambient);
    m_material->setAlphaBlendingEnabled(alphaBlending);
    m_material->setTextureScale(textureScale);

    emit material_changed();
}

}
