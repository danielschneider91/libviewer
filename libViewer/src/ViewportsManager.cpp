#include "libViewer/ViewportsManager.h"

#include <iostream>

#include "libViewer/MetalRoughMaterials.h"
#include "libViewer/ObjectVisibilityListViewItem.h"
#include "libViewer/PoseCalculator.h"

namespace viewer
{
ViewportsManager::ViewportsManager(ViewerConfiguration* viewer_configuration,QObject* parent) :m_viewer_configuration(viewer_configuration),QObject(parent)
{
    m_slice_viewer_behaviour = new ViewerBehaviour();
    m_threeD_viewer_behaviour = new ViewerBehaviour();
}

void ViewportsManager::set_threeD_viewer_camera_pose(const QVector3D& camera_position, const QVector3D& look_at_position,
    const QVector3D& up_vector)
{
    emit threeD_viewer_camera_pose_changed(camera_position, look_at_position, up_vector);
}

void ViewportsManager::threeD_viewer_camera_view_all()
{
    emit threeD_viewer_camera_view_all_changed();
}

ViewerBehaviour* ViewportsManager::get_slice_viewer_behaviour() const
{   
    return m_slice_viewer_behaviour;
}

void ViewportsManager::set_slice_viewer_behaviour(ViewerBehaviour::Behaviour viewer_behaviour)
{
    if (m_slice_viewer_behaviour->get_behaviour() != viewer_behaviour)
    {
        m_slice_viewer_behaviour->set_behaviour(viewer_behaviour);
        emit slice_viewer_behaviour_changed();
    }
}

ViewerBehaviour* ViewportsManager::get_threeD_viewer_behaviour() const
{
    return m_threeD_viewer_behaviour;
}

void ViewportsManager::set_threeD_viewer_behaviour(ViewerBehaviour::Behaviour viewer_behaviour)
{
    if (m_threeD_viewer_behaviour->get_behaviour() != viewer_behaviour)
    {
        m_threeD_viewer_behaviour->set_behaviour(viewer_behaviour);
        emit threeD_viewer_behaviour_changed();
    }
}

ViewerConfiguration* ViewportsManager::get_viewer_configuration() const
{
    return m_viewer_configuration;
}

void ViewportsManager::set_viewer_configuration(ViewerConfiguration* viewer_configuration)
{
    m_viewer_configuration = viewer_configuration;
    emit viewer_configuration_changed();
}
}
