#include "libViewer/SpatialObjectsListModel.h"

#include <iostream>

namespace viewer
{
SpatialObjectsListModel::SpatialObjectsListModel(QObject* parent): QAbstractListModel(parent)
{
}

SpatialObjectsListModel::~SpatialObjectsListModel()
{
}

QHash<int, QByteArray> SpatialObjectsListModel::roleNames() const {
    QHash<int, QByteArray> roles;
    roles[FilePath] = "filePath";
    roles[HasMaterial] = "hasMaterial";
    roles[Pose] = "pose";
    roles[Transform] = "transform";
    roles[Material] = "material";
    roles[TransparentMaterial] = "transparentMaterial";
    roles[Enabled] = "enabled";
    roles[Visible] = "visible";
    roles[Name] = "name";
    roles[Id] = "id";
    roles[PickingEnabled] = "pickingEnabled";
    return roles;
}

QVariant SpatialObjectsListModel::data(const QModelIndex& index, int role) const
{
    QVariant result = QVariant();
    int row = index.row();
    int column = index.column();

    if (!index.isValid() || row >= rowCount())
    {
        std::cout << "index not valid or row larger than rowCount" << std::endl;
        return result;
    }

    switch (role)
    {
    case FilePath:
    {
        return m_data.at(row)->get_mesh_file_path_name();
    }
    case HasMaterial:
    {
        return m_data.at(row)->get_has_material();
    }
    case Pose:
    {
        return m_data.at(row)->get_pose();
    }
    case Transform:
    {
        return QVariant::fromValue(m_data.at(row)->get_transform());
    }
    case Material:
    {
        return QVariant::fromValue(m_data.at(row)->get_material());
    }
    case TransparentMaterial:
    {
        return QVariant::fromValue(m_data.at(row)->get_transparent_material());
    }
    case Enabled:
    {
        return QVariant::fromValue(m_data.at(row)->get_is_enabled());
    }
    case Visible:
    {
        return QVariant::fromValue(m_data.at(row)->get_is_visible());
    }
    case Name:
    {
        return QVariant::fromValue(m_data.at(row)->get_name());
    }

    case Id:
    {
        return QVariant::fromValue(m_data.at(row)->get_id());
    }
    case PickingEnabled:
    {
        return QVariant::fromValue(m_data.at(row)->get_is_picking_enabled());
    }

    default: break;
    }


    return result;
}

int SpatialObjectsListModel::rowCount(const QModelIndex& parent) const
{
    if(parent.isValid())
    {
        std::cout << "parent is valid" << std::endl;
        return 0;
    }
    return m_data.size();
}

Qt::ItemFlags SpatialObjectsListModel::flags(const QModelIndex& index) const
{
    Qt::ItemFlags result = Qt::ItemIsEditable | QAbstractItemModel::flags(index);
    return result;
}

void SpatialObjectsListModel::add_object(UISpatialMeshFromFile* data)
{
    beginInsertRows(QModelIndex(), m_data.size(), m_data.size()); // Specify the firstand last row numbers for the span of rows you want to insert into an item in a model.

    m_id_index_map[data->get_id()] = m_data.size();
    m_data.push_back(data);
    endInsertRows();
}

void SpatialObjectsListModel::remove_object(const QString& id)
{
    auto index = m_id_index_map.at(id);
    beginRemoveRows(QModelIndex(), index, index); // Specify the first and last row numbers for the span of rows you want to remove from an item in a model.
    delete m_data.takeAt(index);
    endRemoveRows();

    // rebuild id_index_map
    m_id_index_map.clear();
    for (int i = 0; i < m_data.count(); ++i)
    {
        m_id_index_map[m_data[i]->get_id()] = i;
    }
}

QList<UISpatialMeshFromFile*> SpatialObjectsListModel::get_objects() const
{
    return m_data;
}

void SpatialObjectsListModel::set_pose(const std::string& id, const viewer::Pose& pose)
{
    auto index = m_id_index_map.at(QString::fromStdString(id));
    //auto position = m_data[index]->get_pose().column(3).toVector3D();
    //auto file_name = m_data[index]->get_mesh_file_path_name().toStdString();
    //if(file_name.find("Bone")!=std::string::npos)
    //{
    //    std::cout << "here it happens..." << std::endl;
    //}
    //std::cout << "----------------------------------------" << std::endl;
    //for (auto const& [key, val] : m_id_index_map)
    //{
    //    std::cout << "id: " << key.toStdString()        // string (key)
    //        <<", index: "
    //        << val        // string's value
    //        << std::endl;
    //}
    //std::cout << "Updating pose with index: " << index << " and id " << id << " and data " << file_name << " at pose " << position.x() << "," << position.y() << "," << position.z() << std::endl;
    //std::cout << "m_data" << std::endl;
    //for (auto const& d : m_data)
    //{
    //    std::cout << "id: " << d->get_id().toStdString() << std::endl;
    //    std::cout << "name: " << d->get_name().toStdString() << std::endl;
    //    auto position = m_data[index]->get_pose().column(3).toVector3D();
    //    std::cout << "position: " << position.x() << "," << position.y() << "," << position.z() << std::endl;
    //}

    m_data[index]->set_pose((pose.get_position()), (pose.get_x_axis()), (pose.get_z_axis()));
    QModelIndex top = createIndex(index, 0);
    emit dataChanged(top, top);
}

void SpatialObjectsListModel::set_material(const std::string& id, const MaterialSettings& material_settings)
{
    auto index = m_id_index_map[QString::fromStdString(id)];
    m_data[index]->set_material(material_settings.get_metal_rough_material_properties().get_base_color(),
        material_settings.get_metal_rough_material_properties().get_metalness(),
        material_settings.get_metal_rough_material_properties().get_roughness(),
        material_settings.get_metal_rough_material_properties().get_normal(),
        material_settings.get_metal_rough_material_properties().get_ambient_occlusion(),
        material_settings.get_metal_rough_material_properties().get_texture_scale(),
        material_settings.get_transparent_color());
    QModelIndex top = createIndex(index, 0);
    emit dataChanged(top, top);
}

MaterialSettings SpatialObjectsListModel::get_material(const std::string& id) const
{
    auto index = m_id_index_map.at(QString::fromStdString(id));
    return m_data[index]->get_material();
}

void SpatialObjectsListModel::set_picking_enabled(const std::string& id, const bool picking_enabled)
{
    auto index = m_id_index_map.at(QString::fromStdString(id));
    m_data[index]->set_is_picking_enabled(picking_enabled);
    QModelIndex top = createIndex(index, 0);
    emit dataChanged(top, top);

}
}
