#include "libViewer/ImagesListModel.h"

#include <iostream>

namespace viewer
{
ImagesListModel::ImagesListModel(QObject* parent): QAbstractListModel(parent)
{
}

ImagesListModel::~ImagesListModel()
{
}

QHash<int, QByteArray> ImagesListModel::roleNames() const
{
    QHash<int, QByteArray> roles;
    roles[FilePath] = "filePath";
    roles[Enabled] = "enabled";
    roles[Visible] = "visible";
    roles[Name] = "name";
    roles[Id] = "id";
    roles[PickingEnabled] = "pickingEnabled";
    return roles;
}

QVariant ImagesListModel::data(const QModelIndex& index, int role) const
{
    auto result = QVariant();
    int row = index.row();
    int column = index.column();

    if (!index.isValid() || row >= rowCount())
    {
        std::cout << "index not valid or row larger than rowCount" << std::endl;
        return result;
    }

    switch (role)
    {
    case Enabled:
        {
            return true;
        }
    case Visible:
        {
            return true;
        }
    case Name:
        {
            for (const auto& [key,value] : m_id_index_map)
            {
                if (value == index.row())
                {
                    return key;
                }
            }
            return "Undefined";
        }
    case Id:
        {
            for (const auto& [key, value] : m_id_index_map)
            {
                if (value == index.row())
                {
                    return key;
                }
            }
            return "Undefined";
        }
    case PickingEnabled:
        {
            return true;
        }
    case VolumeData:
        {
            return m_data.at(row)->get_texture_data();
        }
    case NumberOfChannels:
        {
            return m_data.at(row)->get_channels();
        }
    case Origin:
        {
            return m_data.at(row)->get_origin();
        }
    case OriginBoundingBox:
    {
        return m_data.at(row)->get_bounding_box_origin();
    }
    case Orientation:
        {
            return m_data.at(row)->get_orientation();
        }
    case Dimension:
        {
            return m_data.at(row)->get_dimension();
        }
    case Spacing:
        {
            return m_data.at(row)->get_spacing();
        }
    case DimensionTimesSpacing:
        {
            return m_data.at(row)->get_dimension_times_spacing();
        }
    case MinimumValue:
        {
            return m_data.at(row)->get_minimum_value();
        }
    case MaximumValue:
        {
            return m_data.at(row)->get_maximum_value();
        }
    case MaximumExtent:
        {
            return m_data.at(row)->get_maximum_extent();
        }
    default: break;
    }


    return result;
}

int ImagesListModel::rowCount(const QModelIndex& parent) const
{
    if (parent.isValid())
    {
        std::cout << "parent is valid" << std::endl;
        return 0;
    }
    return m_data.size();
}

Qt::ItemFlags ImagesListModel::flags(const QModelIndex& index) const
{
    Qt::ItemFlags result = Qt::ItemIsEditable | QAbstractItemModel::flags(index);
    return result;
}

void ImagesListModel::add_object(const QString& id, VolumeImageDataQml* data)
{
    beginInsertRows(QModelIndex(), m_data.size(), m_data.size());
    // Specify the firstand last row numbers for the span of rows you want to insert into an item in a model.

    m_id_index_map[id] = m_data.size();
    m_data.push_back(data);
    endInsertRows();
}

void ImagesListModel::remove_object(const QString& id)
{
    auto index = m_id_index_map.at(id);
    beginRemoveRows(QModelIndex(), index, index);
    // Specify the first and last row numbers for the span of rows you want to remove from an item in a model.
    delete m_data.takeAt(index);
    endRemoveRows();

    // rebuild id_index_map

    m_id_index_map.erase(id);

    // update id_index_map
    for (auto& [key, val] : m_id_index_map)
    {
        if (key == id)
            assert(true && "This cannot happen, object with key == id should have been erased.");
        if (val > index)
            val = val - 1;
    }
}

QList<VolumeImageDataQml*> ImagesListModel::get_objects() const
{
    return m_data;
}
}
