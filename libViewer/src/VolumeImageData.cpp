#include "libViewer/VolumeImageData.h"

namespace viewer
{
VolumeImageData::VolumeImageData(const QString& series_uid, std::vector<short> data, QVector3D origin, QMatrix3x3 orientation, QVector3D spacing,
    QVector3D dimension, int channels): m_series_uid(series_uid),m_data(data), m_origin(origin), m_orientation(orientation), m_spacing(spacing), m_dimension(dimension), m_channels(channels)
{
    //create_texture(m_dimension.x(), m_dimension.y(), m_dimension.z(), m_channels);
}

QVector3D VolumeImageData::get_origin() const
{
    return m_origin;
}

QMatrix3x3 VolumeImageData::get_orientation() const
{
    return m_orientation;
}

QVector3D VolumeImageData::get_spacing() const
{
    return m_spacing;
}

QVector3D VolumeImageData::get_dimension() const
{
    return m_dimension;
}

int VolumeImageData::get_channels() const
{
    return m_channels;
}

std::vector<short> VolumeImageData::getTextureData() const
{
    return m_data;
}

QString VolumeImageData::get_series_uid() const
{
    return m_series_uid;
}

void VolumeImageData::set_series_uid(const QString& series_uid)
{
    m_series_uid = series_uid;
}

std::vector<short> VolumeImageData::createTexture(int maxX, int maxY, int maxZ, int numberOfChannels) const
{
    std::vector<short> shortArray(maxX*maxY*maxZ*numberOfChannels);

    int counter = 0;
    int val = 0;
    for(int i=0;i<maxX;i++) {
        for(int j=0;j<maxY;j++) {
            for(int k=0;k<maxZ;k++) {
                if(val>=255) {
                    val = 0;
                }
                for(int c =0;c<numberOfChannels;c++) {
                    if(c==2) {
                        shortArray[counter] = (short)val;//'z';
                    } else {
                        shortArray[counter]=0;
                    }
                    counter++;
                }
                val++;

            }
        }
    }
    return shortArray;
}
}
