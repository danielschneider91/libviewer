#include "libViewer/Pose.h"

namespace viewer {
Pose::Pose() : m_position(QVector3D(0,0,0)), m_x_axis(QVector3D(1,0,0)), m_z_axis(QVector3D(0,0,1))
{
}

Pose::Pose(const QVector3D& position, const QVector3D& x_axis, const QVector3D& z_axis)
    : m_position(position), m_x_axis(x_axis.normalized()), m_z_axis(z_axis.normalized())
{
}

QVector3D Pose::get_position() const
{
    return m_position;
}

void Pose::set_position(const QVector3D& position)
{
    m_position = position;
}

QVector3D Pose::get_x_axis() const
{
    return m_x_axis;
}

void Pose::set_x_axis(const QVector3D& x_axis)
{
    m_x_axis = x_axis;
}

QVector3D Pose::get_z_axis() const
{
    return m_z_axis;
}

void Pose::set_z_axis(const QVector3D& z_axis)
{
    m_z_axis = z_axis;
}

QVector3D Pose::get_y_axis() const
{
    return QVector3D::crossProduct(get_z_axis(), get_x_axis());
}
}
