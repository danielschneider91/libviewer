#include "libViewer/ObjectVisibilityListViewItem.h"

namespace viewer {
ObjectVisibilityListViewItem::ObjectVisibilityListViewItem(): QObject(),m_name(""),m_id(""),m_visibility(new VisibilityClass(VisibilityClass::VISIBLE))
{
}

ObjectVisibilityListViewItem::ObjectVisibilityListViewItem(const QString& name, const QString& id,
	const VisibilityClass::Visibility& visibility) : QObject(), m_name(name), m_id(id), m_visibility(new VisibilityClass(VisibilityClass::VISIBLE))
{
}

QString ObjectVisibilityListViewItem::get_name() const
{
	return m_name;
}

void ObjectVisibilityListViewItem::set_name(const QString& name)
{
	if (m_name != name)
	{
		m_name = name;
		emit name_changed();
	}
}

QString ObjectVisibilityListViewItem::get_id() const
{
	return m_id;
}

void ObjectVisibilityListViewItem::set_id(const QString& id)
{
	if(m_id!=id)
	{
		m_id = id;
		emit id_changed();
	}
}

VisibilityClass* ObjectVisibilityListViewItem::get_visibility() const
{
	return m_visibility;
}

void ObjectVisibilityListViewItem::set_visibility(VisibilityClass::Visibility visibility)
{
	if (m_visibility->get_visibility() != visibility)
	{
		m_visibility->set_visibility(visibility);
		emit visibility_changed();
	}
}

void ObjectVisibilityListViewItem::set_visibility(VisibilityClass* visibility)
{
	if (m_visibility->get_visibility() != visibility->get_visibility())
	{
		m_visibility->set_visibility(visibility->get_visibility());
		emit visibility_changed();
	}
}
}
