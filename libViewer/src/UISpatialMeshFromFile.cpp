#include "libViewer/UISpatialMeshFromFile.h"

#include <iostream>

namespace viewer
{
UISpatialMeshFromFile::UISpatialMeshFromFile(QObject* parent): UISpatialObject(parent),
                                                               m_mesh_file_path_name(""), m_has_material(false)
                                                               , m_material(new Qt3DExtras::QMetalRoughMaterial)
                                                               , m_transparent_material(new Qt3DExtras::QDiffuseSpecularMaterial)
{
}

QString UISpatialMeshFromFile::get_mesh_file_path_name() const
{
	return m_mesh_file_path_name;
}

void UISpatialMeshFromFile::set_mesh_file_path_name(const QString& mesh_file_path_name)
{
	if (m_mesh_file_path_name != mesh_file_path_name)
	{
		m_mesh_file_path_name = mesh_file_path_name;
		emit mesh_file_path_changed();
	}
}

bool UISpatialMeshFromFile::get_has_material() const
{
	return m_has_material;
}

void UISpatialMeshFromFile::set_has_material(const bool has_material)
{
	if (m_has_material != has_material)
	{
		m_has_material = has_material;
		emit has_material_changed();
	}
}

Qt3DExtras::QMetalRoughMaterial* UISpatialMeshFromFile::get_material() const
{
	return m_material;
}

Qt3DExtras::QDiffuseSpecularMaterial* UISpatialMeshFromFile::get_transparent_material() const
{
	return m_transparent_material;
}

void UISpatialMeshFromFile::set_material(const QVariant& baseColor, const QVariant& metalness,
                                         const QVariant& roughness, const QVariant& normal,
                                         const QVariant& ambientOcclusion, float textureScale, const QColor& transparent_color)
{
	if (!baseColor.isNull())
	{
		m_material->setBaseColor(baseColor);
		if (QString(baseColor.typeName()).compare(QString("QColor")) == 0)
		{
			auto color = baseColor.value<QColor>();
			color.setAlpha(122);
			m_transparent_material->setDiffuse(QVariant::fromValue(color));
			m_transparent_material->setTextureScale(1);
			m_transparent_material->setAlphaBlendingEnabled(true);
		}
		else
		{
			m_transparent_material->setDiffuse(QVariant::fromValue(transparent_color));
			m_transparent_material->setAlphaBlendingEnabled(true);
		}
	}
	if (!metalness.isNull())
	{
		m_material->setMetalness(metalness);
	}
	if (!roughness.isNull())
	{
		m_material->setRoughness(roughness);
	}
	if (!normal.isNull())
	{
		m_material->setNormal(normal);
	}
	if (!ambientOcclusion.isNull())
	{
		m_material->setAmbientOcclusion(ambientOcclusion);
	}
	m_material->setTextureScale(textureScale);

	emit material_changed();
	emit transparent_material_changed();
}

bool UISpatialMeshFromFile::get_is_picking_enabled() const
{
	return m_picking_enabled;
}

void UISpatialMeshFromFile::set_is_picking_enabled(const bool picking_enabled)
{
	if(get_is_picking_enabled()!=picking_enabled)
	{
		m_picking_enabled = picking_enabled;
		emit picking_enabled_changed();
	}
}
}
