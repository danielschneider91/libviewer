#include "libViewer/PrimitiveSpatialObjectsListModel.h"

#include <iostream>

namespace viewer
{
PrimitiveSpatialObjectsListModel::PrimitiveSpatialObjectsListModel(QObject* parent): QAbstractListModel(parent)
{
}

PrimitiveSpatialObjectsListModel::~PrimitiveSpatialObjectsListModel()
{
}

QHash<int, QByteArray> PrimitiveSpatialObjectsListModel::roleNames() const {
    QHash<int, QByteArray> roles;
    roles[Pose] = "pose";
    roles[Transform] = "transform";
    roles[Material] = "material";
    roles[TransparentMaterial] = "transparentMaterial";
    roles[Mesh] = "mesh";
    roles[Enabled] = "enabled";
    roles[Visible] = "visible";
    roles[Name] = "name";
    roles[Id] = "id";
    roles[PickingEnabled] = "pickingEnabled";
    return roles;
}

QVariant PrimitiveSpatialObjectsListModel::data(const QModelIndex& index, int role) const
{
    QVariant result = QVariant();
    int row = index.row();
    int column = index.column();

    if (!index.isValid() || row >= rowCount())
    {
        return result;
    }

    switch (role)
    {
    case Pose:
    {
        return m_data.at(row)->get_pose();
    }
    case Transform:
    {
        return QVariant::fromValue(m_data.at(row)->get_transform());
    }
    case Material:
    {
        return QVariant::fromValue(m_data.at(row)->get_material());
    }
    case TransparentMaterial:
    {
        return QVariant::fromValue(m_data.at(row)->get_transparent_material());
    }
    case Mesh:
    {
        return QVariant::fromValue(m_data.at(row)->get_mesh());
    }
    case Enabled:
    {
        return QVariant::fromValue(m_data.at(row)->get_is_enabled());
    }
    case Visible:
    {
        return QVariant::fromValue(m_data.at(row)->get_is_visible());
    }
    case Name:
    {
        return QVariant::fromValue(m_data.at(row)->get_name());
    }
    case Id:
    {
        return QVariant::fromValue(m_data.at(row)->get_id());
    }
    case PickingEnabled:
    {
        return QVariant::fromValue(m_data.at(row)->get_is_picking_enabled());
    }

    default: break;
    }


    return result;
}

int PrimitiveSpatialObjectsListModel::rowCount(const QModelIndex& parent) const
{
    if(parent.isValid())
    {
        return 0;
    }
    return m_data.size();
}

Qt::ItemFlags PrimitiveSpatialObjectsListModel::flags(const QModelIndex& index) const
{
    Qt::ItemFlags result = Qt::ItemIsEditable | QAbstractItemModel::flags(index);
    return result;
}

void PrimitiveSpatialObjectsListModel::add_object(UIPrimitiveMesh* data)
{
    beginInsertRows(QModelIndex(), m_data.size(), m_data.size());
    m_id_index_map[data->get_id()] = m_data.size();
    m_data.push_back(data);
    endInsertRows();
}

void PrimitiveSpatialObjectsListModel::remove_object(const QString& id)
{
    auto index = m_id_index_map.at(id);
    std::cout << "Removing object with id " << id.toStdString() << " at index " << index << std::endl;
    beginRemoveRows(QModelIndex(), index, index); // Specify the first and last row numbers for the span of rows you want to remove from an item in a model.
    delete m_data.takeAt(index);
    endRemoveRows();

    // rebuild id_index_map
    m_id_index_map.clear();
    for (int i = 0; i < m_data.count(); ++i)
    {
        m_id_index_map[m_data[i]->get_id()] = i;
    }
}

QList<UIPrimitiveMesh*> PrimitiveSpatialObjectsListModel::get_objects() const
{
    return m_data;
}

void PrimitiveSpatialObjectsListModel::set_pose(const std::string& id, const viewer::Pose& pose)
{
    auto index = m_id_index_map.at(QString::fromStdString(id));
    m_data[index]->set_pose((pose.get_position()), (pose.get_x_axis()), (pose.get_z_axis()));
    QModelIndex top = createIndex(index, 0);
    emit dataChanged(top, top);
}

void PrimitiveSpatialObjectsListModel::set_material(const std::string& id,
    const viewer::MaterialSettings& material_settings)
{
    auto index = m_id_index_map.at(QString::fromStdString(id));
    m_data[index]->set_material(material_settings.get_metal_rough_material_properties().get_base_color(),
        material_settings.get_metal_rough_material_properties().get_metalness(),
        material_settings.get_metal_rough_material_properties().get_roughness(),
        material_settings.get_metal_rough_material_properties().get_normal(),
        material_settings.get_metal_rough_material_properties().get_ambient_occlusion(),
        material_settings.get_metal_rough_material_properties().get_texture_scale(),
        material_settings.get_transparent_color());
    QModelIndex top = createIndex(index, 0);
    emit dataChanged(top, top);

}

void PrimitiveSpatialObjectsListModel::set_picking_enabled(const std::string& id, const bool picking_enabled)
{
    auto index = m_id_index_map.at(QString::fromStdString(id));
    m_data[index]->set_is_picking_enabled(picking_enabled);
    QModelIndex top = createIndex(index, 0);
    emit dataChanged(top, top);
}

MaterialSettings PrimitiveSpatialObjectsListModel::get_material(const std::string& id) const
{
    auto index = m_id_index_map.at(QString::fromStdString(id));
    return m_data[index]->get_material();
}
}
