#include "libViewer/QMLCrosshairsObjectParameters.h"


namespace viewer
{
QMLCrosshairsObjectParameters::QMLCrosshairsObjectParameters(const float& size)
    : m_size(size)
{
}

QMLCrosshairsObjectParameters::QMLCrosshairsObjectParameters(const QMLCrosshairsObjectParameters& other)
{
    m_size= other.m_size;
}

float QMLCrosshairsObjectParameters::get_size() const
{
    return m_size;
}
}
