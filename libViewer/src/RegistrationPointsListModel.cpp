#include "libViewer/RegistrationPointsListModel.h"

namespace viewer
{
RegistrationPointsListModel::RegistrationPointsListModel(QObject* parent): QAbstractListModel(parent)
{
}

RegistrationPointsListModel::~RegistrationPointsListModel()
{
}

QHash<int, QByteArray> RegistrationPointsListModel::roleNames() const {
    QHash<int, QByteArray> roles;
    roles[FilePath] = "filePath";
    roles[HasMaterial] = "hasMaterial";
    roles[Pose] = "pose";
    roles[Transform] = "transform";
    roles[Material] = "material";
    roles[Enabled] = "enabled";
    roles[Visible] = "visible";
    roles[Name] = "name";
    return roles;
}

QVariant RegistrationPointsListModel::data(const QModelIndex& index, int role) const
{
    QVariant result = QVariant();
    int row = index.row();
    int column = index.column();

    if (!index.isValid() || row >= rowCount())
    {
        return result;
    }

    switch (role)
    {
    case FilePath:
    {
        return m_data.at(row)->get_mesh_file_path_name();
    }
    case HasMaterial:
    {
        return m_data.at(row)->get_has_material();
    }
    case Pose:
    {
        return m_data.at(row)->get_pose();
    }
    case Transform:
    {
        return QVariant::fromValue(m_data.at(row)->get_transform());
    }
    case Material:
    {
        return QVariant::fromValue(m_data.at(row)->get_material());
    }
    case Enabled:
    {
        return QVariant::fromValue(m_data.at(row)->get_is_enabled());
    }
    case Visible:
    {
        return QVariant::fromValue(m_data.at(row)->get_is_visible());
    }
    case Name:
    {
        return QVariant::fromValue(m_data.at(row)->get_name());
    }

    default: break;
    }


    return result;
}

int RegistrationPointsListModel::rowCount(const QModelIndex& parent) const
{
    if(parent.isValid())
    {
        return 0;
    }
    return m_data.size();
}

Qt::ItemFlags RegistrationPointsListModel::flags(const QModelIndex& index) const
{
    Qt::ItemFlags result = Qt::ItemIsEditable | QAbstractItemModel::flags(index);
    return result;
}

void RegistrationPointsListModel::add_object(UISpatialMeshFromFile* data)
{
    beginInsertRows(QModelIndex(), m_data.size(), m_data.size());
    m_id_index_map[data->get_name()] = m_data.size();
    m_data.push_back(data);
    endInsertRows();
}

QList<UISpatialMeshFromFile*> RegistrationPointsListModel::get_objects() const
{
    return m_data;
}

void RegistrationPointsListModel::set_pose(const std::string& id, const viewer::Pose& pose)
{
    auto index = m_id_index_map[QString::fromStdString(id)];
    m_data[index]->set_pose((pose.get_position()), (pose.get_x_axis()), (pose.get_z_axis()));
    QModelIndex top = createIndex(index, 0);
    emit dataChanged(top, top);
}
}
