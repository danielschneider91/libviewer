import QtQuick
import QtQuick.Controls
import QtQuick.Scene3D 2.4
import Qt3D.Core 2.4
import Qt3D.Render 2.4
import Qt3D.Render 2.15
import Qt3D.Input 2.4
import Qt3D.Extras 2.4

import "CoordinateSystem"
import "Line"

import libViewer

Entity {
    id:root

    readonly property alias camm: cam;
    property var viewerLayers:[]
    property var renderLayers:[]

    property bool activeViewport: false
    property bool threeDViewerCam: false

    property alias cameraProjectionType: cam.projectionType
    property alias cameraPosition: cam.position
    property alias cameraViewCenter: cam.viewCenter
    property alias cameraUpVector: cam.upVector
    property alias cameraLeft: cam.left
    property alias cameraRight: cam.right
    property alias cameraBottom: cam.bottom
    property alias cameraTop: cam.top
    property alias cameraFov: cam.fieldOfView
    property alias cameraAspectRatio: cam.aspectRatio
    property alias nearPlane: cam.nearPlane
    property alias farPlane: cam.farPlane

    property alias t_world_camRoot: camRootTransform
    readonly property alias t_camRoot_cam: cam.transform
    readonly property matrix4x4 t_world_cam: camRootTransform.matrix.times(cam.transform.matrix)

    function lookAt(camPosition,lookAtPosition,upDirection) {
        camRootTransform.matrix = Qt.matrix4x4();
        cam.position = Qt.vector3d(camPosition.x,camPosition.y,camPosition.z);
        cam.upVector = Qt.vector3d(upDirection.x,upDirection.y,upDirection.z);
        cam.viewCenter = Qt.vector3d(lookAtPosition.x,lookAtPosition.y.lookAtPosition.z);
    }
    function viewEntity(entity) {
        cam.viewEntity(entity)
    }
    function viewAll() {
        cam.viewAll()
    }

    components: [ Transform { id: camRootTransform } ]

    CoordinateSystem {
        myEnabled: false;//index===0?true:false
        text: "cameraRoot"
        viewerLayers: root.viewerLayers
        renderLayers: root.renderLayers
        scale: 10
    }
    Camera {
        id: cam

        nearPlane: 0.01
        farPlane: 50000.0

        //CoordinateSystem {
        //    myEnabled: false//index===0?true:false
        //    text: "camera"
        //    viewerLayers: root.viewerLayers
        //    renderLayers: root.renderLayers
        //    scale: 10
        //}
    }
}




