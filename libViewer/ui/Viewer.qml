import QtQuick
import QtQuick.Controls
import QtQuick.Scene3D 2.4
import QtQuick.Scene3D 2.15
import Qt3D.Core 2.4
import Qt3D.Render 2.4
import Qt3D.Render 2.15
import Qt3D.Input 2.4
import Qt3D.Extras 2.4
import Qt3D.Extras 2.15

import "ExpandShrinkScreenLogic.js" as ExpandShrinkScreenLogic

import "MathHelper"
import "CoordinateSystem"
import "CameraController"
import "ClipPlanes"
import "VolumeVisualization"
import "Framegraph"

import libViewer

Rectangle {
    id: root
    anchors.fill: parent

    Viewports {
        id:viewportsItem
        viewerConfiguration: root.viewerConfiguration
    }
    MathHelper {id: mathHelper}

    property SpatialObjectsManager spatialObjectsManager
    property ImagesManager imagesManager
    property ViewportsManager viewportsManager
    property ViewerConfiguration viewerConfiguration
    readonly property Viewports viewportsItem: viewportsItem
    readonly property real windowWidth: sceneContent.windowWidth
    readonly property real windowLevel: sceneContent.windowLevel
    readonly property real volumeWindowWidth: sceneContent.volumeWindowWidth
    readonly property real volumeWindowLevel: sceneContent.volumeWindowLevel
    readonly property var cameraViewportWidths: camerasEntity.cameraViewportWidths

    Connections {
        target: viewerViewModel
        function onMove_to_next_slice_requested(viewport_idx,cranial_or_caudal) {
            sceneContent.moveToNextSlice(viewport_idx,cranial_or_caudal);
        }
        function onReset_viewport_requested(threeDOrSlice) {
            scene3dInstance.reset(threeDOrSlice)
        }
    }
    function setVolumeRenderingEnabled(enabled) {
        sceneContent.volumeRenderingEnabled = enabled;
    }
    function toggleWindowWidthLevelAdjustment(enabled) {
        scene3dInstance.adjustingWindowLevelWidth = enabled;
    }
    function toggleVolumeRenderingWindowWidthLevelAdjustment(enabled) {
        scene3dInstance.volumeRenderingLevelingEnabled=enabled;
    }
    function setVolumeRenderingType(type) {
        sceneContent.volRenderingType = type;
    }
    function setVolumeRenderingTFType(type) {
        sceneContent.volRenderingTransferFunctionType = type;
    }

    function zoom(viewerIndex,inOrOut) {
        camerasEntity.changeZoom(viewerIndex,inOrOut);
    }

    Connections {
        target: root.viewportsManager
        function onViewer_configuration_changed() {
            adjustCameraAspectRatios()
        }
    }
    Connections {
        target: root.viewerConfiguration
        function onExpanded_viewport_changed() {
            adjustCameraAspectRatios();
        }
    }

    Connections {
        target: imagesManager
        function onVolume_images_changed() {
            console.log("volume image loaded")
            sceneContent.updateVolumeImage();
            scene3dInstance.reset(1);
            scene3dInstance.reset(0);
        }
        function onImages_viewport_visibility_changed() {
            console.log("images viewport visibility changed")
            sceneContent.updateVolumeImagesViewportVisibility();
        }
    }

    Component.onCompleted: {
        viewportFrameGraph.setWorldSpaceTolerance();

        sceneContent.spatialObjectPicked.connect(function(id,worldIntersection,localIntersection) {
            viewerViewModel.spatialObjectPicked(id,worldIntersection,localIntersection) })
        sceneContent.spatialObjectHover.connect(function(id,worldIntersection,localIntersection) {
            viewerViewModel.spatialObjectHover(id,worldIntersection,localIntersection) })

        if(sceneContent.imagesManager.volumeImages.length>0/* && sceneContent.imagesManager.volumeImages[sceneContent.imagesManager.volumeImages.length-1]*/) {
            sceneContent.updateVolumeImage();
            scene3dInstance.reset(1);
            scene3dInstance.reset(0);
        }

        mouseHandler.windowLevelWidthChanged.connect(sceneContent.setWindowLevelWidth)
        mouseHandler.volumeRenderingWindowLevelWidthChanged.connect(sceneContent.setVolumeRenderingWindowLevelWidth)
        mouseHandler.moveToNextSliceRequested.connect(sceneContent.moveToNextSlice)
        mouseHandler.mouseIsInViewportChanged.connect(function() {})
        mouseHandler.moveSlicesToPositionRequested.connect(function(x,y){
            sliceDoubleClickingRayCaster.trigger(Qt.point(x,y))
        })
    }

    function adjustCameraAspectRatios() {
        for(var i=0;i<viewportsItem.viewportsRepeater.count;i++) {
            var viewportAspectRatio = viewportsItem.viewportsRepeater.itemAt(i).data.width*scene3dInstance.width/
                                        (viewportsItem.viewportsRepeater.itemAt(i).data.height*scene3dInstance.height);

            camerasEntity.adjustCameraAspectRatio(i,viewportAspectRatio,sceneContent.planeSize/2.0);
        }
    }



    Scene3D {
        id: scene3dInstance
        anchors.fill: parent
        aspects: ["input", "logic"]
        hoverEnabled: true

        property bool adjustingWindowLevelWidth:false;
        property bool volumeRenderingLevelingEnabled:false;

        onWidthChanged: {
            adjustCameraAspectRatios()
        }
        onHeightChanged: {
            adjustCameraAspectRatios()
        }

        function reset(sliceViewerOrThreeDViewer/*0: slice, 1: 3d*/) {
            mouseHandler.mouseIsInViewport = -1
            if(sliceViewerOrThreeDViewer===1) {
                resetThreeDViewerCamera()
            } else {
                sceneContent.resetSlicePlanes();
                resetSliceViewerCameras();
            }
        }

        function resetThreeDViewerCamera() {
            camerasEntity.camerasInstantiator.objectAt(root.viewerConfiguration.getThreeDViewportId()).cameraViewCenter= Qt.vector3d(0,0,0)
            camerasEntity.camerasInstantiator.objectAt(root.viewerConfiguration.getThreeDViewportId()).cameraUpVector= Qt.vector3d(0,0,1)
            camerasEntity.camerasInstantiator.objectAt(root.viewerConfiguration.getThreeDViewportId()).cameraPosition= Qt.vector3d(0,-400,0)
            var mat = Qt.matrix4x4();
            var halfDts = sceneContent.imagesManager.volumeImages.length==0?Qt.vector3d(0,0,0): sceneContent.imagesManager.volumeImages[sceneContent.imagesManager.volumeImages.length-1].get_dimension_times_spacing().times(0.5);
            var imageBoundingBoxorigin = sceneContent.imagesManager.volumeImages.length==0?Qt.vector3d(0,0,0):sceneContent.imagesManager.volumeImages[sceneContent.imagesManager.volumeImages.length-1].get_bounding_box_origin();
            var orientation = sceneContent.imagesManager.volumeImages.length==0?Qt.matrix4x4():sceneContent.imagesManager.volumeImages[sceneContent.imagesManager.volumeImages.length-1].get_orientation();
            mat.translate(imageBoundingBoxorigin.plus(orientation.times(Qt.vector4d(halfDts.x, halfDts.y, halfDts.z, 1)).toVector3d()));
            camerasEntity.camerasInstantiator.objectAt(root.viewerConfiguration.getThreeDViewportId()).t_world_camRoot.matrix = mat;
        }
        function resetSliceViewerCameras() {
            for(var j=0;j<camerasEntity.camerasInstantiator.count;j++) {
                if(j===root.viewerConfiguration.getThreeDViewportId()) {
                    continue;
                }

                mouseHandler.mouseIsInViewport=-1;

                var slicePose = Qt.matrix4x4();
                var halfDts = sceneContent.imagesManager.volumeImages.length==0?Qt.vector3d(0,0,0):sceneContent.imagesManager.volumeImages[sceneContent.imagesManager.volumeImages.length-1].get_dimension_times_spacing().times(0.5);
                var imageBoundingBoxorigin = sceneContent.imagesManager.volumeImages.length==0?Qt.vector3d(0,0,0):sceneContent.imagesManager.volumeImages[sceneContent.imagesManager.volumeImages.length-1].get_bounding_box_origin();
                var orientation = sceneContent.imagesManager.volumeImages.length==0?Qt.matrix4x4():sceneContent.imagesManager.volumeImages[sceneContent.imagesManager.volumeImages.length-1].get_orientation();
                var world_p_imageOrigin = imageBoundingBoxorigin.plus(orientation.times(Qt.vector4d(halfDts.x, halfDts.y, halfDts.z, 1)).toVector3d());
                slicePose.translate(world_p_imageOrigin)
                if(root.viewerConfiguration.viewportToSlicetypeMap[j]==='Sagittal') {
                    slicePose.rotate(-90,Qt.vector3d(0.0,1.0,0.0));
                }
                if(root.viewerConfiguration.viewportToSlicetypeMap[j]==='Coronal') {
                    slicePose.rotate(-90,Qt.vector3d(1.0,0.0,0.0));
                }

                sceneContent.setCameraPose(j,slicePose);

                camerasEntity.camerasInstantiator.objectAt(j).cameraViewCenter= Qt.vector3d(0,0,0)
                camerasEntity.camerasInstantiator.objectAt(j).cameraUpVector= Qt.vector3d(0,-1,0)
                camerasEntity.camerasInstantiator.objectAt(j).cameraPosition= Qt.vector3d(0,0,-camerasEntity.planeToCamDistance)
            }
            adjustCameraAspectRatios();
        }

        Entity {
            id: rootEntity

            ViewerLayers {
                id: viewerLayersEntity
                viewerConfiguration: root.viewerConfiguration
            }
            RenderLayers {
                id: renderLayersEntity
            }
            SliceLayers {
                // Layer added to slice planes only. Used to capture double click and move slice positions there
                id: sliceLayersEntity
                numberOfSlices: root.viewerConfiguration.getNumberOfSlices();
            }
            Cameras {
                id: camerasEntity
                viewerConfiguration: root.viewerConfiguration
                cameraViewWidth: sceneContent.planeSize/2.0
                scene3DAspectRatio:scene3dInstance.width/scene3dInstance.height
                mouseIsInViewport: mouseHandler.mouseIsInViewport
                viewerLayersEntity: viewerLayersEntity
                renderLayersEntity: renderLayersEntity
            }
            CameraControllers {
                isCrosshairPicking: sceneContent.isCrosshairPicking
                adjustingWindowLevelWidth: scene3dInstance.adjustingWindowLevelWidth
                volumeRenderingLevelingEnabled: scene3dInstance.volumeRenderingLevelingEnabled
                mouseIsInViewport: mouseHandler.mouseIsInViewport
                camerasInstantiator: camerasEntity.camerasInstantiator
                viewerConfiguration: root.viewerConfiguration
                viewportsRepeater: viewportsItem.viewportsRepeater
                viewerHeight: root.height
                viewerWidth: root.width
            }
            Lights {
                id: lightsEntity
                numberOfViewports: root.viewerConfiguration.viewportsLayout.length
                camerasInstantiator: camerasEntity.camerasInstantiator
                viewerLayers: viewerLayersEntity.viewerLayers
                renderLayers: renderLayersEntity.renderLayers
            }
            MyMouseHandler {
                id:mouseHandler
                camerasEntity: camerasEntity
                viewerHeight: root.height
                viewerWidth: root.width
                volumeRenderingLevelingEnabled: scene3dInstance.volumeRenderingLevelingEnabled
                adjustingWindowLevelWidth: scene3dInstance.adjustingWindowLevelWidth
                viewerConfiguration: root.viewerConfiguration
                viewportsRepeater: viewportsItem.viewportsRepeater
            }

            CoordinateSystem {
                viewerLayers: viewerLayersEntity.viewerLayers ? viewerLayersEntity.viewerLayers : null
                renderLayers: renderLayersEntity.opaqueLayer ? renderLayersEntity.opaqueLayer : null
                myEnabled: false
                scale: 5
                text: "root"
            }

            SceneContent {
                id: sceneContent
                viewerConfiguration: root.viewerConfiguration
                viewerLayersEntity: viewerLayersEntity
                renderLayersEntity: renderLayersEntity
                sliceLayersEntity: sliceLayersEntity
                spatialObjectsManager: root.spatialObjectsManager
                imagesManager: root.imagesManager
                viewportsManager: root.viewportsManager
                crosshairEnabled:(!scene3dInstance.adjustingWindowLevelWidth) && (!scene3dInstance.volumeRenderingLevelingEnabled)// disable crosshairs when adjusting window level or width
                trackedModelPoseIndicatorsEnabled: {
                    if(!viewerViewModel || viewerViewModel.trackedModelPoseIndicatorVisibility===false) {return false}
                    if(root.viewportsManager.sliceViewerBehaviour.behaviour===ViewerBehaviour.MOVING_CROSSHAIR ||
                       root.viewportsManager.sliceViewerBehaviour.behaviour===ViewerBehaviour.CENTERED_CROSSHAIR) {
                        return true;
                    } else {
                        return false;
                    }
                }
                mouseIsInViewport:mouseHandler.mouseIsInViewport
                cameraXYDimensions: {
                    var myArray = [];
                    for(var i=0;i<camerasEntity.camerasInstantiator.count;i++) {
                        if(i===root.viewerConfiguration.getThreeDViewportId()) {
                        } else {
                            var left = camerasEntity.camerasInstantiator.objectAt(i).camm.left
                            var right = camerasEntity.camerasInstantiator.objectAt(i).camm.right
                            var bottom = camerasEntity.camerasInstantiator.objectAt(i).camm.bottom
                            var top = camerasEntity.camerasInstantiator.objectAt(i).camm.top

                            var width  = Math.abs(right)+Math.abs(left)
                            var height = Math.abs(top)+Math.abs(bottom)
                            myArray.push(Qt.size(width,height));
                        }

                    }
                    return myArray;
                }
                visibilityListModel: viewerViewModel ? viewerViewModel.visibilityListModel : null
                enabledListModel: viewerViewModel ? viewerViewModel.enabledListModel : null
                directionalLight: {
                    if(lightsEntity.lightsInstantiator.count!==0) {
                        return lightsEntity.lightsInstantiator.objectAt(root.viewerConfiguration.getThreeDViewportId()).directionalLight
                    }
                }
                lightPosition: {
                    if(lightsEntity.lightsInstantiator.count===0) {
                        return Qt.vector3d(0,1,0)
                    } else {
                        lightsEntity.lightsInstantiator.objectAt(root.viewerConfiguration.getThreeDViewportId()).lightPosition
                    }
                }
                camerasEntity: camerasEntity
                clippingPlanes: clippingPlanes
            }

            ClippingPlanes {
                id: clippingPlanes
                viewerConfiguration: root.viewerConfiguration
                sliceTransformations: sceneContent.sliceTransforms
            }

            Entity {
                FilterKeyCatalogue {
                    id:filterKeyCatalogue
                }
                TorusMesh {
                    id: torusMesh
                    radius: 50
                    minorRadius: 15
                }
                SphereMesh {
                    id: mesh
                    radius: 50
                }
                Mesh {
                    id: trefoilMesh
                    source: "qrc:/libViewer/ui/resources/objects/trefoil.obj"
                }
                Transform {
                    id:torusTrafo
                    translation: Qt.vector3d(0,0,0)
                    scale:1
                }
                Material {
                    id: torusMaterial
                    effect: MetalRoughClippingEffect {
                        baseColor:Qt.rgba(0,1,0,1)
                        overlayColor: Qt.rgba(0,0,1,1)
                        clipPlaneEquation: clippingPlanes.clipPlaneEntities.length > 0 ? clippingPlanes.clipPlaneEntities[0].equation : null
                        viewportVisibility: ViewportVisibility {
                            threeDViewportsVisibility:false
                            sliceViewportsVisibility:false
                            sliceViewportRenderingMode:SliceViewportRenderingModeClass.OVERLAY
                        }
                    }
                }
                components:
                {
                    var viewerLayersArray = []
                    viewerLayersArray = viewerLayersArray.concat(viewerLayersEntity.threeDViewerLayer);
                    viewerLayersArray = viewerLayersArray.concat(viewerLayersEntity.sliceViewerLayers);
                    var myArray=[mesh,/*trefoilMesh,*/torusTrafo,torusMaterial];
                    myArray = myArray.concat(viewerLayersArray);
                    myArray = myArray.concat(renderLayersEntity.opaqueLayer);
                    myArray.push(filterKeyCatalogue.threeDViewerOpaqueModelFilterKey)
                    myArray.push(filterKeyCatalogue.defaultFilterKey)
                    return myArray
                }
            }
            components: [
                ViewportFrameGraph {
                    id: viewportFrameGraph
                    viewerLayersEntity: viewerLayersEntity
                    renderLayersEntity: renderLayersEntity
                    camerasInstantiator: camerasEntity.camerasInstantiator
                    viewportsRepeater: viewportsItem.viewportsRepeater
                    viewerConfiguration: root.viewerConfiguration
                    clippingPlanes: clippingPlanes
                    spatialObjectsFromFileLayers: sceneContent.spatialObjectsFromFileLayers
                    primitiveSpatialObjectsLayers: sceneContent.primitveSpatialObjectsLayers
                },
                // Event Source will be set by the Qt3DQuickWindow
                InputSettings { id: inputSettings }
                ,ScreenRayCaster {
                    id: sliceDoubleClickingRayCaster

                    layers: {
                        var arr=[]
                        for(var i=0;i<sliceLayersEntity.sliceLayers.length;i++) {
                            arr.push(sliceLayersEntity.sliceLayers[i]);
                        }
                        return arr;
                    }
                    onHitsChanged:function(hits) {
                        //rootEntity.printHits("sliceDoubleClickingRayCaster Screen hits", hits)
                        if(hits.length>0) {
                            var worldPos = Qt.vector3d(hits[0].worldIntersection.x,hits[0].worldIntersection.y,hits[0].worldIntersection.z)
                            sceneContent.setPositionOfAllSlices(worldPos)
                        }
                    }
                }
            ]

            function printHits(desc, hits) {
                console.log(desc, hits.length)
                for (var i=0; i<hits.length; i++) {
                    console.log("objectName: "+hits[i].entity.objectName)
                    console.log("objectId: "+hits[i].entityId)
                    console.log("  " + hits[i].entity.objectName, hits[i].entity.id, hits[i].distance,
                                hits[i].worldIntersection.x, hits[i].worldIntersection.y, hits[i].worldIntersection.z)
                }
            }

        }
    }

}
