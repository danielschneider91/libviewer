import Qt3D.Core 2.4
import Qt3D.Render 2.4
import Qt3D.Render 2.15
import Qt3D.Extras 2.4

import libViewer

Effect {
    id: root

    property alias baseColor:baseColorParam.value
    property alias metalness:metalnessParam.value
    property alias roughness:roughnessParam.value
    //property alias ambientOcclusion:ambientOcclusionParam.value
    //property alias normal:normalParam.value
    property alias textureScale: textureScaleParam.value
    property alias overlayColor:overlayColorParam.value
    property alias clipPlaneEquation: clipPlaneEquationParam.value
    property var viewportVisibility:null

    readonly property FilterKeyCatalogue filterKeyCatalogue: FilterKeyCatalogue {}

    parameters: [
        Parameter { id:overlayColorParam; name: "overlayColor"; value: Qt.rgba(1,0,0,0.5) },
        Parameter { id:baseColorParam; name: "baseColor"; value: Qt.rgba(1,0,0,1) },
        Parameter { id:metalnessParam; name: "metalness"; value: 0.1 },
        Parameter { id:roughnessParam; name: "roughness"; value: 0.5 },
        //Parameter { id:ambientOcclusionParam; name: "ambientOcclusion"; value: 1.0 },// this param will probably not work
        Parameter { id:textureScaleParam; name: "texCoordScale"; value: 1.0 },
        //Parameter { id:normalParam; name: "normalMap"; value: 0.0},// this param will probably not work
        Parameter { id:clipPlaneEquationParam; name: "clipPlaneEquation"; value: Qt.vector4d(0.0, 0.0, 1.0, 1.0)}
    ]

    techniques: [
        Technique {
            filterKeys: [FilterKey { name: "renderingStyle"; value: "forward" }]
            graphicsApiFilter {
                api: GraphicsApiFilter.OpenGL
                profile: GraphicsApiFilter.CoreProfile
                majorVersion: 3
                minorVersion: 1
            }

            RenderPass {
                id: renderPassDefault
                filterKeys: {
                    var arr =[]
                    if(root.viewportVisibility==null) {return arr;}
                    if(root.viewportVisibility.threeDViewportsVisibility) {
                        arr.push(root.filterKeyCatalogue.threeDViewerOpaqueModelFilterKey)
                    }
                    if(root.viewportVisibility.sliceViewportsVisibility) {
                        if(root.viewportVisibility.sliceViewportRenderingMode==SliceViewportRenderingModeClass.MODEL ||
                           root.viewportVisibility.sliceViewportRenderingMode==SliceViewportRenderingModeClass.MODEL_DISTANCE_CLIPPED) {
                            arr.push(root.filterKeyCatalogue.sliceViewerOpaqueModelFilterKey)
                        }
                    }
                    return arr;
                }
                shaderProgram: ShaderProgram {
                    id: metalRoughShader
                    vertexShaderCode: loadSource("qrc:/shaders/gl3/default.vert")
                    ShaderProgramBuilder {
                        id: metalRoughMaterialShaderBuilder
                        shaderProgram: metalRoughShader
                        fragmentShaderGraph: "qrc:/shaders/graphs/metalrough.frag.json"
                        enabledLayers: ["baseColor","metalness","roughness","ambientOcclusion","normal"]
                    }
                }
                renderStates: [
                    DepthTest {
                        depthFunction: {
                            if(!root.viewportVisibility) {return DepthTest.Less}
                            else {
                                if(root.viewportVisibility.depthTesting) { return DepthTest.Less}
                                else {return DepthTest.Always}
                            }
                        }
                    }
                ]
            }
            RenderPass {
                id:renderPassClipped
                filterKeys: [ FilterKey { name: "pass"; value: "clipped" } ]

                shaderProgram: ShaderProgram {
                    id: shaderClipped
                    vertexShaderCode: loadSource("qrc:/libViewer/ui/ClipPlanes/shader/phong-clip.vert")
                    fragmentShaderCode: loadSource("qrc:/libViewer/ui/ClipPlanes/shader/phong-clip.frag")
                }
            }
            RenderPass {
                id:renderPassStencilFill
                filterKeys: {
                    var arr =[]
                    if(root.viewportVisibility==null) {return arr;}
                    if(root.viewportVisibility.sliceViewportsVisibility) {
                        if(root.viewportVisibility.sliceViewportRenderingMode==SliceViewportRenderingModeClass.OVERLAY) {
                            arr.push(root.filterKeyCatalogue.sliceViewerOpaqueOverlayStencilFillFilterKey)
                        }
                    }
                    return arr;
                }
                shaderProgram: ShaderProgram {
                    id: shaderStencilFill
                    vertexShaderCode: loadSource("qrc:/libViewer/ui/ClipPlanes/shader/phong-clip.vert")
                    fragmentShaderCode: loadSource("qrc:/libViewer/ui/ClipPlanes/shader/passthrough.frag")
                }
            }
            renderPasses:[renderPassDefault,renderPassStencilFill,renderPassClipped]
        }
    ]
}

