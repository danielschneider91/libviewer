import Qt3D.Core 2.6
import Qt3D.Render 2.6

Entity {
    readonly property var sliceViewerOpaqueOverlayStencilFillFilterKey: FilterKey { name: "pass"; value: "slice_opaque_overlay_stencilFill" }
    readonly property var sliceViewerOpaqueOverlayCappingFilterKey: FilterKey { name: "pass"; value: "slice_opaque_overlay_capping" }
    readonly property var sliceViewerTransparentOverlayStencilFillFilterKey: FilterKey { name: "pass"; value: "slice_transparent_overlay_stencilFill" }
    readonly property var sliceViewerTransparentOverlayCappingFilterKey: FilterKey { name: "pass"; value: "slice_transparent_overlay_capping" }
    readonly property var sliceViewerOpaqueModelFilterKey: FilterKey { name: "pass"; value: "slice_opaque_model" }
    readonly property var sliceViewerTransparentModelFilterKey: FilterKey { name: "pass"; value: "slice_transparent_model" }
    readonly property var threeDViewerOpaqueModelFilterKey: FilterKey { name: "pass"; value: "threed_opaque_model" }
    readonly property var threeDViewerTransparentModelFilterKey: FilterKey { name: "pass"; value: "threed_transparent_model" }
    readonly property var defaultFilterKey: FilterKey { name: "renderingStyle"; value: "forward" }
}
