import QtQuick
import Qt3D.Core 2.4
import Qt3D.Render 2.4
import Qt3D.Input 2.4
import Qt3D.Extras 2.4

import "CameraController"

import libViewer

Entity {
    id: root
    property bool isCrosshairPicking: false // state of slice viewer interaction crosshairs, i.e., whether they are currently being picked by user to move or rotate them
    property bool adjustingWindowLevelWidth: false
    property bool volumeRenderingLevelingEnabled: false
    property int mouseIsInViewport: -1
    property ViewerConfiguration viewerConfiguration
    property NodeInstantiator camerasInstantiator

    property alias viewportsRepeater:threeDViewerCameraController.viewportsRepeater
    property alias viewerWidth:threeDViewerCameraController.viewerWidth
    property alias viewerHeight:threeDViewerCameraController.viewerHeight

    SliceViewerCameraController {
        myEnabled: ((!root.isCrosshairPicking) && (!root.adjustingWindowLevelWidth) && (!root.volumeRenderingLevelingEnabled));
        linearSpeed: 350.0
        lookSpeed: 250.0
        camera: {
            if(root.camerasInstantiator.count<=0 || root.mouseIsInViewport===root.viewerConfiguration.getThreeDViewportId()) {
                return null;
            } else {
                if(root.mouseIsInViewport===-1) {
                    return null
                } else {
                    return root.camerasInstantiator.objectAt(root.mouseIsInViewport).camm;
                }
            }
        }

    }
    //OrbitCameraController {
    //    enabled: (!root.isCrosshairPicking) && (!root.adjustingWindowLevelWidth) && (!root.volumeRenderingLevelingEnabled)
    //    linearSpeed: 350.0
    //    lookSpeed: 300.0
    //    camera: {
    //        if(root.camerasInstantiator.count<=0) {
    //            return null;
    //        } else {
    //            if(root.mouseIsInViewport===root.viewerConfiguration.getThreeDViewportId()) {
    //                return root.camerasInstantiator.objectAt(root.mouseIsInViewport).camm;
    //            } else {
    //                return null;
    //            }
    //        }
    //    }
    //}
    //FirstPersonCameraController {
    //    enabled: (!root.isCrosshairPicking) && (!root.adjustingWindowLevelWidth) && (!root.volumeRenderingLevelingEnabled)
    //    linearSpeed: 350.0
    //    lookSpeed: 300.0
    //    camera: {
    //        if(root.camerasInstantiator.count<=0) {
    //            return null;
    //        } else {
    //            if(root.mouseIsInViewport===root.viewerConfiguration.getThreeDViewportId()) {
    //                return root.camerasInstantiator.objectAt(root.mouseIsInViewport).camm;
    //            } else {
    //                return null;
    //            }
    //        }
    //    }
    //}

    ThreeDViewerCameraController {
        id:threeDViewerCameraController
        myEnabled: (!root.isCrosshairPicking) && (!root.adjustingWindowLevelWidth) && (!root.volumeRenderingLevelingEnabled)
        linearSpeed: 350.0
        lookSpeed: 300.0
        camera: {
            if(root.camerasInstantiator.count<=0) {
                return null;
            } else {
                if(root.mouseIsInViewport===root.viewerConfiguration.getThreeDViewportId()) {
                    return root.camerasInstantiator.objectAt(root.mouseIsInViewport).camm;
                } else {
                    return null;
                }
            }
        }
        viewerConfiguration: root.viewerConfiguration
    }

}
