import Qt3D.Core 2.6
import Qt3D.Render 2.15

import libViewer

GeometryRenderer {
    id: root

    property real width: 1.0
    property real height: 1.0
    property vector3d position: Qt.vector3d(0,0,0)

    // Update the buffers when width or height changes
    //onWidthChanged: vertexBuffer.data = planeVertices
    //onHeightChanged: vertexBuffer.data = planeVertices

    // Calculate vertices and indices
    property var planeVertices: (function() {
        var w = width/2.0;
        var h = height/2.0;

        var numberOfPoints = 4;
        var numberOfCoordinatesPerPoint = 3;
        var myArray = new Float32Array(numberOfPoints * numberOfCoordinatesPerPoint)

        // Vertex positions in (x, y, z) format
        // Vertex 0 Bottom-left
        myArray[0] = -w+position.x;
        myArray[1] = -h+position.y;
        myArray[2] = 0.0+position.z;

        // Vertex 1 Bottom-right
        myArray[3] = w+position.x;
        myArray[4] = -h+position.y;
        myArray[5] = 0.0+position.z;

        // Vertex 2 Top-right
        myArray[6] = w+position.x;
        myArray[7] = h+position.y;
        myArray[8] = 0.0+position.z;

        // Vertex 3 Top-left
        myArray[9] = -w+position.x;
        myArray[10]= h+position.y;
        myArray[11]= 0.0+position.z;

        return myArray;
    })()

    property var planeIndices: (function() {
        // Two triangles to form a rectangle (0, 1, 2) and (2, 3, 0)
        var myArray = new Uint16Array(6)
        myArray[0] = 0;
        myArray[1] = 1;
        myArray[2] = 2;
        myArray[3] = 2;
        myArray[4] = 3;
        myArray[5] = 0;

        return myArray;
    })()

    property var planeNormals: (function() {
        var numberOfPoints = 4;
        var numberOfCoordinatesPerPoint = 3;
        var myArray = new Float32Array(numberOfPoints * numberOfCoordinatesPerPoint)
        var normalDirection = 1.0;

        // Vertex positions in (x, y, z) format
        // Vertex 1 Bottom-left
        myArray[0] = 0.0;
        myArray[1] = 0.0;
        myArray[2] = 1.0*normalDirection;

        // Vertex 2 Bottom-right
        myArray[3] = 0.0;
        myArray[4] = 0.0;
        myArray[5] = 1.0*normalDirection;

        // Vertex 3 Top-right
        myArray[6] = 0.0;
        myArray[7] = 0.0;
        myArray[8] = 1.0*normalDirection;

        // Vertex 4 Top-left
        myArray[9] = 0.0;
        myArray[10]= 0.0;
        myArray[11]= 1.0*normalDirection;

        return myArray;
    })()

    property var planeTexCoords: (function() {
        var numberOfPoints = 4;
        var numberOfCoordinatesPerPoint = 2;
        var myArray = new Float32Array(numberOfPoints * numberOfCoordinatesPerPoint)

        // Tex coord in (x, y) format
        // Vertex 0 Bottom-left
        myArray[0] = 0.0;
        myArray[1] = 0.0;

        // Vertex 1 Bottom-right
        myArray[2] = 1.0;
        myArray[3] = 0.0;

        // Vertex 2 Top-right
        myArray[4] = 1.0;
        myArray[5] = 1.0;

        // Vertex 3 Top-left
        myArray[6] = 0.0;
        myArray[7]= 1.0;

        return myArray;
    })()

    geometry: Geometry {
        id: planeGeometry

        attributes: [
            // Position attribute
            Attribute {
                //name: "vertexPosition"
                name: {
                    console.log("defaultPositionAttributeName")
                    console.log(defaultPositionAttributeName)
                    return defaultPositionAttributeName
                }
                buffer: Buffer {
                    id: vertexBuffer
                    //type: Buffer.VertexBuffer
                    data: planeVertices
                }
                byteOffset: 0 // offset from start of buffer to first element
                byteStride: 3 * 4 // how many bytes between start of one to start of next element, 3 floats per vertex, 4 bytes per float
                count: 4 // number of vertices
                attributeType: Attribute.VertexAttribute
                vertexBaseType: Attribute.Float
                vertexSize: 3 // Specifies the number of components per generic vertex attribute
            },
            // Normals attribute
            Attribute {
                name: {
                    console.log("defaultNormalAttributeName")
                    console.log(defaultNormalAttributeName)
                    return defaultNormalAttributeName
                }
                buffer: Buffer {
                    id: normalBuffer
                    //type: Buffer.VertexBuffer
                    data: planeNormals
                }
                byteOffset: 0
                byteStride: 3 * 4
                count: 4
                attributeType: Attribute.VertexAttribute
                vertexBaseType: Attribute.Float
                vertexSize: 3 // Specifies the number of components per generic vertex attribute
            },
            // Index attribute
            Attribute {
                buffer: Buffer {
                    id: indexBuffer
                    //type: Buffer.IndexBuffer
                    data: planeIndices
                }
                attributeType: Attribute.IndexAttribute
                vertexBaseType: Attribute.UnsignedShort
                count: 6
                vertexSize: 1
                byteOffset: 0
                byteStride: 1 * 2 // 1 short per vertex, 2 bytes per short
            },
            // TexCoord attribute
            Attribute {
                name: {
                    console.log("defaultTextureCoordinateAttributeName")
                    console.log(defaultTextureCoordinateAttributeName)
                    return defaultTextureCoordinateAttributeName
                }
                buffer: Buffer {
                    id: texCoordBuffer
                    //type: Buffer.IndexBuffer
                    data: planeTexCoords
                }
                byteOffset: 0
                byteStride: 2 * 4
                count: 4
                attributeType: Attribute.VertexAttribute
                vertexBaseType: Attribute.Float
                vertexSize: 2 // Specifies the number of components per generic vertex attribute
            }
        ]
    }

    // Define the primitive type
    primitiveType: GeometryRenderer.Triangles
}
