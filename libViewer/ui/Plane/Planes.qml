import Qt3D.Core 2.4
import Qt3D.Render 2.4
import Qt3D.Extras 2.4
import QtQuick
import Qt3D.Input 2.4

import libViewer

GeometryRenderer {
    id:root
    property real width: 40
    property real height: 60
    property int numberOfPlanesMinusOne: 20
    property real zExtent:2
    property var points:{
        var arr = []
        for(var i=0;i<=root.numberOfPlanesMinusOne;i++) {
            var z = -root.zExtent/2+i*root.zExtent/root.numberOfPlanesMinusOne
            arr.push(Qt.vector3d(-1,-1,z))
            arr.push(Qt.vector3d(1,1,z))
            arr.push(Qt.vector3d(1,-1,z))
            arr.push(Qt.vector3d(-1,-1,z))
            arr.push(Qt.vector3d(-1,1,z))
            arr.push(Qt.vector3d(1,1,z))
        }
        return arr;
    }
    property vector3d position: Qt.vector3d(0,0,0)

    instanceCount: 1
    primitiveType: GeometryRenderer.Triangles

    geometry:
    Geometry {

        Attribute {
            attributeType: Attribute.VertexAttribute
            vertexBaseType: Attribute.Float
            vertexSize: 3
            byteOffset: 0
            byteStride: 3*4
            count:root.points.length
            name: "vertexPosition" // Name of attribute in the shader
            buffer: Buffer {
                id: vertexBuffer
                //type: Buffer.VertexBuffer
                data: {
                    var myArray = new Float32Array(root.points.length*3)

                    var counter = 0
                    for(var i=0;i<root.points.length;i++) {
                        myArray[counter]=root.points[i].x*root.width/2.0+root.position.x
                        counter = counter+1;
                        myArray[counter]=root.points[i].y*root.height/2.0+root.position.y
                        counter = counter+1;
                        myArray[counter]=root.points[i].z+root.position.z
                        counter = counter+1;
                    }
                    return myArray
                }
            }
        }
    }
}

