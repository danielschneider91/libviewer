import QtQuick
import QtQuick.Controls
import QtQuick.Scene3D 2.4
import Qt3D.Core 2.4
import Qt3D.Render 2.4
import Qt3D.Input 2.4
import Qt3D.Extras 2.4

import "VolumeVisualization"
import "MathHelper"

import libViewer

Entity {
    id:root
    property ImagesManager imagesManager
    property ViewerConfiguration viewerConfiguration
    property ViewerLayers viewerLayersEntity
    property RenderLayers renderLayersEntity
    property SliceLayers sliceLayersEntity
    property var visibilityListModel
    property real planeSize: 310
    property bool crosshairEnabled: true // property can be used to disable slice viewer crosshairs (e.g. when adjusting window level and width)
    property int mouseIsInViewport: -1
    property var cameraXYDimensions: [] // only contains elements for slice viewers
    readonly property alias windowLevel: volumeShadedSlicePlaneEffect.windowLevel
    readonly property alias windowWidth: volumeShadedSlicePlaneEffect.windowWidth
    property var sliceRootTransforms: { // sliceRootTransforms is equal to pose of axial slice
        var arr=[]
        for(var i=0;i<slicesInstantiator.count;i++) {
            arr.push(slicesInstantiator.objectAt(i).slicePlaneRootPose)
        }
        return arr;
    }
    property var sliceTransforms: { // sliceTransforms are equal to pose of slice in world cos
        var arr=[]
        for(var i=0;i<sliceTransformInstantiator.count;i++) {
            arr.push(sliceTransformInstantiator.objectAt(i).worldMatrix)
        }
        return arr;
    }
    property bool isCrosshairPicking: { // state of slice viewer interaction crosshairs, i.e., whether they are currently being picked by user to move or rotate them
        for(var i =0; i<slicesInstantiator.count;i++) {
            if(slicesInstantiator.objectAt(i).isCrosshairPicking) {// if one of them is being picked, the crosshairs are in use
                return true;
            }
        }
        return false
    }
    onSliceRootTransformsChanged: {
        if(root.sliceRootTransforms.length>0) {
            viewerViewModel.set_axial_slice_pose(root.sliceRootTransforms[0]) // the same for all slices
        }
    }

    signal slicePositionChanged(int viewportIdx,matrix4x4 t_world_plane)
    signal sliceOrientationChanged(int viewportIdx,matrix4x4 t_world_plane)
    signal spatialObjectPicked(string id,vector3d worldIntersection,vector3d localIntersection)
    signal spatialObjectHover(string id,vector3d worldIntersection,vector3d localIntersection)

    Component.onCompleted: {
        for(var i =0; i<slicesInstantiator.count;i++) {
            slicesInstantiator.objectAt(i).positionChanged.connect(function(dPosition) { slicePlanePositionChanged(dPosition,i) })
            slicesInstantiator.objectAt(i).spatialObjectPicked.connect(function(id,worldIntersection,localIntersection) {
                                                                            root.spatialObjectPicked(id,worldIntersection,localIntersection);
                                                                       })
            slicesInstantiator.objectAt(i).spatialObjectHover.connect(function(id,worldIntersection,localIntersection) {
                                                                            root.spatialObjectHover(id,worldIntersection,localIntersection);
                                                                       })

            if(root.viewerConfiguration.viewportToSlicetypeMap[root.viewerConfiguration.getViewportIndexFromSliceIndex(i)]==='Axial') {
                slicesInstantiator.objectAt(i).orientationChanged.connect(function(angleInDeg) {planeOrientationChanged(angleInDeg,Qt.vector3d(0.0,0.0,1.0))})
            } else if(root.viewerConfiguration.viewportToSlicetypeMap[root.viewerConfiguration.getViewportIndexFromSliceIndex(i)]==='Coronal') {
                slicesInstantiator.objectAt(i).orientationChanged.connect(function(angleInDeg) {planeOrientationChanged(angleInDeg,Qt.vector3d(0.0,1.0,0.0))})
            } else if(root.viewerConfiguration.viewportToSlicetypeMap[root.viewerConfiguration.getViewportIndexFromSliceIndex(i)]==='Sagittal') {
                slicesInstantiator.objectAt(i).orientationChanged.connect(function(angleInDeg) {planeOrientationChanged(-angleInDeg,Qt.vector3d(1.0,0.0,0.0))})
            } else {
                console.assert(false,"This cannot happen");
            }
        }
    }

    function updatePlaneSize() {
        var maxExtent = 0.0;
        var origins = [];
        for(var i=0;i<root.imagesManager.volumeImages.length;i++) {
            var maxExt = root.imagesManager.volumeImages[i].get_maximum_extent();
            if(maxExt>maxExtent) {
                maxExtent = maxExt;
            }
            origins.push(root.imagesManager.volumeImages[i].get_bounding_box_origin());
        }

        var maxDist = 0.0;
        for(var k=0;k<origins.length;k++) {
            for(var l=0;l<origins.length;l++) {
                var a = origins[k];
                var b = origins[l];
                var c = b.minus(a);
                var dist = c.length();
                if(dist>maxDist) {
                    maxDist = dist;
                }
            }
        }

        root.planeSize = 2.0*maxExtent+maxDist;
        console.log("plane size: "+root.planeSize)
    }
    function setWindowLevelWidth(level,width) {
        volumeShadedSlicePlaneEffect.setWindowLevelWidth(level,width);
    }
    function updateSliceRenderingEffect() {
        var numberOfImages = root.imagesManager.volumeImages.length;
        if(numberOfImages>volumeShadedSlicePlaneEffect.numberOfSupportedTextures) {
            console.log("Number of images larger than number of supported images ("+volumeShadedSlicePlaneEffect.numberOfSupportedTextures+"). Considering only first "+volumeShadedSlicePlaneEffect.numberOfSupportedTextures+" images.")
            numberOfImages = volumeShadedSlicePlaneEffect.numberOfSupportedTextures;
        }

        for(var i=0;i<numberOfImages;i++) {
            console.log("image number: "+i)
            volumeShadedSlicePlaneEffect.threeDTextures[i].textureImages[0].dimension = root.imagesManager.volumeImages[i].get_dimension();
            volumeShadedSlicePlaneEffect.threeDTextures[i].textureImages[0].spacing = root.imagesManager.volumeImages[i].get_spacing();
            volumeShadedSlicePlaneEffect.threeDTextures[i].textureImages[0].dimensionTimesSpacing = root.imagesManager.volumeImages[i].get_dimension_times_spacing();
            volumeShadedSlicePlaneEffect.threeDTextures[i].textureImages[0].origin = root.imagesManager.volumeImages[i].get_origin();
            volumeShadedSlicePlaneEffect.threeDTextures[i].textureImages[0].orientation = root.imagesManager.volumeImages[i].get_orientation();
            volumeShadedSlicePlaneEffect.threeDTextures[i].textureImages[0].channels = root.imagesManager.volumeImages[i].get_channels();
            volumeShadedSlicePlaneEffect.threeDTextures[i].textureImages[0].textureData = root.imagesManager.volumeImages[i].get_texture_data();

            console.log(volumeShadedSlicePlaneEffect.threeDTextures[i].textureImages[0].dimension)
            console.log(volumeShadedSlicePlaneEffect.threeDTextures[i].textureImages[0].spacing)
            console.log(volumeShadedSlicePlaneEffect.threeDTextures[i].textureImages[0].dimensionTimesSpacing)
            console.log(volumeShadedSlicePlaneEffect.threeDTextures[i].textureImages[0].origin)
            console.log(volumeShadedSlicePlaneEffect.threeDTextures[i].textureImages[0].orientation)
            console.log(volumeShadedSlicePlaneEffect.threeDTextures[i].textureImages[0].channels)
            console.log(volumeShadedSlicePlaneEffect.threeDTextures[i].textureImages[0].textureData)


            volumeShadedSlicePlaneEffect.textureVisibility[i] = root.imagesManager.imageViewportVisibility[i].sliceViewportsVisibility;
            volumeShadedSlicePlaneEffect.activeTexture[i] = true;
            volumeShadedSlicePlaneEffect.spacing[i] = root.imagesManager.volumeImages[i].get_spacing();
            volumeShadedSlicePlaneEffect.dimension[i] = root.imagesManager.volumeImages[i].get_dimension();
            volumeShadedSlicePlaneEffect.dimensionTimesSpacing[i] = root.imagesManager.volumeImages[i].get_dimension_times_spacing();
            volumeShadedSlicePlaneEffect.origin[i] = root.imagesManager.volumeImages[i].get_origin();
            volumeShadedSlicePlaneEffect.orientation[i] = root.imagesManager.volumeImages[i].get_orientation();


            console.log(volumeShadedSlicePlaneEffect.textureVisibility[i])
            console.log(volumeShadedSlicePlaneEffect.activeTexture[i])
            console.log(volumeShadedSlicePlaneEffect.spacing[i])
            console.log(volumeShadedSlicePlaneEffect.dimension[i])
            console.log(volumeShadedSlicePlaneEffect.dimensionTimesSpacing[i])
            console.log(volumeShadedSlicePlaneEffect.origin[i])
            console.log(volumeShadedSlicePlaneEffect.orientation[i])


        }
        for(var j=root.imagesManager.volumeImages.length;j<volumeShadedSlicePlaneEffect.numberOfSupportedTextures;j++) {

            volumeShadedSlicePlaneEffect.activeTexture[j] = false;
        }
        for(var k=0;k<volumeShadedSlicePlaneEffect.numberOfSupportedTextures;k++) {
            console.log("active texture for id "+k+": "+volumeShadedSlicePlaneEffect.activeTexture[k])
        }

        volumeShadedSlicePlaneEffect.threeDTexturesChanged()
        volumeShadedSlicePlaneEffect.textureVisibilityChanged()
        volumeShadedSlicePlaneEffect.activeTextureChanged()
        volumeShadedSlicePlaneEffect.originChanged()
        volumeShadedSlicePlaneEffect.orientationChanged()
        volumeShadedSlicePlaneEffect.spacingChanged()
        volumeShadedSlicePlaneEffect.dimensionChanged()
        volumeShadedSlicePlaneEffect.dimensionTimesSpacingChanged()
    }
    function updateVolumeImagesViewportVisibility() {
        var numberOfImages = root.imagesManager.volumeImages.length;
        if(numberOfImages>volumeShadedSlicePlaneEffect.numberOfSupportedTextures) {
            console.log("Number of images larger than number of supported images ("+volumeShadedSlicePlaneEffect.numberOfSupportedTextures+"). Considering only first "+volumeShadedSlicePlaneEffect.numberOfSupportedTextures+" images.")
            numberOfImages = volumeShadedSlicePlaneEffect.numberOfSupportedTextures;
        }

        for(var i=0;i<numberOfImages;i++) {
            volumeShadedSlicePlaneEffect.textureVisibility[i] = root.imagesManager.imageViewportVisibility[i].sliceViewportsVisibility;
            volumeShadedSlicePlaneEffect.textureVisibilityChanged()
        }
    }

    function updateVolumeBoundedLineRenderingEffect() {
        for(var i=0; i<slicesInstantiator.count;i++) {
            if(root.imagesManager.volumeImages.length==0) {
               slicesInstantiator.objectAt(i).updateVolumeBoundedLineRenderingEffect(Qt.vector3d(0,0,0),Qt.vector3d(0,0,0),Qt.matrix4x4())
            } else {
                slicesInstantiator.objectAt(i).updateVolumeBoundedLineRenderingEffect(
                    root.imagesManager.volumeImages[imagesManager.volumeImages.length-1].get_dimension_times_spacing(),root.imagesManager.volumeImages[imagesManager.volumeImages.length-1].get_bounding_box_origin(),root.imagesManager.volumeImages[imagesManager.volumeImages.length-1].get_orientation());
            }

        }
    }
    function moveToNextSlice(viewerIndex,cranialOrCaudal/*-1,1*/) {
        var minimumSpacingValue = imagesManager.volumeImages.length==0?1:mathHelper.getMinOfVector3d(root.imagesManager.volumeImages[imagesManager.volumeImages.length-1].get_spacing());
        var pose = Qt.matrix4x4();
        if(sliceTransformInstantiator.count>0 && root.viewerConfiguration.getSliceIndexFromViewportIndex(viewerIndex)<sliceTransformInstantiator.count) {
            pose = sliceTransformInstantiator.objectAt(root.viewerConfiguration.getSliceIndexFromViewportIndex(viewerIndex)).worldMatrix
        }

        var translation = Qt.matrix4x4(pose.m11,pose.m12,pose.m13,0,
                                        pose.m21,pose.m22,pose.m23,0,
                                        pose.m31,pose.m32,pose.m33,0,
                                        0,0,0,1).times(Qt.vector3d(0,0,minimumSpacingValue*cranialOrCaudal))

        slicePlanePositionChanged(translation)
    }
    function slicePlanePositionChanged(dPosition,index) {
        if(slicesInstantiator.count>0) {
            /* Take position of any slice (they must be all the same), add delta, and set as new position for all slices. */
            setPositionOfAllPlanes(slicesInstantiator.objectAt(0).slicePlaneRootTranslation.plus(dPosition));

            for(var i=0; i<slicesInstantiator.count;i++) {
                var viewportIdx = root.viewerConfiguration.getViewportIndexFromSliceIndex(i);
                root.slicePositionChanged(viewportIdx,sliceTransformInstantiator.objectAt(i).worldMatrix)
            }
        }
    }
    function setPositionOfAllPlanes(position) {
        var dts = root.imagesManager.volumeImages.length==0?Qt.vector3d(0,0,0):root.imagesManager.volumeImages[imagesManager.volumeImages.length-1].get_dimension_times_spacing();
        var imageBoundingBoxOrigin = root.imagesManager.volumeImages.length==0?Qt.vector3d(0,0,0):root.imagesManager.volumeImages[imagesManager.volumeImages.length-1].get_bounding_box_origin();
        var imagePose = root.imagesManager.volumeImages.length==0?Qt.matrix4x4():root.imagesManager.volumeImages[imagesManager.volumeImages.length-1].get_orientation();
        imagePose.m14 = imageBoundingBoxOrigin.x;
        imagePose.m24 = imageBoundingBoxOrigin.y;
        imagePose.m34 = imageBoundingBoxOrigin.z;
        if(isPositionWithinImageExtent(position,imagePose,dts)) {
            for(var i=0; i<slicesInstantiator.count;i++) {
                slicesInstantiator.objectAt(i).slicePlaneRootTranslation = position; // slicePlaneRootTranslation: position of axial slice (=position of sag/cor slice)
            }
        }
    }
    function setPositionOfAllPlanesAnimated(position) {
        var dts = root.imagesManager.volumeImages.length==0?Qt.vector3d(0,0,0):root.imagesManager.volumeImages[imagesManager.volumeImages.length-1].get_dimension_times_spacing();
        var imageBoundingBoxOrigin = root.imagesManager.volumeImages.length==0?Qt.vector3d(0,0,0):root.imagesManager.volumeImages[imagesManager.volumeImages.length-1].get_bounding_box_origin();
        var imagePose = root.imagesManager.volumeImages.length==0?Qt.matrix4x4():root.imagesManager.volumeImages[imagesManager.volumeImages.length-1].get_orientation();
        imagePose.m14 = imageBoundingBoxOrigin.x;
        imagePose.m24 = imageBoundingBoxOrigin.y;
        imagePose.m34 = imageBoundingBoxOrigin.z;
        if(isPositionWithinImageExtent(position,imagePose,dts)) {
            var objs = []
            for(var i=0; i<slicesInstantiator.count;i++) {
                objs.push(slicesInstantiator.objectAt(i))
            }
            slicePositionAnimation.to=position
            slicePositionAnimation.targets = objs
            slicePositionAnimation.property = "slicePlaneRootTranslation"
            slicePositionAnimation.start()
            //slicesInstantiator.objectAt(i).slicePlaneRootTranslation = position; // slicePlaneRootTranslation: position of axial slice (=position of sag/cor slice)
            //}
        }
    }
    Vector3dAnimation {
        id: slicePositionAnimation
        from: {
            var axialPose = sliceRootTransforms[0];// slicePlaneRootTranslation: position of axial slice (=position of sag/cor slice)
            var position = axialPose.column(3).toVector3d();
            return position
        }
        duration: 150
        easing.type:Easing.InOutCubic;
    }
    function isPositionWithinImageExtent(position,imagePose,dts) {
        var positionInNormalizedImageCos = imagePose.inverted().times(Qt.vector4d(position.x,position.y,position.z,1)).toVector3d().times(Qt.vector3d(1.0/dts.x,1.0/dts.y,1.0/dts.z));

        if(positionInNormalizedImageCos.x>=0 && positionInNormalizedImageCos.x<= 1 &&
            positionInNormalizedImageCos.y>=0 && positionInNormalizedImageCos.y<= 1 &&
            positionInNormalizedImageCos.z>=0 && positionInNormalizedImageCos.z<= 1) {
            return true;
        } else {
            return false;
        }
    }

    function resetSlicePlanes() {
        var halfDts = root.imagesManager.volumeImages.length==0?Qt.vector3d(0,0,0):root.imagesManager.volumeImages[imagesManager.volumeImages.length-1].get_dimension_times_spacing().times(0.5);
        var translation=root.imagesManager.volumeImages.length==0?Qt.vector3d(0,0,0):root.imagesManager.volumeImages[imagesManager.volumeImages.length-1].get_bounding_box_origin().plus(root.imagesManager.volumeImages[imagesManager.volumeImages.length-1].get_orientation().times(Qt.vector4d(halfDts.x, halfDts.y, halfDts.z, 1)).toVector3d());
        var transform = Qt.matrix4x4(1,0,0,translation.x,
                                     0,1,0,translation.y,
                                     0,0,1,translation.z,
                                     0,0,0,1);
        for(var i =0; i<slicesInstantiator.count;i++) {
            slicesInstantiator.objectAt(i).slicePlaneRootPose = transform; // slicePlaneRootPose: pose of axial slice
        }
    }
    function setOrientationOfAllPlanes(rotation) { /* requires rotation to be the orientation of the Axial plane! */
        for(var i=0; i<slicesInstantiator.count;i++) {
            slicesInstantiator.objectAt(i).slicePlaneRootPose = rotation; // slicePlaneRootPose: pose of axial slice
        }
    }

    function planeOrientationChanged(angleInDeg,axis) {
        var rotation = Qt.matrix4x4();
        rotation.rotate(angleInDeg,axis);

        // update plane rotation using rotation of axial plane (slicePlaneRootPose is pose of axial plane for all slices)
        setOrientationOfAllPlanes(slicesInstantiator.objectAt(0).slicePlaneRootPose.times(rotation));

        for(var i=0; i<slicesInstantiator.count;i++) {
            var viewportIdx = root.viewerConfiguration.getViewportIndexFromSliceIndex(i);
            root.sliceOrientationChanged(viewportIdx,sliceTransformInstantiator.objectAt(i).worldMatrix)
        }
    }

    function setPoseOfAllPlanes(pose) { /* requires pose of the Axial plane! */
        var dts = root.imagesManager.volumeImages.length==0?Qt.vector3d(0,0,0):root.imagesManager.volumeImages[imagesManager.volumeImages.length-1].get_dimension_times_spacing();
        var imageBoundingBoxOrigin = root.imagesManager.volumeImages.length==0?Qt.vector3d(0,0,0):root.imagesManager.volumeImages[imagesManager.volumeImages.length-1].get_bounding_box_origin();
        var imagePose = root.imagesManager.volumeImages.length==0?Qt.matrix4x4():root.imagesManager.volumeImages[imagesManager.volumeImages.length-1].get_orientation();
        imagePose.m14 = imageBoundingBoxOrigin.x;
        imagePose.m24 = imageBoundingBoxOrigin.y;
        imagePose.m34 = imageBoundingBoxOrigin.z;

        var rot =  Qt.matrix4x4(pose.m11,pose.m12,pose.m13,0,
                                pose.m21,pose.m22,pose.m23,0,
                                pose.m31,pose.m32,pose.m33,0,
                                0,0,0,1)
        var position = pose.column(3).toVector3d();
        if(isPositionWithinImageExtent(position,imagePose,dts)) {
            setOrientationOfAllPlanes(rot);
            setPositionOfAllPlanes(pose.column(3).toVector3d())
        }
    }

    MathHelper {id:mathHelper}

    VolumeShadedSlicePlaneEffect {
        id: volumeShadedSlicePlaneEffect
        highlightRange:root.imagesManager.imageIntensityRangeHighlightSettings.highlight
        highlightColor:root.imagesManager.imageIntensityRangeHighlightSettings.color
        lowerHighlightThreshold:root.imagesManager.imageIntensityRangeHighlightSettings.lowerThreshold
        upperHighlightThreshold: root.imagesManager.imageIntensityRangeHighlightSettings.upperThreshold
    }

    NodeInstantiator {
        id: sliceTransformInstantiator // pose of individual slices (sliceTransformInstantiator.objectAt(i).worldMatrix is pose of slice in world COS)
        model: { return root.viewerConfiguration.getNumberOfSlices() }
        delegate: Transform {
            rotationX: root.viewerConfiguration.viewportToSlicetypeMap[root.viewerConfiguration.getViewportIndexFromSliceIndex(index)]==='Coronal' ? -90 : 0
            rotationY: root.viewerConfiguration.viewportToSlicetypeMap[root.viewerConfiguration.getViewportIndexFromSliceIndex(index)]==='Sagittal'? -90 : 0
            translation: {
                var sliceId = root.viewerConfiguration.viewportToSliceIdMap[root.viewerConfiguration.getViewportIndexFromSliceIndex(index)];
                var sliceType = root.viewerConfiguration.viewportToSlicetypeMap[root.viewerConfiguration.getViewportIndexFromSliceIndex(index)];
                console.assert(sliceId!==root.viewerConfiguration.getThreeDViewportId(),"This cannot happen")

                if(sliceType==='Axial') {
                    return Qt.vector3d(0,0,root.viewerConfiguration.sliceIdToNormalShiftMap[sliceId])
                } else if(sliceType==='Coronal') {
                    return Qt.vector3d(0,root.viewerConfiguration.sliceIdToNormalShiftMap[sliceId],0)
                } else if(sliceType==='Sagittal') {
                    return Qt.vector3d(-root.viewerConfiguration.sliceIdToNormalShiftMap[sliceId],0,0)
                }
            }
        }
    }

    // Slice planes including crosshairs
    NodeInstantiator {
        id: slicesInstantiator

        model: root.viewerConfiguration.getNumberOfSlices()
        delegate: SlicePlaneWithTransform {
            sliceId: root.viewerConfiguration.viewportToSliceIdMap[root.viewerConfiguration.getViewportIndexFromSliceIndex(index)]
            sliceTransform: { // pose of slice, which is pose of rootSlice (axialSlice) and additional 90 deg rotations around X/Y axis
                if(!sliceTransformInstantiator || sliceTransformInstantiator.count===0){
                } else {
                    return sliceTransformInstantiator.objectAt(index)
                }
            }
            renderLayers: root.visibilityListModel[index] && root.visibilityListModel[index].visibility.visibility===VisibilityClass.TRANSPARENNT ? root.renderLayersEntity.transparentLayer : root.renderLayersEntity.opaqueLayer
            noDepthTestLayer: root.renderLayersEntity.noDepthTestLayer?root.renderLayersEntity.noDepthTestLayer:[]
            sliceViewerLayers: {
                if(root.viewerLayersEntity.sliceViewerLayers.length===0) { return []; } else {
                    var viewerLayerIndex = root.viewerConfiguration.getViewportIndexFromSliceIndex(index)
                    var arr = [root.viewerLayersEntity.sliceViewerLayers[viewerLayerIndex]]
                    return arr;
                }
            }
            threeDViewerLayer: {
                if(root.viewerLayersEntity.viewerLayers.length===0 || !root.visibilityListModel[index]) { return []; } else {
                    if(root.visibilityListModel[index].visibility.visibility!==VisibilityClass.HIDDEN) {
                        if(root.viewerLayersEntity.threeDViewerLayer) {
                            return root.viewerLayersEntity.threeDViewerLayer;
                        } else { return []; }

                    } else {return[];}
                }
            }
            sliceLayers: {
                if(root.sliceLayersEntity.sliceLayers.length===0) { return []; } else {
                    return [sliceLayersEntity.sliceLayers[index]];
                }
            }

            viewerConfiguration: root.viewerConfiguration
            visibilityVariable: root.visibilityListModel[index]
            slicePlaneRootTranslation:root.imagesManager.volumeImages[imagesManager.volumeImages.length-1] ? root.imagesManager.volumeImages[imagesManager.volumeImages.length-1].get_bounding_box_origin().plus(root.imagesManager.volumeImages[imagesManager.volumeImages.length-1].get_dimension_times_spacing().times(0.5)):Qt.vector3d(0,0,0)
            slicePlaneSettings.planeHeight: root.planeSize
            slicePlaneSettings.planeWidth: root.planeSize
            slicePlaneSettings.alpha: root.visibilityListModel[index] && root.visibilityListModel[index].visibility.visibility===VisibilityClass.TRANSPARENNT ? 0.5 : 1
            slicePlaneSettings.name: root.viewerConfiguration.viewportToSlicetypeMap[root.viewerConfiguration.getViewportIndexFromSliceIndex(index)]
            slicePlaneSettings.verticalLineColor: {
                var sliceType = root.viewerConfiguration.viewportToSlicetypeMap[root.viewerConfiguration.getViewportIndexFromSliceIndex(index)]
                if(sliceType==='Axial') {
                    return root.viewerConfiguration.verticalLineColors[0]
                } else if(sliceType==='Coronal') {
                    return root.viewerConfiguration.verticalLineColors[1]
                } else if(sliceType==='Sagittal') {
                    return root.viewerConfiguration.verticalLineColors[2]
                } else {
                    console.assert(false,'This cannot happen!')
                }
            }
            slicePlaneSettings.horizontalLineColor: {
                var sliceType = root.viewerConfiguration.viewportToSlicetypeMap[root.viewerConfiguration.getViewportIndexFromSliceIndex(index)]
                if(sliceType==='Axial') {
                    return root.viewerConfiguration.horizontalLineColors[0]
                } else if(sliceType==='Coronal') {
                    return root.viewerConfiguration.horizontalLineColors[1]
                } else if(sliceType==='Sagittal') {
                    return root.viewerConfiguration.horizontalLineColors[2]
                } else {
                    console.assert(false,'This cannot happen!')
                }
            }
            slicePlaneSettings.color:{
                var sliceType = root.viewerConfiguration.viewportToSlicetypeMap[root.viewerConfiguration.getViewportIndexFromSliceIndex(index)]
                if(sliceType==='Axial') {
                    return root.viewerConfiguration.sliceColors[0]
                } else if(sliceType==='Coronal') {
                    return root.viewerConfiguration.sliceColors[1]
                } else if(sliceType==='Sagittal') {
                    return root.viewerConfiguration.sliceColors[2]
                } else {
                    console.assert(false,'This cannot happen!')
                }
            }
            slicePlaneSettings.planePickingEnabled: root.imagesManager.slicePickingEnabled
            slicePlaneSettings.crosshairEnabled: root.crosshairEnabled // property disables slice viewer crosshairs
            slicePlaneSettings.crosshairInteractionDimensions: root.cameraXYDimensions[index]
            slicePlaneSettings.crosshairInteractionEnabled: { // property disables picking of slice viewer crosshairs
                var pickingInNormalMode = root.crosshairEnabled && (root.mouseIsInViewport===root.viewerConfiguration.getViewportIndexFromSliceIndex(index))
                return pickingInNormalMode;
            }

            volShadedSlicePlaneEffect: volumeShadedSlicePlaneEffect
        }
    }
}
