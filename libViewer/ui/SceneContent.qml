import QtQuick
import QtQuick.Controls
import QtQuick.Scene3D 2.4
import Qt3D.Core 2.4
import Qt3D.Render 2.4
import Qt3D.Input 2.4
import Qt3D.Extras 2.4

import libViewer

import "VolumeVisualization"
import "ClipPlanes"

Entity {
    id: root

    property SpatialObjectsManager spatialObjectsManager
    property ImagesManager imagesManager
    property ViewportsManager viewportsManager
    property ViewerConfiguration viewerConfiguration
    property ViewerLayers viewerLayersEntity
    property RenderLayers renderLayersEntity
    property SliceLayers sliceLayersEntity
    property var cameraXYDimensions: [] // only contains elements for slice viewers
    property var visibilityListModel
    property var enabledListModel
    property DirectionalLight directionalLight
    property vector3d lightPosition: Qt.vector3d(0,0,0)
    property real planeSize: 200
    property bool crosshairEnabled: true // property can be used to disable slice viewer crosshairs (e.g. when adjusting window level and width)
    property bool trackedModelPoseIndicatorsEnabled: true
    property alias isCrosshairPicking: slicePlanes.isCrosshairPicking // forward signal outside
    property int mouseIsInViewport: -1
    readonly property alias windowLevel: slicePlanes.windowLevel
    readonly property alias windowWidth: slicePlanes.windowWidth
    readonly property alias volumeWindowLevel: volumeRenderingInstantiator.windowLevel
    readonly property alias volumeWindowWidth: volumeRenderingInstantiator.windowWidth
    property alias volumeRenderingEnabled: volumeRenderingInstantiator.volumeRenderingEnabled
    property alias volRenderingType: volumeRenderingInstantiator.type
    property alias volRenderingTransferFunctionType: volumeRenderingInstantiator.volRenderingTransferFunctionType
    property alias sliceRootTransforms: slicePlanes.sliceRootTransforms
    property Cameras camerasEntity
    property var sliceTransforms: slicePlanes.sliceTransforms
    property ClippingPlanes clippingPlanes

    property alias spatialObjectsFromFileLayers:spatialObjectsFromFile.spatialObjectsFromFileLayers
    property alias primitveSpatialObjectsLayers:primitiveSpatialObjects.primitiveSpatialObjectsLayers

    signal spatialObjectPicked(string id,vector3d worldIntersection,vector3d localIntersection)
    signal spatialObjectHover(string id,vector3d worldIntersection,vector3d localIntersection)

    Component.onCompleted: {
        spatialObjectsFromFile.spatialObjectPicked.connect(root.spatialObjectPicked);
        spatialObjectsFromFile.spatialObjectHover.connect(root.spatialObjectHover);
        slicePlanes.spatialObjectPicked.connect(root.spatialObjectPicked);
        slicePlanes.spatialObjectHover.connect(root.spatialObjectHover);
    }

    function setCameraPose(viewportIdx,t_world_plane) {
        slicePlanesAndCameraInteractorGUI.setCameraPose(viewportIdx,t_world_plane)
    }

    function setVolumeRenderingWindowLevelWidth(level,width) {
        for(var i=0;i<volumeRenderingInstantiator.count;i++) {
            volumeRenderingInstantiator.objectAt(i).setVolumeRenderingWindowLevelWidth(level,width);
        }
    }
    function setWindowLevelWidth(level,width) {
        slicePlanes.setWindowLevelWidth(level,width)
    }

    function moveToNextSlice(viewerIndex,cranialOrCaudal/*-1:cranial,1:caudal*/) {
        slicePlanes.moveToNextSlice(viewerIndex,cranialOrCaudal)
    }
    function setPositionOfAllSlices(position) {
        slicePlanes.setPositionOfAllPlanesAnimated(position)
    }

    function updateVolumeImage() {
        slicePlanes.updatePlaneSize()
        root.planeSize = slicePlanes.planeSize
        slicePlanes.updateSliceRenderingEffect()
        slicePlanes.updateVolumeBoundedLineRenderingEffect()
        for(var i=0;i<volumeRenderingInstantiator.count;i++) {
            volumeRenderingInstantiator.objectAt(i).updateVolumeRenderingEffect();
        }

    }
    function updateVolumeImagesViewportVisibility() {
        slicePlanes.updateVolumeImagesViewportVisibility()
    }

    function resetSlicePlanes() {
        slicePlanes.resetSlicePlanes()
    }

    // anatomy, annotations, landmarks, instruments (tracked objects)
    PrimitiveSpatialObjects {
        id: primitiveSpatialObjects
        objects: root.spatialObjectsManager ? root.spatialObjectsManager.primitiveSpatialObjects:null
        objectsViewportVisibility: root.spatialObjectsManager ? root.spatialObjectsManager.primitiveSpatialObjectsViewportVisibility:null
        viewerLayersEntity: root.viewerLayersEntity
        renderLayersEntity: root.renderLayersEntity
        visibilityListModel: root.visibilityListModel
        enabledListModel: root.enabledListModel
        clippingPlanes: root.clippingPlanes
        planeSize: root.planeSize
    }

    SpatialObjectsFromFile {
        id: spatialObjectsFromFile
        objects: root.spatialObjectsManager ? root.spatialObjectsManager.spatialObjectsFromFile:null
        objectsViewportVisibility: root.spatialObjectsManager ? root.spatialObjectsManager.spatialObjectsFromFileViewportVisibility:null
        viewerLayersEntity: root.viewerLayersEntity
        renderLayersEntity: root.renderLayersEntity
        visibilityListModel: root.visibilityListModel
        enabledListModel: root.enabledListModel
        clippingPlanes: root.clippingPlanes
        planeSize: root.planeSize
    }

    QMLSpatialObjects {
        id: qmlSpatialObjects
        objects: root.spatialObjectsManager ? root.spatialObjectsManager.qmlSpatialObjects:null
        objectsViewportVisibility: root.spatialObjectsManager ? root.spatialObjectsManager.qmlSpatialObjectsViewportVisibility:null
        viewerLayersEntity: root.viewerLayersEntity
        renderLayersEntity: root.renderLayersEntity
        visibilityListModel: root.visibilityListModel
        enabledListModel: root.enabledListModel
        clippingPlanes: root.clippingPlanes
        animationManager: root.spatialObjectsManager ? root.spatialObjectsManager.animationManager:null
        spatialObjectsManager: root.spatialObjectsManager ? root.spatialObjectsManager:null
    }

    TrackedModelPoseIndicator {
        id:trackedModelPoseIndicators
        spatialObjects: root.spatialObjectsManager ? root.spatialObjectsManager.spatialObjectsFromFile:null
        objectsViewportVisibility: root.spatialObjectsManager ? root.spatialObjectsManager.spatialObjectsFromFileViewportVisibility:null
        viewerLayersEntity: root.viewerLayersEntity
        renderLayersEntity: root.renderLayersEntity
        visibilityListModel: root.visibilityListModel
        indicatorsEnabled: root.trackedModelPoseIndicatorsEnabled
    }

    SlicePlanes {
        id:slicePlanes
        imagesManager: root.imagesManager
        viewerConfiguration:root.viewerConfiguration
        viewerLayersEntity:root.viewerLayersEntity
        renderLayersEntity:root.renderLayersEntity
        sliceLayersEntity: root.sliceLayersEntity
        visibilityListModel:root.visibilityListModel
        planeSize: root.planeSize
        crosshairEnabled: root.crosshairEnabled
        mouseIsInViewport:root.mouseIsInViewport
        cameraXYDimensions: root.cameraXYDimensions // only contains elements for slice viewers
    }

    SlicePlanesAndCameraInteractorGUI {
        id: slicePlanesAndCameraInteractorGUI
        slicePlanesEntity: slicePlanes
        camerasEntity: root.camerasEntity
        mouseIsInViewport: root.mouseIsInViewport
        viewerConfiguration: root.viewerConfiguration
    }

    SlicePlanesAndCameraInteractorBackend {
        id: slicePlanesAndCameraInteractorBackend
        slicePlanesEntity: slicePlanes
        camerasEntity: root.camerasEntity
        viewerConfiguration: root.viewerConfiguration
        viewportsManager: root.viewportsManager
    }

    NodeInstantiator {
        id: volumeRenderingInstantiator
        property real windowLevel:0// for read only
        property real windowWidth:0 // for read only
        property bool volumeRenderingEnabled: false
        property real type: 0
        property real volRenderingTransferFunctionType: 0
        onObjectAdded: {
            for(var i=0;i<volumeRenderingInstantiator.count;i++) {
                volumeRenderingInstantiator.objectAt(i).updateVolumeRenderingEffect();
            }
            if(volumeRenderingInstantiator.count>0) {
                volumeRenderingInstantiator.objectAt(0).onWindowLevelChanged.connect(function() {volumeRenderingInstantiator.windowLevel=volumeRenderingInstantiator.objectAt(0).windowLevel;});
                volumeRenderingInstantiator.objectAt(0).onWindowWidthChanged.connect(function() {volumeRenderingInstantiator.windowWidth=volumeRenderingInstantiator.objectAt(0).windowWidth;});
            }
        }
        onObjectRemoved: {
            for(var i=0;i<volumeRenderingInstantiator.count;i++) {
                volumeRenderingInstantiator.objectAt(i).updateVolumeRenderingEffect();
            }
        }
        model: root.imagesManager.volumeImages.length
        delegate: VolumeRenderingRayTracing {
            id: volumeRendering
            myEnabled: volumeRenderingInstantiator.volumeRenderingEnabled
            viewerLayers: root.viewerConfiguration.getThreeDViewportId()===-1 ? [] : root.viewerLayersEntity.threeDViewerLayer
            renderLayers: root.renderLayersEntity.opaqueLayer
            directionalLight: root.directionalLight
            lightPosition: root.lightPosition
            volumeImageData: {
                if(index==-1||root.imagesManager.volumeImages.length==0) {
                    return null
                }
                else{
                    return root.imagesManager.volumeImages[index]
                }
            }
            type: volumeRenderingInstantiator.type
            volRenderingTransferFunctionType: volumeRenderingInstantiator.volRenderingTransferFunctionType
        }
    }



}
