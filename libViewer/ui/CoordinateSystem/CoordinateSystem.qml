import QtQuick
import Qt3D.Extras 2.4
import Qt3D.Extras 2.15
import Qt3D.Core 2.4
import Qt3D.Render 2.4
import Qt3D.Input 2.4

import "../Arrow"
import ".."

import libViewer

Entity {
    id: root
    property bool myEnabled: false
    property alias scale: rootScale.scale
    //todo revert once but with text2dentity is fixed: property alias text: labelMesh.text
    property string text

    property var viewerLayers:[]
    property var renderLayers:[]

    readonly property FilterKeyCatalogue filterKeyCatalogue:FilterKeyCatalogue{}

    Transform {
        id: rootScale
    }
    components: {
        if(!myEnabled) {
            return []
        } else {
            var myArray=[rootScale];
            if(root.viewerLayers) {
                for(var i=0;i<root.viewerLayers.length;i++) {
                    myArray.push(root.viewerLayers[i]);
                }
            }
            if(root.renderLayers) {
                for(var j=0;j<root.renderLayers.length;j++) {
                    myArray.push(root.renderLayers[j]);
                }
            }

            return myArray;
        }
    }


    Entity {
        id: origin
        components: root.myEnabled ? [originTransform, originMaterial, originMesh] : []

        Transform {
            id: originTransform
            translation: Qt.vector3d(5,0,10)
        }

        SphereMesh {
            id: originMesh
            radius: 0.5
        }

        Material {
            id: originMaterial
            effect: MetalRoughClippingEffect {
                baseColor: Qt.rgba(0,0,1,1)
                viewportVisibility: ViewportVisibility {
                    threeDViewportsVisibility:true
                    sliceViewportsVisibility:true
                    sliceViewportRenderingMode:SliceViewportRenderingModeClass.MODEL
                }
            }
        }
    }

    Entity {
        id: label
        components: root.myEnabled ? [labelTransform] : []

        Transform {
            id: labelTransform
            //rotationX: 90
        }

        // somehow this Text2DEntity yields a assertion in debug mode in the Qt source code
        //Text2DEntity {
        //    id:labelMesh
        //    color: "yellow"
        //    text: "COS"
        //    font.pointSize: 2
        //    height: font.pointSize * 1.2
        //    width: text.length * font.pointSize
        //}
    }


    Arrow {
        id: xAxis
        myEnabled:root.myEnabled
        color: "red"
        arrowTransform: Transform {
            rotationZ: -90
        }
    }
    Arrow {
        id: yAxis
        myEnabled:root.myEnabled
        color: Qt.rgba(0,1,0,1)
    }
    Arrow {
        id: zAxis
        myEnabled:root.myEnabled
        color: "blue"
        arrowTransform:Transform {
            rotationX: 90
        }
    }
}
