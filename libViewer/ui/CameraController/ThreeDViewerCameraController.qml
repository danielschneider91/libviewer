import QtQuick
import Qt3D.Core 2.4
import Qt3D.Render 2.4
import Qt3D.Input 2.4

import libViewer

Entity{
    id: root
    property Camera camera;
    property real dt: 0.001
    property real linearSpeed: 1
    property real lookSpeed: 500
    property real zoomLimit: 0.5
    property bool myEnabled:true

    property ViewerConfiguration viewerConfiguration
    property Repeater viewportsRepeater
    property real viewerWidth:100
    property real viewerHeight:100

    readonly property real rollMin:0.08
    readonly property real rollMax:0.92

    MouseDevice {
        id: mouseDevice
        enabled: root.myEnabled
        //sensitivity: 0.001 // Make it more smooth
    }

    MouseHandler {
        id: mh
        property point lastPos;
        property real rotX;
        property real rotY;
        property bool rollCam:false
        property bool switchRollDirection:false
        property bool pressedInRoll:false
        property bool isPressed:false
        sourceDevice: mouseDevice

        onRotXChanged: {
            if(camera===null || !root.myEnabled) {
                return;
            }
            if(mh.rollCam==true) {
                if(mh.switchRollDirection) {
                    root.camera.rollAboutViewCenter(-rotX);
                } else {
                    root.camera.rollAboutViewCenter(rotX);
                }
            } else {
                root.camera.panAboutViewCenter(rotX);
            }

        }
        onRotYChanged: {
            if(camera===null || !root.myEnabled) {
                return;
            }
            if(mh.rollCam==true) {
                if(mh.switchRollDirection) {
                    root.camera.rollAboutViewCenter(-rotY);
                } else {
                    root.camera.rollAboutViewCenter(rotY);
                }
            } else {
                root.camera.tiltAboutViewCenter(rotY);
            }
        }
        onPressed: function(mouse) {
            if(camera===null || !root.myEnabled) {
                return;
            }
            mh.isPressed=true
            if(isInRollArea(mouse.x,mouse.y)) {
                mh.pressedInRoll = true
                mh.rollCam=true
            } else {
                mh.pressedInRoll=false
                mh.rollCam=false
            }

            lastPos = Qt.point(mouse.x, mouse.y);
        }
        onPositionChanged: function(mouse) {
            if(camera===null || !root.myEnabled) {
                return;
            }
            // You can change the button as you like for rotation or translation
            if (mouse.buttons === 1){ // Left button for rotation

                setSwitchRollDirection(mouse.x,mouse.y);
                //if(isInRollArea(mouse.x,mouse.y)&&mh.isPressed&&mh.pressedInRoll) {
                //    mh.rollCam=true
                //}else {
                //    mh.rollCam=false
                //}
                rotX = -(mouse.x - lastPos.x) * dt * lookSpeed;
                rotY = (mouse.y - lastPos.y) * dt * lookSpeed;

            } else if (mouse.buttons === 2) { // Right button for translate
                var rx = -(mouse.x - lastPos.x) * dt * linearSpeed;
                var ry = (mouse.y - lastPos.y) * dt * linearSpeed;
                camera.translate(Qt.vector3d(rx, ry, 0))
            } else if (mouse.buttons === 3) { // Left & Right button for zoom
                ry = (mouse.y - lastPos.y) * dt * linearSpeed
                zoom(ry)
            }

            lastPos = Qt.point(mouse.x, mouse.y)
        }
        onWheel:function(wheel)  {
            if(camera===null || !root.myEnabled) {
                return;
            }
            zoom(wheel.angleDelta.y * dt * linearSpeed)
        }
        onReleased: {
            mh.isPressed=false
        }

        function zoom(ry) {
            if (ry > 0 && ry>zoomDistance(camera.position, camera.viewCenter)) {
                ry = zoomDistance(camera.position, camera.viewCenter)/1.4
            }

            if (ry > 0 && zoomDistance(camera.position, camera.viewCenter)<zoomLimit) {
                camera.translate(Qt.vector3d(0, 0, ry), Camera.TranslateViewCenter)
                return
            } else {
                camera.translate(Qt.vector3d(0, 0, ry), Camera.DontTranslateViewCenter)
                return
            }

        }

        function zoomDistance(posFirst, posSecond) {
            return posSecond.minus(posFirst).length()
        }

        function setSwitchRollDirection(x,y) {

            var posRel = getRelPos(x,y)

            if(posRel.xRel<root.rollMin||posRel.yRel<root.rollMin) {
                mh.switchRollDirection=true;
            }else{
                mh.switchRollDirection=false;
            }
        }

        function isInRollArea(x,y) {

            var posRel = getRelPos(x,y)

            if(posRel.xRel<root.rollMin||posRel.xRel>root.rollMax||posRel.yRel<root.rollMin||posRel.yRel>root.rollMax) {
                return true // in roll area
            }else {
                return false // outside roll area
            }
        }

        function getRelPos(x,y) {
            var threeDViewportId = root.viewerConfiguration.getThreeDViewportId();
            var viewportLeftInPx = root.viewportsRepeater.itemAt(threeDViewportId).getViewportLeft()*root.viewerWidth;
            var viewportRightInPx = root.viewportsRepeater.itemAt(threeDViewportId).getViewportRight()*root.viewerWidth;
            var viewportTopInPx = root.viewportsRepeater.itemAt(threeDViewportId).getViewportTop()*root.viewerHeight;
            var viewportBottomInPx = root.viewportsRepeater.itemAt(threeDViewportId).getViewportBottom()*root.viewerHeight;
            var viewportWidthInPx = viewportRightInPx - viewportLeftInPx
            var viewportHeightInPx = viewportBottomInPx - viewportTopInPx

            var posXRel = Math.abs(x-viewportLeftInPx)/Math.abs(viewportWidthInPx)
            var posYRel = Math.abs(y-viewportTopInPx)/Math.abs(viewportHeightInPx)
            return {
                xRel:posXRel,
                yRel:posYRel
            }

        }
    }
}
