import Qt3D.Core 2.4
import Qt3D.Render 2.0
import Qt3D.Input 2.4

Entity{
    id: root
    property Camera camera;
    property real dt: 0.001
    property real linearSpeed: 150
    property real lookSpeed: 500
    property real zoomLimit: 0.16
    property real orthographicZoomFactor: 1.03; // decrease height 25% for zooming in
    property real projectiveZoomFactor: 0.1; // translate 10% of camera center to view center distance
    property bool myEnabled:true

    MouseDevice {
        id: mouseDevice
        enabled: root.myEnabled
        //sensitivity: 0.001 // Make it more smooth
    }

    MouseHandler {
        id: mh
        readonly property vector3d upVect: Qt.vector3d(0, 1, 0)
        property point lastPos;
        property real pan;
        property real tilt;
        sourceDevice: mouseDevice

        onPanChanged: {
            if(camera===null || !root.myEnabled) {
                return;
            }
            root.camera.panAboutViewCenter(pan, upVect);
        }
        onTiltChanged: {
            if(camera===null || !root.myEnabled) {
                return;
            }
            root.camera.tiltAboutViewCenter(tilt);
        }

        onPressed:function(mouse) {
            console.log("sliceviewercamcontroller onPressed")
            if(camera===null || !root.myEnabled) {
                return;
            }
            lastPos = Qt.point(mouse.x, mouse.y);
        }
        onPositionChanged:function(mouse) {
            if(camera===null || !root.myEnabled) {
                return;
            }

            // You can change the button as you like for rotation or translation
            if (mouse.buttons === 1){ // Left button for rotation
                // do nothing
                //pan = -(mouse.x - lastPos.x) * dt * lookSpeed;
                //tilt = (mouse.y - lastPos.y) * dt * lookSpeed;
            } else if (mouse.buttons === 2) { // Right button for translate
                var rx = -(mouse.x - lastPos.x) * dt * linearSpeed;
                var ry = (mouse.y - lastPos.y) * dt * linearSpeed;
                camera.translate(Qt.vector3d(rx, ry, 0))
            }
            // zoom disabled
            //else if (mouse.buttons === 3) { // Left & Right button for zoom
            //    ry = (mouse.y - lastPos.y) * dt * linearSpeed
            //    console.log("ry"+ry);
            //    zoom(ry)
            //}

            lastPos = Qt.point(mouse.x, mouse.y)
        }

        function zoom(factor) {
            if(camera.projectionType===CameraLens.PerspectiveProjection) {
                distance = distanceBetween(camera.position, camera.viewCenter)*root.projectiveZoomFactor*Math.sign(factor);
                camera.translate(Qt.vector3d(0,0,distance),Camera.DontTranslateViewCenter);
            }
            if(camera.projectionType===CameraLens.OrthographicProjection) {
                if(Math.sign(factor<0)) {
                    orthographicZoomIn();
                } else {
                    orthographicZoomOut();
                }
            }
        }

        function orthographicZoomIn() {
            camera.bottom = camera.bottom/root.orthographicZoomFactor
            camera.top = camera.top/root.orthographicZoomFactor
            camera.left = camera.left/root.orthographicZoomFactor
            camera.right = camera.right/root.orthographicZoomFactor
        }
        function orthographicZoomOut() {
            camera.bottom = camera.bottom*root.orthographicZoomFactor
            camera.top = camera.top*root.orthographicZoomFactor
            camera.left = camera.left*root.orthographicZoomFactor
            camera.right = camera.right*root.orthographicZoomFactor
        }

        function distanceBetween(posFirst, posSecond) {
            return posSecond.minus(posFirst).length()
        }
    }
}
