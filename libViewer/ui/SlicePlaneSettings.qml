import QtQuick 2.0

Item {
    property color color: "red"
    property color horizontalLineColor: "red"
    property color verticalLineColor: "green"
    property string name: "Default"
    property real lineWidth: 2.0
    property real alpha:1
    property bool frameEnabled: true

    property real planeWidth: 310
    property real planeHeight: 310

    property bool crosshairEnabled: true

    property bool crosshairInteractionEnabled: true
    property size crosshairInteractionDimensions: Qt.size(100,100)

    property bool planePickingEnabled:false
}
