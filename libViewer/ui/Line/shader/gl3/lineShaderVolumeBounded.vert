#version 150 core

in vec3 vertexPosition;

uniform mat4 modelMatrix;
uniform mat4 mvp;

out vec3 worldPosition;

void main()
{
    // Transform position, normal, and tangent to world coords
    worldPosition = vec3(modelMatrix * vec4(vertexPosition, 1.0));

    // Calculate vertex position in clip coordinates
    gl_Position = mvp * vec4(vertexPosition, 1.0);
}
