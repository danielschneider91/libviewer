#version 150 core

in vec3 worldPosition;

uniform vec4 color;
uniform vec3 originBoundingBox;
uniform vec3 dimensionTimesSpacing;
uniform mat4 orientation;

out vec4 fragColor;

void main() {
    // position in world COS to position in image COS
    mat4 imagePose = orientation;
    imagePose[3] = vec4(originBoundingBox,1.0);
    vec3 normalizedPosition = vec3(inverse(imagePose)*vec4(worldPosition,1.0))/dimensionTimesSpacing;
    //vec3 normalizedPosition = (worldPosition - originBoundingBox) / dimensionTimesSpacing;
    if(normalizedPosition.x>1.0||normalizedPosition.x<0.0||
            normalizedPosition.y>1.0||normalizedPosition.y<0.0||
            normalizedPosition.z>1.0||normalizedPosition.z<0.0) { // outside of volume
        discard;
    } else {
        fragColor = color;
    }
}

