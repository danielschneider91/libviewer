#version 330

flat in vec3 startPos;
in vec3 vertPos;

uniform vec4 color;
uniform float width;

uniform vec2  resolution;
uniform float dashLength;
uniform float gapLength;

out vec4 fragColor;

void main()
{
    vec2  dir  = (vertPos.xy-startPos.xy) * resolution/2.0;
    float dist = length(dir);

    if (fract(dist / (dashLength + gapLength)) > dashLength/(dashLength + gapLength))
        discard;

    fragColor = color;
}
