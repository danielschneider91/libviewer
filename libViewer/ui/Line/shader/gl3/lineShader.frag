#version 150 core

in vec3 worldPosition;

uniform vec4 color;
uniform float width;

out vec4 fragColor;

void main() {
    fragColor = color;
}

