import Qt3D.Core 2.4
import Qt3D.Render 2.4
import Qt3D.Render 2.15

import libViewer

import ".."

Effect {
    id: root

    property color color: Qt.rgba(1,0,0,1)
    property real width: 3 // in %
    property real scale: 1.0
    property bool depthTesting: true
    property real dashLength: 30 // length of line in pixel
    property real gapLength: 20 // length of gap in pixel
    property vector2d resolution: Qt.vector2d(800,600) // width, height of viewport
    readonly property FilterKeyCatalogue filterKeyCatalogue:FilterKeyCatalogue{}

    parameters: [
        Parameter { name: "color"; value: Qt.vector4d(root.color.r, root.color.g, root.color.b, root.color.a) },
        Parameter { name: "width"; value: root.width*root.scale },
        Parameter { name: "dashLength"; value: root.dashLength },
        Parameter { name: "gapLength"; value: root.gapLength },
        Parameter { name: "resolution"; value: root.resolution }
    ]

    techniques: [
        //! [3]
        // OpenGL 3.1
        Technique {
            filterKeys: FilterKey {
                id: forward
                name: "renderingStyle"
                value: "forward"
            }
            graphicsApiFilter {
                api: GraphicsApiFilter.OpenGL
                profile: GraphicsApiFilter.CoreProfile
                majorVersion: 3
                minorVersion: 1
            }
            renderPasses: RenderPass {
                filterKeys: [root.filterKeyCatalogue.threeDViewerOpaqueModelFilterKey,
                    root.filterKeyCatalogue.threeDViewerTransparentModelFilterKey,
                    root.filterKeyCatalogue.threeDViewerTransparentModelFilterKey,
                    root.filterKeyCatalogue.sliceViewerTransparentModelFilterKey
                ]
                shaderProgram: ShaderProgram {
                    id: gl3Shader
                    vertexShaderCode: loadSource("qrc:/libViewer/ui/Line/shader/gl3/dashedLineShader.vert")
                    fragmentShaderCode: loadSource("qrc:/libViewer/ui/Line/shader/gl3/dashedLineShader.frag")
                }
                renderStates: [
                    CullFace { mode: CullFace.NoCulling },
                    DepthTest { depthFunction: root.depthTesting ? DepthTest.Less :DepthTest.Always },
                    LineWidth { value:root.width*root.scale }
                ]
            }
        }
    ]
}

