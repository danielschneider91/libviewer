import Qt3D.Core 2.4
import Qt3D.Render 2.4
import Qt3D.Extras 2.4
import QtQuick
import Qt3D.Input 2.4

import libViewer

Entity {
    id: root

    property bool myEnabled:true
    property var viewerLayers:[]
    property var renderLayers: []
    property vector3d startPoint: Qt.vector3d(-12,0,0)
    property vector3d endPoint: Qt.vector3d(3.50,10,0)
    property real lineWidth: 2.0
    property bool picking: false
    property color lineColor: "blue"
    property real scale: 1.0;
    property alias effect: material.effect

    signal positionChanged(vector3d position)

    onScaleChanged: {
        material.effect.scale = root.scale;
    }

    Entity {
        components: {
            if(!root.myEnabled) {return [];}
            var myArray=[transform,geometry,material];

            if(root.picking) {
                //myArray.push(picker)
            }

            for(var i=0;i<root.viewerLayers.length;i++) {
                myArray.push(root.viewerLayers[i]);
            }
            for(var j=0;j<root.renderLayers.length;j++) {
                myArray.push(root.renderLayers[j]);
            }

            return myArray;
        }

        Material {
            id: material
            parameters: [
                Parameter { name: "color"; value: Qt.vector4d(root.lineColor.r, root.lineColor.g, root.lineColor.b, root.lineColor.a) },
                Parameter { name: "width"; value: root.lineWidth }
            ]
        }

        Transform {
            id: transform
        }

        //ObjectPicker {
        //    id: picker
        //    hoverEnabled: false
        //    dragEnabled: true
        //    onMoved: {
        //        if(pick.buttons===1) { // Left mouse button
        //            console.log("line picked")
        //            root.positionChanged(pick.worldIntersection)
        //        }
        //    }
        //}

        GeometryRenderer {
            id: geometry

            instanceCount: 1
            primitiveType: GeometryRenderer.Lines

            geometry: Geometry {
                Attribute {
                    attributeType: Attribute.VertexAttribute
                    vertexBaseType: Attribute.Float
                    vertexSize: 3
                    byteOffset: 0
                    byteStride: 3*4
                    count: 2
                    name: "vertexPosition" // Name of attribute in the shader
                    buffer: Buffer {
                        id: vertexBuffer
                        //type: Buffer.VertexBuffer
                        data: new Float32Array([
                                                root.startPoint.x, root.startPoint.y,root.startPoint.z,
                                                root.endPoint.x, root.endPoint.y,root.endPoint.z,
                                               ])
                    }
                }
            }
        }

    }



}
