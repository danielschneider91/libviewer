import Qt3D.Core 2.4
import Qt3D.Render 2.4
import Qt3D.Render 2.15

import libViewer

import ".."

Effect {
    id: root

    property color color: Qt.rgba(1,0,0,1)
    property real width: 3 // in %
    property real scale: 1.0
    property bool depthTesting: true
    property alias distanceClippingEnabled: distanceClippingEnabledParam.value
    property alias clipPlaneEquation: clipPlaneEquationParam.value
    property alias clippingDistance: clippingDistanceParam.value

    readonly property FilterKeyCatalogue filterKeyCatalogue:FilterKeyCatalogue{}

    parameters: [
        Parameter { name: "color"; value: Qt.vector4d(root.color.r, root.color.g, root.color.b, root.color.a) },
        Parameter { name: "width"; value: root.width*root.scale },
        Parameter { id: distanceClippingEnabledParam; name: "distanceClippingEnabled"; value: false },
        Parameter { id: clipPlaneEquationParam; name: "clipPlaneEquation"; value: Qt.vector4d(0.0, 0.0, 1.0, 1.0)},
        Parameter { id: clippingDistanceParam; name: "clippingDistance"; value: 50.0}
    ]

    ShaderProgram {
        id: gl3ShaderDistanceClipped
        vertexShaderCode: loadSource("qrc:/libViewer/ui/Line/shader/gl3/lineShaderDistanceClipped.vert")
        fragmentShaderCode: loadSource("qrc:/libViewer/ui/Line/shader/gl3/lineShaderDistanceClipped.frag")
    }
    ShaderProgram {
        id: gl3Shader
        vertexShaderCode: loadSource("qrc:/libViewer/ui/Line/shader/gl3/lineShader.vert")
        fragmentShaderCode: loadSource("qrc:/libViewer/ui/Line/shader/gl3/lineShader.frag")
    }

    techniques: [
        Technique {
            filterKeys: FilterKey {
                id: forward
                name: "renderingStyle"
                value: "forward"
            }
            graphicsApiFilter {
                api: GraphicsApiFilter.OpenGL
                profile: GraphicsApiFilter.CoreProfile
                majorVersion: 3
                minorVersion: 1
            }
            RenderPass {
                id:renderPassThreeD
                filterKeys: [root.filterKeyCatalogue.threeDViewerOpaqueModelFilterKey,
                root.filterKeyCatalogue.threeDViewerTransparentModelFilterKey]
                shaderProgram: {
                    console.log("LineShaderEffect renderPassThreeD gl3Shader")
                    return gl3Shader
                }

                renderStates: [
                    CullFace { mode: CullFace.NoCulling },
                    DepthTest { depthFunction: root.depthTesting ? DepthTest.Less :DepthTest.Always },
                    LineWidth { value:root.width*root.scale }
                ]
            }
            RenderPass {
                id:renderPassSlice
                filterKeys: [root.filterKeyCatalogue.sliceViewerOpaqueModelFilterKey,
                    root.filterKeyCatalogue.sliceViewerTransparentModelFilterKey]
                shaderProgram: {
                    if(root.distanceClippingEnabled) {
                        console.log("LineShaderEffect gl3ShaderDistanceClipped")
                        return gl3ShaderDistanceClipped;
                    } else {
                        console.log("LineShaderEffect renderPassSlice gl3Shader")
                        return gl3Shader;
                    }
                }

                renderStates: [
                    CullFace { mode: CullFace.NoCulling },
                    DepthTest { depthFunction: root.depthTesting ? DepthTest.Less :DepthTest.Always },
                    LineWidth { value:root.width*root.scale }
                ]
            }
            renderPasses: [renderPassThreeD,renderPassSlice]
        }
    ]
}

