import Qt3D.Core 2.4
import Qt3D.Render 2.4
import Qt3D.Render 2.15

import libViewer

import ".."

Effect {
    id: root

    property color color: Qt.rgba(1,0,0,1)
    property real width: 3 // in %
    property real scale: 1.0
    property bool depthTesting: true
    property vector3d dimensionTimesSpacing: Qt.vector3d(10,10,10)
    property vector3d originBoundingBox: Qt.vector3d(0,0,0)
    property matrix4x4 orientation: Qt.matrix4x4(1,0,0,0,
                                                 0,1,0,0,
                                                 0,0,1,0,
                                                 0,0,0,1)
    readonly property FilterKeyCatalogue filterKeyCatalogue:FilterKeyCatalogue{}

    parameters: [
        Parameter { name: "color"; value: Qt.vector4d(root.color.r, root.color.g, root.color.b, root.color.a) },
        Parameter { name: "width"; value: root.width*root.scale },
        Parameter { name: "originBoundingBox"; value: root.originBoundingBox },
        Parameter { name: "orientation"; value: root.orientation },
        Parameter { name: "dimensionTimesSpacing"; value: root.dimensionTimesSpacing }
    ]

    techniques: [
        Technique {
            filterKeys: FilterKey {
                id: forward
                name: "renderingStyle"
                value: "forward"
            }
            graphicsApiFilter {
                api: GraphicsApiFilter.OpenGL
                profile: GraphicsApiFilter.CoreProfile
                majorVersion: 3
                minorVersion: 1
            }
            renderPasses: RenderPass {
                filterKeys:[root.filterKeyCatalogue.threeDViewerOpaqueModelFilterKey,
                    root.filterKeyCatalogue.threeDViewerTransparentModelFilterKey,
                    root.filterKeyCatalogue.threeDViewerTransparentModelFilterKey,
                    root.filterKeyCatalogue.sliceViewerTransparentModelFilterKey
                ]
                shaderProgram: ShaderProgram {
                    id: gl3Shader
                    vertexShaderCode: loadSource("qrc:/libViewer/ui/Line/shader/gl3/lineShaderVolumeBounded.vert")
                    fragmentShaderCode: loadSource("qrc:/libViewer/ui/Line/shader/gl3/lineShaderVolumeBounded.frag")
                }

                renderStates: [
                    CullFace { mode: CullFace.NoCulling },
                    DepthTest { depthFunction: root.depthTesting ? DepthTest.Less :DepthTest.Always },
                    LineWidth { value:root.width*root.scale }
                ]
            }
        }
    ]
}

