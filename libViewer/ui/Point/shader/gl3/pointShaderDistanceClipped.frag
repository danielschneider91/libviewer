#version 150 core

in vec3 worldPosition;

uniform vec4 color;
uniform bool distanceClippingEnabled;
uniform float clippingDistance;
uniform vec4 clipPlaneEquation;

out vec4 fragColor;

void main() {
    if(!distanceClippingEnabled) {
        fragColor = color;
    } else {
        float normalization = sqrt(clipPlaneEquation.x*clipPlaneEquation.x+clipPlaneEquation.y*clipPlaneEquation.y+clipPlaneEquation.z*clipPlaneEquation.z);
        float distance = dot(vec4(worldPosition, 1.0), clipPlaneEquation)/normalization;
        if(abs(distance)>clippingDistance) {
            discard;
        } else {
            fragColor = color;
        }
    }
}

