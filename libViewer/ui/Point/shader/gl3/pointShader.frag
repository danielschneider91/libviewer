#version 150 core

in vec3 worldPos;

uniform vec4 color;

out vec4 fragColor;

void main() {
    fragColor = color;
}

