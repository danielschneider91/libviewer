import Qt3D.Core 2.4
import Qt3D.Render 2.4
import Qt3D.Extras 2.4
import QtQuick
import Qt3D.Input 2.4

import libViewer

Entity {
    id: root

    property var viewerLayers:[]
    property var renderLayers:[]
    property var points:[]
    property bool picking: false
    property real scale: 1.0;
    property alias effect: material.effect
    property color color: Qt.rgba(1,1,1,1)

    signal positionChanged(vector3d position)

    onScaleChanged: {
        material.effect.scale = root.scale;
    }

    components: {
        var myArray=[transform,geometry,material];

        //if(root.picking) {
        //    myArray.push(picker)
        //}

        for(var i=0;i<root.viewerLayers.length;i++) {
            myArray.push(root.viewerLayers[i]);
        }
        for(var j=0;j<root.renderLayers.length;j++) {
            myArray.push(root.renderLayers[j]);
        }

        return myArray;
    }

    Material {
        id: material
        parameters: [
            Parameter { name: "color"; value: Qt.vector4d(root.color.r, root.color.g, root.color.b, root.color.a) }
        ]

    }

    Transform {
        id: transform
    }

    //ObjectPicker {
    //    id: picker
    //    hoverEnabled: false
    //    dragEnabled: true
    //    onMoved: {
    //        if(pick.buttons===1) { // Left mouse button
    //            root.positionChanged(pick.localIntersection)
    //        }
    //    }
    //}

    GeometryRenderer {
        id: geometry

        instanceCount: 1
        //indexOffset: 0
        //firstInstance: 0
        primitiveType: GeometryRenderer.Points

        geometry: Geometry {
            Attribute {
                attributeType: Attribute.VertexAttribute
                vertexBaseType: Attribute.Float
                vertexSize: 3
                byteOffset: 0
                byteStride: 3*4
                count:root.points.length
                name: "vertexPosition" // Name of attribute in the shader
                buffer: Buffer {
                    id: vertexBuffer
                    //type: Buffer.VertexBuffer
                    data: {
                        var myArray = new Float32Array(root.points.length*3)

                        var counter = 0
                        for(var i=0;i<root.points.length;i++) {
                            myArray[counter]=root.points[i].x
                            counter = counter+1;
                            myArray[counter]=root.points[i].y
                            counter = counter+1;
                            myArray[counter]=root.points[i].z
                            counter = counter+1;
                        }
                        return myArray
                    }
                }
            }
        }
    }
}
