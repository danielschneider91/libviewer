import Qt3D.Core 2.4
import Qt3D.Render 2.4
import Qt3D.Render 2.15

import libViewer

import ".."

Effect {
    id: root

    property color color: Qt.rgba(0,1,0,1)
    property real size: 5
    property real scale: 1.0
    property bool depthTesting: true
    property alias distanceClippingEnabled: distanceClippingEnabledParam.value
    property alias clipPlaneEquation: clipPlaneEquationParam.value
    property alias clippingDistance: clippingDistanceParam.value

    readonly property FilterKeyCatalogue filterKeyCatalogue:FilterKeyCatalogue{}


    FilterKey {
        id: forward
        name: "renderingStyle"
        value: "forward"
    }

    ShaderProgram {
        id: gl3Shader
        vertexShaderCode: loadSource("qrc:/libViewer/ui/Point/shader/gl3/pointShader.vert")
        fragmentShaderCode: loadSource("qrc:/libViewer/ui/Point/shader/gl3/pointShader.frag")
    }
    ShaderProgram {
        id: gl3ShaderDistanceClipped
        vertexShaderCode: loadSource("qrc:/libViewer/ui/Point/shader/gl3/pointShaderDistanceClipped.vert")
        fragmentShaderCode: loadSource("qrc:/libViewer/ui/Point/shader/gl3/pointShaderDistanceClipped.frag")
    }

    parameters: [
        Parameter { name: "color";value: Qt.vector4d(root.color.r, root.color.g, root.color.b, root.color.a) },
        Parameter { name: "size"; value: root.size*root.scale },
        Parameter { id: distanceClippingEnabledParam; name: "distanceClippingEnabled"; value: false },
        Parameter { id: clipPlaneEquationParam; name: "clipPlaneEquation"; value: Qt.vector4d(0.0, 0.0, 1.0, 1.0)},
        Parameter { id: clippingDistanceParam; name: "clippingDistance"; value: 50.0}
    ]

    techniques: [
        Technique {
            filterKeys: [forward]
            graphicsApiFilter {
                api: GraphicsApiFilter.OpenGL
                profile: GraphicsApiFilter.CoreProfile
                majorVersion: 3
                minorVersion: 1
            }
            RenderPass {
                id:renderPassThreeD
                filterKeys: [root.filterKeyCatalogue.threeDViewerOpaqueModelFilterKey,
                            root.filterKeyCatalogue.threeDViewerTransparentModelFilterKey]
                shaderProgram: gl3Shader

                renderStates: [
                    CullFace { mode: CullFace.NoCulling },
                    DepthTest { depthFunction: root.depthTesting ? DepthTest.Less :DepthTest.Always },
                    PointSize { value:root.size*root.scale; sizeMode: PointSize.Fixed }
                ]
            }
            RenderPass {
                id:renderPassSlice
                filterKeys: [root.filterKeyCatalogue.sliceViewerOpaqueModelFilterKey,
                    root.filterKeyCatalogue.sliceViewerTransparentModelFilterKey]
                shaderProgram: {
                    if(root.distanceClippingEnabled) {
                        return gl3ShaderDistanceClipped;
                    } else {
                        return gl3Shader;
                    }
                }

                renderStates: [
                    CullFace { mode: CullFace.NoCulling },
                    DepthTest { depthFunction: root.depthTesting ? DepthTest.Less :DepthTest.Always },
                    PointSize { value:root.size*root.scale; sizeMode: PointSize.Fixed },
                    AlphaCoverage {},
                    MultiSampleAntiAliasing{}
                ]
            }
            renderPasses: [renderPassThreeD,renderPassSlice]
        }
    ]
}

