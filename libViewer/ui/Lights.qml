import QtQuick
import Qt3D.Core 2.4
import Qt3D.Render 2.4
import Qt3D.Input 2.4
import Qt3D.Extras 2.4

Entity {
    id: root
    property alias lightsInstantiator: lightsInstantiator

    property int numberOfViewports: 0
    property NodeInstantiator camerasInstantiator
    property var viewerLayers: []
    property var renderLayers: []

    NodeInstantiator {
        id: lightsInstantiator
        model: root.numberOfViewports
        delegate: SliceViewerLights {
            lightDirection: {
                if(root.camerasInstantiator.count<=0) { return null; }
                else {
                    root.camerasInstantiator.objectAt(index).camm.viewVector
                }
            }
            viewerLayers: root.viewerLayers
            renderLayers: root.renderLayers
        }
    }
}
