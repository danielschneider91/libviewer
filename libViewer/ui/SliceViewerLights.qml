import Qt3D.Core 2.4
import Qt3D.Extras 2.4
import Qt3D.Render 2.4
import QtQml
import Qt3D.Input 2.4
import QtQuick

Entity {
    id: root
    property alias lightDirection: directionalLight.worldDirection
    property alias lightPosition: lightTransform.translation
    property var viewerLayers:[]
    property var renderLayers:[]
    readonly property alias directionalLight:directionalLight

    MyEnvironmentLight {
        id: environmentLight
        viewerLayers: root.viewerLayers
        renderLayers: root.renderLayers
    }

    Entity {
        id: lightEntity
        Transform {
            id: lightTransform
            translation: Qt.vector3d(0,0,0)
        }

        DirectionalLight {
            id: directionalLight
            intensity: 1.0
            color: Qt.rgba(1.0, 1.0, 1.0, 1.0)
        }
        components: {
            var anArray=[];
            anArray.push(lightTransform)
            anArray.push(directionalLight)
            for(var i=0;i<root.viewerLayers.length;i++) {
                anArray.push(root.viewerLayers[i]);
            }
            for(var j=0;j<root.renderLayers.length;j++) {
                anArray.push(root.renderLayers[j]);
            }

            return anArray;
        }
    }
}
