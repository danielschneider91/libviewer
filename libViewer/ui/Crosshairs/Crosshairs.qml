import Qt3D.Core 2.4
import Qt3D.Extras 2.4
import Qt3D.Render 2.4
import QtQml
import Qt3D.Input 2.4
import QtQuick

import "../Line"
import "../Point"

Entity {
    id: root

    property var viewerLayers:[]
    property var renderLayers: []

    property int crosshairsSize: 100
    property real scale: 1.0;
    property bool crosshairEnabled:true
    property bool distanceClippingEnabled:false
    property real clippingDistance: 75.0
    property vector4d clipPlaneEquation:Qt.vector4d(0.0,0.0,1.0,1.0)
    property color horizontalLineColor: "green"
    property color verticalLineColor: "green"
    property real lineWidth: 2.0
    property real gapSize: 0.2
    property Effect lineEffect
    property Effect pointEffect


    Line {
        id: horizontalLineLeft
        enabled: root.crosshairEnabled
        viewerLayers: root.viewerLayers
        renderLayers: root.renderLayers
        startPoint: Qt.vector3d(-root.crosshairsSize*root.scale/2.0,0,0)
        endPoint: Qt.vector3d(-root.crosshairsSize*root.scale*root.gapSize/2.0,0,0)
        lineColor: root.horizontalLineColor
        lineWidth: root.lineWidth
        effect: root.lineEffect
    }
    Line {
        id: horizontalLineRight
        enabled: root.crosshairEnabled
        viewerLayers: root.viewerLayers
        renderLayers: root.renderLayers
        startPoint: Qt.vector3d(root.crosshairsSize*root.scale*root.gapSize/2.0,0,0)
        endPoint: Qt.vector3d(root.crosshairsSize*root.scale/2.0,0,0)
        lineColor: root.horizontalLineColor
        lineWidth: root.lineWidth
        effect: root.lineEffect
    }
    Line {
        id: verticalLineLower
        enabled: root.crosshairEnabled
        viewerLayers: root.viewerLayers
        renderLayers: root.renderLayers
        startPoint: Qt.vector3d(0,-root.crosshairsSize*root.scale/2,0)
        endPoint: Qt.vector3d(0,-root.crosshairsSize*root.scale*root.gapSize/2,0)
        lineColor: root.verticalLineColor
        lineWidth: root.lineWidth
        effect: root.lineEffect
    }
    Line {
        id: verticalLineUpper
        enabled: root.crosshairEnabled
        viewerLayers: root.viewerLayers
        renderLayers: root.renderLayers
        startPoint: Qt.vector3d(0,root.crosshairsSize*root.scale*root.gapSize/2,0)
        endPoint: Qt.vector3d(0,root.crosshairsSize*root.scale/2,0)
        lineColor: root.verticalLineColor
        lineWidth: root.lineWidth
        effect: root.lineEffect
    }
    Line {
        id: backLine
        enabled: root.crosshairEnabled
        viewerLayers: root.viewerLayers
        renderLayers: root.renderLayers
        startPoint: Qt.vector3d(0,0,-root.crosshairsSize*root.scale/2)
        endPoint: Qt.vector3d(0,0,-root.crosshairsSize*root.scale*root.gapSize/2)
        lineColor: root.verticalLineColor
        lineWidth: root.lineWidth
        effect: root.lineEffect
    }
    Line {
        id: frontLine
        enabled: root.crosshairEnabled
        viewerLayers: root.viewerLayers
        renderLayers: root.renderLayers
        startPoint: Qt.vector3d(0,0,root.crosshairsSize*root.scale*root.gapSize/2)
        endPoint: Qt.vector3d(0,0,root.crosshairsSize*root.scale/2)
        lineColor: root.verticalLineColor
        lineWidth: root.lineWidth
        effect: root.lineEffect
    }
    Points {
        id: centerPoint
        enabled: root.crosshairEnabled
        viewerLayers: root.viewerLayers
        renderLayers: root.renderLayers
        effect: root.pointEffect
        points: [Qt.vector3d(0,0,0)]
        color: root.horizontalLineColor
    }
}
