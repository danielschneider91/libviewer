import QtQuick
import QtQuick.Controls
import QtQuick.Scene3D 2.4
import Qt3D.Core 2.4
import Qt3D.Render 2.4
import Qt3D.Input 2.4
import Qt3D.Extras 2.4

import libViewer

Entity {
    id:root
    property SlicePlanes slicePlanesEntity
    property Cameras camerasEntity
    property int mouseIsInViewport: -1
    property ViewerConfiguration viewerConfiguration

    property var cameraOutOfSyncWithPlane: {
        var arr=[]
        for(var i=0;i<root.camerasEntity.camerasInstantiator.count;i++) {
            arr.push(true);
        }
        return arr;
    }

    Component.onCompleted: {
        slicePlanesEntity.slicePositionChanged.connect(onSlicePositionChangedViaGUI)
        slicePlanesEntity.sliceOrientationChanged.connect(onSliceOrientationChangedViaGUI)
    }

    function onSlicePositionChangedViaGUI(viewportIdx,t_world_plane) {
        /* Remember that camera needs to be synchronized again.
         * And update camera. */
        root.cameraOutOfSyncWithPlane[viewportIdx] = true;

        var sliceIdx = root.viewerConfiguration.getSliceIndexFromViewportIndex(viewportIdx);
        setCameraPose(viewportIdx, t_world_plane)

    }

    function onSliceOrientationChangedViaGUI(viewportIdx,t_world_plane) {
        /* If orientation of slice changed through user interaction (via GUI),
         * do not update camera of viewport where user is interacting, however remember that camera needs to be
         * synchronized again.
         * Update cameras of passive viewports, i.e. viewports where the user does not interact. */
        root.cameraOutOfSyncWithPlane[viewportIdx] = true;
        if(true/*root.viewerConfiguration.viewportToSlicetypeMap[viewportIdx] !== root.mouseIsInViewport*/) {
            /* For all other slices, update camera pose (do not update camera pose in viewer pane with which we are interacting). */
            var sliceIdx = root.viewerConfiguration.getSliceIndexFromViewportIndex(viewportIdx);
            setCameraPose(viewportIdx, t_world_plane)
        }
    }

    function setCameraPose(viewportIdx,t_world_plane) {
        var activeViewport = root.mouseIsInViewport===viewportIdx
        if(viewportIdx!==root.viewerConfiguration.getThreeDViewportId()) { // set camera pose in slice viewport
            if(root.cameraOutOfSyncWithPlane[viewportIdx]) { // if cam and slice plane out of sync, sync first
                root.cameraOutOfSyncWithPlane[viewportIdx]=false;
                synchronizeCameraAndSlicePlane(viewportIdx,t_world_plane)
            }
            // set camera pose according to slice plane pose
            root.camerasEntity.camerasInstantiator.objectAt(viewportIdx).t_world_camRoot.matrix = t_world_plane;
        }
    }


    function synchronizeCameraAndSlicePlane(viewportIdx,t_world_plane) {
        var T_plane_cam= getCameraRelToSlicePlane(root.camerasEntity.camerasInstantiator.objectAt(viewportIdx).t_camRoot_cam.matrix,
                                                   root.camerasEntity.camerasInstantiator.objectAt(viewportIdx).t_world_camRoot.matrix,
                                                   t_world_plane)
        root.camerasEntity.camerasInstantiator.objectAt(viewportIdx).camm.viewCenter = Qt.vector3d(T_plane_cam.m14,T_plane_cam.m24,0)
        root.camerasEntity.camerasInstantiator.objectAt(viewportIdx).camm.position = Qt.vector3d(T_plane_cam.m14,T_plane_cam.m24,-root.camerasEntity.planeToCamDistance)
        root.camerasEntity.camerasInstantiator.objectAt(viewportIdx).camm.upVector = Qt.vector3d(T_plane_cam.m12,T_plane_cam.m22,0);
    }

    function getCameraRelToSlicePlane(t_camRoot_cam, t_world_camRoot,t_world_plane) {
        var T_camRoot_cam = Qt.matrix4x4(t_camRoot_cam.m11,t_camRoot_cam.m12,t_camRoot_cam.m13,t_camRoot_cam.m14,
                                         t_camRoot_cam.m21,t_camRoot_cam.m22,t_camRoot_cam.m23,t_camRoot_cam.m24,
                                         t_camRoot_cam.m31,t_camRoot_cam.m32,t_camRoot_cam.m33,0,
                                         0,0,0,1);
        var T_world_camRoot = Qt.matrix4x4(t_world_camRoot.m11,t_world_camRoot.m12,t_world_camRoot.m13,t_world_camRoot.m14,
                                           t_world_camRoot.m21,t_world_camRoot.m22,t_world_camRoot.m23,t_world_camRoot.m24,
                                           t_world_camRoot.m31,t_world_camRoot.m32,t_world_camRoot.m33,t_world_camRoot.m34,
                                           0,0,0,1);
        var T_world_plane = Qt.matrix4x4(t_world_plane.m11,t_world_plane.m12,t_world_plane.m13,t_world_plane.m14,
                                         t_world_plane.m21,t_world_plane.m22,t_world_plane.m23,t_world_plane.m24,
                                         t_world_plane.m31,t_world_plane.m32,t_world_plane.m33,t_world_plane.m34,
                                         0,0,0,1);

        var T_plane_camRoot = T_world_plane.inverted().times(T_world_camRoot);
        var T_plane_cam = (T_plane_camRoot).times(T_camRoot_cam);

        return T_plane_cam;
    }

}
