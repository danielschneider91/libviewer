import QtQuick
import QtQuick.Controls
import QtQuick.Scene3D 2.4
import Qt3D.Core 2.4
import Qt3D.Render 2.4
import Qt3D.Input 2.4
import Qt3D.Extras 2.4

import "Line"
import "CoordinateSystem"

import libViewer

Entity {
    id: root
    property string sliceId
    property bool isCrosshairPicking: slicePlane.isCrosshairPicking // state of slice viewer interaction crosshairs, i.e., whether they are currently being picked by user to move or rotate them
    property alias slicePlaneRootTranslation: slicePlaneRootTransform.translation // pose of axial slice in world COS
    property alias slicePlaneRootPose: slicePlaneRootTransform.matrix // pose of axial slice in world COS
    property Transform sliceTransform // pose of slice in world COS, this is rootPose (axial pose) plus 90 deg rotations around X/Y axis
    property ViewerConfiguration viewerConfiguration
    property var renderLayers:[]
    property var noDepthTestLayer:[]
    property var sliceViewerLayers:[]
    property var sliceLayers:[]
    property var threeDViewerLayer:[]
    property var visibilityVariable
    property alias volShadedSlicePlaneEffect: slicePlane.volumeShadedSlicePlaneEffect
    property SlicePlaneSettings slicePlaneSettings: SlicePlaneSettings {}

    signal positionChanged(vector3d deltaPosition)
    signal orientationChanged(real angleInDeg)
    signal spatialObjectPicked(string id,vector3d worldIntersection,vector3d localIntersection)
    signal spatialObjectHover(string id,vector3d worldIntersection,vector3d localIntersection)

    Component.onCompleted: {
        slicePlane.positionChanged.connect(root.onPositionChanged)
        slicePlane.orientationChanged.connect(root.onOrientationChanged)
        slicePlane.spatialObjectPicked.connect(root.onSpatialObjectPicked)
        slicePlane.spatialObjectHover.connect(root.onSpatialObjectHover)

    }

    onSlicePlaneSettingsChanged: {
        console.log("onSlicePlaneSettingsChanged: "+root.slicePlaneSettings.width)
    }

    function onPositionChanged(deltaPosition) {
        root.positionChanged(deltaPosition);
    }
    function onOrientationChanged(angleInDeg) {
        root.orientationChanged(angleInDeg);
    }
    function updateVolumeBoundedLineRenderingEffect(dimensionsTimesSpacing,originBoundingBox,orientation) {
        slicePlane.updateVolumeBoundedLineRenderingEffect(dimensionsTimesSpacing,originBoundingBox,orientation)
    }
    function onSpatialObjectPicked(worldIntersection,localIntersection) {
        console.log("root.sliceId: "+root.sliceId)
        root.spatialObjectPicked(root.sliceId,worldIntersection,localIntersection);
    }
    function onSpatialObjectHover(worldIntersection,localIntersection) {
        root.spatialObjectHover(root.sliceId,worldIntersection,localIntersection);
    }

    components: [ Transform {id: slicePlaneRootTransform;} ]
    CoordinateSystem {
        viewerLayers: root.threeDViewerLayer
        renderLayers: root.renderLayers
        myEnabled: false;//index===0?true:false
        scale: 5
        text: "slices"
    }
    Entity {
        components: [root.sliceTransform]
        SlicePlane {
            id: slicePlane
            sliceViewerLayers: root.sliceViewerLayers
            threeDViewerLayer: root.threeDViewerLayer
            sliceLayers: root.sliceLayers
            renderLayers: root.renderLayers
            noDepthTestLayer: root.noDepthTestLayer

            slicePlaneSettings: root.slicePlaneSettings
        }
    }
}
