import Qt3D.Core 2.4
import Qt3D.Extras 2.4
import Qt3D.Render 2.4
import Qt3D.Render 2.15
import Qt3D.Input 2.4
import QtQuick
import QtQml

Entity {
    id: root
    property Layer opaqueLayer
    property Layer noDepthCheckLayer
    property var viewerLayers: []
    property var renderLayers: []

    EnvironmentLight {
        id: environmentLight
        irradiance: TextureLoader {
            source: "qrc:/libViewer/ui/resources/environmentMaps/cedar_bridge_irradiance.dds"

            minificationFilter: Texture.LinearMipMapLinear
            magnificationFilter: Texture.Linear
            wrapMode {
                x: WrapMode.ClampToEdge
                y: WrapMode.ClampToEdge
            }
            generateMipMaps: false
        }
        specular: TextureLoader {
            source: "qrc:/libViewer/ui/resources/environmentMaps/cedar_bridge_specular.dds"

            minificationFilter: Texture.LinearMipMapLinear
            magnificationFilter: Texture.Linear
            wrapMode {
                x: WrapMode.ClampToEdge
                y: WrapMode.ClampToEdge
            }
            generateMipMaps: false
        }
    }
    components: {
        var anArray=[environmentLight];
        for(var i=0;i<root.viewerLayers.length;i++) {
            anArray.push(root.viewerLayers[i]);
        }
        for(var j=0;j<root.renderLayers.length;j++) {
            anArray.push(root.renderLayers[j]);
        }

        //return []
        return anArray;
    }
}
