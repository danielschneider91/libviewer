function reset(viewportIndex, numberOfColumns,numberOfRows) {
    var x = (viewportIndex%numberOfColumns)*1/numberOfColumns;
    var y = Math.floor(viewportIndex/numberOfColumns)*1/numberOfRows;
    var width = 1/numberOfColumns;
    var height = 1/numberOfRows;

    return [x,y,width,height]
}

function resetToOriginal(initialViewportSetting) {
    var x = initialViewportSetting.x;
    var y = initialViewportSetting.y;
    var width = initialViewportSetting.width;
    var height = initialViewportSetting.height;

    return [x,y,width,height]
}

function expand(viewportIndex, numberOfColumns,numberOfRows) {
    return [0,0,1,1]
}

function minimize(viewportIndex,viewerIndexToBeMaximized, numberOfColumns,numberOfRows, behaviour/*0: completely disappear, 1: to the right, 99: top left corner small*/) {
    var x = 0
    var y = 0
    var width = 0
    var height = 0

    switch(behaviour) {
    case 0:
        var proportion0 = 0.001
        x = (viewportIndex%numberOfColumns)*1/numberOfColumns*proportion0;
        y = Math.floor(viewportIndex/numberOfColumns)*1/numberOfRows*proportion0;
        width = 1/numberOfColumns*proportion0;
        height = 1/numberOfRows*proportion0;
        break;
    case 1:
        height = 1.0/(numberOfRows*numberOfColumns-1);
        width = height
        x = 1-width;
        if(viewportIndex<viewerIndexToBeMaximized) { y=viewportIndex*height }
        else if (viewportIndex===viewerIndexToBeMaximized) { throw "Viewport to be minimized and viewport to be maximized cannot be the same" }
        else { y = (viewportIndex-1)*height }
        break;
    case 99:
        var proportion = 0.1
        x = (viewportIndex%numberOfColumns)*1/numberOfColumns*proportion;
        y = Math.floor(viewportIndex/numberOfColumns)*1/numberOfRows*proportion;
        width = 1/numberOfColumns*proportion;
        height = 1/numberOfRows*proportion;
        break;
    }

    return [x,y,width,height]
}
