import Qt3D.Core 2.4
import Qt3D.Render 2.4
import Qt3D.Extras 2.4
import QtQuick
import Qt3D.Input 2.4

import libViewer

Entity {
    id: root
    property bool myEnabled: false

    property var viewerLayers:[]
    property var renderLayers:[]
    property vector3d spacing: Qt.vector3d(1,1,1)
    property vector3d dimension: Qt.vector3d(10,10,10)
    property vector3d dimensionTimesSpacing: Qt.vector3d(10,10,10)
    property vector3d origin: Qt.vector3d(0,0,0)
    property matrix4x4 orientation: Qt.matrix4x4(1,0,0,0,
                                                 0,1,0,0,
                                                 0,0,1,0,
                                                 0,0,0,1)
    property DirectionalLight directionalLight
    property vector3d lightPosition: Qt.vector3d(0,0,0)
    readonly property alias windowLevel: volumeRenderingEffect.windowLevel
    readonly property alias windowWidth: volumeRenderingEffect.windowWidth
    property alias type: volumeRenderingEffect.type
    property alias volRenderingTransferFunctionType: volumeRenderingEffect.volRenderingTransferFunctionType
    property VolumeImageDataQml volumeImageData:null

    components: {
        if(!root.myEnabled) {return []}
        var myArray=[mesh,material,transform];

        for(var i=0;i<root.viewerLayers.length;i++) {
            myArray.push(root.viewerLayers[i]);
        }
        for(var j=0;j<root.renderLayers.length;j++) {
            myArray.push(root.renderLayers[j]);
        }
        return myArray;
    }

    function setVolumeRenderingWindowLevelWidth(level,width) {
        volumeRenderingEffect.windowLevel += level
        var wtmp = volumeRenderingEffect.windowWidth+width;
        volumeRenderingEffect.windowWidth = Math.max(wtmp,0)
    }

    function updateVolumeRenderingEffect() {
        if(!root.volumeImageData) {
            return;
        }

        textureImage3D.dimension = volumeImageData.get_dimension();
        textureImage3D.spacing = volumeImageData.get_spacing();
        textureImage3D.dimensionTimesSpacing = volumeImageData.get_dimension_times_spacing();
        textureImage3D.origin = volumeImageData.get_origin();
        textureImage3D.orientation = volumeImageData.get_orientation();
        textureImage3D.channels = volumeImageData.get_channels();
        textureImage3D.textureData = volumeImageData.get_texture_data();

        //volumeRenderingEffect.precomputedGradient = volumeImageData.get_texture_gradient_precomputed();
        //if(volumeImageData.get_texture_gradient_precomputed()) {
        //    textureImage3DGradient.textureData = volumeImageData.get_texture_gradient();
        //    textureImage3DGradient.dimension = volumeImageData.get_dimension();
        //}

        volumeRenderingEffect.dimension = volumeImageData.get_dimension();
        volumeRenderingEffect.spacing = volumeImageData.get_spacing();
        volumeRenderingEffect.dimensionTimesSpacing = volumeImageData.get_dimension_times_spacing();
        volumeRenderingEffect.origin = volumeImageData.get_origin();
        volumeRenderingEffect.orientation = volumeImageData.get_orientation();

        root.dimension = volumeImageData.get_dimension();
        root.spacing = volumeImageData.get_spacing();
        root.dimensionTimesSpacing = volumeImageData.get_dimension_times_spacing();
        root.origin = volumeImageData.get_origin();
        root.orientation = volumeImageData.get_orientation();
    }

    VolumeRenderingEffectRayTracing {
        id: volumeRenderingEffect
        renderLayers: root.renderLayers
        threeDTexture: Texture3D { minificationFilter: Texture.Linear; magnificationFilter: Texture.Linear; textureImages: TextureImage3D { id: textureImage3D } }
        lightColor: root.directionalLight.color
        lightDirection: root.directionalLight.worldDirection
        lightIntensity: root.directionalLight.intensity
        lightPosition: root.lightPosition
    }

    Transform {
        id: transform
    }
//    Material {
//        id: materialc
//        effect: MetalRoughClippingEffect {
//            baseColor:Qt.rgba(0,1,0,1)
//            overlayColor: Qt.rgba(0,0,1,1)
//            //clipPlaneEquation: clippingPlanes.clipPlaneEntities.length > 0 ? clippingPlanes.clipPlaneEntities[0].equation : null
//            viewportVisibility: ViewportVisibility {
//                threeDViewportsVisibility:false
//                sliceViewportsVisibility:false
//                sliceViewportRenderingMode:SliceViewportRenderingModeClass.OVERLAY
//            }
//        }
//    }

    VolumeShadedMaterial {
        id: material
        effect: volumeRenderingEffect
    }

    //SphereMesh {
    //    id:meshc
    //    radius:{
    //        console.log("sphere radius: "+Math.max(Math.max(root.dimensionTimesSpacing.x,root.dimensionTimesSpacing.y),root.dimensionTimesSpacing.z)/2.0)
    //        return Math.max(Math.max(root.dimensionTimesSpacing.x,root.dimensionTimesSpacing.y),root.dimensionTimesSpacing.z)/2.0;
    //    }
    //}

    CuboidMesh {
        id: mesh
        xExtent: root.dimensionTimesSpacing.x
        yExtent: root.dimensionTimesSpacing.y
        zExtent: root.dimensionTimesSpacing.z
    }
}
