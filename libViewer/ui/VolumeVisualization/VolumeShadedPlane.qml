import Qt3D.Core 2.6
import Qt3D.Render 2.6
import Qt3D.Extras 2.6

import "../Point"
import "../Plane"

import libViewer

/* Plane (XY-plane) shaded with an effect (volume texture slicing)
 * Crosshair picking is implemented here, a dot is displayed to indicate the mouse position
 * Picking is implemented here, since the line is infinitely small, and picking was not (yet)
 * successfully implemented. */
Entity {
    id: root

    property alias effect: material.effect
    property var sliceViewerLayers:[]
    property var threeDViewerLayer:[]
    property var sliceLayers:[]
    property var renderLayers: []
    property var noDepthTestLayer:[]
    property VolumeShadedPlaneSettings volumeShadedPlaneSettings: VolumeShadedPlaneSettings{}

    property bool isCrosshairPicking: false; // this entity sets the state isCrosshairPicking, i.e., whether the user is currently moving the crosshair around (transl or rot)

    signal planePositionChanged(vector3d localPosition)
    signal planeOrientationChanged(real angleInDeg)
    signal spatialObjectPicked(vector3d worldIntersection,vector3d localIntersection)
    signal spatialObjectHover(vector3d worldIntersection,vector3d localIntersection)


    function updateMouseIndicator(pick) {
        if((Math.abs(pick.localIntersection.x)<=root.volumeShadedPlaneSettings.crosshairInteractionDimensions.width*root.volumeShadedPlaneSettings.pickingShiftProportion/2.0 &&
            Math.abs(pick.localIntersection.y)<=root.volumeShadedPlaneSettings.crosshairInteractionDimensions.height*root.volumeShadedPlaneSettings.pickingPerpendicularExtension/2.0)) { // horizontal shift

            mouseIndicatorTransform.translation = Qt.vector3d(pick.localIntersection.x,0,0);
            pointEffect.color=Qt.rgba(0,1,0,1)
            mouseIndicatorPoint.color=Qt.rgba(0,1,0,1)

        } else if((Math.abs(pick.localIntersection.x)<=root.volumeShadedPlaneSettings.crosshairInteractionDimensions.width*root.volumeShadedPlaneSettings.pickingPerpendicularExtension/2.0) &&
                  (Math.abs(pick.localIntersection.y)<=root.volumeShadedPlaneSettings.crosshairInteractionDimensions.height*root.volumeShadedPlaneSettings.pickingShiftProportion/2.0) &&
                  (Math.abs(pick.localIntersection.y)>root.volumeShadedPlaneSettings.crosshairInteractionDimensions.height*root.volumeShadedPlaneSettings.pickingPerpendicularExtension/2.0)) { // vertical shifting

            mouseIndicatorTransform.translation = Qt.vector3d(0,pick.localIntersection.y,0);
            pointEffect.color=Qt.rgba(0,1,0,1)
            mouseIndicatorPoint.color=Qt.rgba(0,1,0,1)

        } else if((Math.abs(pick.localIntersection.x)>root.volumeShadedPlaneSettings.crosshairInteractionDimensions.width*root.volumeShadedPlaneSettings.pickingShiftProportion/2.0 &&
                   Math.abs(pick.localIntersection.y)<root.volumeShadedPlaneSettings.crosshairInteractionDimensions.height*root.volumeShadedPlaneSettings.pickingPerpendicularExtension/2.0)) { // rotating horizontal line
            mouseIndicatorTransform.translation = Qt.vector3d(pick.localIntersection.x,0,0);
            pointEffect.color=Qt.rgba(0,0,1,1)
            mouseIndicatorPoint.color=Qt.rgba(0,0,1,1)

        } else if((Math.abs(pick.localIntersection.y)>root.volumeShadedPlaneSettings.crosshairInteractionDimensions.height*root.volumeShadedPlaneSettings.pickingShiftProportion/2.0 &&
                   Math.abs(pick.localIntersection.x)<root.volumeShadedPlaneSettings.crosshairInteractionDimensions.width*root.volumeShadedPlaneSettings.pickingPerpendicularExtension/2.0)) { // rotating vertical line
            mouseIndicatorTransform.translation = Qt.vector3d(0,pick.localIntersection.y,0);
            pointEffect.color=Qt.rgba(0,0,1,1)
            mouseIndicatorPoint.color=Qt.rgba(0,0,1,1)

        } else {
        }
    }

    function identifyCrosshairPickSection(pick) {
        if((Math.abs(pick.localIntersection.x)<=root.volumeShadedPlaneSettings.crosshairInteractionDimensions.width*root.volumeShadedPlaneSettings.pickingShiftProportion/2.0 &&
            Math.abs(pick.localIntersection.y)<=root.volumeShadedPlaneSettings.crosshairInteractionDimensions.height*root.volumeShadedPlaneSettings.pickingPerpendicularExtension/2.0) ||
           (Math.abs(pick.localIntersection.y)<=root.volumeShadedPlaneSettings.crosshairInteractionDimensions.height*root.volumeShadedPlaneSettings.pickingShiftProportion/2.0 &&
            Math.abs(pick.localIntersection.x)<=root.volumeShadedPlaneSettings.crosshairInteractionDimensions.width*root.volumeShadedPlaneSettings.pickingPerpendicularExtension/2.0)) {
            // shifting, i.e., picking in center

            root.isCrosshairPicking = true;
            objectPicker.shiftOrRotation = 0;
            objectPicker.lastPosition = pick.worldIntersection;

        } else if((Math.abs(pick.localIntersection.x)>root.volumeShadedPlaneSettings.crosshairInteractionDimensions.width*root.volumeShadedPlaneSettings.pickingShiftProportion/2.0 &&
                   Math.abs(pick.localIntersection.y)<root.volumeShadedPlaneSettings.crosshairInteractionDimensions.height*root.volumeShadedPlaneSettings.pickingPerpendicularExtension/2.0)) {
            // rotating horizontal line

            root.isCrosshairPicking = true;
            objectPicker.shiftOrRotation = 1;
            objectPicker.horizontalOrVertical = 0;
            if(pick.localIntersection.x<0) {
                objectPicker.leftOrRight=0;
            } else {
                objectPicker.leftOrRight=1;
            }

        } else if((Math.abs(pick.localIntersection.y)>root.volumeShadedPlaneSettings.crosshairInteractionDimensions.height*root.volumeShadedPlaneSettings.pickingShiftProportion/2.0 &&
                   Math.abs(pick.localIntersection.x)<root.volumeShadedPlaneSettings.crosshairInteractionDimensions.width*root.volumeShadedPlaneSettings.pickingPerpendicularExtension/2.0)) {
            // rotating vertical line

            root.isCrosshairPicking = true;
            objectPicker.shiftOrRotation = 1;
            objectPicker.horizontalOrVertical = 1;
            if(pick.localIntersection.y<0) {
                objectPicker.lowerOrUpper=1; //upper
            } else {
                objectPicker.lowerOrUpper=0; // lower
            }
        } else {
            root.isCrosshairPicking = false;
        }
    }

    Entity {
        components: {
            var myArray=[mesh,material];

            if(root.volumeShadedPlaneSettings.crosshairInteractionEnabled || root.volumeShadedPlaneSettings.planePickingEnabled) {
                myArray.push(objectPicker)
            }

            for(var i=0;i<root.sliceViewerLayers.length;i++) {
                myArray.push(root.sliceViewerLayers[i]);
            }
            for(var k=0;k<root.threeDViewerLayer.length;k++) {
                myArray.push(root.threeDViewerLayer[k]);
            }

            for(var l=0;l<root.sliceLayers.length;l++) {
                myArray.push(root.sliceLayers[l]);
            }

            for(var j=0;j<root.renderLayers.length;j++) {
                myArray.push(root.renderLayers[j]);
            }

            console.log("VolumeShadedPlane return myArray")
            console.log("VolumeShadedPlane return myArray1")

            return myArray;
        }


        VolumeShadedMaterial {
            id: material
            borderColor: root.volumeShadedPlaneSettings.color
            alpha: root.volumeShadedPlaneSettings.alpha
        }


        //SphereMesh {
        //    id: mesh1
        //    radius:60
        //}
        //PlaneMesh {
        //    id:mesh11
        //    height: 60
        //    width: 60
        //}

        Plane {
            id: mesh
            width: root.volumeShadedPlaneSettings.planeWidth
            height: root.volumeShadedPlaneSettings.planeHeight
            onWidthChanged: {
                console.log("VolumeShadedplane width changed to "+width)
                console.log("VolumeShadedplane height changed to "+height)
            }
        }

        ObjectPicker {
            id: objectPicker
            property int shiftOrRotation:0 //0: shift, 1: rotation
            property real horizontalOrVertical: 0 //0: horizontal, 1: vertical
            property real lowerOrUpper: 0 //0: lower, 1: upper
            property real leftOrRight:0 // 0: left, 1: right
            property vector3d lastPosition: Qt.vector3d(0,0,0)

            property vector3d pickedWorldPosition:Qt.vector3d(0,0,0)
            property vector3d pickedLocalPosition:Qt.vector3d(0,0,0)
            property bool isPressed:false
            property bool hasMoved:false
            property int pressedButton:-1

            hoverEnabled: true
            dragEnabled: true

            onPressed:function(pick) {
                console.log("volumeShadedPlane onPressed")

                objectPicker.pressedButton = pick.button
                if(pick.buttons==1) { // Left button

                    objectPicker.pickedWorldPosition = pick.worldIntersection;
                    objectPicker.pickedLocalPosition = pick.localIntersection;
                    objectPicker.hasMoved = false;
                    objectPicker.isPressed = true;

                    if(root.volumeShadedPlaneSettings.crosshairInteractionEnabled) {
                        identifyCrosshairPickSection(pick)
                    }

                }
            }
            onReleased:function(pick) {
                // check if left-clicked (not moved and pressed and released with left mouse button)
                // If so, send position of picked point (local and world)
                console.log("volumeShadedPlane onPressed")

                // plane picking in 3d and slice viewers to get coordinates of picked position
                if(root.volumeShadedPlaneSettings.planePickingEnabled) {
                    root.spatialObjectPicked(objectPicker.pickedWorldPosition,objectPicker.pickedLocalPosition)
                }

                if(root.volumeShadedPlaneSettings.crosshairInteractionEnabled) {
                   root.isCrosshairPicking = false;
                    if(pick.accepted && objectPicker.pressedButton==1/*left button*/) {
                        objectPicker.isPressed = false;
                        if(objectPicker.hasMoved==false) {
                            // do nothing


                        }
                        objectPicker.hasMoved = false
                    }
                }
            }

            onMoved: function(pick) {
                console.log("volumeShadedPlane onMoved")
                objectPicker.hasMoved = true;

                // plane picking in 3d and slice viewers to get coordinates of picked position
                if(root.volumeShadedPlaneSettings.planePickingEnabled) {
                    root.spatialObjectHover(pick.worldIntersection,pick.localIntersection)
                }

                if(root.volumeShadedPlaneSettings.crosshairInteractionEnabled) {

                    if(!root.isCrosshairPicking) {
                        updateMouseIndicator(pick)
                    }

                    if(pick.buttons===1 && root.isCrosshairPicking) { // Left mouse button

                        if(objectPicker.shiftOrRotation===0) { // shift

                            var dPosition = pick.worldIntersection.minus(objectPicker.lastPosition)
                            objectPicker.lastPosition = pick.worldIntersection
                            root.planePositionChanged(dPosition); // in world cos, the relative shift
                        }

                        if(objectPicker.shiftOrRotation===1) { // rotation
                            var rad = 0;

                            if(objectPicker.horizontalOrVertical===0) { // horizontal

                                if(objectPicker.leftOrRight===0) { // left
                                    rad = Math.atan2(pick.localIntersection.y,pick.localIntersection.x)+Math.PI;
                                } else {
                                    rad = Math.atan2(pick.localIntersection.y,pick.localIntersection.x);
                                }

                            } else { // vertical

                                if(objectPicker.lowerOrUpper===0) { // lower
                                    rad = Math.atan2(pick.localIntersection.y,pick.localIntersection.x);
                                    rad = rad-Math.PI/2.0

                                } else {
                                    rad = Math.atan2(pick.localIntersection.y,pick.localIntersection.x)+Math.PI/2.0;
                                }
                            }

                            root.planeOrientationChanged(rad/Math.PI*180.0);
                        }
                    }
                }
            }
        }
    }
    Entity {
        components: root.volumeShadedPlaneSettings.crosshairEnabled ? [mouseIndicatorTransform] :[]
        Transform {id: mouseIndicatorTransform }

        PointShaderEffect {
            id: pointEffect
            distanceClippingEnabled: false
            size: 10.0
            color: "green"
            depthTesting: false
        }
        Points {
            id:mouseIndicatorPoint
            points: [Qt.vector3d(0,0,0)]
            viewerLayers: root.sliceViewerLayers
            renderLayers: root.noDepthTestLayer
            picking: false
            effect: pointEffect
        }
    }
}
