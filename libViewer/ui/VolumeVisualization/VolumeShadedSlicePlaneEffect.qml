import Qt3D.Core 2.4
import Qt3D.Render 2.4
import Qt3D.Render 2.15
import Qt3D.Extras 2.4

import libViewer

Effect {
    id: root

    readonly property int numberOfSupportedTextures:8
    property string vertex: "qrc:/libViewer/ui/VolumeVisualization/shader/gl3/VolumeShaderSlicePlane.vert"
    property string fragment: "qrc:/libViewer/ui/VolumeVisualization/shader/gl3/VolumeShaderSlicePlane.frag"

    property bool depthTesting: true

    property color borderColor: Qt.rgba(1,0,0,1)
    property real borderWidth: 1.25 // in %
    property real alpha: 1

    property real windowLevel:500
    property real windowWidth:5000
    property real lightIntensity: 1.0//,Qt.vector3d(-20,-30,-40),Qt.vector3d(0,0,0),Qt.vector3d(0,0,0)]
    property color lightColor: Qt.rgba(1.0,1.0,1.0,1.0)//,Qt.vector3d(-20,-30,-40),Qt.vector3d(0,0,0),Qt.vector3d(0,0,0)]

    readonly property FilterKeyCatalogue filterKeyCatalogue: FilterKeyCatalogue{}

    readonly property int interpolationMode: Texture.Linear
    property list<Texture3D> threeDTextures: [
            Texture3D { id: texture3D0; minificationFilter: root.interpolationMode; magnificationFilter: root.interpolationMode; textureImages:TextureImage3D {}},
            Texture3D { id: texture3D1; minificationFilter: root.interpolationMode; magnificationFilter: root.interpolationMode; textureImages:TextureImage3D {}},
            Texture3D { id: texture3D2; minificationFilter: root.interpolationMode; magnificationFilter: root.interpolationMode; textureImages:TextureImage3D {}},
            Texture3D { id: texture3D3; minificationFilter: root.interpolationMode; magnificationFilter: root.interpolationMode; textureImages:TextureImage3D {}},
            Texture3D { id: texture3D4; minificationFilter: root.interpolationMode; magnificationFilter: root.interpolationMode; textureImages:TextureImage3D {}},
            Texture3D { id: texture3D5; minificationFilter: root.interpolationMode; magnificationFilter: root.interpolationMode; textureImages:TextureImage3D {}},
            Texture3D { id: texture3D6; minificationFilter: root.interpolationMode; magnificationFilter: root.interpolationMode; textureImages:TextureImage3D {}},
            Texture3D { id: texture3D7; minificationFilter: root.interpolationMode; magnificationFilter: root.interpolationMode; textureImages:TextureImage3D {}}]

    property var windowLevelWidth: [Qt.vector2d(500,5000),Qt.vector2d(500,5000),Qt.vector2d(500,5000),Qt.vector2d(500,5000),
                        Qt.vector2d(500,5000),Qt.vector2d(500,5000),Qt.vector2d(500,5000),Qt.vector2d(500,5000)]//500
    property var activeTexture: [false,false,false,false,false,false,false,false]
    property var textureVisibility: [true,true,true,true,true,true,true,true]
    property var spacing: [Qt.vector3d(1,1,1),Qt.vector3d(1,1,1),Qt.vector3d(1,1,1),Qt.vector3d(1,1,1),
        Qt.vector3d(1,1,1),Qt.vector3d(1,1,1),Qt.vector3d(1,1,1),Qt.vector3d(1,1,1)]
    property var dimension: [Qt.vector3d(1,1,1),Qt.vector3d(1,1,1),Qt.vector3d(1,1,1),Qt.vector3d(1,1,1),
        Qt.vector3d(1,1,1),Qt.vector3d(1,1,1),Qt.vector3d(1,1,1),Qt.vector3d(1,1,1)]
    property var dimensionTimesSpacing: [Qt.vector3d(0,0,0),Qt.vector3d(0,0,0),Qt.vector3d(0,0,0),Qt.vector3d(0,0,0),
        Qt.vector3d(0,0,0),Qt.vector3d(0,0,0),Qt.vector3d(0,0,0),Qt.vector3d(0,0,0)]
    property var origin: [Qt.vector3d(0,0,0),Qt.vector3d(0,0,0),Qt.vector3d(0,0,0),Qt.vector3d(0,0,0),
        Qt.vector3d(0,0,0),Qt.vector3d(0,0,0),Qt.vector3d(0,0,0),Qt.vector3d(0,0,0)]
    property var orientation: [Qt.matrix4x4(),Qt.matrix4x4(),Qt.matrix4x4(),Qt.matrix4x4(),
        Qt.matrix4x4(),Qt.matrix4x4(),Qt.matrix4x4(),Qt.matrix4x4()]

    // Highlighting
    property bool highlightRange: true
    property real upperHighlightThreshold: 3000
    property real lowerHighlightThreshold: 300
    property color highlightColor: Qt.rgba(0,0,1,1)


    function setWindowLevelWidth(level,width) {
        for(var i=0;i<root.numberOfSupportedTextures;i++) {
            var wtmp = root.windowLevelWidth[i].y+width;
            root.windowLevelWidth[i] = Qt.vector2d(root.windowLevelWidth[i].x+level,Math.max(wtmp,0));
        }

        root.windowLevel = root.windowLevelWidth[0].x;
        root.windowWidth = root.windowLevelWidth[0].y;

        root.windowLevelWidthChanged();
    }

    parameters: [
        Parameter { name: "borderColor"; value: Qt.vector3d(root.borderColor.r, root.borderColor.g, root.borderColor.b) },
        Parameter { name: "borderWidth"; value: root.borderWidth },
        Parameter { name: "lightIntensity"; value: root.lightIntensity },
        Parameter { name: "lightColor"; value: Qt.vector3d(root.lightColor.r, root.lightColor.g, root.lightColor.b) },
        Parameter { name: "alpha"; value: root.alpha },
        Parameter { name: "windowLevelWidth[0]"; value: root.windowLevelWidth },
        Parameter { name: "activeTexture[0]"; value: root.activeTexture },
        Parameter { name: "textureVisibility[0]"; value: root.textureVisibility },
        Parameter { name: "origin[0]"; value: root.origin },
        Parameter { name: "orientation[0]"; value: root.orientation },
        Parameter { name: "dimension[0]"; value: root.dimension },
        Parameter { name: "spacing[0]"; value: root.spacing },
        Parameter { name: "dimensionTimesSpacing[0]"; value: root.dimensionTimesSpacing },
        Parameter { name: "highlightRange"; value: root.highlightRange },
        Parameter { name: "upperHighlightThreshold"; value: root.upperHighlightThreshold },
        Parameter { name: "lowerHighlightThreshold"; value: root.lowerHighlightThreshold },
        Parameter { name: "highlightColor"; value: root.highlightColor },
        Parameter { name: "texture3D0"; value: root.threeDTextures[0] },
        Parameter { name: "texture3D1"; value: root.threeDTextures[1] },
        Parameter { name: "texture3D2"; value: root.threeDTextures[2] },
        Parameter { name: "texture3D3"; value: root.threeDTextures[3] },
        Parameter { name: "texture3D4"; value: root.threeDTextures[4] },
        Parameter { name: "texture3D5"; value: root.threeDTextures[5] },
        Parameter { name: "texture3D6"; value: root.threeDTextures[6] },
        Parameter { name: "texture3D7"; value: root.threeDTextures[7] }
    ]

    techniques: [
        Technique {
            filterKeys: [FilterKey { name: "renderingStyle"; value: "forward" }]

            graphicsApiFilter {
                api: GraphicsApiFilter.OpenGL
                profile: GraphicsApiFilter.CoreProfile
                majorVersion: 3
                minorVersion: 1
            }
            RenderPass {
                id: renderPassDefault

                filterKeys: [root.filterKeyCatalogue.threeDViewerOpaqueModelFilterKey,
                    root.filterKeyCatalogue.threeDViewerTransparentModelFilterKey,
                    root.filterKeyCatalogue.sliceViewerOpaqueModelFilterKey,
                    root.filterKeyCatalogue.sliceViewerTransparentModelFilterKey
                ]
                shaderProgram: ShaderProgram {
                     id: gl3Shader
                     vertexShaderCode: loadSource(root.vertex)
                     fragmentShaderCode:loadSource(root.fragment)
                 }
                renderStates: [
                    CullFace { mode: CullFace.NoCulling },
                    DepthTest { depthFunction: root.depthTesting ? DepthTest.Less :DepthTest.Always },
                    AlphaCoverage {},
                    MultiSampleAntiAliasing {}
                ]
            }
            renderPasses:[renderPassDefault]
        }
    ]
}

