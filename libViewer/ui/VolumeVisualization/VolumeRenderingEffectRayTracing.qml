import Qt3D.Core 2.4
import Qt3D.Render 2.4

import libViewer

import ".."

Effect {
    id: root

    //! [1]
    property string vertex: "qrc:/libViewer/ui/VolumeVisualization/shader/gl3/VolumeRenderingRayTracingShader.vert"
    property string fragment: "qrc:/libViewer/ui/VolumeVisualization/shader/gl3/VolumeRenderingRayTracingShader.frag"
    //! [1]

    property var renderLayers: []
    property bool depthTesting: true

    property real windowWidth: 1000
    property real windowLevel: 500
    property real type: 0//0: normal, 1:mip
    property real volRenderingTransferFunctionType: 0//0: ramp , 1:ramp up and down

    property Texture3D threeDTexture
    property real minValue: -1.0;
    property real maxValue: 1.0;
    property vector3d spacing: Qt.vector3d(1,1,1)
    property vector3d dimension: Qt.vector3d(10,10,10)
    property vector3d dimensionTimesSpacing: Qt.vector3d(10,10,10)
    property vector3d origin: Qt.vector3d(0,0,0)
    property matrix4x4 orientation: Qt.matrix4x4(1,0,0,0,
                                                 0,1,0,0,
                                                 0,0,1,0,
                                                 0,0,0,1)
    property var lightEnabled: true
    property vector3d lightPosition: Qt.vector3d(0,0,0)
    property vector3d lightDirection: Qt.vector3d(0.25,0,-1)
    property real lightIntensity: 1.0
    property color lightColor: Qt.rgba(1.0,1.0,1.0,1.0)
    property color ambientColor: Qt.rgba(0.89/10.0,0.85/10.0,0.79/10.0, 1.0)
    property color specularColor: Qt.rgba(1,1,1,1.0)
    property real shininess: 250.0
    readonly property FilterKeyCatalogue filterKeyCatalogue: FilterKeyCatalogue{}


    Texture1D {
        id: transferFunction
        textureImages:
        TextureImage1D {
            colors: {
                switch(root.volRenderingTransferFunctionType) {
                case 0: return [Qt.rgba(0.7,0.2,0.2,0.0),Qt.rgba(0.7,0.62,0.5,0.8),Qt.rgba(0.85,0.72,0.7,1.0)]
                //case 0: return [Qt.rgba(0.8,0.1,0.1,0.0),Qt.rgba(0.89,0.85,0.79,1.0)]
                case 1: return [Qt.rgba(0.8,0.0,0.0,0.0),Qt.rgba(0.89,0.85,0.79,1.0),Qt.rgba(0.89,0.85,0.79,1.0),Qt.rgba(0.89,0.85,0.79,1.0),Qt.rgba(0.89,0.85,0.79,0.0)]
                }
            }
        }
    }


    FilterKey {
        id: forward
        name: "renderingStyle"
        value: "forward"
    }

    //! [2]
    ShaderProgram {
        id: gl3Shader
        vertexShaderCode: loadSource(parent.vertex)
        fragmentShaderCode: loadSource(parent.fragment)
    }

    parameters: [
        Parameter { name: "windowWidth"; value: root.windowWidth },
        Parameter { name: "windowLevel"; value: root.windowLevel },
        Parameter { name: "spacing"; value: root.spacing },
        Parameter { name: "dimension"; value: root.dimension },
        Parameter { name: "origin"; value: root.origin },
        Parameter { name: "orientation"; value: root.orientation },
        Parameter { name: "dimensionTimesSpacing"; value: root.dimensionTimesSpacing },
        Parameter { name: "lightEnabled";  value: root.lightEnabled },
        Parameter { name: "lightPosition";  value: root.lightPosition },
        Parameter { name: "lightDirection";  value: root.lightDirection },
        Parameter { name: "lightIntensity"; value: root.lightIntensity },
        Parameter { name: "lightColor"; value: Qt.vector3d(root.lightColor.r, root.lightColor.g, root.lightColor.b) },
        Parameter { name: "ka"; value: Qt.vector3d(root.ambientColor.r, root.ambientColor.g, root.ambientColor.b) },
        Parameter { name: "ks"; value: Qt.vector3d(root.specularColor.r, root.specularColor.g, root.specularColor.b) },
        Parameter { name: "shininess"; value: root.shininess },
        Parameter { name: "transferFunction"; value: transferFunction },
        Parameter { name: "texture3D"; value: root.threeDTexture }
    ]

    techniques: [
        Technique {
            filterKeys: [FilterKey { name: "renderingStyle"; value: "forward" }]

            graphicsApiFilter {
                api: GraphicsApiFilter.OpenGL
                profile: GraphicsApiFilter.CoreProfile
                majorVersion: 3
                minorVersion: 1
            }
            renderPasses: RenderPass {
                filterKeys: [root.filterKeyCatalogue.threeDViewerOpaqueModelFilterKey,
                root.filterKeyCatalogue.threeDViewerTransparentModelFilterKey]
                shaderProgram: gl3Shader
                renderStates: [
                    CullFace { mode: CullFace.NoCulling },
                    DepthTest { depthFunction: root.depthTesting ? DepthTest.Less :DepthTest.Always },
                    AlphaCoverage {},
                    MultiSampleAntiAliasing {},
                    BlendEquation{ blendFunction: {
                            switch(root.type) {
                            case 0: return BlendEquation.Add;
                            case 1: return BlendEquation.ReverseSubtract;
                            case 2: return BlendEquation.Min;
                            }
                        }
                    }
                ]
            }
        }
    ]
}

