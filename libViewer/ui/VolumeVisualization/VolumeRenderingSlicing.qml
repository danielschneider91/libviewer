import Qt3D.Core 2.4
import Qt3D.Render 2.4
import Qt3D.Extras 2.4
import QtQuick
import Qt3D.Input 2.4

import libViewer

import "VolumeImageHelper.js" as VolumeImageHelper
import "../Plane"

Entity {
    id: root
    property bool myEnabled: true

    property alias effect: material.effect
    property var viewerLayers:[]
    property var renderLayers:[]
    property int planeWidth: 5
    property int planeHeight: 10
    property color color: "red"
    property real alpha: 1
    property vector3d spacing: Qt.vector3d(1,1,1)
    property vector3d dimension: Qt.vector3d(10,10,10)
    property vector3d dimensionTimesSpacing: Qt.vector3d(10,10,10)
    property vector3d origin: Qt.vector3d(0,0,0)
    property matrix4x4 orientation: Qt.matrix4x4(1,0,0,0,
                                                 0,1,0,0,
                                                 0,0,1,0,
                                                 0,0,0,1)


    components: {
        if(!root.myEnabled) {return [];}
        var myArray=[mesh,material,transform];

        for(var i=0;i<root.viewerLayers.length;i++) {
            myArray.push(root.viewerLayers[i]);
        }
        for(var j=0;j<root.renderLayers.length;j++) {
            myArray.push(root.renderLayers[j]);
        }
        return myArray;
    }

    function getMaxExtent() {
        return VolumeImageHelper.getMaximumExtent(root.dimensionTimesSpacing)
    }
    function getMinimumSpacing() {
        var minSpacing = 100000;
        if(root.spacing.x<minSpacing)
            minSpacing = root.spacing.x

        if(root.spacing.y<minSpacing)
            minSpacing = root.spacing.y

        if(root.spacing.z<minSpacing)
            minSpacing = root.spacing.z

        return minSpacing
    }

    function getNumberOfPlanes() {
        var maxExtent = getMaxExtent();
        var minimumSpacing = getMinimumSpacing()
        return maxExtent/minimumSpacing*2
    }
    function getVolumeCenter() {
        return root.origin.plus(root.dimensionTimesSpacing.times(0.5))
    }

    Transform {
        id: transform
    }

    PhongMaterial {
        id: material1
        diffuse: root.color
        specular: Qt.rgba(1,1,1,1)
    }

    VolumeShadedMaterial {
        id: material
        borderColor: root.color
        alpha: root.alpha
    }

    Planes {
        id: mesh
        position: Qt.vector3d(0,0,0)//getVolumeCenter()
        width: root.planeWidth
        height: root.planeHeight
        numberOfPlanesMinusOne: {
            return Math.ceil(getNumberOfPlanes()/3.0)
        }
        zExtent: getMaxExtent()
    }

    PlaneMesh { // in X-Z plane
        id: mesh1
        width: root.planeWidth
        height: root.planeHeight
        meshResolution: Qt.size(20, 20)
    }
}
