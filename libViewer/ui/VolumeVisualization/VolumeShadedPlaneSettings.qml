import QtQuick 2.0

Item {
    property color color: "red"
    property real alpha: 1
    property string name: "Default"

    property real planeWidth: 310*2
    property real planeHeight: 310*2

    property real pickingPerpendicularExtension: 0.2 // how wide around the crosshair the user can pick, in%/100
    property real pickingShiftProportion: 0.5 // how much is for translation and how much for rotation, in%/100, pickingRotationProportion = 1-pickingShiftProportion
    property bool crosshairEnabled: true // whether crosshairs are enabled (visible)
    property bool crosshairInteractionEnabled: true // whether planes are used to interact with crosshairs via picking, this is different from planePickingEnabled in that here it is all
    // about interacting with the crosshairs which happens by picking the plane whereas in planePickingEnabled it is about picking a position on the plane which then can be used by the
    // user, e.g. to define landmarks
    property size crosshairInteractionDimensions: Qt.size(100,100) // in units of the 3d scene/camera

    property bool planePickingEnabled:false // whether planes are subject to picking in by user
}
