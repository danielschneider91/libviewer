function getMaximumExtent(dimensionTimesSpacing) {
    return Math.sqrt(dimensionTimesSpacing.x*dimensionTimesSpacing.x+
                     dimensionTimesSpacing.y*dimensionTimesSpacing.y+
                     dimensionTimesSpacing.z*dimensionTimesSpacing.z)
}

function isNormalizedImagePositionWithinVolumeImage(position) {
    if(position.x>=0 && position.x<= 1 &&
        position.y>=0 && position.y<= 1 &&
        position.z>=0 && position.z<= 1) {
        return true;
    } else {
        return false;
    }
}
