#version 330 core

in vec3 worldPosition;
in vec3 worldNormal;

out vec4 fragColor;

uniform vec3 eyePosition;

uniform vec3 borderColor;
uniform float borderWidth;
uniform float lightIntensity;
uniform vec3 lightColor;
uniform float alpha;

uniform sampler3D texture3D0;
uniform sampler3D texture3D1;
uniform sampler3D texture3D2;
uniform sampler3D texture3D3;
uniform sampler3D texture3D4;
uniform sampler3D texture3D5;
uniform sampler3D texture3D6;
uniform sampler3D texture3D7;

uniform bool activeTexture[8];
uniform bool textureVisibility[8];
uniform vec3 dimension[8];
uniform vec3 spacing[8];
uniform vec3 dimensionTimesSpacing[8];
uniform vec3 origin[8]; /*origin of voxel center*/
uniform mat4 orientation[8];

uniform vec2 windowLevelWidth[8];

// highlighting
uniform bool highlightRange;
uniform float upperHighlightThreshold;
uniform float lowerHighlightThreshold;
uniform vec3 highlightColor;


mat4 getPose(mat4 o,vec4 p) {
    mat4 imagePose = o;
    imagePose[3] = p;
    return imagePose;
}

vec3 getNormalizedTextureCoordinates(mat4 imgPose,vec3 worldPosition,vec3 dts) {
    // (T_world<-imageBoundingBox)^(-1)*world_p = imageBoundingBox_p
    // imageBoundingBox_p/dts = imageBoundingBoxNormalized_p
    vec3 c = vec3(inverse(imgPose)*vec4(worldPosition,1.0))/dts;
    return c;
}

bool isInImageVolume(vec3 normalizedTextureCoordinate3d) {
    if(normalizedTextureCoordinate3d.x>1.0||normalizedTextureCoordinate3d.x<0.0||
            normalizedTextureCoordinate3d.y>1.0||normalizedTextureCoordinate3d.y<0.0||
            normalizedTextureCoordinate3d.z>1.0||normalizedTextureCoordinate3d.z<0.0) {
        return false;
    } else {
        return true;
    }
}

bool isInImageBorder(vec3 textureCoordinate3d,float borderWidth,vec3 dts) {
    if((textureCoordinate3d.x>(1-borderWidth/dts.x) && textureCoordinate3d.x<1) ||
       (textureCoordinate3d.x<borderWidth/dts.x && textureCoordinate3d.x>0) ||
       (textureCoordinate3d.y>(1-borderWidth/dts.y) && textureCoordinate3d.y<1) ||
       (textureCoordinate3d.y<borderWidth/dts.y && textureCoordinate3d.y>0) ||
       (textureCoordinate3d.z>(1-borderWidth/dts.z) && textureCoordinate3d.z<1) ||
       (textureCoordinate3d.z<borderWidth/dts.z) && textureCoordinate3d.z>0) {
        return true;
    } else {
        return false;
    }
}

float windowTextureValue(float textureValue, float wl, float ww) {
    int shortMaxVal = 32767;
    if(textureValue*shortMaxVal<=(wl-ww/2.0)) {
        return 0.0;
    } else if(textureValue*shortMaxVal>=(wl+ww/2.0)) {
        return 1.0;
    } else {
        return (textureValue*shortMaxVal-(wl-ww/2.0))/ww; // value between 1 and 0
    }
}

void getTextureColor(in bool activeTexture, in bool textureVisible,in sampler3D volumeTexture,in mat4 orientation,in vec3 origin,in vec3 worldPosition, in vec3 dts, in vec3 spacing, in float wl, in float ww, inout float volTexVal/*value of volume texture, range: [0,1]*/,inout vec3 texClr, inout bool dsc) {
    if(activeTexture && textureVisible) {
        // position in world COS to position in image COS
        vec3 originImage = origin-spacing/2.0;
        mat4 imagePose = getPose(orientation,vec4(originImage,1.0));
        // sample texture
        vec3 texCoord3d = getNormalizedTextureCoordinates(imagePose,worldPosition,dts);
        if(!isInImageVolume(texCoord3d)) {
        } else if(isInImageBorder(texCoord3d,borderWidth,dts)) {
            dsc = false;
            texClr = borderColor;
        } else {
            dsc = false;
            //float volTexVal = texture(volumeTexture, texCoord3d.xyz).r;
            //texClr = vec3(windowTextureValue(texture(volumeTexture, texCoord3d.xyz).r,wl,ww));
            //if(volTexVal>500 && volTexVal <3000) {
            //    texClr.r=texClr.r/2.0;
            //    texClr.g=1.0;
            //    texClr.b=texClr.b/2.0;
            //}
            volTexVal = texture(volumeTexture, texCoord3d.xyz).r;
            texClr = vec3(windowTextureValue(volTexVal,wl,ww));
        }
    }
}

void highlightVoxel(in float volTexVal/*value of volume texture, range: [0,1]*/,in float low, in float high, in vec3 highlightColor, inout vec3 texClr) {
    int shortMaxVal = 32767;
    if(volTexVal*shortMaxVal>low && volTexVal*shortMaxVal<high) {
        texClr = texClr*2+highlightColor;
        texClr=texClr/3.0;
    }
}

void main()
{


    float maxX = 1.0 - borderWidth;
    float minX = borderWidth;
    float maxY = maxX;
    float minY = minX;

    float alphaValue = 1.0;

    vec3 texClrs[8]= vec3[8](
                vec3(0.0),
                vec3(0.0),
                vec3(0.0),
                vec3(0.0),
                vec3(0.0),
                vec3(0.0),
                vec3(0.0),
                vec3(0.0)
              );
    float volTexVals[8] = float[8](0,0,0,0,   0,0,0,0);
    bool discardForAll[8] = bool[8](true,true,true,true,   true,true,true,true);

    getTextureColor(activeTexture[0],textureVisibility[0],texture3D0,orientation[0],origin[0],worldPosition,dimensionTimesSpacing[0],spacing[0],windowLevelWidth[0].x,windowLevelWidth[0].y,volTexVals[0],texClrs[0],discardForAll[0]);
    getTextureColor(activeTexture[1],textureVisibility[1],texture3D1,orientation[1],origin[1],worldPosition,dimensionTimesSpacing[1],spacing[1],windowLevelWidth[1].x,windowLevelWidth[1].y,volTexVals[1],texClrs[1],discardForAll[1]);
    getTextureColor(activeTexture[2],textureVisibility[2],texture3D2,orientation[2],origin[2],worldPosition,dimensionTimesSpacing[2],spacing[2],windowLevelWidth[2].x,windowLevelWidth[2].y,volTexVals[2],texClrs[2],discardForAll[2]);
    getTextureColor(activeTexture[3],textureVisibility[3],texture3D3,orientation[3],origin[3],worldPosition,dimensionTimesSpacing[3],spacing[3],windowLevelWidth[3].x,windowLevelWidth[3].y,volTexVals[3],texClrs[3],discardForAll[3]);
    getTextureColor(activeTexture[4],textureVisibility[4],texture3D4,orientation[4],origin[4],worldPosition,dimensionTimesSpacing[4],spacing[4],windowLevelWidth[4].x,windowLevelWidth[4].y,volTexVals[4],texClrs[4],discardForAll[4]);
    getTextureColor(activeTexture[5],textureVisibility[5],texture3D5,orientation[5],origin[5],worldPosition,dimensionTimesSpacing[5],spacing[5],windowLevelWidth[5].x,windowLevelWidth[5].y,volTexVals[5],texClrs[5],discardForAll[5]);
    getTextureColor(activeTexture[6],textureVisibility[6],texture3D6,orientation[6],origin[6],worldPosition,dimensionTimesSpacing[6],spacing[6],windowLevelWidth[6].x,windowLevelWidth[6].y,volTexVals[6],texClrs[6],discardForAll[6]);
    getTextureColor(activeTexture[7],textureVisibility[7],texture3D7,orientation[7],origin[7],worldPosition,dimensionTimesSpacing[7],spacing[7],windowLevelWidth[7].x,windowLevelWidth[7].y,volTexVals[7],texClrs[7],discardForAll[7]);

    if(highlightRange) {
        highlightVoxel(volTexVals[0],lowerHighlightThreshold, upperHighlightThreshold, highlightColor, texClrs[0]);
        highlightVoxel(volTexVals[1],lowerHighlightThreshold, upperHighlightThreshold, highlightColor, texClrs[1]);
        highlightVoxel(volTexVals[2],lowerHighlightThreshold, upperHighlightThreshold, highlightColor, texClrs[2]);
        highlightVoxel(volTexVals[3],lowerHighlightThreshold, upperHighlightThreshold, highlightColor, texClrs[3]);
        highlightVoxel(volTexVals[4],lowerHighlightThreshold, upperHighlightThreshold, highlightColor, texClrs[4]);
        highlightVoxel(volTexVals[5],lowerHighlightThreshold, upperHighlightThreshold, highlightColor, texClrs[5]);
        highlightVoxel(volTexVals[6],lowerHighlightThreshold, upperHighlightThreshold, highlightColor, texClrs[6]);
        highlightVoxel(volTexVals[7],lowerHighlightThreshold, upperHighlightThreshold, highlightColor, texClrs[7]);
    }

    bool dscForAll = true;
    for(int i=0;i<8;i++) {
        if(discardForAll[i]==false) {
            dscForAll = false;
        }
    }

    if(dscForAll) {
        //fragColor = vec4(0.0,0.0,1.0,1.0);
        discard;
    } else {


        vec3 L = normalize(eyePosition - worldPosition);//world to light, head light
        float diffCoeff = abs(dot(L,worldNormal));// instead of max(dot(L,N),0.0), to make sure that plane is shaded from both sides.
        diffCoeff = max(0.2,diffCoeff);// make sure that it is visible a little bit all the times
        vec3 diffuseColor = lightIntensity*lightColor*diffCoeff;

        vec3 texClr = vec3(0.0);
        for(int i=0;i<8;i++) {
            if(activeTexture[i]) {
                texClr = texClr+texClrs[i];
            }
        }
        fragColor = vec4(diffuseColor*texClr,alpha);//vec4(worldNormal,alpha);
    }
}

