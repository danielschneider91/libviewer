#version 150 core

in vec3 worldPosition;/*position of current vertex in world coordinate system*/
in vec3 imagePosition;/*current vertex/fragment position in model/object coordinate system*/
in vec3 imagePositionNormalized;/*current vertex/fragment position in normalized model/object coordinate system*/

out vec4 fragColor;

uniform sampler3D texture3D;
uniform sampler1D transferFunction;

uniform vec3 dimension;
uniform vec3 spacing;
uniform vec3 origin;
uniform vec3 dimensionTimesSpacing;
uniform mat4 orientation;
uniform vec3 eyePosition;
uniform bool lightEnable;   // light enable flags
uniform vec3 lightPosition; // light positions in world space
uniform vec3 lightDirection;// light direction in world
uniform float lightIntensity;
uniform vec3 lightColor;
uniform vec3 ka;            // Material property: Ambient reflectivity
//uniform vec3 kd;          // Material property: Diffuse reflectivity --> sampled from volume/transfer function
uniform vec3 ks;            // Material property: Specular reflectivity
uniform float shininess;    // Material property: Specular shininess factor

uniform float windowWidth;
uniform float windowLevel;

void blinn_phong(vec3 N, vec3 V, vec3 L,
                 out vec3 diffuseColor,out vec3 specularColor)
{
    diffuseColor = vec3(0.0);
    specularColor = vec3(0.0);

    // diffuse coefficient
    float diff_coeff = max(dot(L,N),0.0);

    // specular coefficient
    vec3 H = normalize(L+V);
    float spec_coeff = pow(max(dot(H,N), 0.0), shininess);
    if (diff_coeff <= 0.0)
        spec_coeff = 0.0;

    diffuseColor += lightIntensity*lightColor*diff_coeff;
    specularColor += lightIntensity*lightColor*spec_coeff;
}

vec3 compute_normal(vec3 samplePosition,vec3 deltaSampling, sampler3D texture3D,mat4 imagePose)
{
    vec3 sample1, sample2;
    vec3 x1 = samplePosition-vec3(deltaSampling.x,0.0,0.0);
    sample1.x = texture(texture3D,  x1).r;

    vec3 x2 = samplePosition+vec3(deltaSampling.x,0.0,0.0);
    sample2.x = texture(texture3D, x2 ).r;

    vec3 y1 = samplePosition-vec3(0.0,deltaSampling.y,0.0);
    sample1.y = texture(texture3D, y1 ).r;
    vec3 y2 = samplePosition+vec3(0.0,deltaSampling.y,0.0);
    sample2.y = texture(texture3D, y2 ).r;

    vec3 z1 = samplePosition-vec3(0.0,0.0,deltaSampling.z);
    sample1.z = texture(texture3D, z1 ).r;
    vec3 z2 = samplePosition+vec3(0.0,0.0,deltaSampling.z);
    sample2.z = texture(texture3D, z2 ).r;

    vec3 N  = normalize( (sample1 - sample2)/(deltaSampling*2.0) );
    //Normal vector from image to world
    vec3 N_world = vec3(imagePose*vec4(N,0.0));

    return N_world;
}

void main()
{
    // position in world COS to position in image COS
    mat4 imagePose = orientation;
    vec3 originImageBoundingBox = origin-spacing/2.0;
    imagePose[3] = vec4(originImageBoundingBox,1.0);

    vec3 V  = normalize(eyePosition - worldPosition); // world to eye
    vec3 L = normalize(-lightDirection);//world to light, head light

    if(imagePositionNormalized.x>1.0||imagePositionNormalized.x<0.0||
            imagePositionNormalized.y>1.0||imagePositionNormalized.y<0.0||
            imagePositionNormalized.z>1.0||imagePositionNormalized.z<0.0) { // outside of volume
        discard;// should never end up here if cuboidmesh has same size as image bounding box
    } else {

        int maxIter = 500;
        float stepSize = 1.0/(maxIter-1.0);
        vec4 colour  =vec4(0);

        vec3 rayStart = imagePositionNormalized;
        vec3 viewDirectionPerspective = (worldPosition-eyePosition);
        vec3 viewDirectionPerspectiveInImage = vec3(inverse(imagePose)*vec4(viewDirectionPerspective,0.0));
        vec3 viewDirectionPerspectiveInImageNormalized = viewDirectionPerspectiveInImage/dimensionTimesSpacing;
        vec3 rayDirection = normalize(viewDirectionPerspectiveInImageNormalized);
        vec3 samplePosition = rayStart;
        vec3 N_out = vec3(0.0);
        bool positionOutsideVolume = any(lessThan(samplePosition,vec3(0,0,0))) || any(greaterThan(samplePosition,vec3(1,1,1)));
        while(!positionOutsideVolume) {

            int shortMaxVal = 32767;
            float clampedTextureValue = 0.0;
            float texVal = texture(texture3D, samplePosition).r*shortMaxVal;
            if(texVal<=(windowLevel-windowWidth/2.0)) {
                clampedTextureValue = 0.0;
            } else if(texVal>=(windowLevel+windowWidth/2.0)) {
                clampedTextureValue = 1.0;
            } else {
                clampedTextureValue = (texVal-(windowLevel-windowWidth/2.0))/windowWidth;
            }

            vec4 transferFunctionColor = texture(transferFunction,clampedTextureValue).rgba;

            // compute normals
            vec3 deltaSampling = spacing/dimensionTimesSpacing/2.0;
            vec3 N_world = compute_normal(samplePosition,deltaSampling,texture3D,imagePose);

            // Calculate the lighting model, keeping the specular component separate
            vec3 diffuseParameter, specularParameter;
            blinn_phong(N_world,V,L,diffuseParameter,specularParameter);

            // Combine spec with ambient+diffuse for final fragment color
            // TODO switch between ct coloring and Transfer function
            vec4 shadedColor = vec4(0);
            bool useTransferFunction = true;
            if(useTransferFunction) {
                shadedColor = vec4((vec3(transferFunctionColor.rgb)*diffuseParameter
                                    +ks*specularParameter
                                    +ka*lightIntensity*lightColor)*transferFunctionColor.a,
                                   transferFunctionColor.a);
            } else {
                shadedColor = vec4((vec3(clampedTextureValue)*diffuseParameter
                                   +ks*specularParameter
                                   +ka*lightIntensity*lightColor)*clampedTextureValue,clampedTextureValue);
            }

            // source: from dataset
            // dst: accumulated
            // c_dst = c_dst+(1-a_dst)*c_src
            // a_dst = a_dst+(1-a_dst)*a_src
            colour.rgb = colour.rgb + (1-colour.a)*shadedColor.rgb;
            colour.a = colour.a + (1-colour.a)*shadedColor.a;
            if(colour.a>0.98) { // early termination
                break;
            }

            samplePosition = samplePosition+rayDirection*stepSize;
            positionOutsideVolume = any(lessThan(samplePosition,vec3(0,0,0))) || any(greaterThan(samplePosition,vec3(1,1,1)));
        }
        if(colour.a<=0) {
            discard;
        } else {
            fragColor = colour;
        }

    }

}

