#version 150 core

in vec3 worldPosition;
in vec3 fragPosition;

out vec4 fragColor;

uniform vec3 borderColor;
uniform float borderWidth;
uniform float alpha;
uniform bool precomputedGradient;
uniform isampler3D texture3D;
uniform sampler3D texture3DGradient;
uniform isampler1D transferFunction;

uniform vec3 dimension;
uniform vec3 spacing;
uniform vec3 origin;
uniform vec3 dimensionTimesSpacing;
uniform mat4 orientation;
uniform float minValue;
uniform float maxValue;
uniform vec3 eyePosition;
uniform bool lightEnable;   // light enable flags
uniform vec3 lightPosition; // light positions in world space
uniform vec3 lightDirection;// light direction in world
uniform float lightIntensity;
uniform vec3 lightColor;
uniform vec3 ka;            // Material property: Ambient reflectivity
//uniform vec3 kd;          // Material property: Diffuse reflectivity --> sampled from volume/transfer function
uniform vec3 ks;            // Material property: Specular reflectivity
uniform float shininess;    // Material property: Specular shininess factor

uniform float windowWidth;
uniform float windowLevel;


void adsModel(const in vec3 worldPos,
              const in vec3 worldNormal,
              const in vec3 worldView,
              const in vec3 worldLightDir,
              const in float shininess,
              out vec3 diffuseColor,
              out vec3 specularColor)
{
    diffuseColor = vec3(0.0);
    specularColor = vec3(0.0);

    // We perform all work in world space
    vec3 n = normalize(worldNormal);
    vec3 s = vec3(0.0);

    float att = 1.0;
    float sDotN = 0.0;

    // Directional lights
    // The light direction is in world space already
    s = normalize(-worldLightDir);
    sDotN = dot(s, n);

    // Calculate the diffuse factor
    float diffuse = max(sDotN, 0.0);

    // Calculate the specular factor
    float specular = 0.0;
    if (diffuse > 0.0 && shininess > 0.0) {
        float normFactor = (shininess + 2.0) / 2.0;
        vec3 r = reflect(-s, n);   // Reflection direction in world space
        specular = normFactor * pow(max(dot(r, worldView), 0.0), shininess);
    }

    // Accumulate the diffuse and specular contributions
    diffuseColor += att * lightIntensity * diffuse * lightColor;
    specularColor += att * lightIntensity * specular * lightColor;
}

void blinn_phong(vec3 N, vec3 V, vec3 L,
                 out vec3 diffuseColor,out vec3 specularColor)
{
    diffuseColor = vec3(0.0);
    specularColor = vec3(0.0);

    // diffuse coefficient
    float diff_coeff = max(dot(L,N),0.0);

    // specular coefficient
    vec3 H = normalize(L+V);
    float spec_coeff = pow(max(dot(H,N), 0.0), shininess);
    if (diff_coeff <= 0.0)
        spec_coeff = 0.0;

    diffuseColor += lightIntensity*lightColor*diff_coeff;
    specularColor += lightIntensity*lightColor*spec_coeff;
}

void main()
{
    //output color from material


    float maxX = 1.0 - borderWidth;
    float minX = borderWidth;
    float maxY = maxX;
    float minY = minX;

    // position in world COS to position in image COS
    mat4 imagePose = orientation;
    imagePose[3] = vec4(origin,1.0);
    vec3 texCoord3d = vec3(inverse(imagePose)*vec4(worldPosition,1.0))/dimensionTimesSpacing;
    //vec3 texCoord3d = (worldPosition - origin) / dimensionTimesSpacing;
    if(texCoord3d.x>1.0||texCoord3d.x<0.0||
            texCoord3d.y>1.0||texCoord3d.y<0.0||
            texCoord3d.z>1.0||texCoord3d.z<0.0) { // outside of volume
        //fragColor = vec4(0.0,0.0,0.0,0.0);
        discard;
    } else { // inside volume
        int textureValue = texture(texture3D, texCoord3d.xyz).r;
        if(textureValue<=(windowLevel-windowWidth/2.0)) { // texture below windowLeve-windowWidth/2
            //fragColor = vec4(0.0,0.0,0.0,0.0);
            discard;
        } else { // texture in or above window

            float clampedTextureValue = 0.0;
            if(textureValue>=(windowLevel+windowWidth/2.0)) {
                clampedTextureValue = 1.0;
            } else {
                clampedTextureValue = (textureValue-(windowLevel-windowWidth/2.0))/windowWidth;
            }
            vec4 transferFunctionColor = texture(transferFunction,clampedTextureValue).rgba;

            vec4 color_tmp = vec4(0.0, 0.0, 0.0,0.0);

            if(transferFunctionColor.a>0.02) { // do shading

                //vec3 N;
                vec3 N;
                N = normalize(N);
                if(precomputedGradient==true) {
                     N = normalize(texture(texture3DGradient, texCoord3d.xyz ).rgb);
                } else {
                    vec3 deltaSampling = spacing;
                    vec3 sample1, sample2;
                    sample1.x = texture(texture3D, texCoord3d.xyz-vec3(deltaSampling.x,0.0,0.0) ).r;
                    sample2.x = texture(texture3D, texCoord3d.xyz+vec3(deltaSampling.x,0.0,0.0) ).r;
                    sample1.y = texture(texture3D, texCoord3d.xyz-vec3(0.0,deltaSampling.y,0.0) ).r;
                    sample2.y = texture(texture3D, texCoord3d.xyz+vec3(0.0,deltaSampling.y,0.0) ).r;
                    sample1.z = texture(texture3D, texCoord3d.xyz-vec3(0.0,0.0,deltaSampling.z) ).r;
                    sample2.z = texture(texture3D, texCoord3d.xyz+vec3(0.0,0.0,deltaSampling.z) ).r;
                    N  = normalize( sample1 - sample2 );
                }

                //
                vec3 V  = normalize(eyePosition - worldPosition); // world to eye
                vec3 L = normalize(eyePosition - worldPosition);//world to light, head light

                // Calculate the lighting model, keeping the specular component separate
                vec3 diffuseColor, specularColor;
                int blinnOrPhong = 0;
                if(blinnOrPhong==0) {
                    blinn_phong(N,V,L,diffuseColor,specularColor);
                } else {
                    adsModel(worldPosition /*frag position in world cos*/, N /*frag normal in world cos*/, -V /*view directio in world space*/,-L/*light dir in world space*/, shininess, diffuseColor/*out*/, specularColor/*out*/);
                }
                // Combine spec with ambient+diffuse for final fragment color
                color_tmp = vec4(normalize(transferFunctionColor.rgb)*diffuseColor+ka* lightIntensity*lightColor,transferFunctionColor.a);//*diffuseColor+specularColor*ks+ka* lightIntensity*lightColor,transferFunctionColor.a);//vec4(colorPhong,transferFunctionColor.a);//transferFunctionColor.rgb;//transferFunctionColor.rgb;//colorPhong;

            } else {
                color_tmp = vec4(normalize(transferFunctionColor.rgb),transferFunctionColor.a);
            }

            fragColor = color_tmp;
        }
    }

}

