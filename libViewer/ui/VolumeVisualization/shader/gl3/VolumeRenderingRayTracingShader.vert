#version 150 core

in vec3 vertexPosition;

out vec3 worldPosition;
out vec3 imagePosition;
out vec3 imagePositionNormalized;

uniform mat4 modelMatrix;
uniform mat4 viewMatrix; // T_view<-world
uniform mat4 projectionMatrix;
uniform mat4 viewProjectionMatrix;
uniform mat4 mvp;
uniform vec3 spacing;
uniform vec3 dimension;
uniform vec3 origin;
uniform vec3 dimensionTimesSpacing;
uniform mat4 orientation;

void main()
{  
    // modelMatrix corresponds to Transform qt3d node, and should be identity in this case here
    imagePosition = vec3(modelMatrix *  vec4(vertexPosition+dimensionTimesSpacing*0.5, 1.0)); // compensate for difference in coordinate system between CuboidMesh and image bounding box (half box size)
    imagePositionNormalized = imagePosition/dimensionTimesSpacing;

    // transform to transform position in world COS to position in imageBoundingBox COS
    mat4 imagePose = orientation;
    vec3 originImageBoundingBox = origin-spacing/2.0;/*origin of image bounding box*/
    imagePose[3] = vec4(originImageBoundingBox,1.0); // T_world<-imageBoundingBox

    // position of current vertex in world coordinate system
    worldPosition = vec3(imagePose*vec4(imagePosition,1.0));

    // Calculate vertex position in clip coordinates
    gl_Position = viewProjectionMatrix*vec4(worldPosition,1.0);
}
