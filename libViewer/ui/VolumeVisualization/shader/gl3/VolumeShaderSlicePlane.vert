#version 150 core

in vec3 vertexPosition;
in vec3 vertexNormal;

out vec3 worldPosition;
//out vec3 worldPos;
out vec3 worldNormal;

uniform mat4 modelMatrix;
uniform mat3 modelNormalMatrix;
uniform mat4 mvp;

void main()
{
    // Transform position, normal, and tangent to world coords
    worldPosition = vec3(modelMatrix * vec4(vertexPosition, 1.0));
    //worldPos = worldPosition;
    worldNormal = normalize(modelNormalMatrix * vertexNormal);

    // Calculate vertex position in clip coordinates
    gl_Position = mvp * vec4(vertexPosition, 1.0);
}
