import Qt3D.Core 2.4
import Qt3D.Render 2.0

import libViewer

Material {
    id: root
    property color borderColor: Qt.rgba(0.0,1.0,0.0,1.0)
    property real alpha: 1

    parameters: [
        Parameter { name: "borderColor"; value: root.borderColor },
        Parameter { name: "alpha"; value: root.alpha }
    ]
}


