import Qt3D.Core 2.4
import Qt3D.Extras 2.4
import Qt3D.Render 2.4
import QtQml
import Qt3D.Input 2.4
import QtQuick

import "../Line"

Entity {
    id: root

    property var sliceViewerLayers:[]
    property var threeDViewerLayer:[]
    property var renderLayers: []
    property var noDepthTestLayer:[]

    property int planeWidth: 100
    property int planeHeight:120
    property color horizontalLineColor: "red"
    property color verticalLineColor: "green"
    property real lineWidth: 2.0
    property bool crosshairEnabled

    function updateVolumeBoundedLineRenderingEffect(dimensionsTimesSpacing,originBoundingBox,orientation) {
        lineEffectVolumeBounded.dimensionTimesSpacing = dimensionsTimesSpacing;
        lineEffectVolumeBounded.originBoundingBox = originBoundingBox;
        lineEffectVolumeBounded.orientation = orientation;
    }

    LineShaderEffect {
        id: lineEffect
        depthTesting: false
    }
    LineShaderEffectVolumeBounded {
        id: lineEffectVolumeBounded
        depthTesting: true
    }

    Line {
        id: horizontalLine
        myEnabled: root.crosshairEnabled
        viewerLayers: root.sliceViewerLayers
        renderLayers: root.noDepthTestLayer
        startPoint: Qt.vector3d(-root.planeWidth*2.0,0,0)
        endPoint: Qt.vector3d(root.planeWidth*2.0,0,0)
        lineColor: root.horizontalLineColor
        lineWidth: root.lineWidth
        effect: lineEffect//root.lineRenderingEffect
    }
    Line {
        id: verticalLine
        myEnabled: root.crosshairEnabled
        viewerLayers: root.sliceViewerLayers
        renderLayers: root.noDepthTestLayer
        startPoint: Qt.vector3d(0,-root.planeHeight*2,0)
        endPoint: Qt.vector3d(0,root.planeHeight*2,0)
        lineColor: root.verticalLineColor
        lineWidth: root.lineWidth
        effect: lineEffect//root.lineRenderingEffect
    }

    Line {
        id: horizontalLine3D
        myEnabled: root.crosshairEnabled
        viewerLayers: root.threeDViewerLayer
        renderLayers: root.renderLayers
        startPoint: Qt.vector3d(-root.planeWidth*2.0,0,0)
        endPoint: Qt.vector3d(root.planeWidth*2.0,0,0)
        lineColor: root.horizontalLineColor
        lineWidth: root.lineWidth
        effect: lineEffectVolumeBounded//root.lineRenderingEffectVolumeBounded
    }
    Line {
        id: verticalLine3D
        myEnabled: root.crosshairEnabled
        viewerLayers: root.threeDViewerLayer
        renderLayers: root.renderLayers
        startPoint: Qt.vector3d(0,-root.planeHeight*2,0)
        endPoint: Qt.vector3d(0,root.planeHeight*2,0)
        lineColor: root.verticalLineColor
        lineWidth: root.lineWidth
        effect: lineEffectVolumeBounded//root.lineRenderingEffectVolumeBounded
    }

}
