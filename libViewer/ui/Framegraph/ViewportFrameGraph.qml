import Qt3D.Core 2.4
import Qt3D.Render 2.4
import Qt3D.Render 2.15
import QtQuick
import Qt3D.Extras 2.4

import libViewer

import "../ClipPlanes"
import ".."

RenderSettings {
    id: root

    property NodeInstantiator camerasInstantiator
    property Repeater viewportsRepeater
    property RenderLayers renderLayersEntity
    property ViewerLayers viewerLayersEntity
    readonly property real wspt: 0.01;
    property ViewerConfiguration viewerConfiguration
    property ClippingPlanes clippingPlanes
    property NodeInstantiator spatialObjectsFromFileLayers
    property NodeInstantiator primitiveSpatialObjectsLayers
    readonly property FilterKeyCatalogue filterKeyCatalogue: FilterKeyCatalogue {}

    function setWorldSpaceTolerance() {
        root.pickingSettings.setWorldSpaceTolerance(root.wspt)
    }

    activeFrameGraph: RenderSurfaceSelector {
        id: surfaceSelector

        Viewport {
            id: mainViewport
            normalizedRect: Qt.rect(0.0, 0.0, 1.0, 1.0)

            ClearBuffers {
                buffers: ClearBuffers.ColorDepthStencilBuffer
                clearColor: root.viewerConfiguration.backgroundColor
                NoDraw{}
            }

            SortPolicy {
                //sortTypes:  [SortPolicy.BackToFront]

                NodeInstantiator {// instantiate for each viewport
                    id: viewportNodeInstantiator
                    model: root.viewportsRepeater.count
                    delegate: SubtreeEnabler { // only render viewports that are enabled
                        property int viewportNodeInstantiatorIndex: index
                        enabled: root.viewportsRepeater.itemAt(viewportNodeInstantiatorIndex).isEnabled
                        Viewport {
                            normalizedRect: root.viewportsRepeater.itemAt(viewportNodeInstantiatorIndex).data


                            CameraSelector {
                                camera: root.camerasInstantiator.objectAt(viewportNodeInstantiatorIndex).camm

                                LayerFilter { // render only objects that are in certain viewport (they have the layer filter for that viewport assigned)
                                    layers: [root.viewerLayersEntity.viewerLayers[viewportNodeInstantiatorIndex]]
                                    LayerFilter { // render opaque objects
                                        layers: root.renderLayersEntity.opaqueLayer /* opaque objects */
                                        SubtreeEnabler { // 3d viewer
                                            enabled: (root.viewerConfiguration.getThreeDViewportId()==viewportNodeInstantiatorIndex) // 3d viewer
                                            RenderStateSet { renderStates: [AlphaCoverage {},MultiSampleAntiAliasing{},DepthTest {depthFunction:DepthTest.Less}]
                                                RenderPassFilter { matchAny:[root.filterKeyCatalogue.threeDViewerOpaqueModelFilterKey] } // render all opaque objects in 3d viewer
                                                //TechniqueFilter { matchAll: [FilterKey{name:"renderingStyle"; value: "forward"}]
                                                //RenderPassFilter { matchAny:[FilterKey{name: "name"; value:"zFillPass"}]/*FilterKey { name: ""; value: "" }*/ } // render all opaque objects in 3d viewer
                                                //}
                                            }
                                        }
                                        SubtreeEnabler { // slice viewer
                                            enabled: (root.viewerConfiguration.getThreeDViewportId()!=viewportNodeInstantiatorIndex)// slice viewers
                                            RenderStateSet { renderStates: [AlphaCoverage {},MultiSampleAntiAliasing{},DepthTest {depthFunction:DepthTest.Less}]
                                                //RenderPassFilter { matchAny:[root.filterKeyCatalogue.defaultFilterKey] }
                                                RenderPassFilter {  // render all opaque objects in slice viewer
                                                    matchAny:[root.filterKeyCatalogue.sliceViewerOpaqueModelFilterKey]
                                                    parameters: Parameter {name: "clipPlaneEquation"; value: {
                                                        var sliceIndex = root.viewerConfiguration.getSliceIndexFromViewportIndex(viewportNodeInstantiatorIndex);
                                                        return root.clippingPlanes.clipPlaneEntities[sliceIndex].equation
                                                    }}
                                                }
                                            }
                                            NodeInstantiator {// instantiate for each 3d object from file, to render all overlays in slice viewer
                                                id: spatialObjectsFromFileNodeInstantiator
                                                model: root.spatialObjectsFromFileLayers.count+root.primitiveSpatialObjectsLayers.count
                                                delegate: LayerFilter { // filter out that 3d object
                                                    property int spatialObjectsNodeInstantiatorIndex: index

                                                    layers: {
                                                        if((root.spatialObjectsFromFileLayers.count==0 && root.primitiveSpatialObjectsLayers.count==0) || spatialObjectsNodeInstantiatorIndex < 0) {
                                                            return [];
                                                        } else {
                                                            if(spatialObjectsNodeInstantiatorIndex<root.spatialObjectsFromFileLayers.count) {
                                                                return [root.spatialObjectsFromFileLayers.objectAt(spatialObjectsNodeInstantiatorIndex)];
                                                            } else {
                                                                return [root.primitiveSpatialObjectsLayers.objectAt(spatialObjectsNodeInstantiatorIndex-root.spatialObjectsFromFileLayers.count)];
                                                            }
                                                        }
                                                    }
                                                    ClearBuffers {
                                                        // Enable and fill Stencil to later generate caps
                                                        buffers: ClearBuffers.StencilBuffer
                                                        RenderStateSet {
                                                            renderStates: [
                                                                ClipPlane {planeIndex:0},
                                                                CullFace { mode: CullFace.NoCulling },
                                                                StencilTest {
                                                                    front.stencilFunction: StencilTestArguments.Always
                                                                    front.referenceValue: 0
                                                                    front.comparisonMask: 0
                                                                    back.stencilFunction: StencilTestArguments.Always
                                                                    back.referenceValue: 0
                                                                    back.comparisonMask: 0
                                                                },
                                                                StencilOperation {
                                                                    back.allTestsPassOperation: StencilOperationArguments.IncrementWrap
                                                                    front.allTestsPassOperation: StencilOperationArguments.DecrementWrap
                                                                }
                                                                ,DepthTest { depthFunction: DepthTest.Always }
                                                                ,ColorMask { redMasked: false; greenMasked: false; blueMasked: false; alphaMasked: false }
                                                                //,NoDepthMask { }
                                                            ]
                                                            RenderPassFilter {
                                                                matchAny: root.filterKeyCatalogue.sliceViewerOpaqueOverlayStencilFillFilterKey
                                                                parameters: Parameter {name: "clipPlaneEquation"; value: {
                                                                    var sliceIndex = root.viewerConfiguration.getSliceIndexFromViewportIndex(viewportNodeInstantiatorIndex);
                                                                    return root.clippingPlanes.clipPlaneEntities[sliceIndex].equation
                                                                }}
                                                            }
                                                        }
                                                    }
                                                    RenderStateSet {
                                                        // Draw caps using stencil buffer
                                                        // Draw back faces - front faces -> caps
                                                        renderStates: [
                                                            //ColorMask { redMasked: true; greenMasked: true; blueMasked: true; alphaMasked: true},
                                                            StencilTest {
                                                                front.stencilFunction: StencilTestArguments.NotEqual
                                                                front.referenceValue: 0
                                                                front.comparisonMask:~0
                                                                back.stencilFunction: StencilTestArguments.NotEqual
                                                                back.referenceValue: 0
                                                                back.comparisonMask: ~0
                                                            }
                                                            ,AlphaCoverage {}
                                                            ,MultiSampleAntiAliasing {}
                                                            //,DepthTest { depthFunction: DepthTest.Always } // TODO comment
                                                        ]
                                                        LayerFilter {
                                                            layers: [root.renderLayersEntity.cappingLayer]
                                                            RenderPassFilter {
                                                                matchAny: root.filterKeyCatalogue.sliceViewerOpaqueOverlayCappingFilterKey
                                                                parameters: [Parameter {name: "myModelMatrix"; value: {
                                                                    var sliceIndex = root.viewerConfiguration.getSliceIndexFromViewportIndex(viewportNodeInstantiatorIndex);
                                                                    return root.clippingPlanes.clipPlaneEntities[sliceIndex].transform;
                                                                }}]
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                    LayerFilter { // render transparent objects
                                        layers: root.renderLayersEntity.transparentLayer /*transparent objects */
                                        SubtreeEnabler {
                                            enabled: (root.viewerConfiguration.getThreeDViewportId()==viewportNodeInstantiatorIndex) // 3d viewer
                                            RenderStateSet { renderStates: [AlphaCoverage {},MultiSampleAntiAliasing{},DepthTest {depthFunction:DepthTest.Less}]
                                                RenderPassFilter { matchAny:[root.filterKeyCatalogue.threeDViewerTransparentModelFilterKey] /* render all */ }
                                            }
                                        }
                                        SubtreeEnabler {
                                            enabled: (root.viewerConfiguration.getThreeDViewportId()!=viewportNodeInstantiatorIndex)// slice viewers
                                            RenderStateSet { renderStates: [AlphaCoverage {},MultiSampleAntiAliasing{},DepthTest {depthFunction:DepthTest.Less}]
                                                RenderPassFilter { // render all transparent objects in slice viewer
                                                    matchAny:[root.filterKeyCatalogue.sliceViewerTransparentModelFilterKey]
                                                    parameters: Parameter {name: "clipPlaneEquation"; value: {
                                                        var sliceIndex = root.viewerConfiguration.getSliceIndexFromViewportIndex(viewportNodeInstantiatorIndex);
                                                        return root.clippingPlanes.clipPlaneEntities[sliceIndex].equation
                                                    }}
                                                }
                                            }
                                            NodeInstantiator {// instantiate for each 3d object from file, to render all overlays in slice viewer
                                                id: spatialObjectsFromFileNodeInstantiator2
                                                model: root.spatialObjectsFromFileLayers.count+root.primitiveSpatialObjectsLayers.count
                                                delegate: LayerFilter {
                                                    property int spatialObjectsNodeInstantiatorIndex2: index
                                                    layers: {
                                                        if((root.spatialObjectsFromFileLayers.count==0 && root.primitiveSpatialObjectsLayers.count==0) || spatialObjectsNodeInstantiatorIndex2 < 0) {
                                                            return [];
                                                        } else {
                                                            if(spatialObjectsNodeInstantiatorIndex2<root.spatialObjectsFromFileLayers.count) {
                                                                return [root.spatialObjectsFromFileLayers.objectAt(spatialObjectsNodeInstantiatorIndex2)];
                                                            } else {
                                                                return [root.primitiveSpatialObjectsLayers.objectAt(spatialObjectsNodeInstantiatorIndex2-root.spatialObjectsFromFileLayers.count)];
                                                            }
                                                        }
                                                    }
                                                    //layers: {
                                                    //    if(root.spatialObjectsFromFileLayers.count==0 || spatialObjectsFromFileNodeInstantiatorIndex2 < 0) {
                                                    //        return [];
                                                    //    } else {
                                                    //        return [root.spatialObjectsFromFileLayers.objectAt(spatialObjectsFromFileNodeInstantiatorIndex2)];
                                                    //    }
                                                    //}
                                                    ClearBuffers {
                                                        // Enable and fill Stencil to later generate caps
                                                        buffers: ClearBuffers.StencilBuffer
                                                        RenderStateSet {
                                                            renderStates: [
                                                                ClipPlane {planeIndex:0},
                                                                CullFace { mode: CullFace.NoCulling },
                                                                StencilTest {
                                                                    front.stencilFunction: StencilTestArguments.Always
                                                                    front.referenceValue: 0
                                                                    front.comparisonMask: 0
                                                                    back.stencilFunction: StencilTestArguments.Always
                                                                    back.referenceValue: 0
                                                                    back.comparisonMask: 0
                                                                },
                                                                StencilOperation {
                                                                    back.allTestsPassOperation: StencilOperationArguments.IncrementWrap
                                                                    front.allTestsPassOperation: StencilOperationArguments.DecrementWrap
                                                                }
                                                                ,DepthTest { depthFunction: DepthTest.Always }
                                                                ,ColorMask { redMasked: false; greenMasked: false; blueMasked: false; alphaMasked: false }
                                                                //,NoDepthMask { }
                                                            ]
                                                            RenderPassFilter {
                                                                matchAny: root.filterKeyCatalogue.sliceViewerTransparentOverlayStencilFillFilterKey
                                                                parameters: Parameter {name: "clipPlaneEquation"; value: {
                                                                    var sliceIndex = root.viewerConfiguration.getSliceIndexFromViewportIndex(viewportNodeInstantiatorIndex);
                                                                    return root.clippingPlanes.clipPlaneEntities[sliceIndex].equation
                                                                }}
                                                            }
                                                        }
                                                    }
                                                    RenderStateSet {
                                                        // Draw caps using stencil buffer
                                                        // Draw back faces - front faces -> caps
                                                        renderStates: [
                                                            //ColorMask { redMasked: true; greenMasked: true; blueMasked: true; alphaMasked: true},
                                                            StencilTest {
                                                                front.stencilFunction: StencilTestArguments.NotEqual
                                                                front.referenceValue: 0
                                                                front.comparisonMask:~0
                                                                back.stencilFunction: StencilTestArguments.NotEqual
                                                                back.referenceValue: 0
                                                                back.comparisonMask: ~0
                                                            }
                                                            ,AlphaCoverage {}
                                                            ,MultiSampleAntiAliasing {}
                                                            //,DepthTest { depthFunction: DepthTest.Always } // TODO comment
                                                        ]
                                                        LayerFilter {
                                                            layers: [root.renderLayersEntity.cappingLayer]
                                                            RenderPassFilter {
                                                                matchAny: root.filterKeyCatalogue.sliceViewerTransparentOverlayCappingFilterKey
                                                                parameters: [Parameter {name: "myModelMatrix"; value: {
                                                                    var sliceIndex = root.viewerConfiguration.getSliceIndexFromViewportIndex(viewportNodeInstantiatorIndex);
                                                                    return root.clippingPlanes.clipPlaneEntities[sliceIndex].transform;
                                                                }}]
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                    LayerFilter {
                                        layers: {return root.renderLayersEntity.noDepthTestLayer;} /* no depth test */
                                        ClearBuffers {
                                            buffers: ClearBuffers.DepthBuffer
                                        }
                                    }
                                }
                            }
                        }
                    }
                }

            }
        }
    }
    pickingSettings.faceOrientationPickingMode: PickingSettings.FrontAndBackFace;
    pickingSettings.pickMethod: PickingSettings.TrianglePicking;
    pickingSettings.pickResultMode: PickingSettings.NearestPick
}
