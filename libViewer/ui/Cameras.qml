import QtQuick
import Qt3D.Core 2.4
import Qt3D.Render 2.4
import Qt3D.Input 2.4
import Qt3D.Extras 2.4

import libViewer

Entity {
    id: root
    property alias camerasInstantiator: camerasInstantiator
    property ViewerConfiguration viewerConfiguration
    property int mouseIsInViewport: -1
    property int cameraViewWidth: 10
    property real scene3DAspectRatio: 1 // width/height
    readonly property real planeToCamDistance:500
    property ViewerLayers viewerLayersEntity
    property RenderLayers renderLayersEntity
    property int threeDViewportProjectionType: CameraLens.PerspectiveProjection;//CameraLens.OrthographicProjection
    readonly property var cameraViewportWidths: {
        var widths = []
        for(var i=0;i<camerasInstantiator.count;i++) {
            widths.push(camerasInstantiator.objectAt(i).cameraRight-camerasInstantiator.objectAt(i).cameraLeft)
        }
        return widths;
    }

    function changeZoom(viewerIndex,inOrOut) {
        if(viewerIndex===root.viewerConfiguration.getThreeDViewportId()) {
            camerasInstantiator.objectAt(viewerIndex).camm.translate(Qt.vector3d(0,0,inOrOut*10))
        } else {
            for(var i=0;i<camerasInstantiator.count;i++) {
                if(i===root.viewerConfiguration.getThreeDViewportId()) {
                    continue;
                } else {
                    if(inOrOut===-1) { // zoom out
                        camerasInstantiator.objectAt(i).cameraLeft*=1.1
                        camerasInstantiator.objectAt(i).cameraRight*=1.1
                        camerasInstantiator.objectAt(i).cameraBottom*= 1.1
                        camerasInstantiator.objectAt(i).cameraTop*=1.1
                    } else {
                        camerasInstantiator.objectAt(i).cameraLeft*=0.9
                        camerasInstantiator.objectAt(i).cameraRight*=0.9
                        camerasInstantiator.objectAt(i).cameraBottom*=0.9
                        camerasInstantiator.objectAt(i).cameraTop*=0.9
                    }
                }
            }
        }
    }

    function adjustCameraAspectRatio(i,aspectRatio,cameraViewWidth) {
        if(camerasInstantiator.count>i){
            camerasInstantiator.objectAt(i).cameraAspectRatio= aspectRatio
            camerasInstantiator.objectAt(i).cameraLeft= aspectRatio<1 ? -cameraViewWidth/2.0 : -cameraViewWidth/2.0*aspectRatio;
            camerasInstantiator.objectAt(i).cameraRight= aspectRatio<1 ? cameraViewWidth/2.0 : cameraViewWidth/2.0*aspectRatio;
            camerasInstantiator.objectAt(i).cameraBottom= aspectRatio<1 ? -cameraViewWidth/2.0/aspectRatio : -cameraViewWidth/2.0;
            camerasInstantiator.objectAt(i).cameraTop= aspectRatio<1 ? cameraViewWidth/2.0/aspectRatio : cameraViewWidth/2.0;
        }
    }

    NodeInstantiator {
        id: camerasInstantiator
        model: root.viewerConfiguration.viewportsLayout.length
        delegate:
        CameraWithTransform {
            property real adjustedAspectRatio: root.viewerConfiguration.viewportsLayout[index].width/root.viewerConfiguration.viewportsLayout[index].height*root.scene3DAspectRatio
            threeDViewerCam: index===root.viewerConfiguration.getThreeDViewportId()
            activeViewport: root.mouseIsInViewport===index
            cameraProjectionType: {
                if(index===root.viewerConfiguration.getThreeDViewportId()) {
                    return root.threeDViewportProjectionType
                } else {
                    return CameraLens.OrthographicProjection
                }
            }
            cameraFov: 45
            cameraAspectRatio: adjustedAspectRatio
            cameraPosition: {
                if(index===root.viewerConfiguration.getThreeDViewportId()) {
                    return Qt.vector3d(0,-400,0)

                } else {
                    return Qt.vector3d(0,0,-root.planeToCamDistance)
                }
            }
            cameraViewCenter: Qt.vector3d(0,0,0)
            cameraUpVector: Qt.vector3d(0,-1,0)
            cameraLeft: adjustedAspectRatio<1 ? -root.cameraViewWidth/2.0 : -root.cameraViewWidth/2.0*adjustedAspectRatio;
            cameraRight: adjustedAspectRatio<1 ? root.cameraViewWidth/2.0 : root.cameraViewWidth/2.0*adjustedAspectRatio;
            cameraBottom: adjustedAspectRatio<1 ? -root.cameraViewWidth/2.0/adjustedAspectRatio : -root.cameraViewWidth/2.0;
            cameraTop: adjustedAspectRatio<1 ? root.cameraViewWidth/2.0/adjustedAspectRatio : root.cameraViewWidth/2.0;
            //nearPlane: index===root.viewerConfiguration.getThreeDViewportId() ? 0.01 : root.planeToCamDistance-root.cameraViewWidth/200
            //farPlane: index===root.viewerConfiguration.getThreeDViewportId() ? 50000.0 : root.planeToCamDistance+root.cameraViewWidth/200
            viewerLayers: root.viewerLayersEntity.viewerLayers
            renderLayers: root.renderLayersEntity.renderLayers
        }
    }
}
