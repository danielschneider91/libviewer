import QtQuick
import Qt3D.Extras 2.4
import Qt3D.Core 2.4
import Qt3D.Render 2.4
import Qt3D.Input 2.4

import ".."

import libViewer

Entity { // by default, points towards y axis
    id: root
    property color color: Qt.rgba(0,1,0,1)
    property int length: 10
    property int radius: 1
    property bool myEnabled: true
    property Transform arrowTransform: Transform {
        translation: Qt.vector3d(0, 0, 0)
    }
    components: root.myEnabled ? [arrowTransform]:[]

    Entity {
        id: cylinder
        components: root.myEnabled ? [cylinderTransform, cylinderMaterial, cylinderMesh]:[]

        Transform {
            id: cylinderTransform
            translation: Qt.vector3d(0,cylinderMesh.length/2,0)
        }

        CylinderMesh {
            id: cylinderMesh
            length: root.length/4*3
            radius: root.radius/4
        }

        //MetalRoughMaterial  {
        //    id: cylinderMaterial1
        //    baseColor: color
        //}
        Material {
            id: cylinderMaterial
            effect: MetalRoughClippingEffect {
                baseColor:color
                viewportVisibility: ViewportVisibility {
                    threeDViewportsVisibility:true
                    sliceViewportsVisibility:true
                    sliceViewportRenderingMode:SliceViewportRenderingModeClass.MODEL
                }
            }
        }
    }
    Entity {
        id: cone
        components: root.myEnabled ? [coneTransform, coneMaterial, coneMesh]:[]

        Transform {
            id: coneTransform
            translation: Qt.vector3d(0,root.length/4*3,0)
            //rotationX: 180
        }

        ConeMesh {
            id: coneMesh
            length: root.length/4*1
            bottomRadius: root.radius
            //topRadius: root.radius
        }

        //MetalRoughMaterial  {
        //    id: coneMaterial1
        //    baseColor: Qt.lighter(color)
        //}
        Material {
            id: coneMaterial
            effect: MetalRoughClippingEffect {
                baseColor:Qt.lighter(color)
                viewportVisibility: ViewportVisibility {
                    threeDViewportsVisibility:true
                    sliceViewportsVisibility:true
                    sliceViewportRenderingMode:SliceViewportRenderingModeClass.MODEL
                }
            }
        }
    }

}
