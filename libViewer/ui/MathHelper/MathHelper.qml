import QtQuick 2.0
import QtQuick
import QtQuick.Controls
import QtQuick.Scene3D 2.4
import Qt3D.Core 2.4
import Qt3D.Render 2.4
import Qt3D.Input 2.4
import Qt3D.Extras 2.4

Item {

    function fuzzyIsNull(num) {
        return Math.abs(num) < Number.EPSILON;
    }

    function quaternionToRotationMatrix(quat) {
        var q0 = quat.scalar;
        var q1 = quat.x;
        var q2 = quat.y;
        var q3 = quat.z;

        // First row of the rotation matrix
        var r00 = 2 * (q0 * q0 + q1 * q1) - 1
        var r01 = 2 * (q1 * q2 - q0 * q3)
        var r02 = 2 * (q1 * q3 + q0 * q2)

        // Second row of the rotation matrix
        var r10 = 2 * (q1 * q2 + q0 * q3)
        var r11 = 2 * (q0 * q0 + q2 * q2) - 1
        var r12 = 2 * (q2 * q3 - q0 * q1)

        // Third row of the rotation matrix
        var r20 = 2 * (q1 * q3 - q0 * q2)
        var r21 = 2 * (q2 * q3 + q0 * q1)
        var r22 = 2 * (q0 * q0 + q3 * q3) - 1

        // 3x3 rotation matrix
        var rot_matrix = Qt.matrix4x4(r00, r01, r02,0,
                                        r10, r11, r12,0,
                                        r20, r21, r22,0,
                                        0,0,0,1);

        return rot_matrix
    }
    function fromTo(vec1,vec2) {
        var a= vec1.normalized();
        var b= vec2.normalized();

        var v = a.crossProduct(b)
        var c = a.dotProduct(b);
        var s = v.length();

        var kmat = Qt.matrix4x4(0, -v.z, v.y,0,
                                v.z, 0, -v.x,0,
                                -v.y, v.x, 0,0,
                                0,0,0,1)

        var kmat2 = kmat.times(kmat)
        var eyemat = Qt.matrix4x4();
        var kmat3 = kmat2.times(1/(1+c));//((1-c)/(s^2))
        var rotation_matrix = eyemat.plus(kmat).plus(kmat3)
        return rotation_matrix
    }

    function axialSlicePoseToOtherSlicePose(pose,targetSlice) {
        var newPose = Qt.matrix4x4(pose.m11,pose.m12,pose.m13,pose.m14,
                                   pose.m21,pose.m22,pose.m23,pose.m24,
                                   pose.m31,pose.m32,pose.m33,pose.m34,
                                   0,0,0,1);

        if(targetSlice==='Sagittal') {
            newPose.rotate(-90,Qt.vector3d(0.0,1.0,0.0));
        }
        if(targetSlice==='Coronal') {
            newPose.rotate(-90,Qt.vector3d(1.0,0.0,0.0));
        }
        return newPose;
    }

    function getMinOfVector3d(vec3d) {
        var min = vec3d.x;
        if(min > (vec3d.y)) {
            min = (vec3d.y)
        }
        if(min > (vec3d.z)) {
            min = (vec3d.z)
        }
        return min;
    }

}


