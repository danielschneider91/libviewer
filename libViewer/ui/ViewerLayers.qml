import QtQuick
import Qt3D.Core 2.4
import Qt3D.Render 2.4
import Qt3D.Input 2.4
import Qt3D.Extras 2.4

import libViewer

Entity {
    id: root
    property ViewerConfiguration viewerConfiguration

    property var viewerLayers: {
        var arr=[]
        for(var i=0;i<viewerLayersInstantiator.count;i++) {
            arr.push(viewerLayersInstantiator.objectAt(i));
        }
        return arr;
    }
    property var threeDViewerLayer: {
        var arr=[]
        if(viewerLayersInstantiator.count>0) {
            arr.push(viewerLayersInstantiator.objectAt(root.viewerConfiguration.getThreeDViewportId()));
        }
        return arr;
    }
    property var sliceViewerLayers: {
        var arr=[]
        for(var i=0;i<viewerLayersInstantiator.count;i++) {
            if(root.viewerConfiguration.viewportToSlicetypeMap[i]!=='3D') {
                arr.push(viewerLayersInstantiator.objectAt(i));
            }
        }
        return arr;
    }

    NodeInstantiator {
        id: viewerLayersInstantiator
        model: root.viewerConfiguration.viewportsLayout.length
        delegate:
        Layer {
            recursive: true
        }
    }


}
