import QtQuick
import Qt3D.Core 2.4
import Qt3D.Render 2.4
import Qt3D.Input 2.4
import Qt3D.Extras 2.4

import libViewer

Entity {
    id: root

    property var renderLayers: {// [opaqueLayer,transparentLayer,noDepthCheckLayer]
        var arr=[]
        if(renderLayersInstantiator.count>0) {
            for(var i=0;i<renderLayersInstantiator.count;i++) {
                arr.push(renderLayersInstantiator.objectAt(i));
            }
            return arr;
        }
        return null;
    }

    property var opaqueLayer: {
        var arr=[];
        if(renderLayersInstantiator.count>0) {
            arr.push(renderLayersInstantiator.objectAt(0));
            return arr;
        }
        return null;
    }
    property var transparentLayer: {
        var arr=[];
        if(renderLayersInstantiator.count>0) {
            arr.push(renderLayersInstantiator.objectAt(1));
            return arr;
        }
        return null;
    }
    property var noDepthTestLayer: {
        var arr=[];
        if(renderLayersInstantiator.count>=2) {
            arr.push(renderLayersInstantiator.objectAt(2));
            return arr;
        }
        return null;
    }
    property Layer cappingLayer: Layer { id: capLayer }

    NodeInstantiator {
        id: renderLayersInstantiator
        model: 3
        delegate:
        Layer {
            recursive: true
        }
    }


}
