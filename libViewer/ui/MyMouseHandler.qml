import QtQuick
import Qt3D.Core 2.4
import Qt3D.Render 2.4
import Qt3D.Input 2.4
import Qt3D.Extras 2.4

import libViewer

Entity {
    id: root
    property Cameras camerasEntity
    property real viewerWidth:100
    property real viewerHeight:100
    property bool adjustingWindowLevelWidth:false;
    property bool volumeRenderingLevelingEnabled:false;
    property ViewerConfiguration viewerConfiguration
    property int mouseIsInViewport: -1
    property Repeater viewportsRepeater

    signal windowLevelWidthChanged(real level,real width);
    signal volumeRenderingWindowLevelWidthChanged(real level,real width);
    signal moveToNextSliceRequested(int mouseIsInViewport,int cranialOrCaudal/*-1,1*/);
    signal moveSlicesToPositionRequested(int x, int y)
    signal leftClickInThreeDViewport(int x, int y)

    MouseDevice {
        id: mouseDevice
        //sensitivity: 10
    }
    MouseHandler {
        id:mouseHandler
        property bool leftButtonPressed: false
        property bool rightButtonPressed: false
        property bool leftAndRightButtonPressed: false
        property vector2d lastPosition: Qt.vector2d(0,0)
        property vector2d lastLeftAndRightClickPosition: Qt.vector2d(0,0)
        sourceDevice: mouseDevice

        onPressed: function(mouse) {

            // left: 1
            // right: 2
            // middle: 4
            if(mouse.buttons===2) {
                rightButtonPressed=true;
            }

            if(mouse.buttons===1)
            {
                leftButtonPressed=true;

                if(root.adjustingWindowLevelWidth || root.volumeRenderingLevelingEnabled) {
                    lastPosition = Qt.vector2d(mouse.x,mouse.y)
                }
            }
            if(mouse.buttons===3) {
                leftAndRightButtonPressed = true
                lastLeftAndRightClickPosition = Qt.vector2d(mouse.x,mouse.y)
            }

        }
        onWheel: function(wheel) {
            if(!root.adjustingWindowLevelWidth && !root.volumeRenderingLevelingEnabled && root.mouseIsInViewport!=root.viewerConfiguration.getThreeDViewportId()) {
                //sceneContent.moveToNextSlice(root.mouseIsInViewport,Math.sign(wheel.angleDelta.y))
                root.moveToNextSliceRequested(root.mouseIsInViewport,Math.sign(wheel.angleDelta.y));
            }
        }

        onReleased: {
            leftButtonPressed = false;
            rightButtonPressed = false;
            leftAndRightButtonPressed = false
        }

        onPositionChanged: function(mouse) {

            if((!leftButtonPressed) && (!rightButtonPressed) && (!leftAndRightButtonPressed)) {
                var widthInPx=root.viewerWidth
                var heightInPx=root.viewerHeight

                for (var i = 0; i < root.viewportsRepeater.count; i++) {

                    if(mouse.x>root.viewportsRepeater.itemAt(i).getViewportLeft()*widthInPx &&
                       mouse.x<root.viewportsRepeater.itemAt(i).getViewportRight()*widthInPx &&
                       mouse.y>root.viewportsRepeater.itemAt(i).getViewportTop()*heightInPx &&
                       mouse.y<root.viewportsRepeater.itemAt(i).getViewportBottom()*heightInPx) {
                        root.mouseIsInViewport = i;
                        //root.mouseIsInViewport = i;
                    }
                }
            }

            if(root.adjustingWindowLevelWidth && leftButtonPressed) {
                var deltaPosX = mouse.x-lastPosition.x
                var deltaPosY = mouse.y-lastPosition.y
                lastPosition = Qt.vector2d(mouse.x,mouse.y)
                //sceneContent.setWindowLevelWidth(-deltaPosY*10,deltaPosX*10)
                root.windowLevelWidthChanged(-deltaPosY*10,deltaPosX*10)
            }
            if(root.volumeRenderingLevelingEnabled && leftButtonPressed) {
                var deltaPosX2 = mouse.x-lastPosition.x
                var deltaPosY2 = mouse.y-lastPosition.y
                lastPosition = Qt.vector2d(mouse.x,mouse.y)
                //sceneContent.setVolumeRenderingWindowLevelWidth(-deltaPosY2*10,deltaPosX2*10)
                root.volumeRenderingWindowLevelWidthChanged(-deltaPosY2*10,deltaPosX2*10)
            }

            if(rightButtonPressed) {
                // do nothing to disable rotation in cam controller
            }

            else if (leftAndRightButtonPressed) { // Left & Right button for zoom
                var ry = (mouse.y - lastLeftAndRightClickPosition.y)
                lastLeftAndRightClickPosition = Qt.vector2d(mouse.x,mouse.y)
                root.camerasEntity.changeZoom(root.mouseIsInViewport,Math.sign(ry))
            }

        }
        onDoubleClicked:function(mouse) {
            if(mouse.buttons===1 &&  root.mouseIsInViewport!=root.viewerConfiguration.getThreeDViewportId())
            {
                root.moveSlicesToPositionRequested(mouse.x,mouse.y)
            }
        }
        onClicked: function(mouse){
            // do nothing
        }
    }

}
