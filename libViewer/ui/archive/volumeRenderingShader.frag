#version 150 core

in vec3 worldPosition;
in vec2 texCoord;

out vec4 fragColor;

uniform vec3 borderColor;
uniform float borderWidth;
uniform float alpha;
uniform sampler2D myTex;
uniform isampler3D myTex3D;

uniform vec3 dimension;
uniform vec3 spacing;
uniform vec3 origin;
uniform vec3 dimensionTimesSpacing;
uniform mat4 orientation;
uniform float minValue;
uniform float maxValue;

uniform float windowWidth;
uniform float windowLevel;

void main()
{
    //output color from material


    float maxX = 1.0 - borderWidth;
    float minX = borderWidth;
    float maxY = maxX;
    float minY = minX;

    float alphaValue = 1.0;
    //if (texCoord.x < maxX && texCoord.x > minX &&
    //    texCoord.y < maxY && texCoord.y > minY) {

        float texClr = 0.0;

        // position in world COS to position in image COS
        mat4 imagePose = orientation;
        imagePose[3] = vec4(origin,1.0);
        vec3 texCoord3d = vec3(inverse(imagePose)*vec4(worldPosition,1.0))/dimensionTimesSpacing;
        //vec3 texCoord3d = (worldPosition - origin) / dimensionTimesSpacing;
        if(texCoord3d.x>1.0||texCoord3d.x<0.0||
                texCoord3d.y>1.0||texCoord3d.y<0.0||
                texCoord3d.z>1.0||texCoord3d.z<0.0) { // outside of volume
            fragColor = vec4(0.0,0.0,0,0);
        //} else if(texCoord3d.x>(1-borderWidth/dimensionTimesSpacing.x)||texCoord3d.x<borderWidth/dimensionTimesSpacing.x||
        //        texCoord3d.y>(1-borderWidth/dimensionTimesSpacing.y)||texCoord3d.y<borderWidth/dimensionTimesSpacing.y||
        //        texCoord3d.z>(1-borderWidth/dimensionTimesSpacing.z)||texCoord3d.z<borderWidth/dimensionTimesSpacing.z) { // frame
        //    fragColor = vec4(borderColor,1.0);
        } else {

            int texVal = texture(myTex3D, texCoord3d.xyz).r;
            if(texVal<=(windowLevel-windowWidth/2.0)) {
                fragColor = vec4(0.0,0.0,0.0,alpha);
            } else if(texVal>=(windowLevel+windowWidth/2.0)) {
                fragColor = vec4(1.0,1.0,1.0,alpha);
            } else {
                float texClr = (texVal-(windowLevel-windowWidth/2.0))/windowWidth;
                fragColor = vec4(texClr,texClr,texClr,alpha);
            }
        }

}

