import Qt3D.Core 2.15
import Qt3D.Render 2.15
import Qt3D.Extras 2.15
import QtQuick 2.15
import Qt3D.Input 2.15

import libViewer

Entity {
    id: root

    property var layers:[]
    property vector3d point1: Qt.vector3d(-10,0,0)
    property vector3d point2: Qt.vector3d(-5,15,0)
    property vector3d point3: Qt.vector3d(0,0,10)
    property bool depthTesting: true
    property bool picking: true
    property color color: "gray"

    property vector3d lastPickingPosition: Qt.vector3d(0,0,0)

    signal positionChanged(vector3d position)


    components: {
        var myArray=[transform,geometry,material];

        if(root.picking) {
            myArray.push(picker)
        }

        for(var i=0;i<root.layers.length;i++) {
            myArray.push(root.layers[i]);
        }

        return myArray;
    }

    TriangleShaderEffect {
        id: effect
        depthTesting: root.depthTesting
        color: root.color
    }

    Material {
        id: material
        effect: effect
    }

    Transform {
        id: transform
    }

    ObjectPicker {
        id: picker
        hoverEnabled: false
        dragEnabled: true
        onMoved: {
            console.log("triangle picker");
            if(pick.buttons===1) { // Left mouse button
                var dp=(pick.localIntersection).minus(root.lastPickingPosition);
                console.log(dp)
                root.lastPickingPosition=pick.localIntersection
                root.positionChanged(pick.worldIntersection)
            }
        }
    }

    GeometryRenderer {
        id: geometry

        instanceCount: 1
        //indexOffset: 0
        //firstInstance: 0
        primitiveType: GeometryRenderer.Triangles

        geometry: Geometry {
            Attribute {
                attributeType: Attribute.VertexAttribute
                vertexBaseType: Attribute.Float
                vertexSize: 3
                byteOffset: 0
                byteStride: 3*4
                count: 3
                name: "vertexPosition" // Name of attribute in the shader
                buffer: Buffer {
                    id: vertexBuffer
                    type: Buffer.VertexBuffer
                    data: new Float32Array([root.point1.x, root.point1.y,root.point1.z,
                                            root.point2.x, root.point2.y,root.point2.z,
                                            root.point3.x, root.point3.y,root.point3.z,
                                           ])
                }
            }
        }
    }
}
