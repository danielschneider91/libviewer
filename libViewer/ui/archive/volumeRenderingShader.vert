#version 150 core

in vec3 vertexPosition;

out vec3 worldPosition;
//out vec3 worldPos;

uniform mat4 modelMatrix;
uniform mat4 viewMatrix;
uniform mat4 projectionMatrix;
uniform mat4 viewProjectionMatrix;
uniform mat4 mvp;
uniform vec3 origin;
uniform vec3 dimensionTimesSpacing;

void main()
{
    // Transform position, normal, and tangent to world coords
    vec3 offset = origin+dimensionTimesSpacing/2.0;
    worldPosition = vec3(modelMatrix *  ((mat4(inverse(mat3(viewMatrix)))*vec4(vertexPosition, 1.0))+vec4(offset,0)));

    // Calculate vertex position in clip coordinates
    gl_Position = viewProjectionMatrix *modelMatrix * ((mat4(inverse(mat3(viewMatrix)))*vec4(vertexPosition, 1.0))+vec4(offset,0));
}
