import Qt3D.Core 2.15
import Qt3D.Render 2.15
import Qt3D.Extras 2.15
import QtQuick 2.15
import Qt3D.Input 2.15

import libViewer

Entity {
    id: root

    property var layers:[]
    property var vertices:[]
    property bool depthTesting: true
    property bool picking: true
    property color color: "gray"

    signal positionChanged(vector3d position)


    components: {
        var myArray=[transform,geometry,material];

        if(root.picking) {
            myArray.push(picker)
        }

        for(var i=0;i<root.layers.length;i++) {
            myArray.push(root.layers[i]);
        }

        return myArray;
    }

    TriangleShaderEffect {
        id: effect
        depthTesting: root.depthTesting
        color: root.color
    }

    Material {
        id: material
        effect: effect
    }

    Transform {
        id: transform
    }

    ObjectPicker {
        id: picker
        hoverEnabled: false
        dragEnabled: true
        onMoved: {
            console.log("triangles picker");
            if(pick.buttons===1) { // Left mouse button
                root.positionChanged(pick.localIntersection)
            }
        }
    }

    GeometryRenderer {
        id: geometry

        instanceCount: 1
        //indexOffset: 0
        //firstInstance: 0
        primitiveType: GeometryRenderer.Triangles

        geometry: Geometry {
            Attribute {
                attributeType: Attribute.VertexAttribute
                vertexBaseType: Attribute.Float
                vertexSize: 3
                byteOffset: 0
                byteStride: 3*4
                count: root.vertices.length
                name: "vertexPosition" // Name of attribute in the shader
                buffer: Buffer {
                    id: vertexBuffer
                    type: Buffer.VertexBuffer
                    //data: new Float32Array([root.point1.x, root.point1.y,root.point1.z,
                    //                        root.point2.x, root.point2.y,root.point2.z,
                    //                        root.point3.x, root.point3.y,root.point3.z,
                    //                       ])
                    data: {
                        console.assert(root.vertices.length%3===0,"Triangles: Number of vertices must be a multiple of 3.");
                        var myArray = new Float32Array(root.vertices.length*3)


                        var counter = 0
                        for(var i=0;i<root.vertices.length;i++) {
                            myArray[counter]=root.vertices[i].x
                            counter = counter+1;
                            myArray[counter]=root.vertices[i].y
                            counter = counter+1;
                            myArray[counter]=root.vertices[i].z
                            counter = counter+1;
                        }
                        return myArray
                    }
                }
            }
        }
    }
}
