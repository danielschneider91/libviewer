import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Scene3D 2.14
import Qt3D.Core 2.15
import Qt3D.Render 2.15
import Qt3D.Input 2.15
import Qt3D.Extras 2.15

Item {
    id: root
    // 6 viewports
    //readonly property var viewportsLayout: [Qt.rect(0.0, 0.0, 0.5, 0.5),Qt.rect(0.5, 0.0, 0.25, 0.5),Qt.rect(0.75, 0.0, 0.25, 0.5),
    //    Qt.rect(0.0, 0.5, 0.5, 0.5),Qt.rect(0.5, 0.5, 0.25,0.5),Qt.rect(0.75, 0.5, 0.25,0.5)]
    //
    //readonly property var viewportsEnabled: [true,true,true,true,true,true]
    //readonly property int numberOfRows:2
    //readonly property int numberOfColumns:3
    //
    //readonly property variant viewportToSlicetypeMap: { 0: 'Axial', 1: 'Coronal',2:'Sagittal',3:'3D',4: 'Axial', 5: 'Coronal' }
    //readonly property variant viewportToSliceIdMap: { 0: 'Axial0', 1: 'Coronal0',2:'Sagittal0',3:'3D',4: 'Axial1', 5: 'Coronal1' }
    //readonly property variant viewportToViewportNameMap: { 0: 'Axial', 1: 'Coronal',2:'Sagittal',3:'3D',4: 'Axial1', 5: 'Coronal1' }
    //readonly property variant sliceIdToNormalShiftMap: { 'Axial0':0, 'Coronal0':0,'Sagittal0':0,'Axial1':10,'Coronal1':-10 }
    //readonly property real threeDViewportId: 3

    // 4 viewports
    readonly property var viewportsLayout: [Qt.rect(0.0, 0.0, 0.5, 0.5),Qt.rect(0.5, 0.0, 0.5, 0.5),
                                    Qt.rect(0.0, 0.5, 0.5, 0.5),Qt.rect(0.5, 0.5, 0.5,0.5)]
    readonly property var viewportsEnabled: [true,true,true,true];
    readonly property int numberOfRows:2
    readonly property int numberOfColumns:2

    readonly property variant viewportToSlicetypeMap: { 0: 'Axial', 1: 'Coronal', 2: 'Sagittal', 3: '3D' }
    readonly property variant viewportToSliceIdMap: { 0: 'Axial0', 1: 'Coronal0', 2: 'Sagittal0', 3: '3D' }
    readonly property variant viewportToViewportNameMap: { 0: 'Axial', 1: 'Coronal', 2: 'Sagittal', 3: '3D' }
    readonly property variant sliceIdToNormalShiftMap: { 'Axial0':0, 'Coronal0':0,'Sagittal0':0 }
    readonly property real threeDViewportId: 3 //fourth viewport

    property var horizontalLineColors: ["green","blue","green"]
    property var verticalLineColors: ["red","red","blue"]
    property var sliceColors: ["blue","green","red"]

    function getNumberOfSlices() {
        var counter = 0;
        for (var prop in root.viewportToSlicetypeMap) {
            //prop:0,1,2
            //root.viewportToSlicetypeMap[prop]: Axial, Coronal, Sagittal, 3D
            if(root.viewportToSlicetypeMap[prop]!=='3D') {
                counter++;
            }
        }
        return counter;
    }
    function getThreeDViewportId() {
        for (var prop in root.viewportToSlicetypeMap) {
            //prop:0,1,2,3,...
            //root.viewportToSlicetypeMap[prop]: Axial, Coronal, Sagittal, 3D
            if(root.viewportToSlicetypeMap[prop]==='3D') {
                console.assert(prop==root.threeDViewportId, "prop not equal root.threeDViewport: "+prop+", "+root.threeDViewportId)
                return prop;
            }
        }
        return -1;
    }
    // slices numbered from 0 to m
    // 3d viewport at position p
    // slices after position p are on viewport i+1
    function getViewportIndexFromSliceIndex(index) {
        var threeDViewerCounter = 0
        for(var i=0;i<=index;i++) {
            if(root.viewportToSlicetypeMap[i]==='3D') {
                threeDViewerCounter++
            }
        }
        return index+threeDViewerCounter;
    }
    function getSliceIndexFromViewportIndex(index) {
        var threeDViewerCounter = 0
        for(var i=0;i<=index;i++) {
            if(root.viewportToSlicetypeMap[i]==='3D') {
                threeDViewerCounter++
            }
        }
        return index-threeDViewerCounter;
    }
}
