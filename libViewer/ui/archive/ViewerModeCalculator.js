function calculateCameraPose(index,T_world_plane,T_world_camRoot,T_camRoot_cam,viewerMode,activeViewport) {
    /*
       T_world_plane: word pose of slice plane
       T_world_camRoot: word pose of camera transform
       T_camRoot_cam: pose of camera relative to camera transform
    */

    return sceneContent.sliceTransforms[getSliceIndexFromViewportIteration(index)].worldMatrix;
}
