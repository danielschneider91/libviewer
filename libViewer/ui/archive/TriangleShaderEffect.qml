import Qt3D.Core 2.0
import Qt3D.Render 2.15

import libViewer
Effect {
    id: root

    //! [1]
    property string vertex: "qrc:/shaders/gl3/triangleShader.vert"
    property string fragment: "qrc:/shaders/gl3/triangleShader.frag"

    property color color: Qt.rgba(0.5,0.5,0.5,1)
    property bool depthTesting: true


    FilterKey {
        id: forward
        name: "renderingStyle"
        value: "forward"
    }

    //! [2]
    ShaderProgram {
        id: gl3Shader
        vertexShaderCode: loadSource(parent.vertex)
        fragmentShaderCode: loadSource(parent.fragment)
    }

    parameters: [
        Parameter {
            name: "color"
            value: Qt.vector4d(root.color.r, root.color.g, root.color.b,0.3)
        },
        Parameter {
            name: "alpha"
            value: 0.3
        }

    ]

    techniques: [
        //! [3]
        // OpenGL 3.1
        Technique {
            filterKeys: [forward]
            graphicsApiFilter {
                api: GraphicsApiFilter.OpenGL
                profile: GraphicsApiFilter.CoreProfile
                majorVersion: 3
                minorVersion: 1
            }
            renderPasses: RenderPass {
                shaderProgram: gl3Shader
                renderStates: [
                    CullFace { mode: CullFace.NoCulling },
                    DepthTest { depthFunction: root.depthTesting ? DepthTest.Less :DepthTest.Always }
                ]
            }
        }
    ]
}

