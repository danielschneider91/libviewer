import Qt3D.Core 2.15
import Qt3D.Render 2.15
import Qt3D.Extras 2.15
import QtQuick 2.15
import Qt3D.Input 2.15

import libViewer

Entity {
    id: root

    property var viewerLayers:[]
    property var renderLayers:[]
    property bool depthTesting: true
    property bool pickingEnabled: true
    property color color: "gray"
    property vector3d position: Qt.vector3d(0,0,0)
    property real width: 20
    property real height: 2
    property int shiftMode: 0;//0: x-y, 1: x (horizontal), 2: y (vertical)
    property int shiftOrRotation: 0;//0: shift; 1: rotation
    property bool isPicking: false;
    readonly property var points:[Qt.vector3d(-1,-1,0),Qt.vector3d(1,1,0), Qt.vector3d(1,-1,0),Qt.vector3d(-1,-1,0), Qt.vector3d(-1,1,0), Qt.vector3d(1,1,0)]
    signal planePositionChanged(vector3d localPosition)
    signal planeOrientationChanged(real angleInDeg)

    //property matrix4x4 t_world_crosshair: Qt.matrix4x4(1,0,0,0,0,1,0,0,0,0,1,0,0,0,0,1)
    property real horizontalOrVertical: 0 //0: horizontal, 1: vertical
    property real lowerOrUpper: 0 //0: lower, 1: upper
    property real leftOrRight:0 // 0: left, 1: right
    components: {
        var myArray=[transform,geometry,material];

        if(root.pickingEnabled) {
            myArray.push(picker)
        }

        for(var i=0;i<root.viewerLayers.length;i++) {
            myArray.push(root.viewerLayers[i]);
        }
        for(var j=0;j<root.renderLayers.length;j++) {
            myArray.push(root.renderLayers[j]);
        }

        return myArray;
    }

    function rotationTo(from, to) {
        // Based on Stan Melax's article in Game Programming Gems
        var fromNormalized = from.normalized();
        var toNormalized = to.normalized();

        var d = fromNormalized.dotProduct(toNormalized) + 1.0;

        // if dest vector is close to the inverse of source vector, ANY axis of rotation is valid
        if (fuzzyIsNull(d)) {


            var axis = Qt.vector3d(1.0, 0.0, 0.0).crossProduct(fromNormalized);
            if (fuzzyIsNull(axis.length()*axis.length())) {
                axis = Qt.vector3d(0.0, 1.0, 0.0).crossProduct(fromNormalized);
            }
            axis = axis.normalized();

            // same as QQuaternion::fromAxisAndAngle(axis, 180.0f)
            return Qt.quaternion(0.0, axis.x, axis.y, axis.z);
        }

        d = Math.sqrt(2.0 * d);
        var ax = fromNormalized.crossProduct(toNormalized).times(1.0/d);
        var vec4 = Qt.vector4d(d * 0.5, ax.x,ax.y,ax.z).normalized();
        return Qt.quaternion(vec4.x, vec4.y,vec4.z,vec4.w);
    }

    TriangleShaderEffect {
        id: effect
        depthTesting: root.depthTesting
        color: root.color
    }

    Material {
        id: material
        effect: effect
    }

    Transform {
        id: transform
    }

    property vector3d lastPosition: Qt.vector3d(0,0,0)
    property real lastAngle: 0
    ObjectPicker {
        id: picker
        hoverEnabled: false
        dragEnabled: true
        onReleased: {
            root.isPicking = false;
        }

        onPressed: {
            root.isPicking = true;
            if(root.shiftOrRotation===0) { // shift
                lastPosition = pick.worldIntersection;
            }
        }

        onMoved: {
            root.isPicking = true;

            if(root.shiftOrRotation===0) { // shift

                if(pick.buttons===1) { // Left mouse button

                    if(root.shiftMode===0) {
                        //console.log("shift mode 0");

                        var dPosition = pick.worldIntersection.minus(lastPosition)
                        lastPosition = pick.worldIntersection
                        root.planePositionChanged(dPosition);
                    }
                    if(root.shiftMode===1) { // shift in x (horizontal) --> do not change y coord
                        // TODO
                    }
                    if(root.shiftMode===2) { // shift in y (vertical) --> do not change x coord
                        // TODO
                    }
                }
            }

            if(root.shiftOrRotation===1) { // rotation
                var rad = 0;

                if(root.horizontalOrVertical===0) { // horizontal
                    if(root.leftOrRight===0) { // left
                        rad = Math.atan2(pick.localIntersection.y,pick.localIntersection.x);
                        if(rad>=0) {
                            rad = rad-Math.PI;
                        } else if(rad<0) {
                            rad = rad+Math.PI;
                        } else {
                            console.assert(false,"something went wrong...");
                        }

                    } else if(root.leftOrRight===1) { // right
                        rad = Math.atan2(pick.localIntersection.y,pick.localIntersection.x);
                    }
                } else if(root.horizontalOrVertical===1) {
                    if(root.lowerOrUpper===0) {// lower
                        rad = Math.atan2(pick.localIntersection.y,pick.localIntersection.x)+Math.PI/2.0;
                    } else if(root.lowerOrUpper===1) {// upper
                        rad = Math.atan2(pick.localIntersection.y,pick.localIntersection.x)-Math.PI/2.0;
                    }
                }

                root.planeOrientationChanged(rad/Math.PI*180.0);
            }
        }
    }

    GeometryRenderer {
        id: geometry
        instanceCount: 1
        primitiveType: GeometryRenderer.Triangles

        geometry: Geometry {
            Attribute {
                attributeType: Attribute.VertexAttribute
                vertexBaseType: Attribute.Float
                vertexSize: 3
                byteOffset: 0
                byteStride: 3*4
                count:root.points.length
                name: "vertexPosition" // Name of attribute in the shader
                buffer: Buffer {
                    id: vertexBuffer
                    type: Buffer.VertexBuffer
                    data: {
                        var myArray = new Float32Array(root.points.length*3)

                        var counter = 0
                        for(var i=0;i<root.points.length;i++) {
                            myArray[counter]=root.points[i].x*root.width/2.0+root.position.x
                            counter = counter+1;
                            myArray[counter]=root.points[i].y*root.height/2.0+root.position.y
                            counter = counter+1;
                            myArray[counter]=root.points[i].z+root.position.z
                            counter = counter+1;
                        }
                        return myArray
                    }
                }
            }
        }
    }
}
