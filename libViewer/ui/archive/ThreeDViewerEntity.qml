import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Scene3D 2.14
import Qt3D.Core 2.15
import Qt3D.Render 2.15
import Qt3D.Input 2.15
import Qt3D.Extras 2.15

Entity {
    id: root
    property Layer viewerLayer
    property var renderLayers: []

    components: [root.viewerLayer, root.renderLayers[0]/*opaque*/]

    //Entity {
    //    components: [
    //        SphereMesh { radius: 2 },
    //        DiffuseSpecularMaterial {diffuse: "red"},
    //        Transform { translation: Qt.vector3d(0,0,0) }
    //    ]
    //}
}
