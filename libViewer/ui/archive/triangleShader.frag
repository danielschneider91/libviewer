#version 150 core

in vec3 worldPos;

uniform vec4 color;
uniform float alpha;

out vec4 fragColor;

void main() {
    fragColor = vec4(color.rgb,alpha);
}

