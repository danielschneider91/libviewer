import Qt3D.Core 2.0
import Qt3D.Render 2.0
import Qt3D.Input 2.0
import QtQml 2.15
import libViewer

Camera {
    id: camera

    property CameraHandler cameraHandler
    Binding {
        target: cameraHandler.cameraParameters
        property: "position"
        value: camera.position
    }
    Binding {
        target: cameraHandler.cameraParameters
        property: "viewCenter"
        value: camera.viewCenter
    }
    Binding {
        target: cameraHandler.cameraParameters
        property: "upVector"
        value: camera.upVector
    }
    Binding {
        target: cameraHandler.cameraParameters
        property: "bottom"
        value: camera.bottom
    }
    Binding {
        target: cameraHandler.cameraParameters
        property: "top"
        value: camera.top
    }
    Binding {
        target: cameraHandler.cameraParameters
        property: "left"
        value: camera.left
    }
    Binding {
        target: cameraHandler.cameraParameters
        property: "right"
        value: camera.right
    }
    Binding {
        target: cameraHandler.cameraParameters
        property: "fov"
        value: camera.fov
    }

    projectionType: {
        if(cameraHandler.cameraParameters.orthogonal) {
            return CameraLens.OrthographicProjection
        } else {
            return CameraLens.PerspectiveProjection
        }
    }
    nearPlane:   0.01
    farPlane:    1000.0
    fieldOfView: cameraHandler.cameraParameters.fov
    position: cameraHandler.cameraParameters.position
    viewCenter: cameraHandler.cameraParameters.viewCenter
    upVector: cameraHandler.cameraParameters.upVector
    bottom: cameraHandler.cameraParameters.bottom
    top: cameraHandler.cameraParameters.top
    left: cameraHandler.cameraParameters.left
    right: cameraHandler.cameraParameters.right
    aspectRatio: cameraHandler.cameraParameters.aspectRatio
    onAspectRatioChanged: {
        console.log("Aspect ratio changed:" + camera.aspectRatio)
        cameraHandler.cameraParameters.aspectRatio=camera.aspectRatio
        cameraHandler.qml_aspect_ratio_changed(aspectRatio);
    }
}
