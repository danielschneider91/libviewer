import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Scene3D 2.14
import Qt3D.Core 2.15
import Qt3D.Render 2.15
import Qt3D.Input 2.15
import Qt3D.Extras 2.15

Entity {
    id: root

    property color horizontalLineColor: "red"
    property color verticalLineColor: "green"
    property real lineWidth: 1
    property var viewerLayers: []
    property var renderLayers: []
    property bool pickingEnabled: false
    property bool isPicking: {
        return horizontalLeftRotationPickingPlane.isPicking || horizontalRightRotationPickingPlane.isPicking ||
                verticalLowerRotationPickingPlane .isPicking || verticalUpperRotationPickingPlane.isPicking ||
                centerPickingPlane.isPicking;
    }

    property real width: 10.0
    property real height: 10.0
    property real rotationLineProportion: 0.1 // in %/100, each side
    property real centerProportion: 0.15 // in %/100
    readonly property real gapProportion: 0.01 // in %/100
    //readonly property real pickingPlaneWidth: 0.01 // %/100

    readonly property real zOffset: 0.5


    //signal positionChanged(vector3d position)
    //signal orientationChanged(matrix4x4 orientation)
    signal positionChanged(vector3d localPosition)
    signal orientationChanged(real angleInDeg)

    Component.onCompleted: {
        horizontalLeftPickingPlane.planePositionChanged.connect(root.positionChanged)
        horizontalRightPickingPlane.planePositionChanged.connect(root.positionChanged)
        verticalLowerPickingPlane.planePositionChanged.connect(root.positionChanged)
        verticalUpperPickingPlane.planePositionChanged.connect(root.positionChanged)
        centerPickingPlane.planePositionChanged.connect(root.positionChanged)

        horizontalLeftRotationPickingPlane.planeOrientationChanged.connect(root.orientationChanged)
        horizontalRightRotationPickingPlane.planeOrientationChanged.connect(root.orientationChanged)
        verticalLowerRotationPickingPlane.planeOrientationChanged.connect(root.orientationChanged)
        verticalUpperRotationPickingPlane.planeOrientationChanged.connect(root.orientationChanged)
    }

    Entity {
        Line {
            id: horizontalLine
            viewerLayers: root.viewerLayers
            renderLayers: [root.renderLayers[2]]/*no depth test*/
            startPoint: Qt.vector3d(-root.width/2,0,0)
            endPoint: Qt.vector3d(root.width/2,0,0)
            lineColor: root.horizontalLineColor
            lineWidth: root.lineWidth
            depthTesting: false
        }
        Line {
            id: verticalLine
            viewerLayers: root.viewerLayers
            renderLayers: [root.renderLayers[2]]/*no depth test*/
            startPoint: Qt.vector3d(0,-root.height/2,0)
            endPoint: Qt.vector3d(0,root.height/2,0)
            lineColor: root.verticalLineColor
            lineWidth: root.lineWidth
            depthTesting: false
        }

        // ---------------- horizontal ----------------
        PickingPlane {
            id: horizontalLeftPickingPlane
            viewerLayers: root.viewerLayers
            renderLayers: [root.renderLayers[1]]/*transparent*/

            position: {
                var shiftLineProportion = (1.0-2.0*root.rotationLineProportion-4.0*root.gapProportion-root.centerProportion)/2.0;
                return Qt.vector3d((-1.0*root.gapProportion-root.centerProportion/2.0-shiftLineProportion/2.0)*root.width,0,root.zOffset);
            }
            width: {
                var shiftLineProportion = (1.0-2.0*root.rotationLineProportion-4.0*root.gapProportion-root.centerProportion)/2.0;
                return shiftLineProportion*root.width
            }
            height: root.centerProportion*root.height
            pickingEnabled:false;// root.pickingEnabledEnabled
            color: "blue"
            shiftOrRotation: 0
            leftOrRight: 0
            shiftMode: 2
        }
        PickingPlane {
            id: horizontalLeftRotationPickingPlane
            viewerLayers: root.viewerLayers
            renderLayers: [root.renderLayers[1]]/*transparent*/
            position: {
                var shiftLineProportion = (1.0-2.0*root.rotationLineProportion-4.0*root.gapProportion-root.centerProportion)/2.0;
                return Qt.vector3d((-root.rotationLineProportion/2.0-2.0*root.gapProportion-root.centerProportion/2.0-shiftLineProportion)*root.width,0,root.zOffset);
            }
            width: root.rotationLineProportion*root.width
            height: root.centerProportion*root.height
            pickingEnabled: root.pickingEnabled
            color: "red"
            shiftOrRotation:1
            leftOrRight: 0
            horizontalOrVertical: 0
        }
        PickingPlane {
            id: horizontalRightPickingPlane
            viewerLayers: root.viewerLayers
            renderLayers: [root.renderLayers[1]]/*transparent*/
            position: {
                var shiftLineProportion = (1.0-2.0*root.rotationLineProportion-4.0*root.gapProportion-root.centerProportion)/2.0;
                return Qt.vector3d((1.0*root.gapProportion+root.centerProportion/2.0+shiftLineProportion/2.0)*root.width,0,root.zOffset);
            }
            width: {
                var shiftLineProportion = (1.0-2.0*root.rotationLineProportion-4.0*root.gapProportion-root.centerProportion)/2.0;
                return shiftLineProportion*root.width
            }
            height: root.centerProportion*root.height
            pickingEnabled:false;// root.pickingEnabled
            color: Qt.rgba(0.1,0.1,0.8,1)
            shiftOrRotation:0
            leftOrRight: 1
            shiftMode: 2
        }
        PickingPlane {
            id: horizontalRightRotationPickingPlane
            viewerLayers: root.viewerLayers
            renderLayers: [root.renderLayers[1]]/*transparent*/
            position: {
                var shiftLineProportion = (1.0-2.0*root.rotationLineProportion-4.0*root.gapProportion-root.centerProportion)/2.0;
                return Qt.vector3d((root.rotationLineProportion/2.0+2.0*root.gapProportion+root.centerProportion/2.0+shiftLineProportion)*root.width,0,root.zOffset);
            }
            width: root.rotationLineProportion*root.width
            height: root.centerProportion*root.height
            pickingEnabled: root.pickingEnabled
            color: Qt.rgba(0.8,0.1,0.1,1)
            shiftOrRotation:1
            leftOrRight: 1
            horizontalOrVertical: 0
        }


        // ---------------- vertical ----------------
        PickingPlane {
            id: verticalLowerPickingPlane
            viewerLayers: root.viewerLayers
            renderLayers: [root.renderLayers[1]]/*transparent*/
            position: {
                var shiftLineProportion = (1.0-2.0*root.rotationLineProportion-4.0*root.gapProportion-root.centerProportion)/2.0;
                return Qt.vector3d(0.0,(-1.0*root.gapProportion-root.centerProportion/2.0-shiftLineProportion/2.0)*root.height,root.zOffset);
            }
            height: {
                var shiftLineProportion = (1.0-2.0*root.rotationLineProportion-4.0*root.gapProportion-root.centerProportion)/2.0;
                return shiftLineProportion*root.height
            }
            width: root.centerProportion*root.width
            pickingEnabled:false;// root.pickingEnabled
            color: "blue"
            shiftMode: 1
            shiftOrRotation:0
        }
        PickingPlane {
            id: verticalLowerRotationPickingPlane
            viewerLayers: root.viewerLayers
            renderLayers: [root.renderLayers[1]]/*transparent*/
            position: {
                var shiftLineProportion = (1.0-2.0*root.rotationLineProportion-4.0*root.gapProportion-root.centerProportion)/2.0;
                return Qt.vector3d(0.0,(-root.rotationLineProportion/2.0-2.0*root.gapProportion-root.centerProportion/2.0-shiftLineProportion)*root.width,root.zOffset);
            }
            height: root.rotationLineProportion*root.height
            width: root.centerProportion*root.width
            pickingEnabled: root.pickingEnabled
            color: "red"
            shiftOrRotation:1
            horizontalOrVertical: 1
            lowerOrUpper: 0
        }


        PickingPlane {
            id: verticalUpperPickingPlane
            viewerLayers: root.viewerLayers
            renderLayers: [root.renderLayers[1]]/*transparent*/
            position: {
                var shiftLineProportion = (1.0-2.0*root.rotationLineProportion-4.0*root.gapProportion-root.centerProportion)/2.0;
                return Qt.vector3d(0.0,(1.0*root.gapProportion+root.centerProportion/2.0+shiftLineProportion/2.0)*root.height,root.zOffset);
            }
            height: {
                var shiftLineProportion = (1.0-2.0*root.rotationLineProportion-4.0*root.gapProportion-root.centerProportion)/2.0;
                return shiftLineProportion*root.height
            }
            width: root.centerProportion*root.width
            pickingEnabled:false;// root.pickingEnabled
            color: Qt.rgba(0.1,0.1,0.8,1)
            shiftMode: 1
            shiftOrRotation:0
        }
        PickingPlane {
            id: verticalUpperRotationPickingPlane
            viewerLayers: root.viewerLayers
            renderLayers: [root.renderLayers[1]]/*transparent*/
            position: {
                var shiftLineProportion = (1.0-2.0*root.rotationLineProportion-4.0*root.gapProportion-root.centerProportion)/2.0;
                return Qt.vector3d(0.0,(root.rotationLineProportion/2.0+2.0*root.gapProportion+root.centerProportion/2.0+shiftLineProportion)*root.width,root.zOffset);
            }
            height: root.rotationLineProportion*root.height
            width: root.centerProportion*root.width
            pickingEnabled: root.pickingEnabled
            color: Qt.rgba(0.8,0.1,0.1,1)
            shiftOrRotation:1
            horizontalOrVertical: 1
            lowerOrUpper: 1
        }

        // ---------------- center ----------------
        PickingPlane {
            id: centerPickingPlane
            viewerLayers: root.viewerLayers
            renderLayers: [root.renderLayers[1]]/*transparent*/

            position: Qt.vector3d(0,0,root.zOffset)
            height: root.centerProportion*root.height
            width: root.centerProportion*root.width
            pickingEnabled: root.pickingEnabled
            color: "orange"
            //color: Qt.rgba(0.0,0.0,0.0,0.0)
            shiftMode: 0
            shiftOrRotation:0
        }

    }



}
