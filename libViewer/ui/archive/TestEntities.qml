import Qt3D.Core 2.12
import Qt3D.Extras 2.15
import Qt3D.Render 2.12
import QtQml 2.14
import Qt3D.Input 2.0
import QtQuick 2.15

Entity {
    id: root
    property var viewerLayers: []
    property var renderLayers: []
    property var position: Qt.vector3d(0,0,0)

    Entity {
        Transform {
            id: testTrafo
            translation: root.position//Qt.vector3d(0,0,-40)
        }
        SphereMesh {
            id: testMesh
            radius: 20
        }
        DiffuseSpecularMaterial {
            id: testMaterial
            diffuse: "red"
        }
        components: {
            var arr=[testTrafo,testMesh,testMaterial]
            for(var i=0;i<root.viewerLayers.length;i++) {
                arr.push(root.viewerLayers[i])
            }
            arr.push(root.renderLayers[0]/*opaque*/)
            return arr;
        }
    }
    Entity {
        Transform {
            id: test1Trafo
            translation: Qt.vector3d(10,0,-40)
        }
        TorusMesh {
            id: test1Mesh
            radius: 6
            minorRadius: 5
        }
        DiffuseSpecularMaterial {
            id: test1Material
            alphaBlending: true
            diffuse: Qt.rgba(0,0.9,0,0.3)
            shininess: 250
        }
        components: {
            var arr=[test1Trafo,test1Mesh,test1Material]
            for(var i=0;i<root.viewerLayers.length;i++) {
                arr.push(root.viewerLayers[i])
            }
            arr.push(root.renderLayers[1]/*transparent*/)
            return arr;
        }
    }
    Entity {
        Transform {
            id: test2Trafo
            translation: Qt.vector3d(0,10,-40)
        }
        CuboidMesh {
            id: test2Mesh
            xExtent: 7
            yExtent: 2
            zExtent: 6
        }
        DiffuseSpecularMaterial {
            id: test2Material
            diffuse: "blue"
        }
        components: {
            var arr=[test2Trafo,test2Mesh,test2Material]
            for(var i=0;i<root.viewerLayers.length;i++) {
                arr.push(root.viewerLayers[i])
            }
            arr.push(root.renderLayers[2]/*opaque*/)
            return arr;
        }
    }
}
