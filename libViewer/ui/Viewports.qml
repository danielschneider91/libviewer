import QtQuick
import Qt3D.Core 2.4
import Qt3D.Render 2.4
import Qt3D.Input 2.4
import Qt3D.Extras 2.4

import libViewer

import "ExpandShrinkScreenLogic.js" as ExpandShrinkScreenLogic

Item {
    id: root
    property alias viewportsRepeater: viewportsRepeater
    property ViewerConfiguration viewerConfiguration

    Repeater {
        id: viewportsRepeater
        model: root.viewerConfiguration.viewportsLayout.length
        ViewportData {
            data: root.viewerConfiguration.viewportsLayout[index]
            isEnabled: root.viewerConfiguration.viewportsEnabled[index]
        }
    }

    //function toggleExpandViewport(viewerIndex) {
    //    if(viewerIndex===-1 ||
    //            viewportsRepeater.itemAt(viewerIndex).data.width===1.0 && viewportsRepeater.itemAt(viewerIndex).data.height===1.0) { // reduce
    //
    //        for(var i=0;i<viewportsRepeater.count;i++) { // all same size/ normal
    //            var rect0 = ExpandShrinkScreenLogic.resetToOriginal(root.viewerConfiguration.viewportsLayout[i])
    //            viewportsRepeater.itemAt(i).data = root.viewerConfiguration.viewportsLayout[index];//Qt.rect(rect0[0],rect0[1],rect0[2],rect0[3])
    //            viewportsRepeater.itemAt(i).isEnabled = root.viewerConfiguration.viewportsEnabled[index];//true
    //            viewportsRepeater.itemAt(i).state = 1
    //        }
    //    } else { // expand viewport with id viewerIndex
    //        for(var j=0;j<viewportsRepeater.count;j++) { // expanded
    //            if(j===viewerIndex) {
    //                var rect1 = ExpandShrinkScreenLogic.expand(j,root.viewerConfiguration.numberOfColumns,root.viewerConfiguration.numberOfRows)
    //                viewportsRepeater.itemAt(j).data = Qt.rect(rect1[0],rect1[1],rect1[2],rect1[3])
    //                viewportsRepeater.itemAt(j).isEnabled = true
    //                viewportsRepeater.itemAt(j).state = 2
    //
    //
    //            } else { // minimized
    //                var behaviour = 0
    //                var rect2 = ExpandShrinkScreenLogic.minimize(j,viewerIndex,root.viewerConfiguration.numberOfColumns,root.viewerConfiguration.numberOfRows,behaviour)
    //                viewportsRepeater.itemAt(j).data = Qt.rect(rect2[0],rect2[1],rect2[2],rect2[3])
    //                viewportsRepeater.itemAt(j).state = 0
    //
    //                if(behaviour===0) {
    //                    viewportsRepeater.itemAt(j).isEnabled = false
    //                }
    //            }
    //        }
    //    }
    //}
}
