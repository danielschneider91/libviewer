#version 150 core

// TODO: Replace with a uniform block
//uniform vec4 lightPosition;
//uniform vec3 lightIntensity;

// TODO: Replace with a struct
uniform vec4 ka;            // Ambient reflectivity
uniform vec4 kd;            // Diffuse reflectivity
uniform vec4 ks;            // Specular reflectivity
uniform float shininess;    // Specular shininess factor

in VertexData {
    in vec3 position;
    in vec3 normal;
} v_in;
//in vec3 position;
//in vec3 normal;

out vec4 fragColor;

vec3 adsModel( const in vec3 pos, const in vec3 n )
{
    vec4 lightPosition = vec4(1,1,1,1);
    vec3 lightIntensity = vec3(1,1,1);
    // Calculate the vector from the light to the fragment
    vec3 s = normalize( vec3( lightPosition ) - pos );

    // Calculate the vector from the fragment to the eye position
    // (origin since this is in "eye" or "camera" space)
    vec3 v = normalize( -pos );

    // Reflect the light beam using the normal at this fragment
    vec3 r = reflect( -s, n );

    // Calculate the diffuse component
    float diffuse = max( dot( s, n ), 0.0 );

    // Calculate the specular component
    float specular = 0.0;
    if ( dot( s, n ) > 0.0 )
        specular = pow( max( dot( r, v ), 0.0 ), shininess );

    // Combine the ambient, diffuse and specular contributions
    return lightIntensity * ( vec3(ka) + vec3(kd) * diffuse + vec3(ks) * specular );
}

void main()
{
    fragColor = vec4( adsModel( v_in.position, normalize( v_in.normal ) ), kd.a );
}
