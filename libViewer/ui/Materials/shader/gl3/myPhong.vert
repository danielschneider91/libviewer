#version 150

//layout (location = 0)
in vec3 vertexPosition;
//layout (location = 1)
in vec3 vertexNormal;

out VertexData {
    vec3 position;
    vec3 normal;
} v_out;

uniform mat4 modelView;
uniform mat3 modelViewNormal;
uniform mat4 modelViewProjection;

void main()
{
    v_out.normal = normalize( modelViewNormal * vertexNormal );
    v_out.position = vec3( modelView * vec4( vertexPosition, 1.0 ) );

    gl_Position = modelViewProjection * vec4( vertexPosition, 1.0 );
}
