import Qt3D.Core 2.4
import Qt3D.Extras 2.4
import Qt3D.Render 2.4
import QtQml
import Qt3D.Input 2.4
import QtQuick

import libViewer

import "ClipPlanes"
import "CoordinateSystem"

Entity{
    id: root

    property SpatialObjectsListModel objects
    property var objectsViewportVisibility: []
    property ViewerLayers viewerLayersEntity
    property RenderLayers renderLayersEntity
    property var visibilityListModel: []
    property var enabledListModel: []
    property NodeInstantiator spatialObjectsFromFileLayers: spatialObjectsFromFileLayersInstantiator // only used if visible in slice viewports, used to render capping individually
    property ClippingPlanes clippingPlanes;
    property real planeSize: 800;

    signal spatialObjectPicked(string id,vector3d worldIntersection,vector3d localIntersection)
    signal spatialObjectHover(string id,vector3d worldIntersection,vector3d localIntersection)


    NodeInstantiator {
        id: spatialObjectsFromFileLayersInstantiator
        model: root.objects
        delegate:
        Layer {
            recursive: true
        }
    }

    NodeInstantiator {
        id: threeDModels
        model: root.objects
        onCountChanged: {
            console.log("threeDModels onCount changed: "+threeDModels.count)
        }

        delegate: Entity {
            id: threeDModelAndCappingPlane

            property int visibilityListModelId: {
                if(!root.visibilityListModel) {return -1;}
                var val=-1
                root.visibilityListModel.forEach(function (element, i) {
                    if(element.id===model.id) {
                        val = i
                    }
                });
                return val;
            }
            Entity {
                components: {
                    if(!root.enabledListModel[threeDModelAndCappingPlane.visibilityListModelId].isEnabled) {
                        return [];
                    }

                    if(!root.objectsViewportVisibility[index]) {return false;}
                    if(!root.visibilityListModel || root.visibilityListModel.length<=0 ||
                        !root.enabledListModel || root.enabledListModel.length<=0 ||
                        root.objectsViewportVisibility.length<=0 || index>=root.objectsViewportVisibility.length ||
                        visibilityListModel[threeDModelAndCappingPlane.visibilityListModelId].visibility.visibility == VisibilityClass.HIDDEN) {return [];}
                    var viewerLayersArray = []


                    console.log("Setting up SpatialObjectFromFile with id: "+model.id)
                    if(root.objectsViewportVisibility[index].threeDViewportsVisibility) {
                        viewerLayersArray = viewerLayersArray.concat(root.viewerLayersEntity.threeDViewerLayer);
                    }
                    if(root.objectsViewportVisibility[index].sliceViewportsVisibility) {
                        viewerLayersArray = viewerLayersArray.concat(root.viewerLayersEntity.sliceViewerLayers);
                    }
                    var myArray=[model.transform];
                    if(model.pickingEnabled) {
                      myArray.push(objectPicker)
                    }

                    myArray = myArray.concat(viewerLayersArray);

                   if(model.hasMaterial) {
                       console.log("has material")
                       console.log("NOT IMPLEMENTED!!!!")
                        //myArray.push(threeDModelMeshWithMaterial);
                        myArray = myArray.concat(root.renderLayersEntity.opaqueLayer);
                    return myArray;
                   } else {
                        myArray.push(threeDModelMeshWithoutMaterial)
                        if(root.objectsViewportVisibility[index].sliceViewportsVisibility && spatialObjectsFromFileLayersInstantiator.count>0) {
                            myArray = myArray.concat([spatialObjectsFromFileLayersInstantiator.objectAt(index)])
                        }
                        myArray.push(material);
                        myArray.push(cos)

                        switch(visibilityListModel[threeDModelAndCappingPlane.visibilityListModelId].visibility.visibility){
                            case(VisibilityClass.VISIBLE): {
                                myArray = myArray.concat(root.renderLayersEntity.opaqueLayer);
                                return myArray;
                            }
                            case(VisibilityClass.TRANSPARENNT): {
                                myArray = myArray.concat(root.renderLayersEntity.transparentLayer);
                                return myArray;
                            }
                        }
                   }
                }

            //    //SceneLoader {
            //    //    id: threeDModelMeshWithMaterial
            //    //    source: {
            //    //        console.log("model.filePath: "+model.filePath)
            //    //        model.filePath
            //    //    }
            //    //    onStatusChanged: function(status){
            //    //        console.log("Status changed: "+status)
            //    //    }
            //    //}
                Mesh {
                    id: threeDModelMeshWithoutMaterial
                    source: model.filePath
                    //primitiveType: GeometryRenderer.Lines
                }
                MetalRoughClippingEffect {
                    id:metalRoughClippingEffect
                    baseColor: model.material.baseColor
                    metalness:model.material.metalness
                    roughness: model.material.roughness
                    //ambientOcclusion: model.material.ambientOcclusion
                    //normal: model.material.normal
                    textureScale: model.material.textureScale
                    overlayColor: model.material.baseColor
                    clipPlaneEquation: root.clippingPlanes.clipPlaneEntities.length > 0 ? root.clippingPlanes.clipPlaneEntities[0].equation : null
                    viewportVisibility: root.objectsViewportVisibility.length > 0 ? root.objectsViewportVisibility[index] : null
                }
                DiffuseSpecularMaterialClippingEffect {
                    id:diffuseSpecularMaterialClippingEffect
                    diffuse: model.transparentMaterial.diffuse
                    ambient: model.transparentMaterial.ambient
                    specular: model.transparentMaterial.specular
                    normal: model.transparentMaterial.normal
                    overlayColor: model.transparentMaterial.diffuse
                    clipPlaneEquation: root.clippingPlanes.clipPlaneEntities.length > 0 ? root.clippingPlanes.clipPlaneEntities[0].equation : null
                    viewportVisibility: root.objectsViewportVisibility.length > 0 ? root.objectsViewportVisibility[index] : null
                }

                Material {
                    id: material
                    effect: {
                        switch(visibilityListModel[threeDModelAndCappingPlane.visibilityListModelId].visibility.visibility){
                            case(VisibilityClass.VISIBLE): {
                                return metalRoughClippingEffect
                            }
                            case(VisibilityClass.TRANSPARENNT): {
                                return diffuseSpecularMaterialClippingEffect
                            }
                            case(VisibilityClass.HIDDEN): {
                                return metalRoughClippingEffect
                            }
                        }
                    }
                }

                CoordinateSystem {
                    id: cos
                    scale:10
                    viewerLayers: root.viewerLayersEntity.threeDViewerLayer
                    renderLayers: root.renderLayersEntity.opaqueLayer
                    myEnabled: false
                }

                ObjectPicker {
                    id: objectPicker

                    property vector3d pickedWorldPosition
                    property vector3d pickedLocalPosition
                    property bool isPressed:false
                    property bool hasMoved:false
                    property int pressedButton:-1

                    hoverEnabled: true
                    dragEnabled: true
                    onClicked: console.log("onClicked")
                    onMoved:function(pick)  {
                        objectPicker.hasMoved = true
                        root.spatialObjectHover(model.id,pick.worldIntersection,pick.localIntersection)
                    }
                    onPressed: function(pick) {
                        objectPicker.pressedButton=pick.button;
                        if(pick.button==1/*Left*/ && pick.accepted) {
                            objectPicker.pickedWorldPosition = pick.worldIntersection;
                            objectPicker.pickedLocalPosition = pick.localIntersection;
                            objectPicker.hasMoved = false;
                            objectPicker.isPressed = true;
                        }
                    }
                    onReleased: function(pick) {

                        if(pick.button==1/*Left*/ && pick.accepted && objectPicker.pressedButton==1/*Left*/) {
                            objectPicker.isPressed = false;
                            if(objectPicker.hasMoved==false) {
                                root.spatialObjectPicked(model.id,objectPicker.pickedWorldPosition,objectPicker.pickedLocalPosition)
                            }
                            objectPicker.hasMoved = false
                        }
                    }
                }
            }
            CappingPlane {
                id:cappingPlane

                myEnabled: root.enabledListModel[threeDModelAndCappingPlane.visibilityListModelId].isEnabled
                cappingColor: model.material.baseColor
                planeSize: root.planeSize
                viewerLayersEntity: root.viewerLayersEntity
                renderLayersEntity: root.renderLayersEntity
                spatialObjectLayer: spatialObjectsFromFileLayersInstantiator.count > 0 ? spatialObjectsFromFileLayersInstantiator.objectAt(index) : null
                sliceViewportsVisibility: {
                    if(!root.objectsViewportVisibility[index]) {return false}
                    if(root.objectsViewportVisibility.length <= 0 || index>=root.objectsViewportVisibility.length) {
                        return false;
                    } else {
                        return root.objectsViewportVisibility[index].sliceViewportsVisibility
                    }
                }
                spatialObjectVisibility: root.visibilityListModel[threeDModelAndCappingPlane.visibilityListModelId].visibility.visibility
            }
        }
    }
}
