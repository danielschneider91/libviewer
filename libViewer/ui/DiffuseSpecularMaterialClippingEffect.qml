import Qt3D.Core 2.4
import Qt3D.Render 2.4
import Qt3D.Extras 2.4

import libViewer

Effect {
    id: root

    property alias diffuse:diffuseParam.value
    property alias ambient:ambientParam.value
    property alias specular:specularParam.value
    property alias normal:normalParam.value
    property alias overlayColor:overlayColorParam.value
    property alias clipPlaneEquation: clipPlaneEquationParam.value
    property var viewportVisibility:null

    readonly property FilterKeyCatalogue filterKeyCatalogue: FilterKeyCatalogue {}

    parameters: [
        Parameter { id:overlayColorParam; name: "overlayColor"; value: Qt.rgba(1,0,0,0.5) },
        Parameter { id:diffuseParam; name: "kd"; value: Qt.vector4d(0.2, 0.9, 0.2,0.5) },
        Parameter { id:ambientParam; name: "ka"; value: Qt.vector4d(0.05, 0.05, 0.05,1) },
        Parameter { id:specularParam; name: "ks"; value: Qt.vector4d(0.401, 0.401, 0.401,1)},
        Parameter { id:normalParam; name: "normal"; value: 0.0 },
        Parameter { id:shininessParam; name: "shininess"; value: 0.5 },
        Parameter { id:clipPlaneEquationParam; name: "clipPlaneEquation"; value: Qt.vector4d(0.0, 0.0, 1.0, 1.0)}
    ]

    techniques: [
        Technique {
            filterKeys: [FilterKey { name: "renderingStyle"; value: "forward" }]
            graphicsApiFilter {
                api: GraphicsApiFilter.OpenGL
                profile: GraphicsApiFilter.CoreProfile
                majorVersion: 3
                minorVersion: 1
            }
            RenderPass {
                id: renderPassDefault
                filterKeys: {
                    var arr =[]
                    if(root.viewportVisibility==null) {return arr;}
                    if(root.viewportVisibility.sliceViewportsVisibility) {
                        //if(!root.viewportVisibility.sliceViewportsDrawAsOverlay) {
                        if(root.viewportVisibility.sliceViewportRenderingMode==SliceViewportRenderingModeClass.MODEL ||
                           root.viewportVisibility.sliceViewportRenderingMode==SliceViewportRenderingModeClass.MODEL_DISTANCE_CLIPPED) {
                            arr.push(root.filterKeyCatalogue.sliceViewerTransparentModelFilterKey)
                        }
                    }
                    if(root.viewportVisibility.threeDViewportsVisibility) {
                        arr.push(root.filterKeyCatalogue.threeDViewerTransparentModelFilterKey)
                    }
                    return arr;
                }

                shaderProgram: ShaderProgram {
                    id: diffuseSpecularShader
                    vertexShaderCode: loadSource("qrc:/libViewer/ui/Materials/shader/gl3/myPhong.vert")
                    fragmentShaderCode: loadSource("qrc:/libViewer/ui/Materials/shader/gl3/myPhong.frag")
                }
                renderStates: [
                    DepthTest {
                        depthFunction: {
                            if(!root.viewportVisibility) {return DepthTest.Less}
                            else {
                                if(root.viewportVisibility.depthTesting) { return DepthTest.Less}
                                else {return DepthTest.Always}
                            }
                        }
                    }
                ]
            }

            RenderPass {
                id:renderPassClipped
                filterKeys: [ FilterKey { name: "pass"; value: "clipped" } ]

                shaderProgram: ShaderProgram {
                    id: shaderClipped
                    vertexShaderCode: loadSource("qrc:/libViewer/ui/ClipPlanes/shader/phong-clip.vert")
                    fragmentShaderCode: loadSource("qrc:/libViewer/ui/ClipPlanes/shader/phong-clip.frag")
                }
            }
            RenderPass {
                id:renderPassStencilFill
                filterKeys: {
                    var arr =[]
                    if(root.viewportVisibility==null) {return arr;}
                    if(root.viewportVisibility.sliceViewportsVisibility) {
                        //if(root.viewportVisibility.sliceViewportsDrawAsOverlay) {
                        if(root.viewportVisibility.sliceViewportRenderingMode==SliceViewportRenderingModeClass.OVERLAY) {
                            arr.push(root.filterKeyCatalogue.sliceViewerTransparentOverlayStencilFillFilterKey)
                        }
                    }
                    return arr;
                }

                shaderProgram: ShaderProgram {
                    id: shaderStencilFill
                    vertexShaderCode: loadSource("qrc:/libViewer/ui/ClipPlanes/shader/phong-clip.vert")
                    fragmentShaderCode: loadSource("qrc:/libViewer/ui/ClipPlanes/shader/passthrough.frag")
                }
            }
            renderPasses:[renderPassDefault,renderPassStencilFill,renderPassClipped]
        }
    ]
}

