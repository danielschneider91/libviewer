import QtQuick
import Qt3D.Core 2.4
import Qt3D.Render 2.4
import Qt3D.Input 2.4
import Qt3D.Extras 2.4

import libViewer

Entity {
    id: root
    property int numberOfSlices:3

    property var sliceLayers: {
        var arr=[]
        for(var i=0;i<sliceLayersInstantiator.count;i++) {
            arr.push(sliceLayersInstantiator.objectAt(i));
        }
        return arr;
    }

    NodeInstantiator {
        id: sliceLayersInstantiator
        model: root.numberOfSlices
        delegate:
        Layer {
            recursive: true
        }
    }


}
