import Qt3D.Core 2.4
import Qt3D.Extras 2.4
import Qt3D.Render 2.4
import QtQml
import Qt3D.Input 2.4
import QtQuick

import libViewer

import "ClipPlanes"

Entity{
    id: root

    property PrimitiveSpatialObjectsListModel objects
    property var objectsViewportVisibility: []
    property ViewerLayers viewerLayersEntity
    property RenderLayers renderLayersEntity
    property var visibilityListModel: []
    property var enabledListModel: []
    property NodeInstantiator primitiveSpatialObjectsLayers: primitiveObjectsLayersInstantiator // only used if visible in slice viewports, used to render capping individually
    property ClippingPlanes clippingPlanes;
    property real planeSize: 800;

    signal spatialObjectPicked(string id,vector3d worldIntersection,vector3d localIntersection)
    signal spatialObjectHover(string id,vector3d worldIntersection,vector3d localIntersection)

    NodeInstantiator {
        id: primitiveObjectsLayersInstantiator
        model: root.objects
        delegate:
        Layer {
            recursive: true
        }
    }

    NodeInstantiator {
        id: threeDModels
        model: root.objects ? root.objects:null
        delegate: Entity {
            id: primitiveModelAndCappingPlane
            property int visibilityListModelId: {
                if(!root.visibilityListModel) {return -1;}
                var val=-1
                root.visibilityListModel.forEach(function (element, i) {
                    if(element.id===model.id) {
                        val = i
                    }
                });
                return val;
            }
            Entity {
                components: {
                    if(!root.enabledListModel[primitiveModelAndCappingPlane.visibilityListModelId].isEnabled) {
                        // disabled
                        return []
                    }

                    if(!root.visibilityListModel || root.visibilityListModel.length<=0 ||
                       !root.enabledListModel || root.enabledListModel.length<=0 ||
                       root.objectsViewportVisibility.length<=0 || index>=root.objectsViewportVisibility.length) {return [];}

                    console.log("Setting up PrimitiveSpatialObject with id: "+model.id);

                     var viewerLayersArray = []
                     if(root.objectsViewportVisibility[index].threeDViewportsVisibility) {
                         viewerLayersArray = viewerLayersArray.concat(root.viewerLayersEntity.threeDViewerLayer);
                     }
                     if(root.objectsViewportVisibility[index].sliceViewportsVisibility) {
                         viewerLayersArray = viewerLayersArray.concat(root.viewerLayersEntity.sliceViewerLayers);
                     }



                     var myArray=[model.transform,model.mesh];
                     if(model.pickingEnabled) {
                         myArray.push(objectPicker)
                     }
                     myArray = myArray.concat(viewerLayersArray);

                     if(root.objectsViewportVisibility[index].sliceViewportsVisibility && primitiveObjectsLayersInstantiator.count>0) {
                         myArray = myArray.concat([primitiveObjectsLayersInstantiator.objectAt(index)])
                     }
                     switch(root.visibilityListModel[primitiveModelAndCappingPlane.visibilityListModelId].visibility.visibility){
                     case(VisibilityClass.VISIBLE): {
                         myArray.push(opaqueMaterial);
                         myArray = myArray.concat(root.renderLayersEntity.opaqueLayer);
                         return myArray;
                     }
                     case(VisibilityClass.TRANSPARENNT): {
                         myArray.push(transparentMaterial);
                         myArray = myArray.concat(root.renderLayersEntity.transparentLayer);
                         return myArray;
                     }
                     case(VisibilityClass.HIDDEN): {
                         return []
                     }
                     }
                }
                Material {
                    id: opaqueMaterial
                    effect: MetalRoughClippingEffect {
                        baseColor: model.material.baseColor
                        metalness:model.material.metalness
                        roughness: model.material.roughness
                        //ambientOcclusion: model.material.ambientOcclusion
                        //normal: model.material.normal
                        textureScale: model.material.textureScale
                        overlayColor: model.material.baseColor
                        clipPlaneEquation: root.clippingPlanes.clipPlaneEntities.length > 0 ? root.clippingPlanes.clipPlaneEntities[0].equation : null
                        viewportVisibility: root.objectsViewportVisibility.length > 0 ? root.objectsViewportVisibility[index] : null
                    }
                }
                Material {
                    id: transparentMaterial
                    effect: DiffuseSpecularMaterialClippingEffect {
                        diffuse: model.transparentMaterial.diffuse
                        ambient: model.transparentMaterial.ambient
                        specular: model.transparentMaterial.specular
                        normal: model.transparentMaterial.normal
                        overlayColor: model.transparentMaterial.diffuse
                        clipPlaneEquation: root.clippingPlanes.clipPlaneEntities.length > 0 ? root.clippingPlanes.clipPlaneEntities[0].equation : null
                        viewportVisibility: root.objectsViewportVisibility.length > 0 ? root.objectsViewportVisibility[index] : null
                    }
                }

                ObjectPicker {
                    id: objectPicker
                    property vector3d pickedWorldPosition
                    property vector3d pickedLocalPosition
                    property bool isPressed:false
                    property bool hasMoved:false
                    property int pressedButton:-1

                    hoverEnabled: true
                    dragEnabled: true
                    onClicked: console.log("onClicked")
                    onMoved:function(pick)  {
                        console.log("model.id: "+model.id)
                        console.log("pick.worldIntersection: "+pick.worldIntersection)
                        objectPicker.hasMoved = true
                        root.spatialObjectHover(model.id,pick.worldIntersection,pick.localIntersection)
                    }
                    onPressed: function(pick) {
                        console.log("pick.button: "+pick.button)
                        objectPicker.pressedButton=pick.button;
                        if(pick.button==1/*Left*/ && pick.accepted) {
                            console.log("pick.button: "+pick.button)
                            objectPicker.pickedWorldPosition = pick.worldIntersection;
                            objectPicker.pickedLocalPosition = pick.localIntersection;
                            objectPicker.hasMoved = false;
                            objectPicker.isPressed = true;
                        }
                    }
                    onReleased: function(pick) {
                        console.log("onReleased: "+pick.button)

                        if(pick.button==1/*Left*/ && pick.accepted && objectPicker.pressedButton==1/*Left*/) {
                            objectPicker.isPressed = false;
                            if(objectPicker.hasMoved==false) {
                                console.log("objectPicker.pickedWorldPosition: "+objectPicker.pickedWorldPosition)
                                root.spatialObjectPicked(model.id,objectPicker.pickedWorldPosition,objectPicker.pickedLocalPosition)
                            }
                            objectPicker.hasMoved = false
                        }
                    }
                }

            }
            CappingPlane {
                id:cappingPlane

                myEnabled: root.enabledListModel[primitiveModelAndCappingPlane.visibilityListModelId].isEnabled
                //trafo: root.clippingPlanes.sliceTransformations[0]
                cappingColor: model.material.baseColor
                planeSize: root.planeSize
                viewerLayersEntity: root.viewerLayersEntity
                renderLayersEntity: root.renderLayersEntity
                spatialObjectLayer: primitiveObjectsLayersInstantiator.count > 0 ? primitiveObjectsLayersInstantiator.objectAt(index) : null
                sliceViewportsVisibility: {
                    if(root.objectsViewportVisibility.length <= 0 || index>=root.objectsViewportVisibility.length) {
                        return false;
                    } else {
                        return root.objectsViewportVisibility[index].sliceViewportsVisibility
                    }
                }
                spatialObjectVisibility: root.visibilityListModel[primitiveModelAndCappingPlane.visibilityListModelId].visibility.visibility
            }
        }
    }
}
