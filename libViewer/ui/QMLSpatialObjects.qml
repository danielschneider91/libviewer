import Qt3D.Core 2.4
import Qt3D.Extras 2.4
import Qt3D.Render 2.4
import QtQml
import Qt3D.Input 2.4
import QtQuick

import libViewer

import "Line"
import "Point"
import "Crosshairs"
import "ClipPlanes"

Entity{
    id: root

    property QMLSpatialObjectsListModel objects
    property SpatialObjectsManager spatialObjectsManager
    property AnimationManager animationManager;
    property var objectsViewportVisibility: []
    property ViewerLayers viewerLayersEntity
    property RenderLayers renderLayersEntity
    property var enabledListModel: []
    property var visibilityListModel: []
    property ClippingPlanes clippingPlanes;

    signal spatialObjectPicked(string id,vector3d worldIntersection,vector3d localIntersection)
    signal spatialObjectHover(string id,vector3d worldIntersection,vector3d localIntersection)

    NodeInstantiator {
        id: animationsInstantiator

        model: root.animationManager.qmlSpatialObjectsAnimationParametersContainer.animationParameters
        onObjectAdded: {
            console.log("onObjectAdded: added object at index: "+index)
            var r =root.animationManager.qmlSpatialObjectsAnimationParametersContainer.animationParameters[index].running;
            if(r) {
                animationsInstantiator.objectAt(index).isRunning = true
            } else {
                animationsInstantiator.objectAt(index).completeAnimation()
            }
        }
        onObjectRemoved: {
            console.log("onObjectRemoved: removed object at index: "+index)
            animationsInstantiator.objectAt(index).completeAnimation()
        }

        delegate: Entity {
            id:animationEntity
            property bool ok: (root.animationManager.qmlSpatialObjectsAnimationParametersContainer.animationParameters.length>0 &&
                                root.animationManager.qmlSpatialObjectsAnimationParametersContainer.animatedObjectIds.length>0 &&
                                index<root.animationManager.qmlSpatialObjectsAnimationParametersContainer.animatedObjectIds.length)
            property bool isRunning: false;
            function completeAnimation() {
                console.log("completing animation of node with index "+index)
                anim.complete();
            }

            SequentialAnimation {
                id:anim
                loops: Animation.Infinite
                running: animationEntity.isRunning

                alwaysRunToEnd: true
                NumberAnimation {
                    id:anim1
                    target: {
                        if(index<0||!animationEntity.ok) {
                            return null;
                        }
                        var theId = root.animationManager.qmlSpatialObjectsAnimationParametersContainer.animatedObjectIds[index];
                        var theIdx = root.spatialObjectsManager.get_index_from_id(theId);
                        return objectsInstantiator.objectAt(theIdx).nestedEntity.entity;
                    }
                    //alwaysRunToEnd: true
                    property: index>=0 && animationEntity.ok ? root.animationManager.qmlSpatialObjectsAnimationParametersContainer.animationParameters[index].prop:""
                    to: index>=0 && animationEntity.ok ? root.animationManager.qmlSpatialObjectsAnimationParametersContainer.animationParameters[index].to:0
                    duration: index>=0 && animationEntity.ok ? Math.round(root.animationManager.qmlSpatialObjectsAnimationParametersContainer.animationParameters[index].duration/2.0):0
                    easing.type: Easing.InCubic
                }
                NumberAnimation {
                    id:anim2
                    target: {
                        if(index<0||!animationEntity.ok) {
                            return null;
                        }
                        var theId = root.animationManager.qmlSpatialObjectsAnimationParametersContainer.animatedObjectIds[index];
                        var theIdx = root.spatialObjectsManager.get_index_from_id(theId);
                        return objectsInstantiator.objectAt(theIdx).nestedEntity.entity;
                    }
                    alwaysRunToEnd: true
                    property: index>=0 && animationEntity.ok ? root.animationManager.qmlSpatialObjectsAnimationParametersContainer.animationParameters[index].prop:""
                    to: index>=0 && animationEntity.ok ? root.animationManager.qmlSpatialObjectsAnimationParametersContainer.animationParameters[index].from:0
                    duration: index>=0 && animationEntity.ok ? Math.round(root.animationManager.qmlSpatialObjectsAnimationParametersContainer.animationParameters[index].duration/2.0):0
                    easing.type: Easing.OutQuad
                }
            }
        }

    }

    NodeInstantiator {
        id: objectsInstantiator
        model: root.objects ? root.objects:null
        //{
        //    if( root.spatialObjectsManager && root.spatialObjectsManager.qmlSpatialObjects && !root.spatialObjectsManager.qmlSpatialObjects==="undefined") {
        //        console.log("QMLSpatialObjects model: "+ root.spatialObjectsManager.qmlSpatialObjects)
        //        console.log("QMLSpatialObjects model: "+ root.spatialObjectsManager.qmlSpatialObjects.length)
        //        return root.spatialObjectsManager.qmlSpatialObjects
        //    } else {
        //        console.log("root.spatialObjectsManager or root.spatialObjectsManager.qmlSpatialObjects undefined, returning 0")
        //        return 0;
        //    }
        //}
        onCountChanged: {
            console.log("QMLSpatialObjects count changed: "+ objectsInstantiator.count)
        }

        delegate: Entity {
            id: theEntity
            property alias nestedEntity: geometryLoader
            property int visibilityListModelId: {
                if(!root.visibilityListModel) {return -1;}
                var val=-1
                root.visibilityListModel.forEach(function (element, i) {
                    if(element.id===model.id) {
                        val = i
                    }
                });
                return val;
            }
            components: {
                if(!root.enabledListModel[theEntity.visibilityListModelId].isEnabled) {
                    // disabled
                    console.log("qmlspatialobjects disabled")
                    return [];
                }

                if(!root.visibilityListModel || root.visibilityListModel.length<=0 ||
                    !root.enabledListModel || root.enabledListModel.length<=0 ||
                    root.objectsViewportVisibility.length<=0 || index>=root.objectsViewportVisibility.length) {return [];}

                console.log("Setting up QMLSpatialObject with id: "+model.id)

                 switch(model.type) {
                 case QMLSpatialObjectType.POINTS: {
                     console.log("Setting up POINTS")
                     geometryLoader.source="qrc:/libViewer/ui/Point/Points.qml"
                     geometryLoader.entity.points = model.parameters.positions
                     pointShaderEffect.size = model.parameters.sizes[0]
                     geometryLoader.entity.effect = pointShaderEffect;
                     switch(root.visibilityListModel[theEntity.visibilityListModelId].visibility.visibility){
                        case(VisibilityClass.VISIBLE): { pointShaderEffect.color = model.material.baseColor;break; }
                        case(VisibilityClass.TRANSPARENNT): { pointShaderEffect.color = model.transparentMaterial.diffuse;break; }
                     }
                     if(root.objectsViewportVisibility[index].sliceViewportRenderingMode==SliceViewportRenderingModeClass.MODEL_DISTANCE_CLIPPED) {
                         pointShaderEffect.distanceClippingEnabled = true
                         pointShaderEffect.clippingDistance = root.objectsViewportVisibility[index].clippingDistance;
                         pointShaderEffect.clipPlaneEquation = root.clippingPlanes.clipPlaneEntities.length > 0 ? root.clippingPlanes.clipPlaneEntities[0].equation : null
                     } else {
                         pointShaderEffect.distanceClippingEnabled = false;
                     }
                     break;
                 }
                 case QMLSpatialObjectType.LINE: {
                     console.log("Setting up LINE")
                     geometryLoader.source="qrc:/libViewer/ui/Line/Line.qml"
                     geometryLoader.entity.startPoint = model.parameters.from
                     geometryLoader.entity.endPoint = model.parameters.to
                     geometryLoader.entity.effect = lineShaderEffect;
                     switch(root.visibilityListModel[theEntity.visibilityListModelId].visibility.visibility){
                        case(VisibilityClass.VISIBLE): { geometryLoader.entity.lineColor = model.material.baseColor;break; }
                        case(VisibilityClass.TRANSPARENNT): { geometryLoader.entity.lineColor = model.transparentMaterial.diffuse;break; }
                     }
                     if(root.objectsViewportVisibility[index].sliceViewportsVisibility) {
                         if(root.objectsViewportVisibility[index].sliceViewportRenderingMode==SliceViewportRenderingModeClass.MODEL_DISTANCE_CLIPPED) {
                             lineShaderEffect.distanceClippingEnabled = true
                             lineShaderEffect.clippingDistance = root.objectsViewportVisibility[index].clippingDistance;
                             lineShaderEffect.clipPlaneEquation = root.clippingPlanes.clipPlaneEntities.length > 0 ? root.clippingPlanes.clipPlaneEntities[0].equation : null
                         } else {
                             lineShaderEffect.distanceClippingEnabled = false;
                         }
                     }
                     break;
                 }
                 case QMLSpatialObjectType.CROSSHAIRS: {
                     console.log("Setting up CROSSHAIRS")
                     geometryLoader.source="qrc:/libViewer/ui/Crosshairs/Crosshairs.qml"
                     console.log("model.pickingEnabled: "+model.pickingEnabled)
                     console.log("model.type: "+model.type)
                     console.log("model.parameters: "+model.parameters)
                     console.log("model.parameters.size: "+model.parameters.size)

                     geometryLoader.entity.crosshairsSize = model.parameters.size
                     geometryLoader.entity.lineEffect = lineShaderEffect;
                     geometryLoader.entity.pointEffect = pointShaderEffect;

                     console.log("geometryLoader here0")
                     switch(root.visibilityListModel[theEntity.visibilityListModelId].visibility.visibility){
                        case(VisibilityClass.VISIBLE): { geometryLoader.entity.verticalLineColor = model.material.baseColor;geometryLoader.entity.horizontalLineColor = model.material.baseColor;break; }
                        case(VisibilityClass.TRANSPARENNT): { geometryLoader.entity.verticalLineColor = model.transparentMaterial.diffuse;geometryLoader.entity.horizontalLineColor = model.transparentMaterial.diffuse;break; }
                     }

                     if(root.objectsViewportVisibility[index].sliceViewportsVisibility) {
                         if(root.objectsViewportVisibility[index].sliceViewportRenderingMode==SliceViewportRenderingModeClass.MODEL_DISTANCE_CLIPPED) {
                             lineShaderEffect.distanceClippingEnabled = true
                             lineShaderEffect.clippingDistance = root.objectsViewportVisibility[index].clippingDistance;
                             lineShaderEffect.clipPlaneEquation = root.clippingPlanes.clipPlaneEntities.length > 0 ? root.clippingPlanes.clipPlaneEntities[0].equation : null
                             pointShaderEffect.distanceClippingEnabled = true
                             pointShaderEffect.clippingDistance = root.objectsViewportVisibility[index].clippingDistance;
                             pointShaderEffect.clipPlaneEquation = root.clippingPlanes.clipPlaneEntities.length > 0 ? root.clippingPlanes.clipPlaneEntities[0].equation : null
                             //geometryLoader.entity.distanceClippingEnabled = true
                             //geometryLoader.entity.clippingDistance = root.objectsViewportVisibility[index].clippingDistance;
                             //geometryLoader.entity.clipPlaneEquation = root.clippingPlanes.clipPlaneEntities.length > 0 ? root.clippingPlanes.clipPlaneEntities[0].equation : null
                         } else {
                             lineShaderEffect.distanceClippingEnabled = false;
                             pointShaderEffect.distanceClippingEnabled = false;
                             //geometryLoader.entity.distanceClippingEnabled = false;
                         }
                     }
                     break;
                 }
                 }

                 var viewerLayersArray = []
                 if(root.objectsViewportVisibility[index].threeDViewportsVisibility) {
                     viewerLayersArray = viewerLayersArray.concat(root.viewerLayersEntity.threeDViewerLayer);
                 }
                 if(root.objectsViewportVisibility[index].sliceViewportsVisibility) {
                     viewerLayersArray = viewerLayersArray.concat(root.viewerLayersEntity.sliceViewerLayers);
                 }
                 console.log("here3")

                 var myArray=[model.transform,geometryLoader.entity];
                 if(model.pickingEnabled) {
                     myArray.push(objectPicker)
                 }

                 myArray = myArray.concat(viewerLayersArray);

                 geometryLoader.entity.viewerLayers = viewerLayersArray
                 switch(root.visibilityListModel[theEntity.visibilityListModelId].visibility.visibility){
                 case(VisibilityClass.VISIBLE): {
                     geometryLoader.entity.enabled = true;
                     geometryLoader.entity.renderLayers = root.renderLayersEntity.opaqueLayer;
                     myArray = myArray.concat(root.renderLayersEntity.opaqueLayer);
                     console.log("here4-1")
                     return myArray;
                 }
                 case(VisibilityClass.TRANSPARENNT): {
                     geometryLoader.entity.enabled = true;
                     geometryLoader.entity.renderLayers = root.renderLayersEntity.transparentLayer;
                     myArray = myArray.concat(root.renderLayersEntity.transparentLayer);
                     console.log("here4-2")
                     return myArray;
                 }
                 case(VisibilityClass.HIDDEN): {
                     geometryLoader.entity.enabled = false;
                     console.log("here4-3")
                     return []
                 }
                 }
                 console.log("geometryLoader here4")
            }
            EntityLoader {
                id: geometryLoader
                onStatusChanged: {
                    if(geometryLoader.status == EntityLoader.Null) {
                        console.log("geometryLoader status NULL")
                    }
                    if(geometryLoader.status == EntityLoader.Loading) {
                        console.log("geometryLoader status Loading")
                    }
                    if(geometryLoader.status == EntityLoader.Ready) {
                        console.log("geometryLoader status Ready")
                    }
                    if(geometryLoader.status == EntityLoader.Error) {
                        console.log("geometryLoader status Error")
                    }
                }
            }
            LineShaderEffect {
                id: lineShaderEffect
                depthTesting: {
                    console.log("LineShaderEffect depthTesting")
                    console.log(index)
                    console.log(root.objectsViewportVisibility)
                    console.log(root.objectsViewportVisibility.length)
                    console.log(root.objectsViewportVisibility[index])
                    return root.objectsViewportVisibility[index].depthTesting
                }
            }
            PointShaderEffect {
                id: pointShaderEffect
                depthTesting: root.objectsViewportVisibility[index].depthTesting
            }

            ObjectPicker {
                id: objectPicker

                hoverEnabled: true
                dragEnabled: false
                property vector3d pickedWorldPosition
                property vector3d pickedLocalPosition
                property bool isPressed:false
                property bool hasMoved:false
                property int pressedButton:-1

                onClicked: console.log("onClicked")
                onMoved:function(pick)  {
                    console.log("model.id: "+model.id)
                    console.log("pick.worldIntersection: "+pick.worldIntersection)
                    objectPicker.hasMoved = true
                    root.spatialObjectHover(model.id,pick.worldIntersection,pick.localIntersection)
                }
                onPressed: function(pick) {
                    console.log("pick.button: "+pick.button)
                    objectPicker.pressedButton=pick.button;
                    if(pick.button==1/*Left*/ && pick.accepted) {
                        console.log("pick.button: "+pick.button)
                        objectPicker.pickedWorldPosition = pick.worldIntersection;
                        objectPicker.pickedLocalPosition = pick.localIntersection;
                        objectPicker.hasMoved = false;
                        objectPicker.isPressed = true;
                    }
                }
                onReleased: function(pick) {
                    console.log("onReleased: "+pick.button)

                    if(pick.button==1/*Left*/ && pick.accepted && objectPicker.pressedButton==1/*Left*/) {
                        objectPicker.isPressed = false;
                        if(objectPicker.hasMoved==false) {
                            console.log("objectPicker.pickedWorldPosition: "+objectPicker.pickedWorldPosition)
                            root.spatialObjectPicked(model.id,objectPicker.pickedWorldPosition,objectPicker.pickedLocalPosition)
                        }
                        objectPicker.hasMoved = false
                    }
                }
            }
        }
    }
}
