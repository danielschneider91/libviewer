import QtQuick
import QtQuick.Controls
import QtQuick.Scene3D 2.4
import Qt3D.Core 2.4
import Qt3D.Render 2.4
import Qt3D.Input 2.4
import Qt3D.Extras 2.4

import "MathHelper"

import libViewer

Entity {
    id:root
    property Cameras camerasEntity
    property SlicePlanes slicePlanesEntity
    property ViewerConfiguration viewerConfiguration
    property ViewportsManager viewportsManager


    MathHelper {id: mathHelper}

    Connections {
        target: viewerViewModel
        function onSlice_pose_changed(pose) {
            setSlicePlanePoses(pose);
            for(var i=0; i<root.viewerConfiguration.getNumberOfSlices();i++) {
            }
            setSliceViewerCameraPoses(pose)
            setThreeDViewerCameraPose(pose)
        }
    }
    Connections {
        target: viewportsManager
        function onThreeD_viewer_camera_pose_changed(pos,target,up) {
            console.log("Set 3D viewer camera pose:")
            console.log("Camera position: "+pos)
            console.log("Look at position: "+target)
            console.log("Up direction: "+up)
            var viewportIdx = root.viewerConfiguration.getThreeDViewportId();
            root.camerasEntity.camerasInstantiator.objectAt(viewportIdx).cameraPosition = Qt.vector3d(pos.x,pos.y,pos.z);
            root.camerasEntity.camerasInstantiator.objectAt(viewportIdx).cameraUpVector = Qt.vector3d(up.x,up.y,up.z);
            root.camerasEntity.camerasInstantiator.objectAt(viewportIdx).cameraViewCenter = Qt.vector3d(target.x,target.y,target.z);
        }
        function onThreeD_viewer_camera_view_all_changed() {
            var viewportIdx = root.viewerConfiguration.getThreeDViewportId();
            root.camerasEntity.camerasInstantiator.objectAt(viewportIdx).viewAll();
        }
    }


    function setSlicePlanePoses(pose) {
        var position = Qt.matrix4x4(1,0,0,pose.m14,
                            0,1,0,pose.m24,
                            0,0,1,pose.m34,
                            0,0,0,1).column(3).toVector3d();

        switch(root.viewportsManager.sliceViewerBehaviour.behaviour) {
        //switch(viewerViewModel.viewportsManager.sliceViewerBehaviour.behaviour) {
        case(ViewerBehaviour.MOVING_CROSSHAIR): {
            // set position only, orientation can be chosen manually
            slicePlanesEntity.setPositionOfAllPlanes(position)
            break;
        }
        case(ViewerBehaviour.CENTERED_CROSSHAIR): {
            // set position only, orientation can be chosen manually
            slicePlanesEntity.setPositionOfAllPlanes(position)
            break;
        }
        case(ViewerBehaviour.PROBES_VIEW): {
            // set poses of slices
            slicePlanesEntity.setPoseOfAllPlanes(pose)
            break;
        }
        case(ViewerBehaviour.ROTATING_PROBES_VIEW): {
            // for now: same as PROBES_VIEW
            // set poses of slices
            slicePlanesEntity.setPoseOfAllPlanes(pose)


            /* To be implemented
            // set poses of slices
            var axialPose = slicePlanesEntity.sliceRootTransforms[0];
            var axialZAxis = axialPose.column(2).toVector3d();
            var zAxisTo = pose.column(2).toVector3d();
            // transformation matrix from COS b (zAxisTo) to COS a(axialZAxis) --> T_a<-b
            // at the same time rot_from_to * axialZAxis = zAxisTo
            var rot_from_to = mathHelper.fromTo(axialZAxis,zAxisTo);
            var newAxialSlicePose = rot_from_to.times(axialPose)
            newAxialSlicePose.m14 = axialPose.m14
            newAxialSlicePose.m24 = axialPose.m24
            newAxialSlicePose.m34 = axialPose.m34
            slicePlanesEntity.setPoseOfAllPlanes(newAxialSlicePose)
            */
            break;
        }
        default:
            console.assert(false,"This cannot happen")
        }
    }

    function setSliceViewerCameraPoses(pose) {
        var position = Qt.matrix4x4(1,0,0,pose.m14,
                            0,1,0,pose.m24,
                            0,0,1,pose.m34,
                            0,0,0,1).column(3).toVector3d();

        //switch(viewerViewModel.viewportsManager.sliceViewerBehaviour.behaviour) {
        switch(root.viewportsManager.sliceViewerBehaviour.behaviour) {
        case(ViewerBehaviour.MOVING_CROSSHAIR): {
            for(var i=0; i<root.viewerConfiguration.getNumberOfSlices();i++) {
                var viewportIdx = root.viewerConfiguration.getViewportIndexFromSliceIndex(i);
                var currentAxialPose = slicePlanesEntity.sliceRootTransforms[0];
                var t_world_plane = mathHelper.axialSlicePoseToOtherSlicePose(currentAxialPose,root.viewerConfiguration.viewportToSlicetypeMap[viewportIdx]);
                synchronizeCameraAndSlicePlane(viewportIdx,t_world_plane)
                root.camerasEntity.camerasInstantiator.objectAt(viewportIdx).t_world_camRoot.matrix.m14 = t_world_plane.m14;
                root.camerasEntity.camerasInstantiator.objectAt(viewportIdx).t_world_camRoot.matrix.m24 = t_world_plane.m24;
                root.camerasEntity.camerasInstantiator.objectAt(viewportIdx).t_world_camRoot.matrix.m34 = t_world_plane.m34;
            }
            break;
        }
        case(ViewerBehaviour.CENTERED_CROSSHAIR): {
            // set poses of slice viewer cameras
            for(var i=0; i<root.viewerConfiguration.getNumberOfSlices();i++) {
                var viewportIdx = root.viewerConfiguration.getViewportIndexFromSliceIndex(i);
                var currentAxialPose = slicePlanesEntity.sliceRootTransforms[0];
                var t_world_plane = mathHelper.axialSlicePoseToOtherSlicePose(currentAxialPose,root.viewerConfiguration.viewportToSlicetypeMap[viewportIdx]);
                root.camerasEntity.camerasInstantiator.objectAt(viewportIdx).cameraViewCenter = Qt.vector3d(0,0,0)
                root.camerasEntity.camerasInstantiator.objectAt(viewportIdx).cameraPosition= Qt.vector3d(0,0,-root.camerasEntity.planeToCamDistance)
                root.camerasEntity.camerasInstantiator.objectAt(viewportIdx).t_world_camRoot.matrix.m14 = t_world_plane.m14;
                root.camerasEntity.camerasInstantiator.objectAt(viewportIdx).t_world_camRoot.matrix.m24 = t_world_plane.m24;
                root.camerasEntity.camerasInstantiator.objectAt(viewportIdx).t_world_camRoot.matrix.m34 = t_world_plane.m34;
            }
            break;
        }
        case(ViewerBehaviour.PROBES_VIEW): {
            // set poses of slice viewer cameras
            for(i=0; i<root.viewerConfiguration.getNumberOfSlices();i++) {
                viewportIdx = root.viewerConfiguration.getViewportIndexFromSliceIndex(i);
                t_world_plane = mathHelper.axialSlicePoseToOtherSlicePose(pose,root.viewerConfiguration.viewportToSlicetypeMap[viewportIdx]);
                root.camerasEntity.camerasInstantiator.objectAt(viewportIdx).cameraUpVector= Qt.vector3d(0,-1,0)
                root.camerasEntity.camerasInstantiator.objectAt(viewportIdx).cameraViewCenter= Qt.vector3d(0,0,0)
                root.camerasEntity.camerasInstantiator.objectAt(viewportIdx).cameraPosition= Qt.vector3d(0,0,-root.camerasEntity.planeToCamDistance)
                root.camerasEntity.camerasInstantiator.objectAt(viewportIdx).t_world_camRoot.matrix = t_world_plane
            }
            break;
        }
        case(ViewerBehaviour.ROTATING_PROBES_VIEW): {
            // for now: same as PROBES_VIEW
            // set poses of slice viewer cameras
            for(i=0; i<root.viewerConfiguration.getNumberOfSlices();i++) {
                viewportIdx = root.viewerConfiguration.getViewportIndexFromSliceIndex(i);
                t_world_plane = mathHelper.axialSlicePoseToOtherSlicePose(pose,root.viewerConfiguration.viewportToSlicetypeMap[viewportIdx]);
                root.camerasEntity.camerasInstantiator.objectAt(viewportIdx).cameraUpVector= Qt.vector3d(0,-1,0)
                root.camerasEntity.camerasInstantiator.objectAt(viewportIdx).cameraViewCenter= Qt.vector3d(0,0,0)
                root.camerasEntity.camerasInstantiator.objectAt(viewportIdx).cameraPosition= Qt.vector3d(0,0,-root.camerasEntity.planeToCamDistance)
                root.camerasEntity.camerasInstantiator.objectAt(viewportIdx).t_world_camRoot.matrix = t_world_plane
            }

            /* To be implemented
            for(i=0; i<root.viewerConfiguration.getNumberOfSlices();i++) {
                viewportIdx = root.viewerConfiguration.getViewportIndexFromSliceIndex(i);
                var axialPose = slicePlanesEntity.sliceRootTransforms[0];
                var axialZAxis = axialPose.column(2).toVector3d();
                var zAxisTo = pose.column(2).toVector3d();
                // transformation matrix from COS b (zAxisTo) to COS a(axialZAxis) --> T_a<-b
                // at the same time rot_from_to * axialZAxis = zAxisTo
                var rot_from_to = mathHelper.fromTo(axialZAxis,zAxisTo);
                var newAxialSlicePose = rot_from_to.times(axialPose)
                newAxialSlicePose.m14 = axialPose.m14
                newAxialSlicePose.m24 = axialPose.m24
                newAxialSlicePose.m34 = axialPose.m34
                var t_world_plane = mathHelper.axialSlicePoseToOtherSlicePose(newAxialSlicePose,root.viewerConfiguration.viewportToSlicetypeMap[viewportIdx]);
                root.camerasEntity.camerasInstantiator.objectAt(viewportIdx).cameraViewCenter= Qt.vector3d(0,0,0)
                root.camerasEntity.camerasInstantiator.objectAt(viewportIdx).cameraPosition= Qt.vector3d(0,0,-root.camerasEntity.planeToCamDistance)
                root.camerasEntity.camerasInstantiator.objectAt(viewportIdx).t_world_camRoot.matrix = t_world_plane
            }
            */
            break;
        }
        default:
            console.assert(false,"This cannot happen")
        }
    }

    function setThreeDViewerCameraPose(t_world_plane) {

        var viewportIdx = root.viewerConfiguration.getThreeDViewportId();
        //switch(viewerViewModel.viewportsManager.threeDViewerBehaviour.behaviour) {
        switch(root.viewportsManager.threeDViewerBehaviour.behaviour) {
        case(ViewerBehaviour.MOVING_CROSSHAIR): {
            console.log("MOVING_CROSSHAIR")
            // do not update camera
            break;
        }
        case(ViewerBehaviour.CENTERED_CROSSHAIR): {
            console.log("CENTERED_CROSSHAIR")
            // set pose of 3D viewer camera
            root.camerasEntity.camerasInstantiator.objectAt(viewportIdx).cameraViewCenter = Qt.vector3d(0,0,0)
            root.camerasEntity.camerasInstantiator.objectAt(viewportIdx).t_world_camRoot.matrix.m14 = t_world_plane.m14;
            root.camerasEntity.camerasInstantiator.objectAt(viewportIdx).t_world_camRoot.matrix.m24 = t_world_plane.m24;
            root.camerasEntity.camerasInstantiator.objectAt(viewportIdx).t_world_camRoot.matrix.m34 = t_world_plane.m34;
            break;
        }
        case(ViewerBehaviour.PROBES_VIEW): {
            console.log("PROBES_VIEW")
            // set pose of 3D viewer camera
            root.camerasEntity.camerasInstantiator.objectAt(viewportIdx).cameraUpVector = Qt.vector3d(0,-1,0)
            root.camerasEntity.camerasInstantiator.objectAt(viewportIdx).cameraViewCenter = Qt.vector3d(0,0,0)
            var tmp = root.camerasEntity.camerasInstantiator.objectAt(viewportIdx).cameraPosition.z;
            if(tmp==0) {
                tmp = 100;
            }
            if(tmp<0) {
                tmp=-tmp;
            }
            root.camerasEntity.camerasInstantiator.objectAt(viewportIdx).cameraPosition = Qt.vector3d(0,0,-tmp)
            root.camerasEntity.camerasInstantiator.objectAt(viewportIdx).t_world_camRoot.matrix = t_world_plane

            break;
        }
        case(ViewerBehaviour.ROTATING_PROBES_VIEW): {
            // for now: same as PROBES_VIEW
            // set pose of 3D viewer camera
            root.camerasEntity.camerasInstantiator.objectAt(viewportIdx).cameraUpVector = Qt.vector3d(0,-1,0)
            root.camerasEntity.camerasInstantiator.objectAt(viewportIdx).cameraViewCenter = Qt.vector3d(0,0,0)
            var tmp = root.camerasEntity.camerasInstantiator.objectAt(viewportIdx).cameraPosition.z;
            if(tmp==0) {
                tmp = 100;
            }
            if(tmp<0) {
                tmp=-tmp;
            }
            root.camerasEntity.camerasInstantiator.objectAt(viewportIdx).cameraPosition = Qt.vector3d(0,0,-tmp)
            root.camerasEntity.camerasInstantiator.objectAt(viewportIdx).t_world_camRoot.matrix = t_world_plane

            /* To be implemented
            // set poses of slices
            var axialPose = slicePlanesEntity.sliceRootTransforms[0];
            var axialZAxis = axialPose.column(2).toVector3d();
            var zAxisTo = t_world_plane.column(2).toVector3d();
            // transformation matrix from COS b (zAxisTo) to COS a(axialZAxis) --> T_a<-b
            // at the same time rot_from_to * axialZAxis = zAxisTo
            var rot_from_to = mathHelper.fromTo(axialZAxis,zAxisTo);
            var newAxialSlicePose = rot_from_to.times(axialPose)
            newAxialSlicePose.m14 = axialPose.m14
            newAxialSlicePose.m24 = axialPose.m24
            newAxialSlicePose.m34 = axialPose.m34
            // set pose of 3D viewer camera
            //setCameraPose(root.viewerConfiguration.getThreeDViewportId(), newAxialSlicePose)
            root.camerasEntity.camerasInstantiator.objectAt(viewportIdx).cameraViewCenter= Qt.vector3d(0,0,0)
            root.camerasEntity.camerasInstantiator.objectAt(viewportIdx).cameraPosition= Qt.vector3d(0,0,root.camerasEntity.camerasInstantiator.objectAt(viewportIdx).cameraPosition.z)
            root.camerasEntity.camerasInstantiator.objectAt(viewportIdx).t_world_camRoot.matrix = newAxialSlicePose
            */
            break;
        }
        default:
            console.assert(false,"This cannot happen")
        }
    }

    //--------------------------------
    // TODO:
    // * in slice Viewer set camera pose: ViewerBehaviour.CENTERED_CROSSHAIR) use
     //          var t_world_plane = mathHelper.axialSlicePoseToOtherSlicePose(pose,root.viewerConfiguration.viewportToSlicetypeMap[viewportIdx]);
    function synchronizeCameraAndSlicePlane(viewportIdx,t_world_plane) {
        var T_plane_cam= getCameraRelToSlicePlane(root.camerasEntity.camerasInstantiator.objectAt(viewportIdx).t_camRoot_cam.matrix,
                                                   root.camerasEntity.camerasInstantiator.objectAt(viewportIdx).t_world_camRoot.matrix,
                                                   t_world_plane)
        root.camerasEntity.camerasInstantiator.objectAt(viewportIdx).camm.viewCenter = Qt.vector3d(T_plane_cam.m14,T_plane_cam.m24,0)
        root.camerasEntity.camerasInstantiator.objectAt(viewportIdx).camm.position = Qt.vector3d(T_plane_cam.m14,T_plane_cam.m24,-root.camerasEntity.planeToCamDistance)
    }

    function getCameraRelToSlicePlane(t_camRoot_cam, t_world_camRoot,t_world_plane) {
        var T_camRoot_cam = Qt.matrix4x4(t_camRoot_cam.m11,t_camRoot_cam.m12,t_camRoot_cam.m13,t_camRoot_cam.m14,
                                         t_camRoot_cam.m21,t_camRoot_cam.m22,t_camRoot_cam.m23,t_camRoot_cam.m24,
                                         t_camRoot_cam.m31,t_camRoot_cam.m32,t_camRoot_cam.m33,0,
                                         0,0,0,1);
        var T_world_camRoot = Qt.matrix4x4(t_world_camRoot.m11,t_world_camRoot.m12,t_world_camRoot.m13,t_world_camRoot.m14,
                                           t_world_camRoot.m21,t_world_camRoot.m22,t_world_camRoot.m23,t_world_camRoot.m24,
                                           t_world_camRoot.m31,t_world_camRoot.m32,t_world_camRoot.m33,t_world_camRoot.m34,
                                           0,0,0,1);
        var T_world_plane = Qt.matrix4x4(t_world_plane.m11,t_world_plane.m12,t_world_plane.m13,t_world_plane.m14,
                                         t_world_plane.m21,t_world_plane.m22,t_world_plane.m23,t_world_plane.m24,
                                         t_world_plane.m31,t_world_plane.m32,t_world_plane.m33,t_world_plane.m34,
                                         0,0,0,1);

        var T_plane_camRoot = T_world_plane.inverted().times(T_world_camRoot);
        var T_plane_cam = (T_plane_camRoot).times(T_camRoot_cam);

        return T_plane_cam;
    }

}
