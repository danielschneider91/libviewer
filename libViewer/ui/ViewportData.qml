import QtQuick 2.0

Item {
    id: root
    property rect data
    property bool isEnabled: true
    property int state: 1/* 0: minimized; 1: normal; 2: expanded */

    function getViewportLeft(){
        return root.data.left;
    }
    function getViewportRight(){
        return root.data.right;
    }
    function getViewportBottom(){
        return root.data.bottom;
    }
    function getViewportTop(){
        return root.data.top;
    }
}
