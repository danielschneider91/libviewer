import Qt3D.Core 2.4
import Qt3D.Extras 2.4
import Qt3D.Render 2.4
import QtQml
import Qt3D.Input 2.4
import QtQuick

import "VolumeVisualization"
import "SlicesCrosshairs"

import libViewer

// (XY-)Plane, crosshair lines, picking
Entity {
    id: root

    property var sliceViewerLayers:[]
    property var threeDViewerLayer:[]
    property var sliceLayers:[]
    property var renderLayers: []
    property var noDepthTestLayer:[]

    property alias volumeShadedSlicePlaneEffect: volumeShadedPlane.effect
    property alias isCrosshairPicking: volumeShadedPlane.isCrosshairPicking /* state whether currently the plane is being picked and potentially shifted/rotated */
    property SlicePlaneSettings slicePlaneSettings: SlicePlaneSettings {}

    signal positionChanged(vector3d deltaPosition)
    signal orientationChanged(real angleInDeg)
    signal spatialObjectPicked(vector3d worldIntersection,vector3d localIntersection)
    signal spatialObjectHover(vector3d worldIntersection,vector3d localIntersection)

    Component.onCompleted: {
        volumeShadedPlane.planePositionChanged.connect(root.positionChanged)
        volumeShadedPlane.planeOrientationChanged.connect(root.orientationChanged)
        volumeShadedPlane.spatialObjectPicked.connect(root.spatialObjectPicked)
        volumeShadedPlane.spatialObjectHover.connect(root.spatialObjectHover)
    }
    function onSlicePlaneSettingsChanged() {
        console.log("SlicePlane onSlicePlaneSettingsChanged")
        console.log(slicePlaneSettings.planeWidth)
        console.log(slicePlaneSettings.planeHeight)
    }

    function updateVolumeBoundedLineRenderingEffect(dimensionsTimesSpacing,originBoundingBox,orientation) {
        crosshair.updateVolumeBoundedLineRenderingEffect(dimensionsTimesSpacing,originBoundingBox,orientation)
    }

    VolumeShadedPlane { // (XY-)Plane, crosshair picking, mouse indicator (dot)
        id: volumeShadedPlane
        sliceViewerLayers: root.sliceViewerLayers
        threeDViewerLayer: root.threeDViewerLayer
        sliceLayers: root.sliceLayers

        renderLayers: root.renderLayers
        noDepthTestLayer: root.noDepthTestLayer

        volumeShadedPlaneSettings.planeWidth: root.slicePlaneSettings.planeWidth
        volumeShadedPlaneSettings.planeHeight: root.slicePlaneSettings.planeHeight
        volumeShadedPlaneSettings.color: root.slicePlaneSettings.color
        volumeShadedPlaneSettings.alpha: root.slicePlaneSettings.alpha
        volumeShadedPlaneSettings.crosshairEnabled: root.slicePlaneSettings.crosshairEnabled
        volumeShadedPlaneSettings.crosshairInteractionEnabled: root.slicePlaneSettings.crosshairInteractionEnabled
        volumeShadedPlaneSettings.crosshairInteractionDimensions: root.slicePlaneSettings.crosshairInteractionDimensions
        volumeShadedPlaneSettings.planePickingEnabled: root.slicePlaneSettings.planePickingEnabled
        volumeShadedPlaneSettings.name: root.slicePlaneSettings.name
    }

    SlicesCrosshairs { // crosshair lines
        id: crosshair
        sliceViewerLayers: root.sliceViewerLayers
        threeDViewerLayer: root.threeDViewerLayer
        renderLayers: root.renderLayers
        noDepthTestLayer: root.noDepthTestLayer

        planeWidth: root.slicePlaneSettings.planeWidth
        planeHeight: root.slicePlaneSettings.planeHeight
        horizontalLineColor: root.slicePlaneSettings.horizontalLineColor
        verticalLineColor: root.slicePlaneSettings.verticalLineColor
        lineWidth: root.slicePlaneSettings.lineWidth
        crosshairEnabled: root.slicePlaneSettings.crosshairEnabled
    }


}
