#version 150 core

in vec3 vertexPosition;

out VertexData {
    vec3 position;
} v_out;

uniform mat4 mvp;
uniform mat4 modelMatrix;
uniform mat4 viewMatrix;
uniform mat4 projectionMatrix;
uniform mat4 viewProjectionMatrix;
uniform mat4 myModelMatrix;

void main()
{
    v_out.position = vertexPosition;
    gl_Position = viewProjectionMatrix * myModelMatrix * vec4( vertexPosition, 1.0 );
}
