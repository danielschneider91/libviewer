#version 150 core

in vec3 vertexPosition;
in vec3 vertexNormal;

out VertexData {
    vec3 position;
    vec3 normal;
} v_out;

out gl_PerVertex
{
  vec4 gl_Position;
  float gl_ClipDistance[1];
};

uniform mat4 modelView;
uniform mat4 modelMatrix;
uniform mat3 modelViewNormal;
uniform mat4 mvp;
uniform vec4 clipPlaneEquation;

void main()
{
    v_out.normal = normalize( modelViewNormal * vertexNormal );
    v_out.position = vec4(modelView * vec4( vertexPosition, 1.0 )).xyz;
    vec3 worldPos = vec4(modelMatrix * vec4(vertexPosition, 1.0)).xyz;

    gl_ClipDistance[0] = dot(vec4(worldPos, 1.0), clipPlaneEquation);

    gl_Position = mvp * vec4( vertexPosition, 1.0 );

}
