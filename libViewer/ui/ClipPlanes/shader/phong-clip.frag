#version 150 core

in VertexData {
    in vec4 position;
    in vec3 normal;
} v_in;

out vec4 fragColor;

uniform vec4 overlayColor;

void main()
{
    fragColor = overlayColor;
}
