#version 150 core

out vec4 fragColor;

in FragData {
    vec3 fragPosition;
    vec3 fragNormal;
} f_in;

uniform vec4 overlayColor;

void main()
{
    fragColor = vec4(overlayColor.xyz,0.5);//+ (1.0 * vec4(f_in.fragPosition, 1.0));
}
