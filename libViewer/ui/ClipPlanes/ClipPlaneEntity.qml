import Qt3D.Core 2.4
import Qt3D.Render 2.4
import Qt3D.Extras 2.4

Entity {
    id: root

    property matrix4x4 transform;
    readonly property vector3d center: root.transform.column(3).toVector3d()
    readonly property vector3d normal: root.transform.column(2).toVector3d()
    readonly property vector4d equation: Qt.vector4d(root.normal.x,
                                                     root.normal.y,
                                                     root.normal.z,
                                                     -(root.normal.x * root.center.x +
                                                       root.normal.y * root.center.y +
                                                       root.normal.z * root.center.z))
}

