import Qt3D.Core 2.4
import Qt3D.Render 2.4

import libViewer

import ".."

Effect {
    id: root

    property alias modelMat: modelMatrixParam.value
    property alias overlayColor: overlayColorParam.value

    readonly property FilterKeyCatalogue filterKeyCatalogue: FilterKeyCatalogue{}
    parameters: [
        Parameter { id:modelMatrixParam; name: "myModelMatrix"; value: Qt.matrix4x4(1,0,0,0,0,1,0,0,0,0,1,0,0,0,0,1) },
        Parameter { id:overlayColorParam; name: "overlayColor"; value: Qt.rgba(1,0,0,0.5) }
    ]

    techniques: [
        Technique {

            graphicsApiFilter {
                api: GraphicsApiFilter.OpenGL
                profile: GraphicsApiFilter.CoreProfile
                majorVersion: 3
                minorVersion: 2
            }

            RenderPass {
                id: renderPassCapping
                filterKeys: [root.filterKeyCatalogue.sliceViewerOpaqueOverlayCappingFilterKey,
                root.filterKeyCatalogue.sliceViewerTransparentOverlayCappingFilterKey]
                    //var arr = []
                    //arr.push(root.filterKeyCatalogue.sliceViewerOpaqueOverlayCappingFilterKey)
                    //arr.push(root.filterKeyCatalogue.sliceViewerTransparentOverlayCappingFilterKey)
                    //return arr;
                shaderProgram: ShaderProgram {
                    vertexShaderCode: loadSource("qrc:/libViewer/ui/ClipPlanes/shader/capping.vert")
                    //geometryShaderCode: loadSource("qrc:/libViewer/ui/ClipPlanes/shader/capping.geom")
                    fragmentShaderCode: loadSource("qrc:/libViewer/ui/ClipPlanes/shader/capping.frag")
                }
            }
            renderPasses: [renderPassCapping]
        }
    ]
}

