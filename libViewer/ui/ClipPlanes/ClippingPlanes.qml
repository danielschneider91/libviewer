import QtQuick
import Qt3D.Core 2.4
import Qt3D.Render 2.4
import Qt3D.Extras 2.4

import libViewer

import ".."
import "../Plane"

Entity {
    id: root
    property ViewerConfiguration viewerConfiguration
    property var sliceTransformations // array of Qt.matrix4x4, length: root.viewerConfiguration.getNumberOfSlices()

    property var clipPlaneEntities: {
        var arr=[]
        for(var i=0;i<clipPlanesInstantiator.count;i++) {
            arr.push(clipPlanesInstantiator.objectAt(i))
        }
        return arr;
    }

    NodeInstantiator {
        id: clipPlanesInstantiator
        model: root.viewerConfiguration.getNumberOfSlices()
        delegate: ClipPlaneEntity {
            id: clipPlane
            transform: root.sliceTransformations[index]
        }
    }
}
