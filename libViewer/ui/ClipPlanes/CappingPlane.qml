import Qt3D.Core 2.4
import Qt3D.Render 2.4

import libViewer

import "../Plane"
import "../"

Entity {
    id: root

    property matrix4x4 trafo: Qt.matrix4x4()
    property color cappingColor: "red"
    property int planeSize: 500
    property ViewerLayers viewerLayersEntity
    property RenderLayers renderLayersEntity
    property Layer spatialObjectLayer
    property bool sliceViewportsVisibility: false
    property int spatialObjectVisibility: 0 // 0: hidden; 1: transparent; 2: visible/opaque
    property bool myEnabled:true

    Material {
        id:cappingMaterial
        effect: CappingMaterialEffect {
            modelMat: root.trafo
            overlayColor: root.cappingColor
        }
    }
    Transform {
        id: cappingTransform
        matrix: root.trafo
    }
    Plane {
        id: customPlaneMesh
        width: root.planeSize
        height: root.planeSize
    }
    components:
    {
        if(!root.myEnabled) {return []};
        if(root.spatialObjectLayer==null) {return []};
        if(root.sliceViewportsVisibility) {
            var viewerLayersArray = []
            viewerLayersArray = viewerLayersArray.concat(root.viewerLayersEntity.sliceViewerLayers);
            var myArray=[cappingMaterial, cappingTransform,customPlaneMesh];
            myArray = myArray.concat(viewerLayersArray);
            myArray = myArray.concat(root.renderLayersEntity.cappingLayer);

            if(root.spatialObjectLayer!=null) {
                myArray = myArray.concat(root.spatialObjectLayer)
            }

            switch(root.spatialObjectVisibility){
                case(VisibilityClass.VISIBLE): {
                    myArray = myArray.concat(root.renderLayersEntity.opaqueLayer);
                    return myArray;
                }
                case(VisibilityClass.TRANSPARENNT): {
                    myArray = myArray.concat(root.renderLayersEntity.transparentLayer);
                    return myArray;
                }
                case(VisibilityClass.HIDDEN): {
                    console.log("HIDDEN")
                    return [];
                }
            }
        } else {
            return []
        }
    }
}
