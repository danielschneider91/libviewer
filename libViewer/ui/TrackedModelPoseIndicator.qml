import QtQuick
import QtQuick.Controls
import QtQuick.Scene3D 2.4
import Qt3D.Core 2.4
import Qt3D.Render 2.4
import Qt3D.Input 2.4
import Qt3D.Extras 2.4

import "Line"

import libViewer
Entity {
    id: root

    property SpatialObjectsListModel spatialObjects
    property var objectsViewportVisibility: []
    property ViewerLayers viewerLayersEntity
    property RenderLayers renderLayersEntity
    property var visibilityListModel: []
    property vector2d viewportResolution: Qt.vector2d(800,600) // viewport width height in pixel
    property bool indicatorsEnabled: true

    LineShaderEffect {
        id: lineEffect
        depthTesting: false
    }
    DashedLineShaderEffect {
        id: dashedLineEffect
        depthTesting: false
        resolution: root.viewportResolution
    }

    NodeInstantiator {
        id: trackedObjectIndicatorLine
        model: root.spatialObjects ? root.spatialObjects : null
        delegate: Entity {
            components: {
                if(!root.indicatorsEnabled || !root.visibilityListModel) {
                    return [];
                }

                var visibilityListModelId=-1
                root.visibilityListModel.forEach(function (element, i) {
                    if(element.id===model.id) {
                        visibilityListModelId = i
                    }
                });
                var arr = [model.transform,solidLineMesh,dashedLineMesh];
                switch(root.visibilityListModel[visibilityListModelId].visibility.visibility){
                case(VisibilityClass.VISIBLE): {
                    arr = arr.concat(root.renderLayersEntity.opaqueLayer);
                    return arr;
                }
                case(VisibilityClass.TRANSPARENNT): {
                    arr = arr.concat(root.renderLayersEntity.opaqueLayer);
                    return arr;
                }
                case(VisibilityClass.HIDDEN): {
                    return []
                }
                }


           }
           //Transform {
           //     id: lineTrafo
           //     matrix: {
           //         return model.pose
           //     }
           //}
            Line {
                id: dashedLineMesh
                myEnabled: root.indicatorsEnabled
                startPoint: Qt.vector3d(0,0,-100000)
                endPoint: Qt.vector3d(0,0,-200)
                lineColor: {
                    return "#FFA500"
                    //model.transparentMaterial.diffuse
                }
                lineWidth: 3.0
                viewerLayers: root.viewerLayersEntity.sliceViewerLayers
                renderLayers: root.renderLayersEntity.opaqueLayer
                effect: dashedLineEffect
            }
            Line {
                id: solidLineMesh
                myEnabled: root.indicatorsEnabled
                startPoint: Qt.vector3d(0,0,-200)
                endPoint: Qt.vector3d(0,0,0)
                lineColor: "#FFA500"//model.transparentMaterial.diffuse
                lineWidth: 3.0
                viewerLayers: root.viewerLayersEntity.sliceViewerLayers
                renderLayers: root.renderLayersEntity.opaqueLayer
                effect: lineEffect
            }

        }
    }
}
