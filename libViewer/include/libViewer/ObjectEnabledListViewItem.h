#pragma once

#include <QObject>
#include <QString>

#include "libViewer/VisibilityClass.h"

namespace viewer {
class ObjectEnabledListViewItem : public QObject {
    Q_OBJECT
public:
	Q_PROPERTY(QString name READ get_name WRITE set_name NOTIFY name_changed)
	Q_PROPERTY(QString id READ get_id WRITE set_id NOTIFY id_changed)
	Q_PROPERTY(bool isEnabled READ get_is_enabled WRITE set_is_enabled NOTIFY is_enabled_changed)

	explicit ObjectEnabledListViewItem();
	ObjectEnabledListViewItem(const QString& name, const QString& id, const bool& is_enabled);
	
	QString get_name() const;
	void set_name(const QString& name);
	QString get_id() const;
	void set_id(const QString& id);
	bool get_is_enabled() const;
	void set_is_enabled(bool is_enabled);
	
signals:
	void name_changed();
	void id_changed();
	void is_enabled_changed();

private:
	QString m_name;
	QString m_id;
	bool m_is_enabled;
	
};
}
