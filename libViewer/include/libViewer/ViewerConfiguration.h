#pragma once

#include <QAbstractTextureImage>

#include <QVector3D>
#include <QMatrix4x4>
#include <QList>
#include <QMap>
#include <QRectF>
#include <QString>
#include <QVariantMap>

#include "libViewer/SliceType.h"

namespace viewer {
class ViewerConfiguration : public QObject {
Q_OBJECT
public:
    Q_PROPERTY(QList<QRectF> viewportsLayout READ get_viewports_layout NOTIFY viewports_layout_changed)
    Q_PROPERTY(QList<bool> viewportsEnabled READ get_viewports_enabled NOTIFY viewports_enabled_changed)
    Q_PROPERTY(int numberOfRows READ get_number_of_rows NOTIFY number_of_rows_changed)
    Q_PROPERTY(int numberOfColumns READ get_number_of_columns NOTIFY number_of_columns_changed)
    Q_PROPERTY(QList<QString> horizontalLineColors READ get_horizontal_line_colors NOTIFY horizontal_line_colors_changed)
    Q_PROPERTY(QList<QString> verticalLineColors READ get_vertical_line_colors NOTIFY vertical_line_colors_changed)
    Q_PROPERTY(QList<QString> sliceColors READ get_slice_colors NOTIFY slice_colors_changed)
    Q_PROPERTY(QColor backgroundColor READ get_background_color WRITE set_background_color NOTIFY background_color_changed)

    Q_PROPERTY(QVariantMap viewportToSlicetypeMap READ get_viewport_to_slicetype_map NOTIFY viewport_to_slicetype_map_changed)
    Q_PROPERTY(QVariantMap viewportToSliceIdMap READ get_viewport_to_sliceid_map NOTIFY viewport_to_slice_id_map_changed)
    Q_PROPERTY(QVariantMap viewportToViewportNameMap READ get_viewport_to_viewport_name_map NOTIFY viewport_to_viewport_name_map_changed)
    Q_PROPERTY(QVariantMap sliceIdToNormalShiftMap READ get_slice_id_to_normal_shift_map NOTIFY slice_id_to_normal_shift_map_changed)

    Q_PROPERTY(int expandedViewport READ get_expanded_viewport WRITE expand_viewport NOTIFY expanded_viewport_changed)

    Q_INVOKABLE int getNumberOfSlices();
    Q_INVOKABLE int getThreeDViewportId();
    Q_INVOKABLE int getViewportIndexFromSliceIndex(int index);
    Q_INVOKABLE int getSliceIndexFromViewportIndex(int index);

    ViewerConfiguration(QObject* parent=nullptr);
    ~ViewerConfiguration() override;

    std::pair<QString, QString> get_slice_id_and_name(const viewer::SliceType& type) const;
    std::vector<QString> get_slice_ids() const;

    void expand_viewport(int viewport); // use -1 to expand none
    void reset_viewports_layout(); // same as expand with -1 as argument
    int get_expanded_viewport() const;

    QList<QRectF> get_viewports_layout() const;
    QList<bool> get_viewports_enabled() const;
    int get_number_of_rows() const;
    int get_number_of_columns() const;
    QList<QString> get_horizontal_line_colors() const;
    QList<QString> get_vertical_line_colors() const;
    QList<QString> get_slice_colors() const;
    QVariantMap get_viewport_to_slicetype_map() const;
    QVariantMap get_viewport_to_sliceid_map() const;
    QVariantMap get_viewport_to_viewport_name_map() const;
    QVariantMap get_slice_id_to_normal_shift_map() const;
    QColor get_background_color() const;

    void set_viewports_layout(const QList<QRectF>& i);
    void set_viewports_enabled(const QList<bool>& e);
    void set_number_of_rows(int r);
    void set_number_of_columns(int c);
    void set_horizontal_line_colors(const QList<QString>& clrs);
    void set_vertical_line_colors(const QList<QString>& clrs);
    void set_slice_colors(const QList<QString>& clrs);
    void set_viewport_to_slicetype_map(const QVariantMap& m);
    void set_viewport_to_sliceid_map(const QVariantMap& m);
    void set_viewport_to_viewport_name_map(const QVariantMap& m);
    void set_slice_id_to_normal_shift_map(const QVariantMap& m);
    void set_background_color(const QColor& background_color);

signals:
    void viewports_layout_changed();
    void viewports_enabled_changed();
    void number_of_rows_changed();
    void number_of_columns_changed();
    //void threed_viewport_id_changed();
    void horizontal_line_colors_changed();
    void vertical_line_colors_changed();
    void slice_colors_changed();
    void viewport_to_slicetype_map_changed();
    void viewport_to_slice_id_map_changed();
    void viewport_to_viewport_name_map_changed();
    void slice_id_to_normal_shift_map_changed();
    void expanded_viewport_changed();
    void background_color_changed();

private:
    QList<QRectF> m_viewports_layout = { QRectF(0.0, 0.0, 0.5, 0.5),QRectF(0.5, 0.0, 0.5, 0.5),
                                     QRectF(0.0, 0.5, 0.5, 0.5),QRectF(0.5, 0.5, 0.5,0.5) };
    QList<QRectF> m_initial_viewports_layout;
    QList<bool> m_viewports_enabled={true,true,true,true};
    int m_number_of_rows = 2;
    int m_number_of_columns = 2;
    QList<QString> m_horizontal_line_colors={"green","blue","green"};
    QList<QString> m_vertical_lne_colors={"red","red","blue"};
    QList<QString> m_slice_colors={"blue","green","red"};
    std::pair<QString, QString> m_axial_slice_id_and_name = { "Axial0","Axial" };
    std::pair<QString, QString> m_sagittal_slice_id_and_name = { "Sagittal0","Sagittal" };
    std::pair<QString, QString> m_coronal_slice_id_and_name = { "Coronal0","Coronal" };
    QVariantMap m_viewport_to_slicetype_map={{"0", "Axial"}, {"1", "Coronal"}, {"2", "Sagittal"}, {"3", "3D"}};
    QVariantMap m_viewport_to_slice_id_map={{"0", "Axial0"}, {"1", "Coronal0"}, {"2", "Sagittal0"}, {"3", "3D"}};
    QVariantMap m_viewport_to_viewport_name_map={{"0", "Axial"}, {"1", "Coronal"}, {"2", "Sagittal"}, {"3", "3D"}};
    QVariantMap m_slice_id_to_normal_shift_map={{"Axial0",0}, {"Coronal0",0}, {"Sagittal0",0}};
    int m_expanded_viewport = -1;
    QColor m_background_color = QColor(0, 0, 0);
};
}
