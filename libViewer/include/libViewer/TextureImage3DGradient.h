#include <QAbstractTextureImage>

#include <QVector3D>
#include <QMatrix4x4>

namespace viewer {
class TextureImage3DGradient : public Qt3DRender::QAbstractTextureImage {
    Q_OBJECT
    Q_PROPERTY(QByteArray textureData READ getTextureData WRITE setTextureData NOTIFY textureDataChanged)
    Q_PROPERTY(QVector3D dimension MEMBER m_dimension NOTIFY dimensionChanged)
    Q_PROPERTY(int channels MEMBER m_channels NOTIFY channelsChanged)
public:
    void setTextureData(const QByteArray& data);
    QByteArray getTextureData() const;


signals:
    void textureDataChanged();
    void dimensionChanged();
    void channelsChanged();

protected:
    Qt3DRender::QTextureImageDataGeneratorPtr dataGenerator() const override;

private:
    QByteArray createTexture(int maxX, int maxY, int maxZ, int numberOfChannels, int numberOfBytes) const;

    bool m_defaultTextureData = true;
    int m_channels = 3;
    QVector3D m_dimension{ 256,256,128 };// w x h x d

    QByteArray m_textureData;

};
}