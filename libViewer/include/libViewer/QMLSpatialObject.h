#pragma once

#include <QObject>
#include <Qt3DExtras/Qt3DExtras>
#include <Qt3DExtras/QMetalRoughMaterial>
#include <Qt3DExtras/QDiffuseSpecularMaterial> 

#include "QMLSpatialObjectParameters.h"
#include "QMLSpatialObjectType.h"
#include "UISpatialObject.h"

namespace viewer {
class QMLSpatialObject : public UISpatialObject {
    Q_OBJECT
public:
    Q_PROPERTY(QMLSpatialObjectType* type MEMBER m_type NOTIFY type_changed)
    Q_PROPERTY(QMLSpatialObjectParameters* parameters MEMBER m_parameters NOTIFY parameters_changed)
    Q_PROPERTY(Qt3DExtras::QMetalRoughMaterial* material MEMBER m_material NOTIFY material_changed)
    Q_PROPERTY(Qt3DExtras::QDiffuseSpecularMaterial* transparentMaterial MEMBER m_transparent_material NOTIFY material_changed)
    Q_PROPERTY(bool pickingEnabled READ get_is_picking_enabled WRITE set_is_picking_enabled NOTIFY picking_enabled_changed)

    explicit QMLSpatialObject(QObject* parent=nullptr);

    QMLSpatialObjectType* get_type() const;
    void set_type(const QMLSpatialObjectType::Type& type);
    void set_parameters(QMLSpatialObjectParameters* parameters);
    QMLSpatialObjectParameters* get_parameters() const;
    Qt3DExtras::QMetalRoughMaterial* get_material() const;
    Qt3DExtras::QDiffuseSpecularMaterial* get_transparent_material() const;
    void set_material(const QVariant& baseColor, const QVariant& metalness, const QVariant& roughness, const QVariant& normal, const QVariant& ambientOcclusion, float textureScale=1.0, const QColor& transparent_color=QColor(122,122,122,122));
    bool get_is_picking_enabled() const;
    void set_is_picking_enabled(const bool picking_enabled);

signals:
    void type_changed();
    void parameters_changed();
    void material_changed();
    void transparent_material_changed();
    void picking_enabled_changed();

private:
    QString m_mesh_file_path_name = "";
    QMLSpatialObjectType* m_type = nullptr;
    QMLSpatialObjectParameters* m_parameters= nullptr;
    Qt3DExtras::QMetalRoughMaterial* m_material = nullptr;
    Qt3DExtras::QDiffuseSpecularMaterial* m_transparent_material = nullptr;
    bool m_picking_enabled = false;
};
    }

