#include <QAbstractTextureImage>

#include <QVector3D>
#include <QMatrix4x4>

namespace viewer {
class TextureImage3D : public Qt3DRender::QAbstractTextureImage {
    Q_OBJECT
    Q_PROPERTY(QByteArray textureData READ getTextureData WRITE setTextureData NOTIFY textureDataChanged)
    Q_PROPERTY(QVector3D dimension MEMBER m_dimension NOTIFY dimensionChanged)
    Q_PROPERTY(QVector3D spacing MEMBER m_spacing NOTIFY spacingChanged)
    Q_PROPERTY(QVector3D dimensionTimesSpacing MEMBER m_dimensionTimesSpacing NOTIFY dimensionTimesSpacingChanged)
    Q_PROPERTY(QVector3D origin MEMBER m_origin NOTIFY originChanged)
    Q_PROPERTY(QMatrix4x4 orientation MEMBER m_orientation NOTIFY orientationChanged)
    Q_PROPERTY(int channels MEMBER m_channels NOTIFY channelsChanged)
public:
    Q_INVOKABLE void setTextureInformation(const QByteArray& data, QVector3D dimension, QVector3D spacing, QVector3D origin, QMatrix4x4 orientation, int numberOfChannels);

    void setTextureData(const QByteArray& data);
    QByteArray getTextureData() const;

    Q_INVOKABLE QVector3D getDimensionTimesSpacing() const;

signals:
    void textureDataChanged();
    void dimensionChanged();
    void spacingChanged();
    void dimensionTimesSpacingChanged();
    void originChanged();
    void orientationChanged();
    void channelsChanged();

protected:
    Qt3DRender::QTextureImageDataGeneratorPtr dataGenerator() const override;

private:
    QByteArray createTexture(int maxX, int maxY, int maxZ, int numberOfChannels) const;

    bool m_defaultTextureData = true;
    int m_seed=1;

    QVector3D m_origin{ 0,0,0 };
    QMatrix4x4 m_orientation;

    QVector3D m_spacing{ 1.0f,1.0f,1.0f }; // w x h x d 
    QVector3D m_dimension{ 1,1,1 };// w x h x d
    QVector3D m_dimensionTimesSpacing;// w x h x d

    int m_channels = 3;
    QByteArray m_textureData;

};
}