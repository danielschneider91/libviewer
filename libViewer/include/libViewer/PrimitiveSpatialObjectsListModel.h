#pragma once
#include <QAbstractListModel>

#include "MaterialSettings.h"
#include "libViewer/UIPrimitiveMesh.h"
#include "libViewer/Pose.h"

namespace viewer
{
class PrimitiveSpatialObjectsListModel : public QAbstractListModel
{
    Q_OBJECT
public:
    enum SpatialObjectsRoles {
        Pose = Qt::UserRole,
        Transform,
        Material,
        TransparentMaterial,
        Mesh,
        Enabled,
        Visible,
        Name,
        Id,
        PickingEnabled
    };
    PrimitiveSpatialObjectsListModel(QObject* parent=nullptr);
    ~PrimitiveSpatialObjectsListModel();

    QVariant data(const QModelIndex& index, int role = Qt::DisplayRole) const override;
    int rowCount(const QModelIndex& parent = QModelIndex()) const override;

    Qt::ItemFlags flags(const QModelIndex& index) const override;

    void add_object(UIPrimitiveMesh* data);
    void remove_object(const QString& id);
    QList<UIPrimitiveMesh*> get_objects() const;
    void set_pose(const std::string& id, const viewer::Pose& pose);
    void set_material(const std::string& id, const viewer::MaterialSettings& material_settings);
    MaterialSettings get_material(const std::string& id) const;
    void set_picking_enabled(const std::string& id, const bool picking_enabled);
    virtual QHash<int, QByteArray> roleNames() const override;

private:
    std::map<QString, int> m_id_index_map;
    QList<UIPrimitiveMesh*> m_data;
    int m_number_of_rows;
};
}
