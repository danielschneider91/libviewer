#pragma once

#include <QtGlobal>
#include <QQmlEngine>

namespace viewer
{
// Required derivation from QObject
class QMLSpatialObjectType : public QObject
{
	Q_OBJECT
	Q_PROPERTY(Type type READ get_type NOTIFY type_changed)

public:
	// Default constructor, required for classes you expose to QML.
	QMLSpatialObjectType() : QObject(),m_type(Type::CROSSHAIRS)
	{
	}

	enum Type
	{
		POINTS,
		LINE,
		CROSSHAIRS,
	};
	Q_ENUM(Type)

		Type get_type() const { return m_type; }
	void set_type(const Type& type) { m_type = type; }

	signals:
		void type_changed();
private:
	Type m_type = Type::CROSSHAIRS;

};
}
