#pragma once

#include <string>
#include <algorithm>
#include <exception>
#include <stdexcept>

#include "libViewer/MetalRoughMaterialProperties.h"

namespace viewer
{
enum class MetalRoughMaterialType { ALUMINIUM, ALUMINIUM_TEXTURE, ARTERY, BONE, BONE_TEXTURE, GOLD, GOLD_TEXTURE, NERVE, PLASTIC, SKIN, STEEL, STEEL_TEXTURE, TITANIUM, TUMOR, TUNGSTEN, VEIN };
static std::string get_material_as_string(const MetalRoughMaterialType& type)
{
    switch (type) {
    case MetalRoughMaterialType::ALUMINIUM: return "ALUMINIUM";
    case MetalRoughMaterialType::ALUMINIUM_TEXTURE: return "ALUMINIUM_TEXTURE";
    case MetalRoughMaterialType::ARTERY: return "ARTERY";
    case MetalRoughMaterialType::BONE: return "BONE";
    case MetalRoughMaterialType::BONE_TEXTURE: return "BONE_TEXTURE";
    case MetalRoughMaterialType::GOLD: return "GOLD";
    case MetalRoughMaterialType::GOLD_TEXTURE: return "GOLD_TEXTURE";
    case MetalRoughMaterialType::NERVE: return "NERVE";
    case MetalRoughMaterialType::PLASTIC: return "PLASTIC";
    case MetalRoughMaterialType::SKIN: return "SKIN";
    case MetalRoughMaterialType::STEEL: return "STEEL";
    case MetalRoughMaterialType::STEEL_TEXTURE: return "STEEL_TEXTURE";
    case MetalRoughMaterialType::TUNGSTEN: return "TUNGSTEN";
    case MetalRoughMaterialType::VEIN: return "VEIN";
    default:
        throw std::runtime_error("Unknown material: "+std::to_string(static_cast<int>(type)));
    }
}
static MetalRoughMaterialType get_material_as_enum(const std::string& type)
{
    std::string type_upper = type;
    std::transform(type_upper.begin(), type_upper.end(), type_upper.begin(), ::toupper);
    if (strcmp(type.c_str(), "ALUMINIUM_TEXTURE") == 0)
    {
        return MetalRoughMaterialType::ALUMINIUM_TEXTURE;
    }
    if (strcmp(type_upper.c_str(), "ALUMINIUM") == 0)
    {
        return MetalRoughMaterialType::ALUMINIUM;
    }
    if (strcmp(type_upper.c_str(), "ARTERY") == 0)
    {
        return MetalRoughMaterialType::ARTERY;
    }
    if (strcmp(type_upper.c_str(), "BONE") == 0)
    {
        return MetalRoughMaterialType::BONE;
    }
    if (strcmp(type_upper.c_str(), "BONE_TEXTURE") == 0)
    {
        return MetalRoughMaterialType::BONE_TEXTURE;
    }
    if (strcmp(type_upper.c_str(), "GOLD") == 0)
    {
        return MetalRoughMaterialType::GOLD_TEXTURE;
    }
    if (strcmp(type_upper.c_str(), "GOLD_TEXTURE") == 0)
    {
        return MetalRoughMaterialType::GOLD;
    }
    if (strcmp(type_upper.c_str(), "PLASTIC") == 0)
    {
        return MetalRoughMaterialType::PLASTIC;
    }
    if (strcmp(type_upper.c_str(), "SKIN") == 0)
    {
        return MetalRoughMaterialType::SKIN;
    }
    if (strcmp(type_upper.c_str(), "STEEL") == 0)
    {
        return MetalRoughMaterialType::STEEL;
    }
    if (strcmp(type_upper.c_str(), "STEEL_TEXTURE") == 0)
    {
        return MetalRoughMaterialType::STEEL_TEXTURE;
    }
    if (strcmp(type_upper.c_str(), "TUNGSTEN") == 0)
    {
        return MetalRoughMaterialType::TUNGSTEN;
    }
    if (strcmp(type_upper.c_str(), "VEIN") == 0)
    {
        return MetalRoughMaterialType::VEIN;
    }
    throw std::runtime_error("Unknown material: "+ type_upper);

}
class MetalRoughMaterials
{
public:
    static MetalRoughMaterialProperties get_material(viewer::MetalRoughMaterialType metal_rough_material_type, float roughness = 0.5);
    static MetalRoughMaterialProperties get_material(std::string metal_rough_material_type, float roughness=0.5);

private:
    static QVariant get_texture(const std::string& file);
};

}
