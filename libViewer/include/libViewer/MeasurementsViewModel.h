#pragma once
#include <QObject>

#include "Measurement.h"
#include "ViewerViewModel.h"

#include "libViewer/IdGenerator.h"

namespace viewer
{

class MeasurementsViewModel : public QObject
{
    Q_OBJECT
public:
    Q_PROPERTY(bool rulerEnabled READ get_ruler_enabled WRITE set_ruler_enabled NOTIFY ruler_enabled_changed)
    Q_PROPERTY(Measurement* measurement MEMBER m_measurement NOTIFY measurement_changed)

    explicit MeasurementsViewModel(std::shared_ptr<ViewerViewModel> viewer_vm,QObject* parent=nullptr);

    Q_INVOKABLE void resetMeasurement();
    bool get_ruler_enabled() const;
    void set_ruler_enabled(const bool ruler_enabled);

    int get_position_counter() const;
    void set_position_counter(const int position_counter);
signals:
    void ruler_enabled_changed();
    void measurement_changed();

private:
    void wireup_events();
    void unwire_events();

    void update_line(const QVector3D& from, const QVector3D& to);
    void remove_measurement();
    void reset_measurement();
    void add_empty_measurement();
    void print_position(const QVector3D& position, const QString& msg="") const;
    float get_measurement_visual_size() const;

    std::vector<QMetaObject::Connection> m_connections;

    bool m_ruler_enabled = false;
    int m_position_counter = 0; //0: first position, 1: second position
    
    Measurement* m_measurement = nullptr;
    float m_measurement_visual_size = 20.0f;
    std::unique_ptr<IdGenerator> m_id_generator = nullptr;
    std::shared_ptr<viewer::ViewerViewModel> m_viewer_vm = nullptr;

	
};


}
