#pragma once

#include "libViewer/MetalRoughMaterialProperties.h"

namespace viewer
{
class MaterialSettings : public QObject {
public:
    MaterialSettings(const bool has_material=false, const MetalRoughMaterialProperties& metal_rough_material_properties=MetalRoughMaterialProperties(),
        const QColor& transparent_color=QColor(122, 122, 122, 122));

    bool get_has_material() const;
    void set_has_material(const bool has_material);
    MetalRoughMaterialProperties get_metal_rough_material_properties() const;
    void set_metal_rough_material_properties(const MetalRoughMaterialProperties& metal_rough_material_properties);
    QColor get_transparent_color() const;
    void set_transparent_color(const QColor& transparent_color);

private:
    bool m_has_material = false;
    MetalRoughMaterialProperties m_metal_rough_material_properties;
    QColor m_transparent_color;
};
}
