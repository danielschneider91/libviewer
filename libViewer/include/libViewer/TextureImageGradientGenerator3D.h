#include <QTextureImageDataGenerator>
#include <QAbstractTextureImage>
#include <QVector3D>
#include <QMatrix4x4>

namespace viewer {
class TextureImageGradientGenerator3D : public Qt3DRender::QTextureImageDataGenerator {
public:
    explicit TextureImageGradientGenerator3D();

    Qt3DRender::QTextureImageDataPtr operator()() override;

    bool operator ==(const QTextureImageDataGenerator& other) const final;

    QT3D_FUNCTOR(TextureImageGradientGenerator3D)

    void set_texture_information(const QByteArray& data, QVector3D dimension, int numberOfChannels);
    
private:
    void print_byte_array_values(const QByteArray& cs, int i);
    QByteArray createTexture(int maxX, int maxY, int maxZ, int numberOfChannels) const;
    QByteArray createTextureBytes(int maxX, int maxY, int maxZ, int numberOfChannels, int numberOfBytes) const;

    QVector3D m_dimension{ 256,256,128 };// w x h x d
    int m_channels = 3;

    QByteArray m_textureData;

};
}