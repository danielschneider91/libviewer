#pragma once
#include <QObject>

#include "ViewerViewModel.h"


namespace viewer
{

class Measurement : public QObject
{
    Q_OBJECT
public:
    Q_PROPERTY(float distance READ get_distance NOTIFY distance_changed)

    explicit Measurement(std::tuple<QString, QString, QString> ids,QObject* parent=nullptr);

    QVector3D get_from() const;
    void set_from(const QVector3D& p);
    QVector3D get_to() const;
    void set_to(const QVector3D& p);

    QString get_from_id() const;
    QString get_to_id() const;
    QString get_line_id() const;

    float get_distance() const;

    void reset_measurement();

signals:
    void distance_changed();

private:
    void print_position(const QVector3D& position, const QString& msg="") const;

    std::pair<QVector3D, QVector3D> m_positions;
    std::tuple<QString, QString,QString> m_ids{ "measurement_point_1","measurement_point_2","measurement_line"};
    float m_distance= 0.0;
	
};


}
