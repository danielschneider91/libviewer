#pragma once
#include <QAbstractListModel>

#include "libviewer/VolumeImageDataQml.h"

namespace viewer
{
class ImagesListModel : public QAbstractListModel
{
    Q_OBJECT
public:
    enum SpatialObjectsRoles {
        FilePath = Qt::UserRole,
        Enabled,
        Visible,
        Name,
        Id,
        PickingEnabled,
        VolumeData,
        NumberOfChannels,
        Origin,
        OriginBoundingBox,
        Orientation,
        Dimension,
        Spacing,
        DimensionTimesSpacing,
        MinimumValue,
        MaximumValue,
        MaximumExtent
    };

    ImagesListModel(QObject* parent=nullptr);
    ~ImagesListModel();

    QVariant data(const QModelIndex& index, int role = Qt::DisplayRole) const override;
    int rowCount(const QModelIndex& parent = QModelIndex()) const override;

    Qt::ItemFlags flags(const QModelIndex& index) const override;

    void add_object(const QString& id, VolumeImageDataQml* data);
    void remove_object(const QString& id);

    QList<VolumeImageDataQml*> get_objects() const;
    virtual QHash<int, QByteArray> roleNames() const override;

private:
    std::map<QString, int> m_id_index_map;
    QList<VolumeImageDataQml*> m_data;
};
}
