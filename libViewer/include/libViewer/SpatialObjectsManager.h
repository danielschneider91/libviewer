#pragma once

#include "AnimationContainer.h"
#include "AnimationManager.h"
#include "MaterialSettings.h"
#include "QMLSpatialObjectsListModel.h"
#include "QMLSpatialObjectType.h"
#include "ViewportVisibility.h"
#include "libViewer/SpatialObjectsListModel.h"
#include "libViewer/TrackedObjectsListModel.h"
#include "libViewer/PrimitiveSpatialObjectsListModel.h"
#include "libViewer/Side.h"
#include "libViewer/Pose.h"
#include "libViewer/AnimationProperty.h"
#include "libViewer/AnimationParameters.h"

namespace viewer
{

class SpatialObjectsManager : public QObject
{
    Q_OBJECT
public:
    Q_PROPERTY(SpatialObjectsListModel* spatialObjectsFromFile READ get_spatial_objects_from_file /*MEMBER m_spatial_objects_from_file*/ NOTIFY objects_from_file_changed)
    Q_PROPERTY(QList<ViewportVisibility*> spatialObjectsFromFileViewportVisibility MEMBER m_spatial_objects_from_file_viewport_visibility NOTIFY spatial_objects_from_file_viewport_visibility_changed)
    Q_PROPERTY(PrimitiveSpatialObjectsListModel* primitiveSpatialObjects MEMBER m_primitive_objects NOTIFY primitive_objects_changed)
    Q_PROPERTY(QList<ViewportVisibility*> primitiveSpatialObjectsViewportVisibility MEMBER m_primitive_spatial_objects_viewport_visibility NOTIFY primitive_spatial_objects_viewport_visibility_changed)
    Q_PROPERTY(QMLSpatialObjectsListModel* qmlSpatialObjects MEMBER m_qml_spatial_objects NOTIFY qml_spatial_objects_changed)
    Q_PROPERTY(QList<ViewportVisibility*> qmlSpatialObjectsViewportVisibility MEMBER m_qml_spatial_objects_viewport_visibility NOTIFY qml_spatial_objects_viewport_visibility_changed)
    Q_PROPERTY(AnimationManager* animationManager MEMBER m_animation_manager NOTIFY animation_manager_changed)

    Q_INVOKABLE int get_index_from_id(QString id);
    explicit SpatialObjectsManager(QObject* parent=nullptr);
	
    void add_object_from_file(const std::string& id, const std::string& file_path_name, const Pose& pose = Pose(),
        const MaterialSettings& material_settings=MaterialSettings(), const ViewportVisibility& viewport_visibility = ViewportVisibility(),
        bool picking_enabled=false);

    void add_primitive_geometry(const std::string& id, Qt3DRender::QGeometryRenderer* primitive_geometry, const Pose& pose = Pose(),
        const MaterialSettings& material_settings=MaterialSettings(), const ViewportVisibility& viewport_visibility = ViewportVisibility(),
        bool picking_enabled = false);

    void add_object_by_type(const std::string& id, QMLSpatialObjectType::Type type, QMLSpatialObjectParameters* params, const Pose& pose = Pose(),
        const MaterialSettings& material_settings = MaterialSettings(), const ViewportVisibility& viewport_visibility = ViewportVisibility(), bool picking_enabled=false);

    void remove_object(const std::string& id);

    void set_object_material(const std::string& id, const MaterialSettings& material_settings);
    MaterialSettings get_object_material(const std::string& id) const;

    void add_animation(const std::string& id, const AnimationProperty& animation_property, const AnimationParameters& animation_parameters);
    void remove_animation(const std::string& id, const AnimationProperty& animation_property);
    void remove_animations(const std::string& id);

    void set_object_pose(const std::string& id, const QVector3D& position, const QVector3D& x_axis, const QVector3D& normal/*z axis*/);
    void set_viewport_visibility(const std::string& id, const viewer::ViewportVisibility& viewport_visibility);
    ViewportVisibility* get_viewport_visibility(const std::string& id) const;

    void set_picking_enabled(const std::string& id, bool picking_enabled);

    std::vector<std::string> get_object_ids_in_viewer() const;
    std::vector<std::string> get_primitive_object_ids_in_viewer() const;
    std::vector<std::string> get_object_from_file_ids_in_viewer() const;
    std::vector<std::string> get_qml_object_ids_in_viewer() const;

	Side get_side() const;
    void set_side(const Side& side);

    viewer::SpatialObjectsListModel* get_spatial_objects_from_file();
    QMLSpatialObjectParameters* get_spatial_object_parameters(const std::string& id) const;

signals:
    void objects_from_file_changed();
    void primitive_objects_changed();
    void qml_spatial_objects_changed();
    void qml_spatial_objects_viewport_visibility_changed();
    void animation_manager_changed();
    void primitive_spatial_objects_viewport_visibility_changed();
    void spatial_objects_from_file_viewport_visibility_changed();
private:
    bool contains_object_with_id(const std::string& id) const;
    void remove_spatial_object_from_file(const QString& id);
    void remove_qml_spatial_object(const QString& id);
    void remove_primitive_spatial_object(const QString& id);

    Pose m_current_pose;
    Side m_side = Side::LEFT;
    viewer::SpatialObjectsListModel* m_spatial_objects_from_file = nullptr;
    std::map<std::string, int> m_spatial_objects_from_file_id_to_index_map;
    QList<ViewportVisibility*> m_spatial_objects_from_file_viewport_visibility;
    viewer::PrimitiveSpatialObjectsListModel* m_primitive_objects = nullptr;
    std::map<std::string, int> m_primitive_objects_id_to_index_map;
    QList<ViewportVisibility*> m_primitive_spatial_objects_viewport_visibility;
    viewer::QMLSpatialObjectsListModel* m_qml_spatial_objects = nullptr;
    std::map<std::string, int> m_qml_spatial_objects_id_to_index_map;
    QList<ViewportVisibility*> m_qml_spatial_objects_viewport_visibility;

    AnimationManager* m_animation_manager;
};


}
