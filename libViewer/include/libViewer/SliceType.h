#pragma once
namespace viewer
{
enum class SliceType
{
    AXIAL = 0,
    SAGITTAL = 1,
    CORONAL = 2,
};
}
