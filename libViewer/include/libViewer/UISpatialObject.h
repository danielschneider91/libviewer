#pragma once

#include <QObject>
#include <QMatrix4x4>
#include <Qt3DCore/Qt3DCore>
#include <Qt3DCore/QTransform>

namespace viewer {
class UISpatialObject : public QObject {
    Q_OBJECT
    Q_PROPERTY(QString id READ get_id WRITE set_id NOTIFY idChanged)
    Q_PROPERTY(QString name READ get_name WRITE set_name NOTIFY nameChanged)
    Q_PROPERTY(bool visible READ get_is_visible WRITE set_is_visible NOTIFY visibleChanged)
    Q_PROPERTY(bool enabled READ get_is_enabled WRITE set_is_enabled NOTIFY enabledChanged)
    Q_PROPERTY(QMatrix4x4 pose READ get_pose WRITE set_pose NOTIFY poseChanged)
    Q_PROPERTY(Qt3DCore::QTransform* transform READ get_transform WRITE set_transform NOTIFY transformChanged)

public:
    UISpatialObject(QObject* parent=nullptr);
    ~UISpatialObject() override;

    const QString& get_id() const;
    void set_id(const QString& id);
    const QString& get_name() const;
    void set_name(const QString& name);
    bool get_is_enabled() const;
    void set_is_enabled(bool enabled);
    bool get_is_visible() const;
    void set_is_visible(bool visible);
    const QMatrix4x4& get_pose() const;
    Qt3DCore::QTransform* get_transform() const;
    void set_transform(Qt3DCore::QTransform* transform);
    void set_pose(const QMatrix4x4& pose);
    void set_pose(const QVector3D& position, const QVector3D& x_axis, const QVector3D& z_axis);
    void set_position(const QVector3D& position);

signals:
    void idChanged();
    void nameChanged();
    void enabledChanged();
    void visibleChanged();
    void poseChanged();
    void transformChanged();

private:
    QString m_id="";
    QString m_name="";
    bool m_enabled = true;
    bool m_visible = true;
    QMatrix4x4 m_pose;
    Qt3DCore::QTransform* m_transform=nullptr;
};
}

