#pragma once

#include <QAbstractTextureImage>

#include <QVector3D>
#include <QMatrix4x4>

namespace viewer {
class TextureImage1D : public Qt3DRender::QAbstractTextureImage {
    Q_OBJECT
        Q_PROPERTY(QByteArray textureData READ get_texture_data WRITE set_texture_data NOTIFY textureDataChanged)
        Q_PROPERTY(int dimension MEMBER m_dimension NOTIFY dimensionChanged)
        Q_PROPERTY(float spacing MEMBER m_spacing NOTIFY spacingChanged)
        Q_PROPERTY(float dimensionTimesSpacing MEMBER m_dimension_times_spacing NOTIFY dimensionTimesSpacingChanged)
        Q_PROPERTY(QVector3D origin MEMBER m_origin NOTIFY originChanged)
        Q_PROPERTY(QMatrix4x4 orientation MEMBER m_orientation NOTIFY orientationChanged)
        Q_PROPERTY(int channels MEMBER m_channels NOTIFY channelsChanged)
        Q_PROPERTY(QVector<QColor> colors READ get_colors WRITE set_colors NOTIFY colorsChanged)
public:
    Q_INVOKABLE void setTextureInformation(const QByteArray& data, int dimension, float spacing, QVector3D origin, QMatrix4x4 orientation, int numberOfChannels, QVector<QColor> colors);

    void set_texture_data(const QByteArray& data);
    QByteArray get_texture_data() const;

    QVector<QColor> get_colors() const;
    void set_colors(const QVector<QColor>& colors);

signals:
    void textureDataChanged();
    void dimensionChanged();
    void spacingChanged();
    void dimensionTimesSpacingChanged();
    void originChanged();
    void orientationChanged();
    void channelsChanged();
    void colorsChanged();

protected:
    Qt3DRender::QTextureImageDataGeneratorPtr dataGenerator() const override;

private:
    QByteArray make_color_spectrum(size_t width, const QVector<QColor>& colors) const;

    bool m_default_texture_data = false;

    QVector3D m_origin{ 0,0,0 };
    QMatrix4x4 m_orientation;

    float m_spacing = 1.0f;
    int m_dimension = 1024;
    float m_dimension_times_spacing;// w x h x d

    int m_channels = 4;
    QByteArray m_texture_data;
    QVector<QColor> m_colors{ QColor(255,0,0,0),QColor(0,255,0,255) };

};
}