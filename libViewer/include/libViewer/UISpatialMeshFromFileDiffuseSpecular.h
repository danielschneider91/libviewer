#pragma once

#include <QObject>
#include <Qt3DExtras/Qt3DExtras>
#include <Qt3DExtras/QMetalRoughMaterial>

#include "UISpatialObject.h"

namespace viewer {
class UISpatialMeshFromFileDiffuseSpecular : public UISpatialObject {
    Q_OBJECT
public:
    Q_PROPERTY(QString filePath MEMBER m_mesh_file_path_name NOTIFY mesh_file_path_changed)
    Q_PROPERTY(bool hasMaterial MEMBER m_has_material NOTIFY has_material_changed)
    Q_PROPERTY(Qt3DExtras::QDiffuseSpecularMaterial* material MEMBER m_material NOTIFY material_changed)

    explicit UISpatialMeshFromFileDiffuseSpecular(QObject* parent=nullptr);

    QString get_mesh_file_path_name() const;
    void set_mesh_file_path_name(const QString& mesh_file_path_name);
    bool get_has_material() const;
    void set_has_material(const bool has_material);
    Qt3DExtras::QDiffuseSpecularMaterial* get_material() const;
    void set_material(const QVariant& diffuse, const QVariant& specular, float shininess, const QColor& ambient, const QVariant& normal, bool alphaBlending=false, float textureScale=1.0);

signals:
    void has_material_changed();
    void mesh_file_path_changed();
    void material_changed();

private:
    QString m_mesh_file_path_name = "";
    bool m_has_material = false;
    Qt3DExtras::QDiffuseSpecularMaterial* m_material = nullptr;
};
    }

