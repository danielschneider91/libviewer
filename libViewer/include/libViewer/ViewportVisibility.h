#pragma once

#include "libViewer/SliceViewportRenderingModeClass.h"

namespace viewer
{
class ViewportVisibility : public QObject {
    Q_OBJECT
public:
    Q_PROPERTY(bool threeDViewportsVisibility READ get_three_d_viewports_visibility WRITE set_three_d_viewports_visibility NOTIFY threeD_viewports_visibility_changed)
    Q_PROPERTY(bool sliceViewportsVisibility READ get_slice_viewports_visibility WRITE set_slice_viewports_visibility NOTIFY slice_viewports_visibility_changed)
    Q_PROPERTY(int sliceViewportRenderingMode MEMBER m_slice_viewport_rendering_mode NOTIFY slice_viewport_rendering_mode_changed)
    Q_PROPERTY(float clippingDistance MEMBER m_clipping_distance NOTIFY clipping_distance_changed)
    Q_PROPERTY(bool depthTesting MEMBER m_depth_testing NOTIFY depth_testing_changed)


    ViewportVisibility(const bool three_d_viewports_visibility=true, const bool slice_viewports_visibility=false, const SliceViewportRenderingModeClass::SliceViewportRenderingMode& slice_viewport_rendering_mode= SliceViewportRenderingModeClass::SliceViewportRenderingMode::MODEL, const float& clipping_distance = 10.0f, const bool& depth_testing=true, QObject* parent=nullptr);

    bool get_three_d_viewports_visibility() const;
    void set_three_d_viewports_visibility(const bool three_d_viewports_visibility);
    bool get_slice_viewports_visibility() const;
    void set_slice_viewports_visibility(const bool slice_viewports_visibility);
    SliceViewportRenderingModeClass::SliceViewportRenderingMode get_slice_viewport_rendering_mode() const;
    void set_slice_viewport_rendering_mode(const SliceViewportRenderingModeClass::SliceViewportRenderingMode& slice_viewport_rendering_mode);
    float get_clipping_distance() const;
    void set_clipping_distance(const float& d);
    bool get_depth_testing() const;
    void set_depth_testing(const bool depth_testing);
signals:
    void threeD_viewports_visibility_changed();
    void slice_viewports_visibility_changed();
    void slice_viewport_rendering_mode_changed();
    void clipping_distance_changed();
    void depth_testing_changed();

private:
    bool m_threeD_viewports_visibility = true;
    bool m_slice_viewports_visibility = false;
    int m_slice_viewport_rendering_mode = static_cast<int>(SliceViewportRenderingModeClass::SliceViewportRenderingMode::MODEL);
    float m_clipping_distance = 10.0;
    bool m_depth_testing = true;
};
}
