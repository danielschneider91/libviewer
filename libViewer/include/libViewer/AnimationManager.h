#pragma once

#include "AnimationContainer.h"
#include "AnimationParametersContainer.h"
#include "libViewer/SpatialObjectsListModel.h"
#include "libViewer/TrackedObjectsListModel.h"
#include "libViewer/Side.h"
#include "libViewer/AnimationProperty.h"
#include "libViewer/AnimationParameters.h"

namespace viewer
{

class AnimationManager : public QObject
{
    Q_OBJECT
public:
    Q_PROPERTY(AnimationParametersContainer* qmlSpatialObjectsAnimationParametersContainer MEMBER m_animation_parameters_container NOTIFY qml_spatial_objects_animation_params_container_changed)

    explicit AnimationManager(QObject* parent=nullptr);
	
    void add_animation(const std::string& id, const AnimationProperty& animation_property, const AnimationParameters& animation_parameters, QObject* target);
    void add_animation(const std::string& id, const AnimationProperty& animation_property, const AnimationParameters& animation_parameters);
    void remove_animation(const std::string& id, const AnimationProperty& animation_property);
    void remove_animations(const std::string& id);

signals:
    void qml_spatial_objects_animation_params_container_changed();

private:
    AnimationContainer m_animation_container;
    AnimationParametersContainer* m_animation_parameters_container;
};
}
