#pragma once
#include <string>
#include <vector>

namespace viewer
{

class IdGenerator
{
public:
    virtual ~IdGenerator() {}
    virtual std::string create_unique_id(const std::vector<std::string>& exixting_ids,const std::string& id_root="") const = 0;
};


}
