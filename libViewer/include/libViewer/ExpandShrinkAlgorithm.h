#pragma once
#include <QRectF>
#include <QList>

namespace viewer {
class ExpandShrinkAlgorithm  {
public:
    ExpandShrinkAlgorithm() = default;
    ~ExpandShrinkAlgorithm() = default;

    void toggle_expand(int viewport_idx, bool expand, const QList<QRectF>& initial_layout, QList<QRectF>& actual_layout, QList<bool>& enabled, int number_of_rows, int number_of_columns);

private:
    bool are_sizes_consistent(const QList<QRectF>& initial_layout, QList<QRectF>& actual_layout, QList<bool>& enabled);
    void reset(const QList<QRectF>& initial_layout, QList<QRectF>& actual_layout, QList<bool>& enabled);

    QRectF minimize(int viewportIndex, int viewerIndexToBeMaximized, int numberOfColumns, int numberOfRows, int behaviour=0);

};
}
