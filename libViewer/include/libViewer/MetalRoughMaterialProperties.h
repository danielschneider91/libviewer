#pragma once
#include <string>
#include <exception>
#include <Qt3DExtras/QMetalRoughMaterial>

namespace viewer
{

class MetalRoughMaterialProperties
{
public:
	MetalRoughMaterialProperties(const QColor& base_color, const float& metalness=0.0, const float& roughness=0.0, const float& texture_scale = 1.0f);
	MetalRoughMaterialProperties(const QVariant& base_color= QStringLiteral(""), const QVariant& metalness = QStringLiteral(""), const QVariant& roughness = QStringLiteral(""), const QVariant& normal = 0.0f, const QVariant& ambient_occlusion = 0.0f, const float& texture_scale = 1.0f);
	void set_ambient_occlusion(const QVariant& ambient_occlusion);
	void set_base_color(const QVariant& base_color);
	void set_metalness(const QVariant& metalness);
	void set_normal(const QVariant& normal);
	void set_roughness(const QVariant& roughness);
	void set_texture_scale(const float& texture_scale);
	QVariant get_base_color() const;
	QVariant get_metalness() const;
	QVariant get_normal() const;
	QVariant get_roughness() const;
	float get_texture_scale() const;
	QVariant get_ambient_occlusion() const;

private:
	QVariant m_ambient_occlusion;
	QVariant m_base_color;
	QVariant m_metalness;
	QVariant m_normal;
	QVariant m_roughness;
	float m_texture_scale;
};
}
