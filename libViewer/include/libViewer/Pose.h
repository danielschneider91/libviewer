#pragma once

#include <QVector3D>

namespace viewer {
class Pose {
public:
    Pose();
    Pose(const QVector3D& position, const QVector3D& x_axis, const QVector3D& z_axis);
    QVector3D get_position() const;
    void set_position(const QVector3D& position);
    QVector3D get_x_axis() const;
    void set_x_axis(const QVector3D& x_axis);
    QVector3D get_z_axis() const;
    void set_z_axis(const QVector3D& z_axis);
    QVector3D get_y_axis() const;

private:
	QVector3D m_position;
	QVector3D m_x_axis;
	QVector3D m_z_axis;
};
}
