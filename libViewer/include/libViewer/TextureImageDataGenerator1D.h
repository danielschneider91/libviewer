#include <QTextureImageDataGenerator>
#include <QAbstractTextureImage>
#include <QVector3D>
#include <QMatrix4x4>

namespace viewer {
class TextureImageDataGenerator1D : public Qt3DRender::QTextureImageDataGenerator {
public:
    explicit TextureImageDataGenerator1D();

    Qt3DRender::QTextureImageDataPtr operator()() override;

    bool operator ==(const QTextureImageDataGenerator& other) const final;

    QT3D_FUNCTOR(TextureImageDataGenerator1D)

    void set_texture_information(const QByteArray& data, int dimension, float spacing, QVector3D origin, QMatrix4x4 orientation, int numberOfChannels);
    
private:
    void print_byte_array_values(const QByteArray& cs, int i);
    void print_byte_array_values_rgba(const QByteArray& cs, int numberOfBytes);
    QByteArray create_texture(int maxX, int numberOfChannels) const;

    QVector3D m_origin{ 0,0,0 };
    QMatrix4x4 m_orientation;

    float m_spacing = 1.0f;
    int m_dimension = 16;// w x h x d

    int m_channels = 4;

    QByteArray m_texture_data;

};
}