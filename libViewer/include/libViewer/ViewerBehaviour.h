#pragma once

#include <QtGlobal>
#include <QQmlEngine>

namespace viewer
{
// Required derivation from QObject
class ViewerBehaviour : public QObject
{
	Q_OBJECT
	Q_PROPERTY(Behaviour behaviour READ get_behaviour NOTIFY behavour_changed)

public:
	// Default constructor, required for classes you expose to QML.
	ViewerBehaviour() : QObject()
	{
	}

	enum Behaviour
	{
		MOVING_CROSSHAIR,
		CENTERED_CROSSHAIR,
		PROBES_VIEW,
		ROTATING_PROBES_VIEW
	};
	Q_ENUM(Behaviour)

	Behaviour get_behaviour() const { return m_behaviour; }
	void set_behaviour(const Behaviour& behaviour) { m_behaviour = behaviour; }

	signals:
		void behavour_changed();
private:
	Behaviour m_behaviour = Behaviour::CENTERED_CROSSHAIR;

};
}
