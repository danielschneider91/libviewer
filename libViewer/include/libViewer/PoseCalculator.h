#pragma once

#include <QMatrix4x4>
#include "libViewer/Pose.h"

namespace viewer {
class PoseCalculator {
public:
	static QMatrix4x4 get_transform_from(const QVector3D& position, const QVector3D& x_axis, const QVector3D& z_axis);
	static Pose get_transform_from(const QMatrix4x4& pose);
};
}
