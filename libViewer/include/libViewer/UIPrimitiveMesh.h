#pragma once

#include <QObject>
#include <Qt3DExtras/Qt3DExtras>
#include <Qt3DExtras/QMetalRoughMaterial>
#include <Qt3DExtras/QDiffuseSpecularMaterial> 

#include "UISpatialObject.h"

namespace viewer {
class UIPrimitiveMesh : public UISpatialObject {
    Q_OBJECT
public:
    Q_PROPERTY(Qt3DExtras::QMetalRoughMaterial* material MEMBER m_material NOTIFY material_changed)
    Q_PROPERTY(Qt3DExtras::QDiffuseSpecularMaterial* transparentMaterial MEMBER m_transparent_material NOTIFY material_changed)
    Q_PROPERTY(Qt3DRender::QGeometryRenderer* mesh MEMBER m_mesh NOTIFY mesh_changed)
    Q_PROPERTY(bool pickingEnabled READ get_is_picking_enabled WRITE set_is_picking_enabled NOTIFY picking_enabled_changed)

    explicit UIPrimitiveMesh(QObject* parent=nullptr);

    Qt3DRender::QGeometryRenderer* get_mesh() const;
    void set_mesh(Qt3DRender::QGeometryRenderer* mesh);
    Qt3DExtras::QMetalRoughMaterial* get_material() const;
    Qt3DExtras::QDiffuseSpecularMaterial* get_transparent_material() const;
    void set_material(const QVariant& baseColor, const QVariant& metalness, const QVariant& roughness, const QVariant& normal, const QVariant& ambientOcclusion, float textureScale=1.0, const QColor& transparent_color=QColor(122,122,122,122));
    bool get_is_picking_enabled() const;
    void set_is_picking_enabled(const bool picking_enabled);

signals:
    void mesh_changed();
    void material_changed();
    void transparent_material_changed();
    void picking_enabled_changed();

private:
    Qt3DRender::QGeometryRenderer* m_mesh = nullptr;
    Qt3DExtras::QMetalRoughMaterial* m_material = nullptr;
    Qt3DExtras::QDiffuseSpecularMaterial* m_transparent_material = nullptr;
    bool m_picking_enabled = false;
};
    }

