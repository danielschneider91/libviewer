#pragma once

#include <map>
#include <string>

#include <QObject>

#include <qabstractanimation.h>

#include "libViewer/AnimationProperty.h"
#include "libViewer/AnimationParameters.h"


namespace viewer {
class AnimationParametersContainer : public QObject {
	Q_OBJECT
	Q_PROPERTY(QList<AnimationParameters*> animationParameters MEMBER m_animation_params NOTIFY animation_params_changed)
	Q_PROPERTY(QList<QString> animatedObjectIds MEMBER m_animated_object_ids NOTIFY animated_object_ids_changed)

public:
	AnimationParametersContainer(QObject* parent = nullptr);
	void add_animation_parameters(const std::string& id, const AnimationProperty& animation_property, const AnimationParameters& animation_params);
	void remove_animation_parameters(const std::string& id, const AnimationProperty& animation_property);
	void remove_animations_parameters(const std::string& id);

signals:
	void animation_params_changed();
	void animated_object_ids_changed();
private:
	bool are_sizes_consistent();

	QList<AnimationParameters*> m_animation_params;
	QList<QString> m_animated_object_ids;
	std::map<std::string, std::map<AnimationProperty, int>> m_animated_object_id_to_animation_property_and_index_map;

};
}
