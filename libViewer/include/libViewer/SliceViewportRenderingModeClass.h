#pragma once
#include <QObject>

namespace viewer {
class SliceViewportRenderingModeClass : public QObject {
    Q_OBJECT
public:
    enum SliceViewportRenderingMode {
        MODEL,
        MODEL_DISTANCE_CLIPPED,
        OVERLAY
    };
    Q_ENUM(SliceViewportRenderingMode)
    Q_PROPERTY(SliceViewportRenderingMode mode READ get_mode WRITE set_mode NOTIFY mode_changed)

	SliceViewportRenderingModeClass() : QObject() {}
    SliceViewportRenderingModeClass(SliceViewportRenderingMode mode) : QObject(),m_mode(mode) {}

    SliceViewportRenderingMode get_mode() const { return m_mode; }
    void set_mode(const SliceViewportRenderingMode& mode)
    {
        if (m_mode != mode) {
            m_mode = mode; emit mode_changed();
        }
    }
	
signals:
    void mode_changed();

private:
    SliceViewportRenderingMode m_mode = SliceViewportRenderingMode::MODEL;
	
};
}
