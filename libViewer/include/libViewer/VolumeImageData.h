#pragma once
#include <vector>

#include <QVector3D>
#include <QMatrix4x4>

namespace viewer
{
class VolumeImageData {
public:
    VolumeImageData(const QString& series_uid,std::vector<short> data, QVector3D origin, QMatrix3x3 orientation, QVector3D spacing, QVector3D dimension, int channels=1);

    QVector3D get_origin() const;
    QMatrix3x3 get_orientation() const;
    QVector3D get_spacing() const;
    QVector3D get_dimension() const;
    int get_channels() const;
    std::vector<short> getTextureData() const;
    QString get_series_uid() const;
    void set_series_uid(const QString& series_uid);
private:
    std::vector<short> createTexture(int maxX, int maxY, int maxZ, int numberOfChannels) const;

    QString m_series_uid = "";
    std::vector<short> m_data;
    QVector3D m_origin{0,0,0};
    QMatrix3x3 m_orientation;

    QVector3D m_spacing{1.0f,1.0f,1.0f}; // w x h x d 
    QVector3D m_dimension{256,256,128};// w x h x d

    int m_channels = 3;

};
}
