#pragma once
#include <QObject>

namespace viewer {
class VisibilityClass : public QObject {
    Q_OBJECT
public:
    enum Visibility {
        HIDDEN,
        TRANSPARENNT,
        VISIBLE
    };
    Q_ENUM(Visibility)
    Q_PROPERTY(Visibility visibility READ get_visibility WRITE set_visibility NOTIFY visibility_changed)

	VisibilityClass() : QObject() {}
    VisibilityClass(Visibility visibility) : QObject(),m_visibility(visibility) {}

    Visibility get_visibility() const { return m_visibility; }
    void set_visibility(const Visibility& visibility)
    {
        if (m_visibility != visibility) {
            m_visibility = visibility; emit visibility_changed();
        }
    }
	
signals:
    void visibility_changed();

private:
    Visibility m_visibility = Visibility::VISIBLE;
	
};
}
