#pragma once
namespace viewer
{
enum class ViewerMode
{
    CENTERED = 0,
    OFF_CENTER = 1,
    TOOL_VIEW = 2
};
}
