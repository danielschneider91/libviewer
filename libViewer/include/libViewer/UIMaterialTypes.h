#pragma once
#include <QObject>

namespace viewer
{
    enum class UIMaterialTypes
    {
        BONE,
        ARTERY,
        VEINE,
        NERVE,
        SKIN,
    };

}
