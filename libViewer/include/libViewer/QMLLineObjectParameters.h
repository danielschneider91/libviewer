#pragma once

#include <QObject>
#include <QVector3D>

#include "QMLSpatialObjectParameters.h"

namespace viewer {
class QMLLineObjectParameters : public QMLSpatialObjectParameters {
    Q_OBJECT
public:
    Q_PROPERTY(QVector3D from MEMBER m_from NOTIFY from_changed)
    Q_PROPERTY(QVector3D to MEMBER m_to NOTIFY to_changed)

    QMLLineObjectParameters(const QVector3D& from, const QVector3D& to);
    QMLLineObjectParameters();
    QMLLineObjectParameters(const QMLLineObjectParameters& other);

    QVector3D get_from() const;
    QVector3D get_to() const;
    void set_from(const QVector3D& from);
    void set_to(const QVector3D& to);

signals:
    void from_changed();
    void to_changed();
protected:
    QVector3D m_from;
    QVector3D m_to;
};
}

