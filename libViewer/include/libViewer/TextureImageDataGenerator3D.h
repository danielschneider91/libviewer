#include <QTextureImageDataGenerator>
#include <QAbstractTextureImage>
#include <QVector3D>
#include <QMatrix4x4>

namespace viewer {
class TextureImageDataGenerator3D : public Qt3DRender::QTextureImageDataGenerator {
public:
    explicit TextureImageDataGenerator3D();

    Qt3DRender::QTextureImageDataPtr operator()() override;

    bool operator ==(const QTextureImageDataGenerator& other) const final;

    QT3D_FUNCTOR(TextureImageDataGenerator3D)

    void setInteger(int integer);
    void set_texture_information(const QByteArray& data, QVector3D dimension, QVector3D spacing, QVector3D origin, QMatrix4x4 orientation, int numberOfChannels);
    
private:
    void print_byte_array_values(const QByteArray& cs, int i);
    QByteArray createTexture(int maxX, int maxY, int maxZ, int numberOfChannels) const;
    QByteArray createTexture2(int maxX, int maxY, int maxZ, int numberOfChannels) const;
    QByteArray createTexture3(int maxX, int maxY, int maxZ, int numberOfChannels) const;

    int m_integer = 0;

    QVector3D m_origin{ 0,0,0 };
    QMatrix4x4 m_orientation;

    QVector3D m_spacing{ 1.0f,1.0f,1.0f }; // w x h x d 
    QVector3D m_dimension{ 256,256,128 };// w x h x d

    int m_channels = 1;

    QByteArray m_textureData;

};
}