#pragma once

#include <QObject>
#include <QVariant>

namespace viewer {
class AnimationParameters : public QObject {
    Q_OBJECT
public:
    Q_PROPERTY(bool running MEMBER m_running NOTIFY running_changed)
    Q_PROPERTY(QString prop MEMBER m_property NOTIFY prop_changed)
    Q_PROPERTY(int duration MEMBER m_duration NOTIFY duration_changed)
    Q_PROPERTY(QVariant from MEMBER m_from NOTIFY from_changed)
    Q_PROPERTY(QVariant to MEMBER m_to NOTIFY to_changed)

    AnimationParameters(const int duration, const QVariant& from, const QVariant& to, const bool running=true, const QString& prop="");
    AnimationParameters();
    AnimationParameters(const AnimationParameters& other);

    int get_duration() const;
    QVariant get_from() const;
    QVariant get_to() const;
    void set_duration(int duration);
    void set_from(const QVariant& from);
    void set_to(const QVariant& to);
    bool is_running() const;
    void set_running(const bool running);
    QString get_property() const;
    void set_property(const QString& prop);
signals:
    void running_changed();
    void prop_changed();
    void duration_changed();
    void from_changed();
    void to_changed();
protected:
    bool m_running = true;
    QString m_property = "";
    int m_duration = 250; // in us
    QVariant m_from;
    QVariant m_to;
};
}

