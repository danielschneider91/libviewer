#pragma once

#include <QObject>
#include <QVector3D>
#include <QMatrix4x4>

namespace viewer
{
class VolumeImageDataQml : public QObject {
    Q_OBJECT
public:
    VolumeImageDataQml(QObject* parent=nullptr);
    Q_INVOKABLE QByteArray get_texture_data() const;
    Q_INVOKABLE QByteArray get_texture_gradient() const;
    Q_INVOKABLE bool get_texture_gradient_precomputed() const;
    Q_INVOKABLE int get_channels() const;
    Q_INVOKABLE QVector3D get_origin() const;
    Q_INVOKABLE QVector3D get_bounding_box_origin() const;
    Q_INVOKABLE QMatrix4x4 get_orientation() const;
    Q_INVOKABLE QVector3D get_spacing() const;
    Q_INVOKABLE QVector3D get_dimension() const;
    Q_INVOKABLE QVector3D get_dimension_times_spacing() const;
    Q_INVOKABLE short get_minimum_value() const;
    Q_INVOKABLE short get_maximum_value() const;
    Q_INVOKABLE float get_maximum_extent() const;

    void set_origin(const QVector3D& origin);
    void set_orientation(const QMatrix4x4& orientation);

    void set_spacing(const QVector3D& spacing);
    void set_dimension(const QVector3D& dimension);
    void set_channels(int c);

    void set_texture_data(const QByteArray& data);
    void set_texture_data(const std::vector<short>& data);
    void set_texture_data_gradient(const std::vector<short>& data);

signals:
    void textureDataChanged();

private:
    QByteArray createTexture(int maxX, int maxY, int maxZ, int numberOfChannels) const;
    QByteArray vectorShortToByteArray(const std::vector<short>& short_vector);

    QVector3D m_origin{ 0,0,0 };
    QMatrix4x4 m_orientation;

    QVector3D m_spacing{ 1.0f,1.0f,1.0f }; // w x h x d 
    QVector3D m_dimension{ 256,256,128 };// w x h x d

    short m_minimum_value = -1;
    short m_maximum_value = 1;
    int m_channels = 3;
    QByteArray m_textureData;
    QByteArray m_textureDataGradient;
    bool m_textureGradientPrecomputed = false;

};
}
