#pragma once

#include <QObject>

#include "QMLSpatialObjectParameters.h"

namespace viewer {
class QMLCrosshairsObjectParameters : public QMLSpatialObjectParameters {
    Q_OBJECT
public:
    Q_PROPERTY(float size MEMBER m_size NOTIFY size_changed)

    QMLCrosshairsObjectParameters(const float& size);
    QMLCrosshairsObjectParameters() = default;
    QMLCrosshairsObjectParameters(const QMLCrosshairsObjectParameters& other);

    float get_size() const;

signals:
    void size_changed();
protected:
    float m_size= 10.0f;
};
}

