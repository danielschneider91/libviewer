#pragma once

#include <QObject>
#include <QList>
#include <QVector3D>

#include "QMLSpatialObjectParameters.h"

namespace viewer {
class QMLPointsObjectParameters : public QMLSpatialObjectParameters {
    Q_OBJECT
public:
    Q_PROPERTY(QList<QVector3D> positions MEMBER m_positions NOTIFY positions_changed)
    Q_PROPERTY(QList<float> sizes MEMBER m_sizes NOTIFY sizes_changed)

    QMLPointsObjectParameters(const QList<QVector3D>& positions, const QList<float>& sizes);
    QMLPointsObjectParameters()=default;
    QMLPointsObjectParameters(const QMLPointsObjectParameters& other);

    QList<QVector3D> get_positions() const;
    QList<float> get_sizes() const;

signals:
    void positions_changed();
    void sizes_changed();
protected:
    QList<QVector3D> m_positions;
    QList<float> m_sizes;
};
}

