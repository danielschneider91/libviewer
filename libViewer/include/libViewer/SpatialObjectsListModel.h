#pragma once
#include <QAbstractListModel>

#include "MaterialSettings.h"
#include "libViewer/UISpatialMeshFromFile.h"
#include "libViewer/Pose.h"

namespace viewer
{
class SpatialObjectsListModel : public QAbstractListModel
{
    Q_OBJECT
public:
    enum SpatialObjectsRoles {
        FilePath = Qt::UserRole,
        HasMaterial,
        Pose,
        Transform,
        Material,
        TransparentMaterial,
        Enabled,
        Visible,
        Name,
        Id,
        PickingEnabled
    };
    SpatialObjectsListModel(QObject* parent=nullptr);
    ~SpatialObjectsListModel();

    QVariant data(const QModelIndex& index, int role = Qt::DisplayRole) const override;
    int rowCount(const QModelIndex& parent = QModelIndex()) const override;

    Qt::ItemFlags flags(const QModelIndex& index) const override;

    void add_object(UISpatialMeshFromFile* data);
    void remove_object(const QString& id);

    QList<UISpatialMeshFromFile*> get_objects() const;
    void set_pose(const std::string& id, const viewer::Pose& pose);
    void set_material(const std::string& id, const MaterialSettings& material_settings);
    MaterialSettings get_material(const std::string& id) const;
    void set_picking_enabled(const std::string& id, const bool picking_enabled);
    virtual QHash<int, QByteArray> roleNames() const override;

private:
    std::map<QString, int> m_id_index_map;
    QList<UISpatialMeshFromFile*> m_data;
};
}
