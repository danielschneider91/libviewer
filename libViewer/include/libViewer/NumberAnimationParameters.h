#pragma once

#include <QObject>

#include "libViewer/AnimationParameters.h"

namespace viewer {
class NumberAnimationParameters : public AnimationParameters {
    Q_OBJECT
public:
    NumberAnimationParameters(const int duration, const float& from, const float& to, const bool running=true, const QString& prop="");
    NumberAnimationParameters();
    NumberAnimationParameters(const NumberAnimationParameters& other);

    float get_from() const;
    float get_to() const;


};
}

