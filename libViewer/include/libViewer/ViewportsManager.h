#pragma once

#include <QQuickItem>


#include "AnimationParameters.h"
#include "ImagesManager.h"
#include "MaterialSettings.h"
#include "ViewportVisibility.h"
#include "libViewer/VisibilityClass.h"
#include "libViewer/Pose.h"
#include "libViewer/Side.h"
#include "libViewer/SpatialObjectsManager.h"
#include "libViewer/VolumeImageData.h"
#include "libViewer/VolumeImageDataQml.h"
#include "libViewer/ViewerBehaviour.h"
#include "libViewer/ViewerConfiguration.h"
#include "libViewer/AnimationProperty.h"

namespace viewer
{

class ViewportsManager : public QObject
{
    Q_OBJECT
public:
    Q_PROPERTY(ViewerBehaviour* sliceViewerBehaviour READ get_slice_viewer_behaviour NOTIFY slice_viewer_behaviour_changed)
    Q_PROPERTY(ViewerBehaviour* threeDViewerBehaviour READ get_threeD_viewer_behaviour NOTIFY threeD_viewer_behaviour_changed)
    Q_PROPERTY(ViewerConfiguration* viewerConfiguration READ get_viewer_configuration NOTIFY viewer_configuration_changed)

    explicit ViewportsManager(ViewerConfiguration* viewer_configuration, QObject* parent = nullptr);

    void set_threeD_viewer_camera_pose(const QVector3D& camera_position, const QVector3D& look_at_position, const QVector3D& up_vector);
    void threeD_viewer_camera_view_all();

    void set_slice_viewer_behaviour(ViewerBehaviour::Behaviour viewer_behaviour);
    void set_threeD_viewer_behaviour(ViewerBehaviour::Behaviour viewer_behaviour);


    ViewerConfiguration* get_viewer_configuration() const;
    void set_viewer_configuration(ViewerConfiguration* viewer_configuration);

signals:
    void slice_viewer_behaviour_changed();
    void threeD_viewer_behaviour_changed();
    void threeD_viewer_camera_pose_changed(QVector3D pos, QVector3D target, QVector3D up);
    void threeD_viewer_camera_view_all_changed();
    void viewer_configuration_changed();
private:
    ViewerBehaviour* get_threeD_viewer_behaviour() const;
    ViewerBehaviour* get_slice_viewer_behaviour() const;

    mutable std::mutex m_mutex;
    ViewerConfiguration* m_viewer_configuration;
    ViewerBehaviour* m_slice_viewer_behaviour = nullptr;
    ViewerBehaviour* m_threeD_viewer_behaviour = nullptr;
};

}
