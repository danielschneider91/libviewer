#pragma once
#include <QAbstractListModel>

#include "MaterialSettings.h"
#include "libViewer/QMLSpatialObject.h"
#include "libViewer/Pose.h"

namespace viewer
{
class QMLSpatialObjectsListModel : public QAbstractListModel
{
    Q_OBJECT
public:
    enum SpatialObjectsRoles {
        Type = Qt::UserRole,
        Parameters,
        Transform,
        Material,
        TransparentMaterial,
        Enabled,
        Visible,
        Name,
        Id,
        PickingEnabled
    };
    QMLSpatialObjectsListModel(QObject* parent=nullptr);
    ~QMLSpatialObjectsListModel();

    QVariant data(const QModelIndex& index, int role = Qt::DisplayRole) const override;
    int rowCount(const QModelIndex& parent = QModelIndex()) const override;

    Qt::ItemFlags flags(const QModelIndex& index) const override;

    void add_object(QMLSpatialObject* type);
    void remove_object(const QString& id);
    QList<QMLSpatialObject*> get_objects() const;
    void set_pose(const std::string& id, const viewer::Pose& pose);
    void set_material(const std::string& id, const viewer::MaterialSettings& settings);
    MaterialSettings get_material(const std::string& id) const;
    QMLSpatialObjectParameters* get_parameters(const std::string& id) const;
    void set_picking_enabled(const std::string& id, const bool picking_enabled);
    virtual QHash<int, QByteArray> roleNames() const override;

private:
    std::map<QString, int> m_id_index_map;
    QList<QMLSpatialObject*> m_data;
    int m_number_of_rows;
};
}
