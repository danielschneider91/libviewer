#pragma once

#include <map>
#include <string>

#include <QObject>

#include <qabstractanimation.h>

#include "libViewer/AnimationProperty.h"
#include "libViewer/AnimationParameters.h"
#include "libViewer/NumberAnimationParameters.h"


namespace viewer {
class AnimationContainer : public QObject {
	Q_OBJECT
public:
	void add_animation(const std::string& id, const AnimationProperty& animation_property, const AnimationParameters& animation_params, QObject* target);
	void remove_animation(const std::string& id, const AnimationProperty& animation_property);
	void remove_animations(const std::string& id);

private:
	void add_animation_private(const std::string& id, const AnimationProperty& animation_property, QSequentialAnimationGroup* animation);
	QSequentialAnimationGroup* create_scale_animation(QObject* target, const NumberAnimationParameters& animation_parameters) const;

	std::map<std::string, std::map<AnimationProperty, QSequentialAnimationGroup*>> m_objects_id_to_animation_map;
 
};
}
