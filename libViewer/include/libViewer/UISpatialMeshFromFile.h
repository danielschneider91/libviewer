#pragma once

#include <QObject>
#include <Qt3DExtras/Qt3DExtras>
#include <Qt3DExtras/QMetalRoughMaterial>
#include <Qt3DExtras/QDiffuseSpecularMaterial> 

#include "UISpatialObject.h"

namespace viewer {
class UISpatialMeshFromFile : public UISpatialObject {
    Q_OBJECT
public:
    Q_PROPERTY(QString filePath MEMBER m_mesh_file_path_name NOTIFY mesh_file_path_changed)
    Q_PROPERTY(bool hasMaterial MEMBER m_has_material NOTIFY has_material_changed)
    Q_PROPERTY(Qt3DExtras::QMetalRoughMaterial* material MEMBER m_material NOTIFY material_changed)
    Q_PROPERTY(Qt3DExtras::QDiffuseSpecularMaterial* transparentMaterial MEMBER m_transparent_material NOTIFY material_changed)
    Q_PROPERTY(bool pickingEnabled READ get_is_picking_enabled WRITE set_is_picking_enabled NOTIFY picking_enabled_changed)

    explicit UISpatialMeshFromFile(QObject* parent=nullptr);

    QString get_mesh_file_path_name() const;
    void set_mesh_file_path_name(const QString& mesh_file_path_name);
    bool get_has_material() const;
    void set_has_material(const bool has_material);
    Qt3DExtras::QMetalRoughMaterial* get_material() const;
    Qt3DExtras::QDiffuseSpecularMaterial* get_transparent_material() const;
    void set_material(const QVariant& baseColor, const QVariant& metalness, const QVariant& roughness, const QVariant& normal, const QVariant& ambientOcclusion, float textureScale=1.0, const QColor& transparent_color=QColor(122,122,122,122));
    bool get_is_picking_enabled() const;
    void set_is_picking_enabled(const bool picking_enabled);

signals:
    void has_material_changed();
    void mesh_file_path_changed();
    void material_changed();
    void transparent_material_changed();
    void picking_enabled_changed();

private:
    QString m_mesh_file_path_name = "";
    bool m_has_material = false;
    Qt3DExtras::QMetalRoughMaterial* m_material = nullptr;
    Qt3DExtras::QDiffuseSpecularMaterial* m_transparent_material = nullptr;
    bool m_picking_enabled = false;

};
    }

