#pragma once
#include <QAbstractListModel>

#include "UISpatialMeshFromFile.h"
#include "Pose.h"

namespace viewer
{
class TrackedObjectsListModel : public QAbstractListModel
{
    Q_OBJECT
public:
    enum TrackedObjectsRoles {
        FilePath = Qt::UserRole,
        HasMaterial,
        Pose,
        Transform,
        Material,
        Enabled,
        Visible,
        Name
    };
    TrackedObjectsListModel(QObject* parent=nullptr);
    ~TrackedObjectsListModel();

    QVariant data(const QModelIndex& index, int role = Qt::DisplayRole) const override;
    int rowCount(const QModelIndex& parent = QModelIndex()) const override;

    Qt::ItemFlags flags(const QModelIndex& index) const override;

    void set_pose(const std::string& id, const viewer::Pose& pose);
    void add_object(UISpatialMeshFromFile* data);
    QList<UISpatialMeshFromFile*> get_objects() const;
    virtual QHash<int, QByteArray> roleNames() const override;

private:
    std::map<QString, int> m_id_index_map;
    QList<UISpatialMeshFromFile*> m_data;
};
}
