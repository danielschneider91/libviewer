#pragma once
#include <QAbstractListModel>

#include "libViewer/UISpatialMeshFromFile.h"
#include "libViewer/Pose.h"

namespace viewer
{
class RegistrationPointsListModel : public QAbstractListModel
{
    Q_OBJECT
public:
    enum RegistrationPointsListRoles {
        FilePath = Qt::UserRole,
        HasMaterial,
        Pose,
        Transform,
        Material,
        Enabled,
        Visible,
        Name
    };
    RegistrationPointsListModel(QObject* parent=nullptr);
    ~RegistrationPointsListModel();

    QVariant data(const QModelIndex& index, int role = Qt::DisplayRole) const override;
    int rowCount(const QModelIndex& parent = QModelIndex()) const override;

    Qt::ItemFlags flags(const QModelIndex& index) const override;

    void add_object(UISpatialMeshFromFile* data);
    QList<UISpatialMeshFromFile*> get_objects() const;
    void set_pose(const std::string& id, const viewer::Pose& pose);
    virtual QHash<int, QByteArray> roleNames() const override;

private:
    std::map<QString, int> m_id_index_map;
    QList<UISpatialMeshFromFile*> m_data;
    int m_number_of_rows;
};
}
