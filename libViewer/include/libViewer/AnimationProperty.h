#pragma once

namespace viewer {
enum class AnimationProperty
{
    SCALE = 0,
    COLOR = 1
};
}

