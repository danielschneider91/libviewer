#pragma once

#include <QObject>
#include <QColor>

namespace viewer
{
class ImageIntensityRangeHighlightSettings : public QObject {
    Q_OBJECT
public:
    Q_PROPERTY(bool highlight READ is_highlight WRITE set_highlight NOTIFY highlight_changed)
    Q_PROPERTY(float lowerThreshold READ get_lower_threshold WRITE set_lower_threshold NOTIFY lower_threshold_changed)
    Q_PROPERTY(float upperThreshold READ get_upper_threshold WRITE set_upper_threshold NOTIFY upper_threshold_changed)
    Q_PROPERTY(QColor color READ get_color WRITE set_color NOTIFY color_changed)

    ImageIntensityRangeHighlightSettings(QObject* parent=nullptr);
    bool is_highlight() const;
    void set_highlight(const bool highlight);
    float get_lower_threshold() const;
    void set_lower_threshold(const float lower_threshold);
    float get_upper_threshold() const;
    void set_upper_threshold(const float upper_threshold);
    QColor get_color() const;
    void set_color(const QColor& color);
signals:
    void highlight_changed();
    void lower_threshold_changed();
    void upper_threshold_changed();
    void color_changed();

private:
    bool m_highlight = false;
    float m_lower_threshold = 500;
    float m_upper_threshold = 3000;
    QColor m_color = QColor{ 0,0,255,255 };
};
}
