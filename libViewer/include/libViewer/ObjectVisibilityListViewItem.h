#pragma once

#include <QObject>
#include <QString>

#include "libViewer/VisibilityClass.h"

namespace viewer {
class ObjectVisibilityListViewItem : public QObject {
    Q_OBJECT
public:
	Q_PROPERTY(QString name READ get_name WRITE set_name NOTIFY name_changed)
	Q_PROPERTY(QString id READ get_id WRITE set_id NOTIFY id_changed)
	Q_PROPERTY(VisibilityClass* visibility READ get_visibility WRITE set_visibility NOTIFY visibility_changed)

	explicit ObjectVisibilityListViewItem();
	ObjectVisibilityListViewItem(const QString& name, const QString& id, const VisibilityClass::Visibility& visibility);
	
	QString get_name() const;
	void set_name(const QString& name);
	QString get_id() const;
	void set_id(const QString& id);
	VisibilityClass* get_visibility() const;
	void set_visibility(VisibilityClass* visibility);
	void set_visibility(VisibilityClass::Visibility visibility);
	
signals:
	void name_changed();
	void id_changed();
	void visibility_changed();

private:
	QString m_name;
	QString m_id;
	VisibilityClass* m_visibility;
	
};
}
