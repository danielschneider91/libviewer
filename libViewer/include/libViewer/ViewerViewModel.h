#pragma once

#include <QQuickItem>


#include "AnimationParameters.h"
#include "ImagesManager.h"
#include "MaterialSettings.h"
#include "ViewportType.h"
#include "ViewportVisibility.h"
#include "libViewer/VisibilityClass.h"
#include "libViewer/Pose.h"
#include "libViewer/Side.h"
#include "libViewer/SpatialObjectsManager.h"
#include "libViewer/VolumeImageData.h"
#include "libViewer/VolumeImageDataQml.h"
#include "libViewer/ViewerBehaviour.h"
#include "libViewer/ViewerConfiguration.h"
#include "libViewer/AnimationProperty.h"
#include "libViewer/ViewportsManager.h"

namespace viewer
{

class ViewerViewModel : public QObject
{
    Q_OBJECT
public:
    Q_PROPERTY(SpatialObjectsManager* spatialObjectsManager MEMBER m_spatial_objects_manager NOTIFY spatial_objects_manager_changed)
    Q_PROPERTY(ImagesManager* imagesManager MEMBER m_images_manager NOTIFY images_manager_changed)
    Q_PROPERTY(ViewportsManager* viewportsManager MEMBER m_viewports_manager NOTIFY viewports_manager_changed)
    Q_PROPERTY(QVariantList visibilityListModel READ get_visibility_list_model NOTIFY visibility_list_model_changed)
    Q_PROPERTY(QVariantList enabledListModel READ get_enabled_list_model NOTIFY enabled_list_model_changed)
    Q_PROPERTY(bool trackedModelPoseIndicatorVisibility READ get_tracked_model_pose_indicator_visibility NOTIFY tracked_model_pose_indicator_visibility_changed)

    Q_INVOKABLE void spatialObjectPicked(QString id, QVector3D world_position, QVector3D local_position);
    Q_INVOKABLE void spatialObjectHover(QString id, QVector3D world_position, QVector3D local_position);
    Q_INVOKABLE void set_axial_slice_pose(const QMatrix4x4& mat);
    Q_INVOKABLE void move_to_next_slice(int viewport_idx, int cranial_or_caudal/*-1: cranial, 1: caudal*/);
    Q_INVOKABLE void reset_viewport(int viewport_type/*0: slice viewports, 1: 3d viewport*/);

    explicit ViewerViewModel(QObject* parent=nullptr);
    ~ViewerViewModel() override;

    void add_image(const std::string& id, const viewer::VolumeImageData& image);
    void remove_image(const std::string& id);
    void remove_all_images();
    bool contains_image_with_id(const std::string& id) const;
    void set_ruler_enabled(bool enabled);
    //void set_precomputed_image_gradient(const viewer::VolumeImageData& image);

    void add_object(const std::string& id, const std::string& file_path_name, const Pose& pose = Pose(), const std::string& human_readable_name = "",
        const MaterialSettings& material_settings = MaterialSettings(), const ViewportVisibility& viewport_visibility = ViewportVisibility(), bool picking_enabled = false);
    void add_object(const std::string& id, Qt3DRender::QGeometryRenderer* primitive_geometry, const Pose& pose = Pose(), const std::string& m_human_readable_name = "primitive geometry",
        const MaterialSettings& material_settings = MaterialSettings(), const ViewportVisibility& viewport_visibility = ViewportVisibility(), bool picking_enabled = false);
    void add_object(const std::string& id, QMLSpatialObjectType::Type type, QMLSpatialObjectParameters* params, const Pose& pose = Pose(), const std::string& human_readable_name = "",
        const MaterialSettings& material_settings = MaterialSettings(), const ViewportVisibility& viewport_visibility = ViewportVisibility());
    void remove_object(const std::string& id);
    void remove_all_objects();
    bool contains_object_with_id(const std::string& id) const;
    void set_spatial_object_viewport_visibility(const std::string& id, const viewer::ViewportVisibility& viewport_visibility);
    ViewportVisibility* get_spatial_object_viewport_visibility(const std::string& id) const;

    void set_object_material(const std::string& id, const MaterialSettings& material_settings = MaterialSettings());
    MaterialSettings get_object_material(const std::string& id) const;

    void set_slices_pose(const QVector3D& position, const QVector3D& x_axis, const QVector3D& normal/*z axis*/);
    void set_object_pose(const std::string& id, const QVector3D& position, const QVector3D& x_axis, const QVector3D& normal/*z axis*/);

    void set_threeD_viewer_camera_pose(const QVector3D& camera_position, const QVector3D& look_at_position, const QVector3D& up_vector);
    void threeD_viewer_camera_view_all();

    void set_image_visibility(const std::string& id, const viewer::ViewportVisibility& viewport_visibility);
    void set_object_visibility(const std::string& id, const VisibilityClass::Visibility& visibillity);
    void set_object_is_enabled(const std::string& id, bool is_enabled);
    bool get_object_is_enabled(const std::string& id);
    void hide_all();
    void show_all();

    void show_slice_in_threeD(const SliceType& type);
    void show_slices_in_threeD();
    void hide_slice_in_threeD(const SliceType& type);
    void hide_slices_in_threeD();

    void set_object_picking_enabled(const std::string& id, bool picking_enabled);
    void set_all_object_picking_enabled(bool picking_enabled);
    void set_all_picking_enabled(bool picking_enabled);
    void set_slice_picking_enabled(bool picking_enabled);

    void add_animation(const std::string& id, const AnimationProperty& animation_property, const AnimationParameters& animation_parameters);
    void remove_animations(const std::string& id);
    void remove_animation(const std::string& id, const AnimationProperty& animation_property);

    void set_slice_viewer_behaviour(ViewerBehaviour::Behaviour viewer_behaviour);
    void set_threeD_viewer_behaviour(ViewerBehaviour::Behaviour viewer_behaviour);

    bool get_tracked_model_pose_indicator_visibility() const;
    void set_tracked_model_pose_indicator_visibility(bool visibility);

    void expand_viewport(int viewport); // use -1 to expand none
    int get_expanded_viewport() const;

    void set_viewer_configuration(ViewerConfiguration* viewer_configuration);
    ViewerConfiguration* get_viewer_configuration() const;

    QMatrix4x4 get_axial_slice_pose() const;

    QMLSpatialObjectParameters* get_spatial_object_parameters(const std::string& id) const;

    std::vector<std::string> get_image_ids_in_viewer() const;
    std::vector<std::string> get_object_ids_in_viewer() const;
    std::vector<std::string> get_primitive_spatial_object_ids_in_viewer() const;
    std::vector<std::string> get_spatial_object_from_file_ids_in_viewer() const;
    std::vector<std::string> get_qml_spatial_object_ids_in_viewer() const;

    ImageIntensityRangeHighlightSettings* get_image_intensity_range_highlight_settings() const;
    void set_side(const Side& side);

signals:
    void slice_pose_changed(QMatrix4x4 pose);
    void spatial_objects_manager_changed();
    void images_manager_changed();
    void viewports_manager_changed();
    void visibility_list_model_changed();
    void enabled_list_model_changed();
    void tracked_model_pose_indicator_visibility_changed();
    void spatial_object_or_image_picked(QString id, QVector3D world_position, QVector3D local_position);
    void spatial_object_or_image_hovered(QString id, QVector3D world_position, QVector3D local_position);
    void move_to_next_slice_requested(int viewport_idx, int cranial_or_caudal/*-1: cranial, 1: caudal*/);
    void reset_viewport_requested(int viewport_type/*-0: slice viewports, 1: 3d*/);
private:
    QVariantList get_visibility_list_model() const;
    void add_to_visibility_list(QVariant item);
    void remove_from_visibility_list(const QString& id);
    QVariantList get_enabled_list_model() const;
    void add_to_enabled_list(QVariant item);
    void remove_from_enabled_list(const QString& id);
    Side get_side() const;

    bool is_id_a_slice(const std::string& id) const;

    mutable std::mutex m_mutex;
    Side m_side = Side::LEFT;
    SpatialObjectsManager* m_spatial_objects_manager = nullptr;
    ImagesManager* m_images_manager = nullptr;
    ViewportsManager* m_viewports_manager = nullptr;
    ViewerConfiguration* m_viewer_config = nullptr;
    QVariantList m_visibility_list;
    QVariantList m_enabled_list;
    bool m_tracked_model_pose_indicator_visibility = false;
    QMatrix4x4 m_axial_slice_pose;

};

}
