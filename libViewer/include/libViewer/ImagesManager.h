#pragma once

#include "ImageIntensityRangeHighlightSettings.h"
#include "VolumeImageDataQml.h"
#include "libViewer/VolumeImageData.h"
#include "libViewer/ViewportVisibility.h"
#include "libViewer/ImagesListModel.h"

namespace viewer
{

class ImagesManager : public QObject
{
    Q_OBJECT
public:
    Q_PROPERTY(QList<VolumeImageDataQml*> volumeImages READ get_volume_images NOTIFY volume_images_changed)
    Q_PROPERTY(bool slicePickingEnabled READ get_is_slice_picking_enabled NOTIFY slice_picking_enabled_changed)
    Q_PROPERTY(QList<ViewportVisibility*> imageViewportVisibility MEMBER m_image_viewport_visibility NOTIFY images_viewport_visibility_changed)
    Q_PROPERTY(ImageIntensityRangeHighlightSettings* imageIntensityRangeHighlightSettings MEMBER m_intensity_range_highlight_settings NOTIFY intensity_range_highlight_settings_changed)

    explicit ImagesManager(QObject* parent=nullptr);
	
    void add_image(const QString& id, const viewer::VolumeImageData& image,const ViewportVisibility& viewport_visibility = ViewportVisibility(true,true, SliceViewportRenderingModeClass::SliceViewportRenderingMode::MODEL));
    void remove_image(const QString& id);
    void set_slice_picking_enabled(bool picking_enabled);
    void set_viewport_visibility(const QString& id, const viewer::ViewportVisibility& viewport_visibility);

    std::vector<QString> get_image_ids_in_viewer() const;
    bool contains_image_with_id(const QString& id) const;
    QList<VolumeImageDataQml*> get_volume_images() const;

    ImageIntensityRangeHighlightSettings* get_image_intensity_range_highlight_settings() const;
signals:
    void images_viewport_visibility_changed();
    void volume_images_changed();
    void slice_picking_enabled_changed();
    void intensity_range_highlight_settings_changed();

private:
    bool get_is_slice_picking_enabled() const;

    //std::map<QString, VolumeImageDataQml*> m_image_volume_data_map;
    QList<VolumeImageDataQml*> m_image_volume_data_list;
    QList<ViewportVisibility*> m_image_viewport_visibility;
    ImageIntensityRangeHighlightSettings* m_intensity_range_highlight_settings=nullptr;
    std::map<QString, int> m_id_index_map;
    bool m_slice_picking_enabled = false;
};


}
