#pragma once
#include <string>
#include <stdexcept>

namespace viewer
{
    enum class Side
    {
        LEFT = 0,
        RIGHT = 1,
    };
    static std::string get_side_as_string(const Side& side)
    {
        switch (side) {
        case Side::LEFT: return "Left";
        case Side::RIGHT: return "Right";
        default: return "Unknown";
        }
    }
    static Side get_side_as_enum(const std::string& side)
    {
        if(strcmp(side.c_str(), "Left")==0)
        {
            return Side::LEFT;
        }
        if(strcmp(side.c_str(), "Right") == 0)
        {
            return Side::RIGHT;
        }
        throw std::runtime_error("Unknown image plane: "+side);
    }
}
