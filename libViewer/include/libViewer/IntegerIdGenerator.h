#pragma once
#include <string>
#include <vector>

#include "libViewer/IdGenerator.h"

namespace viewer
{

class IntegerIdGenerator:public IdGenerator
{
public:
    virtual ~IntegerIdGenerator() =default;
    std::string create_unique_id(const std::vector<std::string>& existing_ids, const std::string& id_root="") const override;
};

}
