# libViewer

Qt-based library to visualize dicom data. The library provides a customizable Qml Type `Viewer` which can be integrated into your Qml code.

A demo application (see screenshots at bottom) can be installed on Windows using the installer in `installer\libViewer-1.0.0-win64.exe`. The application will be available in the user-defined install location `install-location\bin\Viewer.exe`.

# How to build and integrate libViewer

To integrate and build libViewer, Qt is required.

To also build and run the demo application, the following additional dependencies are required

- libCore
- libCaseRepositoryService

which themselves depend on the following third-party Libraries

- Eigen, version 3.3 (this specific version is required)
- ITK, version 5.2.0
- Boost

### Setup Qt

1. Download and install Qt (version 6.6.2)

### Setup libViewer

To setup libViewer follow this.

1. `git clone git@bitbucket.org:danielschneider91/libViewer.git` into a local folder (e.g., `C:/Dev/Libraries`)
2. `mkdir build && cd build`
3. `cmake .. -G "Visual Studio 16 2019" -DBUILD_VIEWER_DEMO:BOOL=OFF -DCMAKE_PREFIX_PATH="C:/Qt/6.6.2/msvc2019_64"`
4. Open libViewer.sln in VS2019 and build it (e.g., using configuration `RelWithDebInfo`)

### Use libViewer

In any other project

1. Use `find_package(libViewer)` within `CMakeLists.txt`
2. Use `target_link_libraries(${PROJECT_NAME} PRIVATE libViewer)` within `CMakeLists.txt`
3. Specify path to libViewer build directory `cmake .. [..] -DlibViewer_DIR=<Path to libViewer/build folder` (e.g., `-DlibViewer_DIR="C:/Dev/Projects/libViewer/build"`)

### Setup libViewer demo application

1. `git clone git@bitbucket.org:danielschneider91/libViewer.git` into a local folder (e.g., `C:/Dev/Libraries`)
2. `mkdir build && cd build`
3. `cmake .. -G "Visual Studio 16 2019" -DBUILD_VIEWER_DEMO:BOOL=ON -DCMAKE_PREFIX_PATH="C:/Qt/6.6.2/msvc2019_64;C:/Dev/Libraries/ITK/build" -DEigen3_DIR="C:/Dev/Libraries/eigen/build"  -DBOOST_ROOT="C:/Dev/Libraries/boost_1_78_0/boost_1_78_0"`
4. Set the environment variable `QML_IMPORT_PATH` to `<path-to-parent-of-build-folder>\build\qml` (possibly you need to restart visual studio after setting the variable)
5. Open libViewer.sln in VS2019 and build it (e.g., using configuration `RelWithDebInfo`)

# How to use libViewer

Note: To use libViewer, you only need Qt. Other dependencies are used to load image data from the local disk.

Create Qt Gui appplication:

```cpp
Q_INIT_RESOURCE(QtQuickControls2Config);
Q_IMPORT_QML_PLUGIN(libViewerPlugin);
Q_IMPORT_QML_PLUGIN(libViewerUIPlugin);


// Register types here (see next paragraph in README)
// ...


QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);

auto app = std::make_unique<QGuiApplication>(argc, argv);
auto engine = std::make_shared<QQmlApplicationEngine>();
```

Register qml types used by libViewer (add corresponding includes)

```cpp
qmlRegisterType<viewer::UISpatialObject>("libViewer", 1, 0, "UISpatialObject");
qmlRegisterType<viewer::UISpatialMeshFromFile>("libViewer", 1, 0, "UISpatialMeshFromFile");
qmlRegisterType<viewer::UISpatialMeshFromFileDiffuseSpecular>("libViewer", 1, 0,"UISpatialMeshFromFileDiffuseSpecular");
qmlRegisterType<viewer::TextureImage3D>("libViewer", 1, 0, "TextureImage3D");
qmlRegisterType<viewer::TextureImage3DGradient>("libViewer", 1, 0, "TextureImage3DGradient");
qmlRegisterType<viewer::TextureImage1D>("libViewer", 1, 0, "TextureImage1D");
qmlRegisterType<viewer::ViewerBehaviour>("libViewer", 1, 0, "ViewerBehaviour");
qmlRegisterType<viewer::QMLSpatialObjectType>("libViewer", 1, 0, "QMLSpatialObjectType");
qmlRegisterType<viewer::VisibilityClass>("libViewer", 1, 0, "VisibilityClass");
qmlRegisterType<viewer::SliceViewportRenderingModeClass>("libViewer", 1, 0, "SliceViewportRenderingModeClass");
qmlRegisterType<viewer::ViewportVisibility>("libViewer", 1, 0, "ViewportVisibility");
qmlRegisterType<viewer::ViewerConfiguration>("libViewer", 1, 0, "ViewerConfiguration");

qmlRegisterUncreatableType<viewer::SpatialObjectsListModel>("libViewer", 1, 0, "SpatialObjectsListModel","Cannot create a SpatialObjectsListModel instance from QML.");
qmlRegisterUncreatableType<viewer::PrimitiveSpatialObjectsListModel>(
    "libViewer", 1, 0, "PrimitiveSpatialObjectsListModel","Cannot create a PrimitiveSpatialObjectsListModel instance from QML.");
qmlRegisterUncreatableType<viewer::QMLSpatialObjectsListModel>("libViewer", 1, 0, "QMLSpatialObjectsListModel","Cannot create a QMLSpatialObjectsListModel instance from QML.");
qmlRegisterUncreatableType<viewer::SpatialObjectsManager>("libViewer", 1, 0, "SpatialObjectsManager","Cannot create a SpatialObjectsManager instance from QML.");
qmlRegisterUncreatableType<viewer::ImagesManager>("libViewer", 1, 0, "ImagesManager","Cannot create a ImagesManager instance from QML.");
qmlRegisterUncreatableType<viewer::ViewportsManager>("libViewer", 1, 0, "ViewportsManager","Cannot create a ViewportsManager instance from QML.");
qmlRegisterUncreatableType<viewer::VolumeImageDataQml>("libViewer", 1, 0, "VolumeImageDataQml","Cannot create a VolumeImageDataQml instance from QML.");
qmlRegisterUncreatableType<viewer::AnimationManager>("libViewer", 1, 0, "AnimationManager","Cannot create a AnimationManager instance from QML.");
qmlRegisterUncreatableType<viewer::AnimationParametersContainer>("libViewer", 1, 0, "AnimationParametersContainer","Cannot create a AnimationParametersContainer instance from QML.");
```

Initialize viewmodel of viewer. This class should be used to control the viewer from within your application.

```cpp
auto viewer_vm = std::make_shared<viewer::ViewerViewModel>();
auto viewer_vm_ptr = viewer_vm.get();

auto measurements_vm = std::make_shared<viewer::MeasurementsViewModel>(viewer_vm);
auto measurements_vm_ptr = measurements_vm.get();

engine->rootContext()->setContextProperty("viewerViewModel", viewer_vm_ptr);
engine->rootContext()->setContextProperty("measurementsViewModel", measurements_vm_ptr);
```

Read image data into vector of short values (see ViewerDemo for example based on libCaseRepositoryService)

```cpp
[...]
std::vector<short> short_array(nx * ny * nz);

int counter = 0;
for (int z = 0; z < nz; z++) // for all depths
{
  for (int y = 0; y < ny; y++) // for all Rows
  {
    for (int x = 0; x < nx; x++) // for all Columns
    {
      ImageType::IndexType idx;
      idx[0] = x;
      idx[1] = y;
      idx[2] = z;
      short_array[counter] = itk_image->GetPixel(idx); // need to fill itk_image before, see ViewerDemo
      counter++;
    }
  }
}

const ImageType::SpacingType& spacing = itk_image->GetSpacing();
const ImageType::PointType& origin = itk_image->GetOrigin();
const ImageType::DirectionType& direct = itk_image->GetDirection();

QMatrix3x3 orientation(new float[] {
  (float)direct(0, 0), (float)direct(0, 1), (float)direct(0, 2),
  (float)direct(1, 0), (float)direct(1, 1), (float)direct(1, 2),
  (float)direct(2, 0), (float)direct(2, 1), (float)direct(2, 2)
});

viewer::VolumeImageData volumeImageData(short_array,
  QVector3D(origin[0], origin[1], origin[2]),
  orientation,
  QVector3D(spacing[0], spacing[1], spacing[2]),
  QVector3D(nx, ny, nz),
  1
);
```

Give image to viewer viewmodel, and start application.

```cpp
viewer_vm->add_image("demo_image", volumeImageData);

const QUrl url(QStringLiteral("qrc:/YourQmlApplication.qml"));
engine->load(url);
app->exec();
```

In your qml application (YourQmlApplication), use the Qml type `Viewer` provided by libViewer as follows

```cpp
Rectangle {
    id: viewerArea
    anchors.centerIn: parent
    anchors.fill: parent
    Viewer {
        id: viewer
        anchors.fill: parent
        spatialObjectsManager:viewerViewModel ? viewerViewModel.spatialObjectsManager : null
        imagesManager: viewerViewModel ? viewerViewModel.imagesManager : null
        viewerConfiguration: viewerViewModel ? viewerViewModel.viewportsManager.viewerConfiguration : null
        viewportsManager: viewerViewModel ? viewerViewModel.viewportsManager : null
    }

    ViewerOverlay {
        id: viewerOverlay
        numberOfViewports: viewerViewModel.viewportsManager.viewerConfiguration.viewportsLayout.length
        threeDViewportId: viewerViewModel.viewportsManager.viewerConfiguration.getThreeDViewportId()
        expandedViewport: viewerViewModel.viewportsManager.viewerConfiguration?viewerViewModel.viewportsManager.viewerConfiguration.expandedViewport:-1
        viewportsRepeater: viewer.viewportsItem.viewportsRepeater
        viewportToSlicetypeMap: viewerViewModel.viewportsManager.viewerConfiguration.viewportToSlicetypeMap
        viewportToViewportName: viewerViewModel.viewportsManager.viewerConfiguration.viewportToViewportNameMap
        anchors.fill: parent
        windowWidth: viewer.windowWidth
        windowLevel: viewer.windowLevel
        volumeWindowWidth: viewer.volumeWindowWidth
        volumeWindowLevel: viewer.volumeWindowLevel
        visibilityModel: viewerViewModel ? viewerViewModel.visibilityListModel : null
        distanceMeasurement: measurementsViewModel ? measurementsViewModel.measurement.distance : null
        cameraViewportWidths: viewer.cameraViewportWidths
    }

    Component.onCompleted: {
        for(var i=0;i<viewerOverlay.viewerButtonsRepeater.count;i++) {
            viewerOverlay.viewerButtonsRepeater.itemAt(i).fullscreenButtonClicked.connect(function(viewerIndex) { viewerViewModel.viewportsManager.viewerConfiguration.expandedViewport = viewerIndex; })
            viewerOverlay.viewerButtonsRepeater.itemAt(i).nextCaudalSliceRequested.connect(function(viewerIndex) { viewerViewModel.move_to_next_slice(viewerIndex,-1); })
            viewerOverlay.viewerButtonsRepeater.itemAt(i).nextCranialSliceRequested.connect(function(viewerIndex) { viewerViewModel.move_to_next_slice(viewerIndex,1); })
            viewerOverlay.viewerButtonsRepeater.itemAt(i).resetViewerRequested.connect(function(viewerIndex) { var empty = (viewerIndex===viewerViewModel.viewportsManager.viewerConfiguration.getThreeDViewportId()) ? viewerViewModel.reset_viewport(1) : viewerViewModel.reset_viewport(0); })
            viewerOverlay.viewerButtonsRepeater.itemAt(i).adjustingWindowLevelAndWidthChanged.connect(function(viewerIndex,adjusting) { viewer.toggleWindowWidthLevelAdjustment(adjusting) })
            viewerOverlay.viewerButtonsRepeater.itemAt(i).zoomChanged.connect(function(viewerIndex,inOrOut) { viewer.zoom(viewerIndex,inOrOut) })
            viewerOverlay.viewerButtonsRepeater.itemAt(i).volRenderingEnabledChanged.connect(function(viewerIndex,enabled) { viewer.setVolumeRenderingEnabled(enabled); })
            viewerOverlay.viewerButtonsRepeater.itemAt(i).volRenderingLevelingEnabled.connect(function(viewerIndex,enabled) { viewer.toggleVolumeRenderingWindowWidthLevelAdjustment(enabled); })
            viewerOverlay.viewerButtonsRepeater.itemAt(i).volRenderingTypeChanged.connect(function(viewerIndex,type) { viewer.setVolumeRenderingType(type); })
            viewerOverlay.viewerButtonsRepeater.itemAt(i).volRenderingTransferFunctionTypeChanged.connect(function(viewerIndex,type) { viewer.setVolumeRenderingTFType(type); })
            viewerOverlay.viewerButtonsRepeater.itemAt(i).rulerEnbldChanged.connect(function(viewerIndex,enabled) { measurementsViewModel.rulerEnabled = enabled; })
            viewerOverlay.viewerButtonsRepeater.itemAt(i).rulerReset.connect(function() { measurementsViewModel.resetMeasurement(); })
        }
    }
}
```

# How to configure libViewer

Use the `ViewerViewModel` to interact with the viewer from a c++ backend.

libViewer provides the following functionalities:

Add and remove image data which will be visualized, check if image is already in viewer.

```cpp
void add_image(const std::string& id, const viewer::VolumeImageData& image);
void remove_image(const std::string& id);
void remove_all_images();
bool contains_image_with_id(const std::string& id) const;
```

Add 3d model (_.stl/_.obj) to view. The `id` specified here can later be used to reference the object.

```cpp
void add_object(const std::string& id, const std::string& file_path_name, const Pose& pose = Pose(), const std::string& human_readable_name = "", const MaterialSettings& material_settings = MaterialSettings(), const ViewportVisibility& viewport_visibility = ViewportVisibility(), bool picking_enabled = false);
```

Add any geometry deriving from `Qt3DRender::QGeometryRenderer` to view. The `id` specified here can later be used to reference the object. Use to add Qt3DExtras Mesh types (e.g., `Qt3DExtras::QSphereMesh`).

```cpp
void add_object(const std::string& id, Qt3DRender::QGeometryRenderer* primitive_geometry, const Pose& pose = Pose(), const std::string& m_human_readable_name = "primitive geometry", const MaterialSettings& material_settings = MaterialSettings(), const ViewportVisibility& viewport_visibility = ViewportVisibility(), bool picking_enabled = false);
```

Add predefined types (see `QMLSpatialObjectType::Type`) to view. The `id` specified here can later be used to reference the object.

```cpp
void add_object(const std::string& id, QMLSpatialObjectType::Type type, QMLSpatialObjectParameters* params, const Pose& pose = Pose(), const std::string& human_readable_name = "", const MaterialSettings& material_settings = MaterialSettings(), const ViewportVisibility& viewport_visibility = ViewportVisibility());
```

Remove objects from viewer, and check if an object is already in viewer.

```cpp
void remove_object(const std::string& id);
void remove_all_objects();
bool contains_object_with_id(const std::string& id) const;

```

Define viewports in which object is rendered (3D/MPR).

```cpp
void set_spatial_object_viewport_visibility(const std::string& id, const viewer::ViewportVisibility& viewport_visibility);
```

Get/update appearance of objects in viewer.

```cpp
void set_object_material(const std::string& id, const MaterialSettings& material_settings = MaterialSettings());
MaterialSettings get_object_material(const std::string& id) const;
```

Set pose of axial slice plane. At this pose, the volume image will be sliced.

```cpp
void set_slices_pose(const QVector3D& position, const QVector3D& x_axis, const QVector3D& normal/*z axis*/);
```

Set pose of object in viewer with the `id`.

```cpp
void set_object_pose(const std::string& id, const QVector3D& position, const QVector3D& x_axis, const QVector3D& normal/*z axis*/);
```

Get slices pose. Returns the pose of the axial slice, other slices (coronal, sagittal) are perpendicular to axial.

```cpp
QMatrix4x4 get_axial_slice_pose() const;
```

Set 3D viewer camera pose.

```cpp
void set_threeD_viewer_camera_pose(const QVector3D& camera_position, const QVector3D& look_at_position, const QVector3D& up_vector);
void threeD_viewer_camera_view_all();
```

Set visibility of object/image/slice with the `id`.

```cpp
void set_image_visibility(const std::string& id, const viewer::ViewportVisibility& viewport_visibility);
void set_object_visibility(const std::string& id, const VisibilityClass::Visibility& visibillity);
void hide_all();
void show_all();
void show_slice_in_threeD(const SliceType& type);
void show_slices_in_threeD();
void hide_slice_in_threeD(const SliceType& type);
void hide_slices_in_threeD();
```

Toggle picking of slices and objects with the `id`. On picked, the signal `void spatial_object_or_image_picked(QString id, QVector3D world_position, QVector3D local_position)` is emitted.

```cpp
void set_object_picking_enabled(const std::string& id, bool picking_enabled);
void set_slice_picking_enabled(bool picking_enabled);
```

Add/remove animation to object with `id`. Currently only the scale of objects deriving from `Qt3DRender::QGeometryRenderer` and predefined types (see `QMLSpatialObjectType::Type`) can be animated. On the others, animations have no effect.

```cpp
void add_animation(const std::string& id, const AnimationProperty& animation_property, const AnimationParameters& animation_parameters);
void remove_animations(const std::string& id);
void remove_animation(const std::string& id, const AnimationProperty& animation_property);
```

Set behaviour of slice/3d viewer when `set_object_pose` or `set_slices_pose` is called.

```cpp
void set_slice_viewer_behaviour(ViewerBehaviour::Behaviour viewer_behaviour);
void set_threeD_viewer_behaviour(ViewerBehaviour::Behaviour viewer_behaviour);
```

Expand viewport/ get expanded viewport id.

```cpp
void expand_viewport(int viewport); // use -1 to expand none
int get_expanded_viewport() const;
```

Change/get viewer configuration.

```cpp
ViewerConfiguration* get_viewer_configuration() const;
void set_viewer_configuration(ViewerConfiguration* viewer_configuration);
```

Get ids of objects/images in viewer.

```cpp
std::vector<std::string> get_image_ids_in_viewer() const;
std::vector<std::string> get_object_ids_in_viewer() const;
```

# Screenshots

A few screenshots of the demoviewer:

![Alt text](/testing/ViewerDemo/screenshots/DemoViewer1.png)

![Alt text](/testing/ViewerDemo/screenshots/DemoViewer2.png)

![Alt text](/testing/ViewerDemo/screenshots/DemoViewer3.png)

![Alt text](/testing/ViewerDemo/screenshots/DemoViewer4.png)

![Alt text](/testing/ViewerDemo/screenshots/DemoViewer5.png)

### Configure Qt Creator to run demo application

Add the following parameters to the list under Project --> Build & Run --> Build --> Build Settings --> Cmake --> initial CMake Parameters

```
-DCMAKE_PREFIX_PATH=C:/Qt/6.6.2/msvc2019_64;C:/Dev/Libraries/ITK/build
-DEigen3_DIR=C:/Dev/Libraries/eigen/build
-DBOOST_ROOT=C:/Dev/Libraries/boost_1_78_0/boost_1_78_0
-DBUILD_VIEWER_DEMO=ON
```
