import QtQuick 2.14
import QtQuick.Controls
import QtQuick.Scene3D 2.4

import QtQuick.Window 2.2

import libViewer 1.0

ApplicationWindow {
    id: window
    width: 1920/4*2
    height: 1080/4*2
    visible: true
    //visibility: "FullScreen"
    title: qsTr("Viewer test")

    header: ToolBar {
        id: toolBar
        contentHeight: toolButton.implicitHeight

        ToolButton {
            id: toolButton
            text: stackView.depth > 1 ? "\u25C0" : "\u2630"
            font.pixelSize: Qt.application.font.pixelSize * 1.6
        }

        Label {
            text: window.title
            anchors.centerIn: parent
        }
    }

    StackView {
        id: stackView
        anchors.fill: parent

        initialItem: Page {
            id: viewerPage
            width: window.width
            height: window.height-toolBar.height
            title: window.title

            Rectangle {
                anchors.centerIn: parent
                anchors.fill: parent
                color: Qt.rgba(1,1,0,1)
                Viewer {
                    anchors.fill: parent

                    spatialObjectsManager:viewerViewModel ? viewerViewModel.spatialObjectsManager : null
                    imagesManager: viewerViewModel ? viewerViewModel.imagesManager : null
                    viewerConfiguration: viewerViewModel ? viewerViewModel.viewerConfiguration : null

                }
            }

        }
    }

}
