#include <qcoreapplication.h>
#include <QGuiApplication.h>
#include <QQmlApplicationEngine>
#include <QQmlContext>
#include <QtQml/QQmlExtensionPlugin>

#include "libCommon/PatientPreviewInfo.h"
#include "libCommon/CaseInfo.h"
#include "libCaseRepositoryService/CaseLoaderService.h"
#include "libCaseRepositoryService/ICaseLoaderService.h"

#include "libViewer/TextureImage3D.h"
#include "libViewer/TextureImage3DGradient.h"
#include "libViewer/TextureImage1D.h"
#include "libViewer/SpatialObjectsManager.h"
#include "libViewer/UISpatialMeshFromFile.h"
#include "libViewer/UISpatialMeshFromFileDiffuseSpecular.h"
#include "libViewer/VolumeImageData.h"
#include "libViewer/VolumeImageDataQml.h"
#include "libViewer/ViewerViewModel.h"
#include "libViewer/ViewerBehaviour.h"
#include "libViewer/VisibilityClass.h"
#include "libViewer/SliceViewportRenderingModeClass.h"
#include "libViewer/ViewerConfiguration.h"
#include "libViewer/SpatialObjectsListModel.h"
#include "libViewer/PrimitiveSpatialObjectsListModel.h"
#include "libViewer/QMLSpatialObjectsListModel.h"
#include "libViewer/QMLSpatialObject.h"
#include "libViewer/QMLSpatialObjectType.h"

#include "libViewer/ImagesManager.h"
#include "libViewer/MeasurementsViewModel.h"

#include "srcs/DemoViewerViewModel.h"
//#include "ViewerConfig.h"

int main(int argc, char* argv[])
{
	if (argc < 2) {
    // report version
    //std::cout << argv[0] << " Version " << Viewer_VERSION_MAJOR << "."
    //          << Viewer_VERSION_MINOR << std::endl;
    std::cout << "Usage: " << argv[0] << " number" << std::endl;
  }


    // Viewer
    Q_INIT_RESOURCE(QtQuickControls2Config);

	// libViewer
	Q_IMPORT_QML_PLUGIN(libViewerPlugin);

	// libViewerUI
	Q_IMPORT_QML_PLUGIN(libViewerUIPlugin);

    qmlRegisterType<viewer::UISpatialObject>("libViewer", 1, 0, "UISpatialObject");
    qmlRegisterType<viewer::UISpatialMeshFromFile>("libViewer", 1, 0, "UISpatialMeshFromFile");
    qmlRegisterType<viewer::UISpatialMeshFromFileDiffuseSpecular>("libViewer", 1, 0,
                                                                  "UISpatialMeshFromFileDiffuseSpecular");
    qmlRegisterType<viewer::TextureImage3D>("libViewer", 1, 0, "TextureImage3D");
    qmlRegisterType<viewer::TextureImage3DGradient>("libViewer", 1, 0, "TextureImage3DGradient");
    qmlRegisterType<viewer::TextureImage1D>("libViewer", 1, 0, "TextureImage1D");
    qmlRegisterType<viewer::ViewerBehaviour>("libViewer", 1, 0, "ViewerBehaviour");
    qmlRegisterType<viewer::QMLSpatialObjectType>("libViewer", 1, 0, "QMLSpatialObjectType");
    qmlRegisterType<viewer::VisibilityClass>("libViewer", 1, 0, "VisibilityClass");
    qmlRegisterType<viewer::SliceViewportRenderingModeClass>("libViewer", 1, 0, "SliceViewportRenderingModeClass");
    qmlRegisterType<viewer::ViewportVisibility>("libViewer", 1, 0, "ViewportVisibility");
    qmlRegisterType<viewer::ViewerConfiguration>("libViewer", 1, 0, "ViewerConfiguration");

    qmlRegisterUncreatableType<viewer::SpatialObjectsListModel>("libViewer", 1, 0, "SpatialObjectsListModel",
                                                                "Cannot create a SpatialObjectsListModel instance from QML.");
    qmlRegisterUncreatableType<viewer::PrimitiveSpatialObjectsListModel>(
        "libViewer", 1, 0, "PrimitiveSpatialObjectsListModel",
        "Cannot create a PrimitiveSpatialObjectsListModel instance from QML.");
    qmlRegisterUncreatableType<viewer::QMLSpatialObjectsListModel>("libViewer", 1, 0, "QMLSpatialObjectsListModel",
                                                                   "Cannot create a QMLSpatialObjectsListModel instance from QML.");
    qmlRegisterUncreatableType<viewer::SpatialObjectsManager>("libViewer", 1, 0, "SpatialObjectsManager",
                                                              "Cannot create a SpatialObjectsManager instance from QML.");
    qmlRegisterUncreatableType<viewer::ImagesManager>("libViewer", 1, 0, "ImagesManager",
                                                      "Cannot create a ImagesManager instance from QML.");
    qmlRegisterUncreatableType<viewer::ViewportsManager>("libViewer", 1, 0, "ViewportsManager",
                                                         "Cannot create a ViewportsManager instance from QML.");
    qmlRegisterUncreatableType<viewer::VolumeImageDataQml>("libViewer", 1, 0, "VolumeImageDataQml",
                                                           "Cannot create a VolumeImageDataQml instance from QML.");
    qmlRegisterUncreatableType<viewer::AnimationManager>("libViewer", 1, 0, "AnimationManager",
                                                         "Cannot create a AnimationManager instance from QML.");
    qmlRegisterUncreatableType<viewer::AnimationParametersContainer>("libViewer", 1, 0, "AnimationParametersContainer",
                                                                     "Cannot create a AnimationParametersContainer instance from QML.");
  

    auto viewer_vm = std::make_shared<viewer::ViewerViewModel>();
    auto viewer_vm_ptr = viewer_vm.get();

    auto measurements_vm = std::make_shared<viewer::MeasurementsViewModel>(viewer_vm);
    auto measurements_vm_ptr = measurements_vm.get();

    auto case_repo_path_provider = std::make_shared< case_repo::CaseRepositoryPathProvider >("./demoData/");
    std::shared_ptr<case_repo::ICaseLoaderService> case_loader_service = std::make_unique<
        case_repo::CaseLoaderService>(case_repo_path_provider);
    auto infos = case_loader_service->load_case_preview_infos();

    //std::sort(infos.begin(), infos.end(), [](const common::PatientPreviewInfo& i, const common::PatientPreviewInfo& j)
    //    {
    //        return i.get_patient_meta_data().get_name() < j.get_patient_meta_data().get_name();
    //    });

    std::string patient_id = "";
    std::string series_instance_id = "";
    for (const auto& [id0, info] : infos)
    {
        auto pat_id = info.get_patient_meta_data().get_patient_id();
        assert(id0 == pat_id);

        patient_id = pat_id;

        for (const auto& [id1, im] : info.get_image_meta_datas())
        {
            assert(id1 == im.get_series_instance_id());
            assert(pat_id == im.get_patient_id());
            series_instance_id = id1;
            break;
        }
        break;
    }

    auto case_info = case_loader_service->load_case(std::vector < std::string>{ series_instance_id}, std::vector < std::string>{}, std::vector < std::string>{ });

    using PixelType = signed short;
    using ImageType = itk::Image<PixelType, 3>;

    auto itk_image = case_info.get_image_datas().front().get_image();

    ImageType::RegionType region = itk_image->GetLargestPossibleRegion();
    ImageType::SizeType size = region.GetSize();

    auto nx = size[0];
    auto ny = size[1];
    auto nz = size[2];
    std::vector<short> short_array(nx * ny * nz);

    int counter = 0;
    for (int z = 0; z < nz; z++) // for all depths
    {
        for (int y = 0; y < ny; y++) // for all Rows
        {
            for (int x = 0; x < nx; x++) // for all Columns
            {
                ImageType::IndexType idx;
                idx[0] = x;
                idx[1] = y;
                idx[2] = z;
                short_array[counter] = itk_image->GetPixel(idx);
                counter++;
            }
        }
    }

    const ImageType::SpacingType& spacing = itk_image->GetSpacing();
    const ImageType::PointType& origin = itk_image->GetOrigin();
    const ImageType::DirectionType& direct = itk_image->GetDirection();

    QMatrix3x3 orientation(new float[]{
        static_cast<float>(direct(0, 0)), static_cast<float>(direct(0, 1)), static_cast<float>(direct(0, 2)),
        static_cast<float>(direct(1, 0)), static_cast<float>(direct(1, 1)), static_cast<float>(direct(1, 2)),
        static_cast<float>(direct(2, 0)), static_cast<float>(direct(2, 1)), static_cast<float>(direct(2, 2))
    });

    viewer::VolumeImageData volumeImageData("test", short_array,
                                            QVector3D(origin[0], origin[1], origin[2]),
                                            orientation,
                                            QVector3D(spacing[0], spacing[1], spacing[2]),
                                            QVector3D(nx, ny, nz),
                                            1
    );
    viewer_vm->add_image("demo_image", volumeImageData);



    auto demo_viewer_vm = std::make_shared<demo_viewer::DemoViewerViewModel>(viewer_vm, case_loader_service);
    auto demo_viewer_vm_ptr = demo_viewer_vm.get();

    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);

    auto app = std::make_unique<QGuiApplication>(argc, argv);
    qputenv("QT3D_RENDERER", "opengl");
    qputenv("QSG_RHI_BACKEND", "opengl");
    auto engine = std::make_shared<QQmlApplicationEngine>();

    engine->rootContext()->setContextProperty("viewerViewModel", viewer_vm_ptr);
    engine->rootContext()->setContextProperty("measurementsViewModel", measurements_vm_ptr);
    engine->rootContext()->setContextProperty("demoViewerViewModel", demo_viewer_vm_ptr);

    const QUrl url(QStringLiteral("qrc:/Viewer/ui/ViewerDemo.qml"));
    engine->load(url);
    int res = app->exec();
    std::cout << "res: " << res << std::endl;
}
