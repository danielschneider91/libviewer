import QtQuick
import QtQuick.Controls
import QtQuick.Dialogs

Item {
    id: root

    readonly property int titleFontSize: 14
    readonly property int myMargins: 3

    Flickable {
        id: actionBarArea
        width: root.width
        height: root.height
        contentWidth: actionBarArea.width
        contentHeight: actionBarColumn.height
        property real buttonHeight:actionBarArea.height/12


        Column {
            id: actionBarColumn
            width: parent.width
            anchors.horizontalCenter: parent.horizontalCenter

            Label {
                anchors.horizontalCenter: parent.horizontalCenter
                horizontalAlignment: Text.AlignHCenter
                verticalAlignment: Text.AlignVCenter
                width: actionBarArea.width
                height: actionBarArea.buttonHeight
                text: "Images"
                font.bold: true
            }
            Rectangle {
                color: "transparent"
                width: actionBarArea.width
                height: actionBarArea.buttonHeight*3
                anchors.horizontalCenter: parent.horizontalCenter
                ListView {
                    id: imagesListView
                    anchors.horizontalCenter: parent.horizontalCenter
                    width: actionBarArea.width-root.myMargins*2
                    height: parent.height
                    clip: true

                    model: demoViewerViewModel.imagesListModel
                    delegate: Row {
                        Rectangle {
                            width: imagesListView.width-actionBarArea.buttonHeight*2
                            height: actionBarArea.buttonHeight
                            anchors.verticalCenter: parent.verticalCenter
                            color: "transparent"
                            Label {
                                anchors.fill: parent
                                verticalAlignment: Text.AlignVCenter
                                text: model.id + " "+model.humanReadableName
                            }
                        }
                        Button {
                            width: actionBarArea.buttonHeight
                            height: actionBarArea.buttonHeight
                            anchors.verticalCenter: parent.verticalCenter
                            icon.source: "qrc:/libViewerUI/ui/resources/icons/eye.svg"
                            checkable: true
                            Component.onCompleted: {
                                checked=true
                            }
                            onClicked: {
                                demoViewerViewModel.toggleImageVisibility(model.id,checked);
                            }
                        }
                        Button {
                            width: actionBarArea.buttonHeight
                            height: actionBarArea.buttonHeight
                            anchors.verticalCenter: parent.verticalCenter
                            icon.source: "qrc:/libViewerUI/ui/resources/icons/delete.svg"
                            onClicked: {
                                demoViewerViewModel.removeImage(model.id);
                            }
                        }
                    }
                }
            }
            Button {
                width: actionBarArea.width
                height: actionBarArea.buttonHeight
                anchors.horizontalCenter: parent.horizontalCenter
                text: "Add image"
                onClicked: {
                    imageDialog.visible = true
                    imageDialog.open()
                }
            }

            Label {
                anchors.horizontalCenter: parent.horizontalCenter
                horizontalAlignment: Text.AlignHCenter
                verticalAlignment: Text.AlignVCenter
                width: actionBarArea.width
                height: actionBarArea.buttonHeight
                text: "Objects"
                font.bold: true
            }
            Rectangle {
                id: objectsListViewContainer
                width: actionBarArea.width
                height: actionBarArea.buttonHeight*3
                anchors.horizontalCenter: parent.horizontalCenter
                color: "transparent"
                property int currentlyEditedItemIndex: 0
                property string currentlyEditedItemId: ""
                ColorDialog {
                    id:colorDialog
                    visible: false
                    //height: 300
                    //width: 300
                    onSelectedColorChanged: {
                        demoViewerViewModel.setObjectColor(objectsListViewContainer.currentlyEditedItemId,selectedColor)
                    }
                    onAccepted: {
                        demoViewerViewModel.setObjectColor(objectsListViewContainer.currentlyEditedItemId,selectedColor)
                    }
                }

                ListView {
                    id: objectsListView
                    anchors.horizontalCenter: parent.horizontalCenter
                    width: actionBarArea.width-root.myMargins*2
                    height: parent.height
                    clip: true

                    model: demoViewerViewModel.spatialObjectsListModel
                    delegate: Row {
                        id: itemContainer
                        Button {
                            width: imagesListView.width-actionBarArea.buttonHeight*2.0
                            height: actionBarArea.buttonHeight
                            anchors.verticalCenter: parent.verticalCenter
                            contentItem: Label {
                                verticalAlignment: Text.AlignVCenter
                                text: model.id+", "+model.humanReadableName
                                wrapMode: Text.Wrap
                            }
                            flat:true
                            checkable: true
                            onClicked: {
                                console.log("Object with id "+ model.id+" selected (checked: "+checked+")")
                                demoViewerViewModel.obectSelected(model.id,checked)
                            }
                        }
                        Button {
                            width: actionBarArea.buttonHeight
                            height: actionBarArea.buttonHeight
                            anchors.verticalCenter: parent.verticalCenter
                            background: Rectangle {
                                height: actionBarArea.buttonHeight/2.0
                                width: height//actionBarArea.buttonHeight/2.0
                                color: model.color
                            }
                            onClicked: {
                                objectsListViewContainer.currentlyEditedItemIndex = index;
                                objectsListViewContainer.currentlyEditedItemId = model.id;
                                colorDialog.open();
                            }
                        }
                        Button {
                            width: actionBarArea.buttonHeight
                            height: actionBarArea.buttonHeight
                            anchors.verticalCenter: parent.verticalCenter
                            icon.source: "qrc:/libViewerUI/ui/resources/icons/delete.svg"
                            onClicked: {
                                demoViewerViewModel.removeObject(model.id);
                            }
                        }
                    }
                }
            }
            Button {
                width: actionBarArea.width
                height: actionBarArea.buttonHeight
                anchors.horizontalCenter: parent.horizontalCenter
                text: "Add *.stl/obj"
                onClicked: {
                    threedModelDialog.visible = true
                    threedModelDialog.open()
                }
            }
            Rectangle {
                color: "transparent"
                id: addGenericObject
                width: actionBarArea.width
                height: actionBarArea.buttonHeight*2
                anchors.horizontalCenter: parent.horizontalCenter
                Column {
                    width: parent.width
                    anchors.horizontalCenter: parent.horizontalCenter
                    Rectangle{
                        color: "transparent"
                        height: actionBarArea.buttonHeight
                        width: parent.width
                        anchors.horizontalCenter: parent.horizontalCenter

                        Row {
                            height: actionBarArea.buttonHeight
                            anchors.horizontalCenter: parent.horizontalCenter
                            ComboBox {
                                id: typeDropdown
                                width: actionBarArea.width*2/3.0
                                height: actionBarArea.buttonHeight
                                model: [ "Point", "Line", "Crosshair","Sphere" ]
                                onCurrentIndexChanged: {
                                    demoViewerViewModel.setPrimitiveObjectType(currentIndex)
                                }
                            }
                            Rectangle{
                                width: actionBarArea.width*1/3.0
                                height: actionBarArea.buttonHeight
                                color: "transparent"
                                Button {
                                    width: parent.width
                                    height: actionBarArea.buttonHeight
                                    text: "Add"
                                    onClicked: {
                                        var xC = isNaN(parseFloat(xCoord.text)) ? 0 : parseFloat(xCoord.text)
                                        var yC = isNaN(parseFloat(yCoord.text)) ? 0 : parseFloat(yCoord.text)
                                        var zC = isNaN(parseFloat(zCoord.text)) ? 0 : parseFloat(zCoord.text)
                                        var vec = Qt.vector3d(xC,yC,zC);
                                        demoViewerViewModel.addPrimitiveObject(typeDropdown.currentIndex,vec);
                                    }
                                }
                            }

                        }
                    }

                    Row{
                        id: addGenericObjectPositionRow
                        height: actionBarArea.buttonHeight*0.75
                        anchors.horizontalCenter: parent.horizontalCenter
                        TextField {
                            id: xCoord
                            width: actionBarArea.width/3
                            height: parent.height
                            placeholderText: "x"
                            horizontalAlignment: TextInput.AlignHCenter
                            validator: DoubleValidator {}
                        }
                        TextField {
                            id: yCoord
                            width: actionBarArea.width/3
                            height: parent.height
                            placeholderText: "y"
                            horizontalAlignment: TextInput.AlignHCenter
                            validator: DoubleValidator {}
                        }
                        TextField {
                            id: zCoord
                            width: actionBarArea.width/3
                            height: parent.height
                            placeholderText: "z"
                            horizontalAlignment: TextInput.AlignHCenter
                            validator: DoubleValidator {}
                        }
                    }
                }
            }
        }
    }

    FileDialog {
        id: threedModelDialog
        title: "Please choose a file"
        //currentFolder: StandardPaths.HomeLocation
        //selectMultiple: true
        fileMode:FileDialog.OpenFiles
        nameFilters: ["*.obj","*.stl"]
        onAccepted: {
            console.log("You chose: " + threedModelDialog.selectedFiles)
            demoViewerViewModel.loadThreedModel(threedModelDialog.selectedFiles)
            visible = false
        }
        onRejected: {
            visible = false
        }
        Component.onCompleted: visible = false
    }
    FolderDialog {
        id: imageDialog
        title: "Please choose a file"
        //currentFolder: StandardPaths.HomeLocation
        //selectFolder: true
        onAccepted: {
            console.log("You chose: " + imageDialog.selectedFolder)
            demoViewerViewModel.loadImage(imageDialog.selectedFolder)
            visible = false
            //Qt.quit()
        }
        onRejected: {
            visible = false
            //Qt.quit()
        }
        Component.onCompleted: visible = false
    }
}
