import QtQuick 2.0
import QtQuick.Controls

Item {
    id: root

    Flickable {
        id: actionBarArea
        width: root.width
        height: root.height
        contentWidth: actionBarArea.width
        contentHeight: actionBarColumn.height
        property real buttonHeight:actionBarArea.height/12

        Column {
            id: actionBarColumn

            Button {
                id: sliceViewerBehaviourButton
                property int counter: 0
                width: actionBarArea.width
                height: actionBarArea.buttonHeight
                text: "Slice viewer behaviour "+sliceViewerBehaviourButton.counter
                onClicked: {
                    sliceViewerBehaviourButton.counter++;
                    if(sliceViewerBehaviourButton.counter>3) {
                        sliceViewerBehaviourButton.counter = 0;
                    }
                    demoViewerViewModel.toggleSliceViewerBehaviour(sliceViewerBehaviourButton.counter);
                }
            }
            Button {
                id: threeDViewerBehaviourButton
                property int counter: 0
                width: actionBarArea.width
                height: actionBarArea.buttonHeight
                text: "3D viewer behaviour "+threeDViewerBehaviourButton.counter
                onClicked: {
                    threeDViewerBehaviourButton.counter++;
                    if(threeDViewerBehaviourButton.counter>3) {
                        threeDViewerBehaviourButton.counter = 0;
                    }
                    demoViewerViewModel.toggleThreedViewerBehaviour(threeDViewerBehaviourButton.counter);
                }
            }
            Button {
                property int counter: 0
                width: actionBarArea.width
                height: actionBarArea.buttonHeight
                text: "Toggle viewer configuration"
                onClicked: {
                    counter++;
                    if(counter>2)
                        counter = 0;
                    demoViewerViewModel.toggleViewerConfiguration(counter);
                }
            }
            Button {
                property int counter: -1
                width: actionBarArea.width
                height: actionBarArea.buttonHeight
                text: "Expand viewport"
                onClicked: {
                    counter++;
                    if(counter>3)
                        counter = -1;
                    demoViewerViewModel.expandViewport(counter);
                }
            }
            Button {
                width: actionBarArea.width
                height: actionBarArea.buttonHeight
                checkable: true
                text: checked ? "Stop animation" : "Start animation"
                onClicked: demoViewerViewModel.toggleAnimate(checked);
            }
            Button {
                width: actionBarArea.width
                height: actionBarArea.buttonHeight
                checkable: true
                text: checked ? "Stop tracking" : "Start tracking"
                onClicked: demoViewerViewModel.toggleTracking(checked);
            }
            Button {
                width: actionBarArea.width
                height: actionBarArea.buttonHeight
                checkable: true
                text: "Toggle indicator visibility"
                onClicked: demoViewerViewModel.toggleIndicatorVisibility(checked)
            }
            Button {
                property bool trueOrFalse:true
                width: actionBarArea.width
                height: actionBarArea.buttonHeight
                text: "Object picking"
                checkable: true
                onClicked: {
                    demoViewerViewModel.toggleObjectPicking(checked);
                    trueOrFalse = !trueOrFalse;
                }
            }
            Button {
                property bool trueOrFalse:true
                width: actionBarArea.width
                height: actionBarArea.buttonHeight
                checkable: true
                text: "Slice picking"
                onClicked: {
                    demoViewerViewModel.toggleSlicePicking(checked);
                    trueOrFalse = !trueOrFalse;
                }
            }
            Button {
                enabled: false
                visible: enabled
                width: actionBarArea.width
                height: actionBarArea.buttonHeight
                text: "Random"
                checkable: true
                onClicked: {
                    demoViewerViewModel.testRandom(checked);
                }
            }
            //Button {
            //    property bool trueOrFalse:true
            //    width: actionBarArea.width
            //    height: actionBarArea.buttonHeight
            //    text: "Random"
            //    onClicked: {
            //        demoViewerViewModel.testRandom(trueOrFalse);
            //        trueOrFalse = !trueOrFalse;
            //    }
            //}
        }

    }

}
