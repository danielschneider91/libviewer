import QtQuick
import QtQuick.Controls
import QtQuick.Scene3D 2.4

import QtQuick.Window 2.2
import QtQuick.Dialogs

import QtQuick.Controls.Material
import QtQuick.Controls.Universal

import libViewer
import libViewerUI


ApplicationWindow {
    id: window
    width: 1920/4*2
    height: 1080/4*2
    visible: true
    //visibility: "FullScreen"
    title: qsTr("Demo Viewer")

    header: ToolBar {
        id: toolBar
        contentHeight: toolButton.implicitHeight

        ToolButton {
            id: toolButton
            text: stackView.depth > 1 ? "\u25C0" : "\u2630"
            font.pixelSize: Qt.application.font.pixelSize * 1.6
            onClicked: {
                if(drawer.visible) {
                    drawer.close()
                } else {
                    drawer.open()
                }
            }
        }

        Label {
            text: window.title
            anchors.centerIn: parent
        }
    }
    Drawer {
        id: drawer
        width: 0.2 * window.width
        height: window.height-toolBar.height
        y: toolBar.height

        Column {
            anchors.fill: parent
            Button {
                text: "Exit"
                flat: true
                width: parent.width
                onClicked: {
                    Qt.quit()
                }
            }
        }
    }

    StackView {
        id: stackView
        anchors.fill: parent

        initialItem: Item {
            id: viewerPage
            width: window.width
            height: window.height-toolBar.height

            Row {
                LeftSidebar {
                    id: leftSidebar
                    width: viewerPage.width*0.2
                    height: viewerPage.height
                }

                Rectangle {
                    id: viewerArea
                    width: viewerPage.width*0.6
                    height: viewerPage.height
                    //anchors.centerIn: parent
                    //anchors.fill: parent
                    color: "red"
                    Viewer {
                        id: viewer
                        anchors.fill: parent
                        spatialObjectsManager:viewerViewModel ? viewerViewModel.spatialObjectsManager : null
                        imagesManager: viewerViewModel ? viewerViewModel.imagesManager : null
                        viewerConfiguration: viewerViewModel ? viewerViewModel.viewportsManager.viewerConfiguration : null
                        viewportsManager: viewerViewModel ? viewerViewModel.viewportsManager : null
                    }

                    ViewerOverlay {
                        id: viewerOverlay
                        numberOfViewports: viewerViewModel.viewportsManager.viewerConfiguration.viewportsLayout.length
                        threeDViewportId: viewerViewModel.viewportsManager.viewerConfiguration.getThreeDViewportId()
                        expandedViewport: viewerViewModel.viewportsManager.viewerConfiguration?viewerViewModel.viewportsManager.viewerConfiguration.expandedViewport:-1
                        viewportsRepeater: viewer.viewportsItem.viewportsRepeater
                        viewportToSlicetypeMap: viewerViewModel.viewportsManager.viewerConfiguration.viewportToSlicetypeMap
                        viewportToViewportName: viewerViewModel.viewportsManager.viewerConfiguration.viewportToViewportNameMap
                        anchors.fill: parent
                        windowWidth: viewer.windowWidth
                        windowLevel: viewer.windowLevel
                        volumeWindowWidth: viewer.volumeWindowWidth
                        volumeWindowLevel: viewer.volumeWindowLevel
                        visibilityModel: viewerViewModel ? viewerViewModel.visibilityListModel : null
                        distanceMeasurement: measurementsViewModel ? measurementsViewModel.measurement.distance : null
                        cameraViewportWidths: viewer.cameraViewportWidths
                    }

                    Component.onCompleted: {
                        for(var i=0;i<viewerOverlay.viewerButtonsRepeater.count;i++) {
                            viewerOverlay.viewerButtonsRepeater.itemAt(i).fullscreenButtonClicked.connect(function(viewerIndex) { viewerViewModel.viewportsManager.viewerConfiguration.expandedViewport = viewerIndex; })
                            viewerOverlay.viewerButtonsRepeater.itemAt(i).nextCaudalSliceRequested.connect(function(viewerIndex) { viewerViewModel.move_to_next_slice(viewerIndex,-1); })
                            viewerOverlay.viewerButtonsRepeater.itemAt(i).nextCranialSliceRequested.connect(function(viewerIndex) { viewerViewModel.move_to_next_slice(viewerIndex,1); })
                            viewerOverlay.viewerButtonsRepeater.itemAt(i).resetViewerRequested.connect(function(viewerIndex) { var empty = (viewerIndex===viewerViewModel.viewportsManager.viewerConfiguration.getThreeDViewportId()) ? viewerViewModel.reset_viewport(1) : viewerViewModel.reset_viewport(0); })
                            viewerOverlay.viewerButtonsRepeater.itemAt(i).adjustingWindowLevelAndWidthChanged.connect(function(viewerIndex,adjusting) { viewer.toggleWindowWidthLevelAdjustment(adjusting) })
                            viewerOverlay.viewerButtonsRepeater.itemAt(i).zoomChanged.connect(function(viewerIndex,inOrOut) { viewer.zoom(viewerIndex,inOrOut) })
                            viewerOverlay.viewerButtonsRepeater.itemAt(i).volRenderingEnabledChanged.connect(function(viewerIndex,enabled) { viewer.setVolumeRenderingEnabled(enabled); })
                            viewerOverlay.viewerButtonsRepeater.itemAt(i).volRenderingLevelingEnabled.connect(function(viewerIndex,enabled) { viewer.toggleVolumeRenderingWindowWidthLevelAdjustment(enabled); })
                            viewerOverlay.viewerButtonsRepeater.itemAt(i).volRenderingTypeChanged.connect(function(viewerIndex,type) { viewer.setVolumeRenderingType(type); })
                            viewerOverlay.viewerButtonsRepeater.itemAt(i).volRenderingTransferFunctionTypeChanged.connect(function(viewerIndex,type) { viewer.setVolumeRenderingTFType(type); })
                            viewerOverlay.viewerButtonsRepeater.itemAt(i).rulerEnbldChanged.connect(function(viewerIndex,enabled) { measurementsViewModel.rulerEnabled = enabled; })
                            viewerOverlay.viewerButtonsRepeater.itemAt(i).rulerReset.connect(function() { measurementsViewModel.resetMeasurement(); })
                        }
                    }
                }
                RightSidebar {
                    id: rightSidebar
                    width: viewerPage.width*0.2
                    height: viewerPage.height
                }
            }
        }
    }

}
