#include "StringsListModel.h"

#include <iostream>

namespace demo_viewer
{
StringsListModel::StringsListModel(QObject* parent): QAbstractListModel(parent)
{
}

StringsListModel::~StringsListModel()
{
}

QHash<int, QByteArray> StringsListModel::roleNames() const {
    QHash<int, QByteArray> roles;
    roles[Id] = "id";
    roles[HumanReadableName] = "humanReadableName";
    return roles;
}

QVariant StringsListModel::data(const QModelIndex& index, int role) const
{
    QVariant result = QVariant();
    int row = index.row();
    int column = index.column();

    if (!index.isValid() || row >= rowCount())
    {
        std::cout << "index not valid or row larger than rowCount" << std::endl;
        return result;
    }

    switch (role)
    {


    case Id:
    {
        return m_data_id.at(row);
    }
    case HumanReadableName:
    {
        return m_data_human_readable_name.at(row);
    }

    default: break;
    }


    return result;
}

int StringsListModel::rowCount(const QModelIndex& parent) const
{
    if(parent.isValid())
    {
        std::cout << "parent is valid" << std::endl;
        return 0;
    }
    return m_data_id.size();
}

Qt::ItemFlags StringsListModel::flags(const QModelIndex& index) const
{
    Qt::ItemFlags result = Qt::ItemIsEditable | QAbstractItemModel::flags(index);
    return result;
}

void StringsListModel::add_object(const QString& id, const QString& human_readable_name)
{
    m_id_index_map[id] = m_data_id.size();

    beginInsertRows(QModelIndex(), m_data_id.size(), m_data_id.size()); // Specify the first and last row numbers for the span of rows you want to insert into an item in a model.
    m_data_id.push_back(id);
    m_data_human_readable_name.push_back(human_readable_name);
    endInsertRows();

    assert(m_data_id.size() == m_data_human_readable_name.size());
}

void StringsListModel::remove_object(const QString& id)
{
    auto index = m_id_index_map.at(id);
    beginRemoveRows(QModelIndex(), index, index); // Specify the first and last row numbers for the span of rows you want to remove from an item in a model.
    m_data_id.takeAt(index);
    m_data_human_readable_name.takeAt(index);
    endRemoveRows();

    // rebuild id_index_map
    m_id_index_map.clear();
    for (int i = 0; i < m_data_id.count(); ++i)
    {
        m_id_index_map[m_data_id[i]] = i;
    }

    assert(m_data_id.size() == m_data_human_readable_name.size());
}

}
