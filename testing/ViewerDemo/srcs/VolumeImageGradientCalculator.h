#pragma once

#include <itkImage.h>

namespace demo_viewer
{
class VolumeImageGradientCalculator
{
    struct Gradient
    {
        itk::Image<signed short, 3>::Pointer gradient_x = nullptr;
        itk::Image<signed short, 3>::Pointer gradient_y = nullptr;
        itk::Image<signed short, 3>::Pointer gradient_z = nullptr;
    };
    struct GradientAsVectors
    {
         std::vector<short>  gradient_x;
         std::vector<short> gradient_y;
         std::vector<short> gradient_z;
    };
public:
    VolumeImageGradientCalculator();
    void set_image(itk::Image<signed short, 3>::Pointer image);
    Gradient get_gradient() const;
    GradientAsVectors get_gradient_as_vectors() const;
    std::vector<short> get_gradient_as_vector() const;
private:
    VolumeImageGradientCalculator::Gradient  calculate_gradient(itk::SmartPointer<itk::Image<short, 3>> image) const;
    itk::SmartPointer<itk::Image<short, 3>> calculate_gradient_in_direction(const unsigned int direction, itk::SmartPointer<itk::Image<short, 3>> image) const;
    itk::Image<signed short, 3>::Pointer m_image = nullptr;
    Gradient m_gradient;
};
}
