#include "MockBackendCommands.h"

#include <QMatrix4x4>

namespace demo_viewer {
MockBackendCommands::MockBackendCommands()
{
	setup_mock_poses();
	m_tracking_mock_running = true;
	m_tracking_mock_paused = true;
	m_tracking_mock_future = std::async(std::launch::async, &MockBackendCommands::tracking_loop,this);
}

MockBackendCommands::~MockBackendCommands()
{
	m_tracking_mock_running = false;
	m_tracking_mock_paused = true;
	m_tracking_mock_future.get();
}

void MockBackendCommands::start_tracking_mock()
{
	m_tracking_mock_paused = false;
}

void MockBackendCommands::stop_tracking_mock()
{
	m_tracking_mock_paused = true;
}

bool MockBackendCommands::get_is_tracking() const
{
	return !m_tracking_mock_paused;
}

void MockBackendCommands::tracking_loop()
{
	int counter = 0;
	while(m_tracking_mock_running)
	{
		if (!m_tracking_mock_paused) {
			if (counter >= m_mock_poses.size())
			{
				counter = 0;
			}
			QMatrix4x4 pose = m_mock_poses[counter];
			emit pose_changed(pose);
			std::this_thread::sleep_for(std::chrono::milliseconds(500));
			counter++;
		}
		else { counter = 0; }
	}
}

void MockBackendCommands::setup_mock_poses()
{
	m_mock_poses.clear();

	QMatrix4x4 pose; 
	float x = 5.0f; float y = 20; float z = -550;

    for(int i=0;i<m_number_of_mock_poses;i++)
	{
		QVector3D translation(x,y,z);
		pose.setToIdentity();
		pose.translate(translation);
		//pose.rotate(5.0f+float(i*2), QVector3D(1, 0.3, 0.8).normalized());
		m_mock_poses.push_back(pose);
		x += float(i) / 8.0f;
		y += float(i) / 7.0f;
		z += -0.3f;
	}
}
}
