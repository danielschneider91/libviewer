#include "DemoViewerViewModel.h"

#include <filesystem>
#include <iostream>

#include <QVector3D>
#include <QMatrix4x4>
#include <QMatrix3x3>
#include <QColor>

#include "libViewer/PoseCalculator.h"
#include "libViewer/Pose.h"
#include "libViewer/MetalRoughMaterialProperties.h"
#include "libViewer/MetalRoughMaterials.h"
#include "libViewer/QMLCrosshairsObjectParameters.h"
#include "libViewer/QMLLineObjectParameters.h"
#include "libViewer/QMLPointsObjectParameters.h"

namespace demo_viewer
{
DemoViewerViewModel::DemoViewerViewModel(std::shared_ptr<viewer::ViewerViewModel> viewer_view_model,
                                         std::shared_ptr<case_repo::ICaseLoaderService> case_loader_service) :
    m_viewer_view_model(viewer_view_model), m_case_loader_service(case_loader_service)
{
    m_images_list = new StringsListModel(this);
    m_spatial_objects_list = new StringColorListModel(this);

    auto image_ids = m_viewer_view_model->get_image_ids_in_viewer();
    for (const auto& img_id : image_ids)
    {
        m_images_list->add_object(QString::fromStdString(img_id), QString::fromStdString(img_id + "_hrn"));
    }
    auto obj_ids = m_viewer_view_model->get_object_ids_in_viewer();
    for (const auto& obj_id : obj_ids)
    {
        m_spatial_objects_list->add_object(QString::fromStdString(obj_id), QString::fromStdString(obj_id + "_hrn"),Qt::yellow);
    }

    connect(&m_mock_backend_commands, &MockBackendCommands::pose_changed, this,
            &DemoViewerViewModel::on_mock_pose_received);
    connect(m_viewer_view_model.get(), &viewer::ViewerViewModel::spatial_object_or_image_picked,
            [this](QString id, QVector3D world_position, QVector3D local_position)
            {
                std::cout << "Spatial object with id " << id.toStdString() << " was picked." << std::endl;
                std::cout << "World: " << world_position.x() << "," << world_position.y() << "," << world_position.z()
                    << std::endl;
                std::cout << "Local: " << local_position.x() << "," << local_position.y() << "," << local_position.z()
                    << std::endl;
                //addPrimitiveObject(get_primitive_object_type(), world_position);
            });
}

void DemoViewerViewModel::toggleTracking(bool on)
{
    if (on)
    {
        m_mock_backend_commands.start_tracking_mock();
    }
    else
    {
        m_mock_backend_commands.stop_tracking_mock();
    }
}

void DemoViewerViewModel::toggleSliceViewerBehaviour(int behaviour)
{
    switch (behaviour)
    {
    case 0:
        m_viewer_view_model->set_slice_viewer_behaviour(viewer::ViewerBehaviour::Behaviour::CENTERED_CROSSHAIR);
        break;
    case 1:
        m_viewer_view_model->set_slice_viewer_behaviour(viewer::ViewerBehaviour::Behaviour::MOVING_CROSSHAIR);
        break;
    case 2:
        m_viewer_view_model->set_slice_viewer_behaviour(viewer::ViewerBehaviour::Behaviour::PROBES_VIEW);
        break;
    case 3:
        m_viewer_view_model->set_slice_viewer_behaviour(viewer::ViewerBehaviour::Behaviour::ROTATING_PROBES_VIEW);
        break;
    default:
        m_viewer_view_model->set_slice_viewer_behaviour(viewer::ViewerBehaviour::Behaviour::CENTERED_CROSSHAIR);
        break;
    }
}

void DemoViewerViewModel::toggleThreedViewerBehaviour(int behaviour)
{
    switch (behaviour)
    {
    case 0:
        m_viewer_view_model->set_threeD_viewer_behaviour(viewer::ViewerBehaviour::Behaviour::CENTERED_CROSSHAIR);
        break;
    case 1:
        m_viewer_view_model->set_threeD_viewer_behaviour(viewer::ViewerBehaviour::Behaviour::MOVING_CROSSHAIR);
        break;
    case 2:
        m_viewer_view_model->set_threeD_viewer_behaviour(viewer::ViewerBehaviour::Behaviour::PROBES_VIEW);
        break;
    case 3:
        m_viewer_view_model->set_threeD_viewer_behaviour(viewer::ViewerBehaviour::Behaviour::ROTATING_PROBES_VIEW);
        break;
    default:
        m_viewer_view_model->set_threeD_viewer_behaviour(viewer::ViewerBehaviour::Behaviour::CENTERED_CROSSHAIR);
        break;
    }
}

void DemoViewerViewModel::toggleIndicatorVisibility(bool on)
{
    m_viewer_view_model->set_tracked_model_pose_indicator_visibility(on);
}

void DemoViewerViewModel::toggleViewerConfiguration(int configuration)
{
    auto config = new viewer::ViewerConfiguration();
    switch (configuration)
    {
    case 0:
        {
            QList<QRectF> input = {
                QRectF(0.0, 0.0, 0.5, 0.5), QRectF(0.5, 0.0, 0.5, 0.5),
                QRectF(0.0, 0.5, 0.5, 0.5), QRectF(0.5, 0.5, 0.5, 0.5)
            };
            config->set_viewports_layout(input);
            break;
        }
    case 1:
        {
            QList<QRectF> input1 = {
                QRectF(0.0, 0.0, 1, 0.7), QRectF(0, 0.7, 0.3, 0.3),
                QRectF(0.3, 0.7, 0.3, 0.3), QRectF(0.6, 0.7, 0.4, 0.3)
            };
            config->set_viewports_layout(input1);
            break;
        }
    case 2:
        {
            QList<QRectF> input = {
                QRectF(0, 0, 0.25, 1), QRectF(0.25, 0, 0.25, 1),
                QRectF(0.5, 0, 0.25, 1), QRectF(0.75, 0, 0.25, 1)
            };
            config->set_viewports_layout(input);
            break;
        }
    default:
        {
            QList<QRectF> input = {
                QRectF(0.0, 0.0, 0.5, 0.5), QRectF(0.5, 0.0, 0.5, 0.5),
                QRectF(0.0, 0.5, 0.5, 0.5), QRectF(0.5, 0.5, 0.5, 0.5)
            };
            config->set_viewports_layout(input);
            break;
        }
    }
    m_viewer_view_model->set_viewer_configuration(config);
}

void DemoViewerViewModel::loadThreedModel(const QList<QUrl>& files)
{
    QList<QUrl>::const_iterator i;
    for (i = files.begin(); i != files.end(); ++i)
    {
        auto file = (*i).toString().toStdString();
        std::cout << file << std::endl;
        auto id = generate_object_id();
        viewer::MaterialSettings material_settings{
            false, viewer::MetalRoughMaterials::get_material(viewer::MetalRoughMaterialType::BONE),
            QColor(122, 122, 122, 122)
        };
        viewer::SliceViewportRenderingModeClass::SliceViewportRenderingMode slice_viewport_rendering_mode =
            viewer::SliceViewportRenderingModeClass::SliceViewportRenderingMode::OVERLAY;
        material_settings.set_metal_rough_material_properties(
            viewer::MetalRoughMaterials::get_material(viewer::MetalRoughMaterialType::BONE));
        m_viewer_view_model->add_object(id, file, viewer::Pose(), id, material_settings,
                                        viewer::ViewportVisibility{true, true, slice_viewport_rendering_mode});

        m_spatial_objects_list->add_object(QString::fromStdString(id), QString::fromStdString(id + "_hrn"),material_settings.get_metal_rough_material_properties().get_base_color().value<QColor>());
        emit spatial_objects_list_changed();
    }
}

void DemoViewerViewModel::loadImage(const QUrl& path)
{
    std::cout << path.toLocalFile().toStdString() << std::endl;
    std::filesystem::path p(path.toLocalFile().toStdString());

    using PixelType = signed short;
    using ImageType = itk::Image<PixelType, 3>;

    itk::Image<signed short, 3>::Pointer itk_image = nullptr;
    try
    {
        auto itk_image_and_dicts = m_case_loader_service->load_image(p);
        itk_image = std::get<0>(itk_image_and_dicts);
    }
    catch (std::exception& ex)
    {
        std::cerr << "Could not load image. Exception: " << ex.what();
        return;
    }

    ImageType::RegionType region = itk_image->GetLargestPossibleRegion();
    ImageType::SizeType size = region.GetSize();

    auto nx = size[0];
    auto ny = size[1];
    auto nz = size[2];
    std::vector<short> short_array(nx * ny * nz);

    int counter = 0;
    for (int z = 0; z < nz; z++) // for all depths
    {
        for (int y = 0; y < ny; y++) // for all Rows
        {
            for (int x = 0; x < nx; x++) // for all Columns
            {
                ImageType::IndexType idx;
                idx[0] = x;
                idx[1] = y;
                idx[2] = z;
                short_array[counter] = itk_image->GetPixel(idx);
                counter++;
            }
        }
    }

    const ImageType::SpacingType& spacing = itk_image->GetSpacing();
    const ImageType::PointType& origin = itk_image->GetOrigin();
    const ImageType::DirectionType& direct = itk_image->GetDirection();

    QMatrix3x3 orientation(new float[]{
        static_cast<float>(direct(0, 0)), static_cast<float>(direct(0, 1)), static_cast<float>(direct(0, 2)),
        static_cast<float>(direct(1, 0)), static_cast<float>(direct(1, 1)), static_cast<float>(direct(1, 2)),
        static_cast<float>(direct(2, 0)), static_cast<float>(direct(2, 1)), static_cast<float>(direct(2, 2))
    });

    viewer::VolumeImageData volumeImageData("test", short_array,
                                            QVector3D(origin[0], origin[1], origin[2]),
                                            orientation,
                                            QVector3D(spacing[0], spacing[1], spacing[2]),
                                            QVector3D(nx, ny, nz),
                                            1
    );
    auto id = "image_" + std::to_string(m_image_counter);
    m_image_counter++;
    m_viewer_view_model->add_image(id, volumeImageData);

    m_images_list->add_object(QString::fromStdString(id), QString::fromStdString(id + "_hrn"));
    emit images_list_changed();
}

void DemoViewerViewModel::addPrimitiveObject(const int& idx, const QVector3D& position)
{
    assert(idx == get_primitive_object_type());
    auto id = generate_object_id();
    viewer::Pose p;
    viewer::MaterialSettings material_settings{
        false, viewer::MetalRoughMaterialProperties{QVariant::fromValue(QColor{255,124,0}),QVariant::fromValue(0.3),QVariant::fromValue(0.5)},QColor{255,124,122},
    };

    viewer::SliceViewportRenderingModeClass::SliceViewportRenderingMode slice_viewport_rendering_mode =
        viewer::SliceViewportRenderingModeClass::SliceViewportRenderingMode::MODEL;
    switch (idx)
    {
    case(0):
        {
            slice_viewport_rendering_mode =
                viewer::SliceViewportRenderingModeClass::SliceViewportRenderingMode::MODEL_DISTANCE_CLIPPED;
            auto params_points = new viewer::QMLPointsObjectParameters{QList{position}, QList{10.0f}};
            id = id + "_points";
            m_viewer_view_model->add_object(id, viewer::QMLSpatialObjectType::Type::POINTS, params_points, p,
                                            id + "_points", material_settings, viewer::ViewportVisibility{
                                                true, true, slice_viewport_rendering_mode, 10
                                            });
            break;
        }
    case(1):
        {
            slice_viewport_rendering_mode =
                viewer::SliceViewportRenderingModeClass::SliceViewportRenderingMode::MODEL_DISTANCE_CLIPPED;
            auto params_line = new viewer::QMLLineObjectParameters{QVector3D(0, 0, 0), position};
            id = id + "_line";
            m_viewer_view_model->add_object(id, viewer::QMLSpatialObjectType::Type::LINE, params_line, p, id + "_line",
                                            material_settings, viewer::ViewportVisibility{
                                                true, true, slice_viewport_rendering_mode, 30
                                            });
            break;
        }
    case(2):
        {
            slice_viewport_rendering_mode =
                viewer::SliceViewportRenderingModeClass::SliceViewportRenderingMode::MODEL_DISTANCE_CLIPPED;
            p.set_position(position);
            auto params_crosshairs = new viewer::QMLCrosshairsObjectParameters{20};
            id = id + "_crosshair";
            m_viewer_view_model->add_object(id, viewer::QMLSpatialObjectType::Type::CROSSHAIRS, params_crosshairs, p,
                                            id + "_crosshair", material_settings, viewer::ViewportVisibility{
                                                true, true, slice_viewport_rendering_mode
                                            });
            break;
        }
    case(3):
        {
            slice_viewport_rendering_mode =
                viewer::SliceViewportRenderingModeClass::SliceViewportRenderingMode::OVERLAY;
            p.set_position(position);
            auto sphere = new Qt3DExtras::QSphereMesh();
            sphere->setRadius(10.0);
            id = id + "_sphere";
            m_viewer_view_model->add_object(id, sphere, p, id + "_sphere", material_settings,
                                            viewer::ViewportVisibility{true, true, slice_viewport_rendering_mode});
            break;
        }
    default:
        {
            std::cout << "Type unknown, defaulting to points." << std::endl;
            auto params_points_default = new viewer::QMLPointsObjectParameters{QList{position}, QList{10.0f}};
            id = id + "_points";
            m_viewer_view_model->add_object(id + "_points", viewer::QMLSpatialObjectType::Type::POINTS,
                                            params_points_default, p, id + "_points", material_settings,
                                            viewer::ViewportVisibility{true, true, slice_viewport_rendering_mode});
            break;
        }
    }

    m_spatial_objects_list->add_object(QString::fromStdString(id), QString::fromStdString(id + "_hrn"), material_settings.get_metal_rough_material_properties().get_base_color().value<QColor>());
    emit spatial_objects_list_changed();
}

void DemoViewerViewModel::toggleAnimate(bool startOrStop)
{
    if (startOrStop) //start
    {
        auto ids = m_viewer_view_model->get_primitive_spatial_object_ids_in_viewer();
        auto ids2 = m_viewer_view_model->get_qml_spatial_object_ids_in_viewer();
        ids.insert(ids.end(), ids2.begin(), ids2.end());

        if (ids.size() == 0)
        {
            return;
        }
        if (ids.size() == 1)
        {
            m_animated_object_id = ids[0];
        }
        else
        {
            std::random_device dev;
            std::mt19937 rng(dev());
            std::uniform_int_distribution<std::mt19937::result_type> dist(0, ids.size() - 1);
            auto random_integer = dist(rng);
            m_animated_object_id = ids[random_integer];
        }

        m_viewer_view_model->add_animation(m_animated_object_id, viewer::AnimationProperty::SCALE,
                                           viewer::NumberAnimationParameters{2000, 1.0, 0.25, true, "scale"});
    }
    else
    {
        try
        {
            m_viewer_view_model->remove_animations(m_animated_object_id);
        }
        catch (std::exception& ex)
        {
            std::cerr << "Could not remove animation, viewer does not contain object with id: " << m_animated_object_id << ". Exception: " << ex.what()
                << std::endl;
        }
    }
}

void DemoViewerViewModel::removeObject(QString id)
{
    if (m_viewer_view_model->contains_object_with_id(id.toStdString()))
    {
        m_viewer_view_model->remove_object(id.toStdString());
        m_spatial_objects_list->remove_object(id);
        emit spatial_objects_list_changed();
    }
}

void DemoViewerViewModel::obectSelected(QString id, bool checked)
{
    if (checked) //start
    {
        try {
            m_viewer_view_model->add_animation(id.toStdString(), viewer::AnimationProperty::SCALE,
                viewer::NumberAnimationParameters{ 2000, 1.0, 0.25, true, "scale" });
        } catch(std::exception& ex)
        {
            std::cerr << "Could not add animation to object with id: " << id.toStdString() << ". Exception: " << ex.what()
                << std::endl;
        }
    }
    else
    {
        try
        {
            m_viewer_view_model->remove_animations(id.toStdString());
        }
        catch (std::exception& ex)
        {
            std::cerr << "Could not remove animation, viewer does not contain object with id: " << id.toStdString() << ". Exception: " << ex.what()
                << std::endl;
        }
    }
}

void DemoViewerViewModel::removeImage(QString id)
{
    if (m_viewer_view_model->contains_image_with_id(id.toStdString()))
    {
        m_viewer_view_model->remove_image(id.toStdString());
        m_images_list->remove_object(id);
        emit images_list_changed();
    }
}

void DemoViewerViewModel::toggleObjectPicking(bool startOrStop)
{
    auto ids = m_viewer_view_model->get_object_ids_in_viewer();
    for (const auto& id : ids)
    {
        m_viewer_view_model->set_object_picking_enabled(id, startOrStop);
    }
}

void DemoViewerViewModel::toggleSlicePicking(bool startOrStop)
{
    m_viewer_view_model->set_slice_picking_enabled(startOrStop);
}

void DemoViewerViewModel::toggleImageVisibility(QString id, bool startOrStop)
{
    viewer::SliceViewportRenderingModeClass::SliceViewportRenderingMode slice_viewport_rendering_mode =
        viewer::SliceViewportRenderingModeClass::SliceViewportRenderingMode::MODEL;
    m_viewer_view_model->set_image_visibility(id.toStdString(),
                                              viewer::ViewportVisibility{
                                                  startOrStop, startOrStop, slice_viewport_rendering_mode
                                              });
}

void DemoViewerViewModel::testRandom(bool on)
{
    auto ids = m_viewer_view_model->get_primitive_spatial_object_ids_in_viewer();
    auto ids2 = m_viewer_view_model->get_qml_spatial_object_ids_in_viewer();
    auto ids3 = m_viewer_view_model->get_spatial_object_from_file_ids_in_viewer();
    ids.insert(ids.end(), ids2.begin(), ids2.end());
    ids.insert(ids.end(), ids3.begin(), ids3.end());

    std::string id_to_enable;
    if (ids.size() == 0)
    {
        return;
    }
    if (ids.size() == 1)
    {
        id_to_enable = ids[0];
    }
    else
    {
        std::random_device dev;
        std::mt19937 rng(dev());
        std::uniform_int_distribution<std::mt19937::result_type> dist(0, ids.size() - 1);
        auto random_integer = dist(rng);
        id_to_enable = ids[random_integer];
    }

    try {
        auto is_enabled = m_viewer_view_model->get_object_is_enabled(id_to_enable);
        m_viewer_view_model->set_object_is_enabled(id_to_enable, !on);
    }catch(std::exception& ex)
    {
        std::cerr << "Could not set enabled to " << std::boolalpha << on << std::noboolalpha << " for object with id " + id_to_enable + ". Exception: " << ex.what() << std::endl;
    }
}

void DemoViewerViewModel::setPrimitiveObjectType(int type)
{
    m_primitive_object_type = type;
}

void DemoViewerViewModel::setObjectColor(QString id, QColor color)
{
    auto material_settings = m_viewer_view_model->get_object_material(id.toStdString());
    m_spatial_objects_list->set_color(id, color);
    m_viewer_view_model->set_object_material(id.toStdString(), viewer::MaterialSettings{ false,viewer::MetalRoughMaterialProperties{color,material_settings.get_metal_rough_material_properties().get_metalness(),material_settings.get_metal_rough_material_properties().get_roughness(),material_settings.get_metal_rough_material_properties().get_normal(),material_settings.get_metal_rough_material_properties().get_ambient_occlusion(),material_settings.get_metal_rough_material_properties().get_texture_scale()},QColor{color.red(),color.green(),color.blue(),122} });
}

void DemoViewerViewModel::expandViewport(int viewport_id)
{
    m_viewer_view_model->expand_viewport(viewport_id);
}

void DemoViewerViewModel::on_mock_pose_received(QMatrix4x4 pose)
{
    auto p = viewer::PoseCalculator::get_transform_from(pose);
    auto ids = m_viewer_view_model->get_spatial_object_from_file_ids_in_viewer();
    if (ids.size() > 0)
    {
        m_viewer_view_model->set_object_pose(ids.back(), p.get_position(), p.get_x_axis(), p.get_z_axis());
        m_viewer_view_model->set_slices_pose(p.get_position(), p.get_x_axis(), p.get_z_axis());
    }
}

std::string DemoViewerViewModel::generate_object_id()
{
    auto id = "object_" + std::to_string(m_spatial_object_counter);
    m_spatial_object_counter++;
    return id;
}

int DemoViewerViewModel::get_primitive_object_type() const
{
    return m_primitive_object_type;
}
}
