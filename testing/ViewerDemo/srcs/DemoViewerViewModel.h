#pragma once

#include <QString>

#include "libCaseRepositoryService/ICaseLoaderService.h"

#include "libViewer/ViewerViewModel.h"

#include "MockBackendCommands.h"
#include "StringsListModel.h"
#include "StringColorListModel.h"

namespace demo_viewer {
class DemoViewerViewModel : public QObject {
    Q_OBJECT
public:
    Q_INVOKABLE void toggleTracking(bool on);
    Q_INVOKABLE void toggleSliceViewerBehaviour(int behaviour);
    Q_INVOKABLE void toggleThreedViewerBehaviour(int behaviour);
    Q_INVOKABLE void toggleIndicatorVisibility(bool on);
    Q_INVOKABLE void toggleViewerConfiguration(int configuration);
    Q_INVOKABLE void expandViewport(int viewport_id);
    Q_INVOKABLE void loadThreedModel(const QList<QUrl>& files);
    Q_INVOKABLE void loadImage(const QUrl& path);
    Q_INVOKABLE void addPrimitiveObject(const int& idx,const QVector3D& position);
    Q_INVOKABLE void toggleAnimate(bool startOrStop);
    Q_INVOKABLE void removeObject(QString id);
    Q_INVOKABLE void obectSelected(QString id, bool checked);
    Q_INVOKABLE void removeImage(QString id);
    Q_INVOKABLE void toggleObjectPicking(bool startOrStop);
    Q_INVOKABLE void toggleSlicePicking(bool startOrStop);
    Q_INVOKABLE void toggleImageVisibility(QString id, bool startOrStop);

    Q_INVOKABLE void testRandom(bool on);
    Q_INVOKABLE void setPrimitiveObjectType(int type);
    Q_INVOKABLE void setObjectColor(QString id, QColor color);


    Q_PROPERTY(StringsListModel* imagesListModel MEMBER m_images_list NOTIFY images_list_changed)
    Q_PROPERTY(StringColorListModel* spatialObjectsListModel MEMBER m_spatial_objects_list NOTIFY spatial_objects_list_changed)

    DemoViewerViewModel(std::shared_ptr<viewer::ViewerViewModel> viewer_view_model,std::shared_ptr<case_repo::ICaseLoaderService> case_loader_service);


signals:
    void images_list_changed();
    void spatial_objects_list_changed();

private:
    void on_mock_pose_received(QMatrix4x4 pose);
    std::string generate_object_id();
    int get_primitive_object_type() const;
    MockBackendCommands m_mock_backend_commands;
    std::shared_ptr<viewer::ViewerViewModel> m_viewer_view_model = nullptr;
    std::shared_ptr<case_repo::ICaseLoaderService> m_case_loader_service = nullptr;
    std::string m_animated_object_id = "";

    int m_primitive_object_type = 0;

    int m_image_counter = 0;
    int m_spatial_object_counter = 0;
    StringsListModel* m_images_list = nullptr;
    StringColorListModel* m_spatial_objects_list = nullptr;

};
}
