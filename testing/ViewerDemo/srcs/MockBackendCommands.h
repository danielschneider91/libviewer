#pragma once

#include <future>
#include <QObject>
#include <QMatrix4x4>

namespace demo_viewer {
class MockBackendCommands:public QObject {
    Q_OBJECT
public:
    MockBackendCommands();
    ~MockBackendCommands();

    void start_tracking_mock();
    void stop_tracking_mock();
    bool get_is_tracking() const;
signals:
    void pose_changed(QMatrix4x4 pose);
private:
    void tracking_loop();
    void setup_mock_poses();
    std::atomic_bool m_tracking_mock_running = false;
    std::atomic_bool m_tracking_mock_paused = true;
    std::future<void> m_tracking_mock_future;
    std::vector<QMatrix4x4> m_mock_poses;
    int m_number_of_mock_poses = 100;
};
}
