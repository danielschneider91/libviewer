#include "VolumeImageGradientCalculator.h"



#include <itkImageRegionIterator.h>
#include <itkNeighborhoodInnerProduct.h>
#include <itkSobelOperator.h>

namespace demo_viewer
{
VolumeImageGradientCalculator::VolumeImageGradientCalculator()
{
}


void VolumeImageGradientCalculator::set_image(itk::Image<short, 3>::Pointer image)
{
    using PixelType = signed short;
    using ImageType = itk::Image<PixelType, 3>;

    ImageType::RegionType region = image->GetLargestPossibleRegion();

    m_image = ImageType::New();

    m_image->SetRegions(region);
    m_image->Allocate();

    m_image = image;

    auto g = calculate_gradient(m_image);
    m_gradient.gradient_x = ImageType::New();
    m_gradient.gradient_y = ImageType::New();
    m_gradient.gradient_z = ImageType::New();
    m_gradient.gradient_x->SetRegions(g.gradient_x->GetRequestedRegion());
    m_gradient.gradient_y->SetRegions(g.gradient_y->GetRequestedRegion());
    m_gradient.gradient_z->SetRegions(g.gradient_z->GetRequestedRegion());
    m_gradient.gradient_x->Allocate();
    m_gradient.gradient_y->Allocate();
    m_gradient.gradient_z->Allocate();

    m_gradient.gradient_x = g.gradient_x;
    m_gradient.gradient_y = g.gradient_y;
    m_gradient.gradient_z = g.gradient_z;
}

VolumeImageGradientCalculator::Gradient VolumeImageGradientCalculator::get_gradient() const
{
    return m_gradient;
}

VolumeImageGradientCalculator::GradientAsVectors VolumeImageGradientCalculator::get_gradient_as_vectors() const
{
    GradientAsVectors gradient_as_vectors;
    using PixelType = signed short;
    using ImageType = itk::Image<PixelType, 3>;

    ImageType::RegionType region = m_image->GetLargestPossibleRegion();
    ImageType::SizeType size = region.GetSize();

    ImageType::SizeType::SizeValueType nx = size[0];
    auto ny = size[1];
    auto nz = size[2];
    gradient_as_vectors.gradient_x.resize(nx * ny * nz);
    gradient_as_vectors.gradient_y.resize(nx * ny * nz);
    gradient_as_vectors.gradient_z.resize(nx * ny * nz);

    int counter = 0;
    for (size_t z = 0; z < nz; z++) // for all depths
    {
        for (size_t y = 0; y < ny; y++) // for all Rows
        {
            for (size_t x = 0; x < nx; x++) // for all Columns
            {
                ImageType::IndexType idx;
                idx[0] = x;
                idx[1] = y;
                idx[2] = z;
                ImageType::IndexType idx_px = idx;
                idx_px[0] = x + 1;
                ImageType::IndexType idx_mx = idx;
                idx_mx[0] = x - 1;
                ImageType::IndexType idx_py = idx;
                idx_py[1] = y + 1;
                ImageType::IndexType idx_my = idx;
                idx_my[1] = y - 1;
                ImageType::IndexType idx_pz = idx;
                idx_pz[2] = z + 1;
                ImageType::IndexType idx_mz = idx;
                idx_mz[2] = z - 1;

                short dx, dy, dz;
                if (idx_mx[0] < 0 || idx_px[0] >= nx)
                    dx = 0;
                else
                    dx = m_gradient.gradient_x->GetPixel(idx_mx);// itk_image->GetPixel(idx_px) - itk_image->GetPixel(idx_mx);

                if (idx_my[1] < 0 || idx_py[1] >= ny)
                    dy = 0;
                else
                    dy = m_gradient.gradient_y->GetPixel(idx_my);// itk_image->GetPixel(idx_py) - itk_image->GetPixel(idx_my);

                if (idx_mz[2] < 0 || idx_pz[2] >= nz)
                    dz = 0;
                else
                    dz = m_gradient.gradient_z->GetPixel(idx_mz);// itk_image->GetPixel(idx_pz) - itk_image->GetPixel(idx_mz);

                gradient_as_vectors.gradient_x[counter] = dx;
                gradient_as_vectors.gradient_y[counter] = dy;
                gradient_as_vectors.gradient_z[counter] = dz;

                counter++;
            }
        }
    }

    return gradient_as_vectors;
}

std::vector<short> VolumeImageGradientCalculator::get_gradient_as_vector() const
{
    std::vector<short> gradient_xyz;
    using PixelType = signed short;
    using ImageType = itk::Image<PixelType, 3>;

    ImageType::RegionType region = m_image->GetLargestPossibleRegion();
    ImageType::SizeType size = region.GetSize();

    auto nx = size[0];
    auto ny = size[1];
    auto nz = size[2];

    for (int z = 0; z < nz; z++) // for all depths
    {
        for (int y = 0; y < ny; y++) // for all Rows
        {
            for (int x = 0; x < nx; x++) // for all Columns
            {
                ImageType::IndexType idx;
                idx[0] = x;
                idx[1] = y;
                idx[2] = z;
                ImageType::IndexType idx_px = idx;
                idx_px[0] = x + 1;
                ImageType::IndexType idx_mx = idx;
                idx_mx[0] = x - 1;
                ImageType::IndexType idx_py = idx;
                idx_py[1] = y + 1;
                ImageType::IndexType idx_my = idx;
                idx_my[1] = y - 1;
                ImageType::IndexType idx_pz = idx;
                idx_pz[2] = z + 1;
                ImageType::IndexType idx_mz = idx;
                idx_mz[2] = z - 1;

                short dx, dy, dz;
                if (idx_mx[0] < 0 || idx_px[0] >= nx)
                    dx = 0;
                else
                    dx = m_gradient.gradient_x->GetPixel(idx_mx);// itk_image->GetPixel(idx_px) - itk_image->GetPixel(idx_mx);

                if (idx_my[1] < 0 || idx_py[1] >= ny)
                    dy = 0;
                else
                    dy = m_gradient.gradient_y->GetPixel(idx_my);// itk_image->GetPixel(idx_py) - itk_image->GetPixel(idx_my);

                if (idx_mz[2] < 0 || idx_pz[2] >= nz)
                    dz = 0;  
                else
                    dz = m_gradient.gradient_z->GetPixel(idx_mz);// itk_image->GetPixel(idx_pz) - itk_image->GetPixel(idx_mz);

                gradient_xyz.push_back(-dx);
                gradient_xyz.push_back(-dy);
                gradient_xyz.push_back(-dz);

            }
        }
    }
    return gradient_xyz;
}

itk::SmartPointer<itk::Image<short, 3>> VolumeImageGradientCalculator::calculate_gradient_in_direction(const unsigned direction,
                                                                                                       itk::SmartPointer<itk::Image<short, 3>> image) const
{

    using PixelType = signed short;
    using ImageType = itk::Image<PixelType, 3>;

    using SobelOperatorType = itk::SobelOperator<signed short, 3>;
    SobelOperatorType sobelOperator;
    sobelOperator.SetDirection(direction); // Create the operator for the a certain axis derivative
    sobelOperator.CreateDirectional();

    std::cout << sobelOperator << std::endl;

    for (unsigned int i = 0; i < 27; i++)
    {
        std::cout << sobelOperator.GetElement(i) << std::endl;
    }

    using NeighborhoodIteratorType = itk::ConstNeighborhoodIterator<ImageType>;
    NeighborhoodIteratorType::RadiusType radius = sobelOperator.GetRadius();
    NeighborhoodIteratorType it(radius, image, image->GetRequestedRegion());

    itk::NeighborhoodInnerProduct<ImageType> innerProduct;

    VolumeImageGradientCalculator::Gradient g;
    itk::Image<signed short, 3>::Pointer gradient = ImageType::New();
    gradient->SetRegions(image->GetRequestedRegion());
    gradient->Allocate();

    using IteratorType = itk::ImageRegionIterator<ImageType>;
    IteratorType out(gradient, image->GetRequestedRegion());
    for (it.GoToBegin(), out.GoToBegin(); !it.IsAtEnd(); ++it, ++out)
    {
        out.Set(innerProduct(it, sobelOperator));
    }

    return gradient;
}

VolumeImageGradientCalculator::Gradient VolumeImageGradientCalculator::calculate_gradient(itk::SmartPointer<itk::Image<short, 3>> image) const
{
    using PixelType = signed short;
    using ImageType = itk::Image<PixelType, 3>;

    VolumeImageGradientCalculator::Gradient g;
    g.gradient_x = ImageType::New();
    g.gradient_y = ImageType::New();
    g.gradient_z = ImageType::New();
    g.gradient_x->SetRegions(image->GetRequestedRegion());
    g.gradient_y->SetRegions(image->GetRequestedRegion());
    g.gradient_z->SetRegions(image->GetRequestedRegion());
    g.gradient_x->Allocate();
    g.gradient_y->Allocate();
    g.gradient_z->Allocate();

    g.gradient_x = calculate_gradient_in_direction(0, image);
    g.gradient_y = calculate_gradient_in_direction(1, image);
    g.gradient_z = calculate_gradient_in_direction(2, image);

    return g;
}

}
