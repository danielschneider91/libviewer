#include "StringColorListModel.h"

#include <iostream>

namespace demo_viewer
{
StringColorListModel::StringColorListModel(QObject* parent) : QAbstractListModel(parent)
{
}

StringColorListModel::~StringColorListModel()
{
}

QHash<int, QByteArray> StringColorListModel::roleNames() const {
    QHash<int, QByteArray> roles;
    roles[Id] = "id";
    roles[HumanReadableName] = "humanReadableName";
    roles[Color] = "color";
    return roles;
}

QVariant StringColorListModel::data(const QModelIndex& index, int role) const
{
    QVariant result = QVariant();
    int row = index.row();
    int column = index.column();

    if (!index.isValid() || row >= rowCount())
    {
        std::cout << "index not valid or row larger than rowCount" << std::endl;
        return result;
    }

    switch (role)
    {


    case Id:
    {
        return m_data_id.at(row);
    }
    case HumanReadableName:
    {
        return m_data_human_readable_name.at(row);
    }
    case Color:
    {
        QColor col = m_data_color.at(row);
        return col;
    }

    default: break;
    }


    return result;
}

int StringColorListModel::rowCount(const QModelIndex& parent) const
{
    if (parent.isValid())
    {
        std::cout << "parent is valid" << std::endl;
        return 0;
    }
    return m_data_id.size();
}

Qt::ItemFlags StringColorListModel::flags(const QModelIndex& index) const
{
    Qt::ItemFlags result = Qt::ItemIsEditable | QAbstractItemModel::flags(index);
    return result;
}

void StringColorListModel::add_object(const QString& id, const QString& human_readable_name, const QColor& color)
{
    m_id_index_map[id] = m_data_id.size();

    beginInsertRows(QModelIndex(), m_data_id.size(), m_data_id.size()); // Specify the first and last row numbers for the span of rows you want to insert into an item in a model.
    m_data_id.push_back(id);
    m_data_human_readable_name.push_back(human_readable_name);
    m_data_color.push_back(color);
    endInsertRows();

    assert(m_data_id.size() == m_data_human_readable_name.size());
    assert(m_data_id.size() == m_data_color.size());
}

void StringColorListModel::remove_object(const QString& id)
{
    auto index = m_id_index_map.at(id);
    beginRemoveRows(QModelIndex(), index, index); // Specify the first and last row numbers for the span of rows you want to remove from an item in a model.
    m_data_id.takeAt(index);
    m_data_human_readable_name.takeAt(index);
    m_data_color.takeAt(index);
    endRemoveRows();

    // rebuild id_index_map
    m_id_index_map.clear();
    for (int i = 0; i < m_data_id.count(); ++i)
    {
        m_id_index_map[m_data_id[i]] = i;
    }

    assert(m_data_id.size() == m_data_human_readable_name.size());
    assert(m_data_id.size() == m_data_color.size());
}

void StringColorListModel::set_color(const QString& id,const QColor& color)
{
    auto index = m_id_index_map.at(id);
    m_data_color[index] = color;
    QModelIndex top = createIndex(index, 0);
    emit dataChanged(top, top);
}
}
