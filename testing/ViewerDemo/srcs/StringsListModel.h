#pragma once
#include <QAbstractListModel>

namespace demo_viewer
{
class StringsListModel : public QAbstractListModel
{
    Q_OBJECT
public:
    enum ImagesRoles {
        Id,
        HumanReadableName
    };
    StringsListModel(QObject* parent=nullptr);
    ~StringsListModel();

    QVariant data(const QModelIndex& index, int role = Qt::DisplayRole) const override;
    int rowCount(const QModelIndex& parent = QModelIndex()) const override;

    Qt::ItemFlags flags(const QModelIndex& index) const override;

    void add_object(const QString& id,const QString& human_readable_name);
    void remove_object(const QString& id);

    virtual QHash<int, QByteArray> roleNames() const override;

private:
    std::map<QString, int> m_id_index_map;
    QList<QString> m_data_id;
    QList<QString> m_data_human_readable_name;
};
}
