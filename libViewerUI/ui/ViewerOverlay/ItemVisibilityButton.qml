import QtQuick 2.14
import QtQuick.Controls 2.14

import libViewer

Item {
    id: root
    width: 500
    height: 500
    property var visibilityModel
    signal visibilityChanged(real index)
    property real buttonSize: root.height/7.0
    property color visibleColor: "blue"
    property color transparentColor: "lightblue"
    property color hiddenColor: "grey"

    ListView {
        id: visibilityList
        model: root.visibilityModel
        anchors.bottom: visibilityButton.top
        anchors.right: parent.right
        width: parent.width
        height: parent.height
        verticalLayoutDirection: ListView.BottomToTop
        visible: false
        delegate: Component {
            Rectangle {
                color: "transparent"
                width: parent.width
                height: root.buttonSize
                Row {
                    height: parent.height
                    Rectangle {

                        anchors.verticalCenter: parent.verticalCenter
                        width: parent.height
                        height: parent.height
                        color: "transparent"

                        Button {
                            anchors.verticalCenter: parent.verticalCenter
                            width: parent.height
                            height: parent.height
                            icon.source: {
                                if(!root.visibilityModel) {return ""}
                                switch(root.visibilityModel[index].visibility.visibility) {
                                    case(VisibilityClass.HIDDEN): return "qrc:/libViewerUI/ui/resources/icons/invisible.svg";
                                    case(VisibilityClass.VISIBLE): return "qrc:/libViewerUI/ui/resources/icons/eye.svg";
                                    case(VisibilityClass.TRANSPARENNT): return "qrc:/libViewerUI/ui/resources/icons/eye.svg";
                                }
                            }
                            icon.width: parent.height
                            icon.height: parent.height
                            icon.color: {
                                if(!root.visibilityModel) {return "gray"}

                                switch(root.visibilityModel[index].visibility.visibility) {
                                    case(VisibilityClass.HIDDEN): return  root.hiddenColor;
                                    case(VisibilityClass.VISIBLE): return root.visibleColor;
                                    case(VisibilityClass.TRANSPARENNT): root.transparentColor;
                                }
                            }
                            flat: true
                            highlighted: false
                        }
                    }
                    Label {
                        anchors.verticalCenter: parent.verticalCenter
                        text: root.visibilityModel? '   ' + root.visibilityModel[index].name : ''
                    }
                }
                MouseArea {
                    hoverEnabled:true
                    anchors.fill: parent
                    onClicked: {
                        visibilityList.currentIndex = index
                        root.visibilityChanged(visibilityList.currentIndex);
                    }
                    onEntered: {
                        visibilityList.currentIndex = index;
                    }
                }
            }
        }
        highlight: Rectangle {
            color:Qt.rgba(0.5,0.5,0.5,0.5)
        }
        focus: true
        //onCurrentItemChanged: console.log(root.visibilityModel[visibilityList.currentIndex].name + ' selected')
    }
    Button {
        id: visibilityButton
        anchors.right: parent.right
        anchors.bottom: parent.bottom
        anchors.bottomMargin: 5
        anchors.rightMargin: 5
        height: parent.height/14.0
        width: parent.height/14.0
        icon.source: "qrc:/libViewerUI/ui/resources/icons/eye.svg"
        icon.width: parent.height/14.0
        icon.height: parent.height/14.0
        visible: true
        checkable: true
        onClicked: {
            visibilityList.visible=checked
        }
    }
}
