import QtQuick
import QtQuick.Controls

Item {
    id: root
    property real buttonSize: 30
    signal volRenderingEnabled(real checked)
    signal volRenderingWindowingEnabled(real checked)
    signal volRenderingTypeChanged(real type)
    signal volRenderingTransferFunctionTypeChanged(real type)
    property var iconSources: ["qrc:/libViewerUI/ui/resources/icons/volume_rendering.svg","qrc:/libViewerUI/ui/resources/icons/settings.svg","qrc:/libViewerUI/ui/resources/icons/brightness.svg"]
    property var tfTypeSources: ["qrc:/libViewerUI/ui/resources/icons/ramp.svg","qrc:/libViewerUI/ui/resources/icons/rampUpDown.svg"]

    Column {
        Button {
            id:enablingButton
            checkable: true
            height: root.buttonSize
            width: root.buttonSize
            icon.source: root.iconSources[0]
            icon.width: root.buttonSize
            icon.height: root.buttonSize
            onCheckedChanged: {
                volRenderingEnabled(checked)
                if(!checked) {
                    windowingButton.checked = false;
                    settingsButton.checked = false;
                }
            }
        }

        Row {
            Button {
                id: settingsButton
                icon.source: root.iconSources[1]
                icon.width: root.buttonSize
                icon.height: root.buttonSize
                enabled: enablingButton.checked
                visible: enablingButton.checked
                checkable: true
                height: root.buttonSize
                width: root.buttonSize
                onCheckedChanged: {
                    if(!checked) {
                        windowingButton.checked = false;
                    }
                }
            }
            Button {
                id: windowingButton
                enabled: settingsButton.checked
                visible: settingsButton.checked
                icon.source: root.iconSources[2]
                icon.width: root.buttonSize
                icon.height: root.buttonSize
                checkable: true
                height: root.buttonSize
                width: root.buttonSize
                onCheckedChanged: {
                    volRenderingWindowingEnabled(checked)
                }
            }
        }
        Row {
            Rectangle {
                enabled: settingsButton.checked
                visible: settingsButton.checked
                color: "transparent"
                height: root.buttonSize
                width: root.buttonSize
            }
            Button {
                property real tfType: 0;//0: normal, 1: max ip, 2: min ip
                id: transferFunctionType
                enabled: settingsButton.checked
                visible: settingsButton.checked
                height: root.buttonSize
                width: root.buttonSize
                onClicked: {
                    tfType = tfType+1;
                    if(tfType>1) {
                        tfType = 0;
                    }
                    volRenderingTransferFunctionTypeChanged(tfType);
                }
                icon.source: {
                    switch(tfType) {
                    case(0): return root.tfTypeSources[0]
                    case(1): return root.tfTypeSources[1]
                    }
                }
                icon.width: root.buttonSize
                icon.height: root.buttonSize

            }
            Button {
                property real type: 0;//0: normal, 1: max ip, 2: min ip
                id: volumeRenderingType
                enabled: false;//settingsButton.checked
                visible: false;//settingsButton.checked
                height: root.buttonSize
                width: root.buttonSize
                onClicked: {
                    type = type+1;
                    if(type>2) {
                        type = 0;
                    }
                    volRenderingTypeChanged(type);
                }
                text: {
                    switch(type){
                    case 0: return "Std";
                    case 1: return "Max";
                    case 2: return "Min";
                    case 3: return "U";
                    }
                }
            }
        }
    }


}
