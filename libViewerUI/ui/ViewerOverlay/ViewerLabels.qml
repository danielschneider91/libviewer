import QtQuick
import QtQuick.Controls

Item {
    id: root
    property int numberOfViewports
    property Repeater viewportsRepeater
    property variant viewportToViewportName: { 0: 'Axial', 1: 'Coronal',2:'Sagittal',3:'' }
    property int threeDViewportId: 9

    Repeater {
        id: labelRepeater

        model: root.numberOfViewports
        Rectangle {
            height: root.viewportsRepeater.count>0 ? root.viewportsRepeater.itemAt(index).data.height*root.height : 20
            width: root.viewportsRepeater.count>0 ? root.viewportsRepeater.itemAt(index).data.width*root.width : 20
            x: root.viewportsRepeater.count>0 ? root.viewportsRepeater.itemAt(index).data.x*root.width : 20
            y: root.viewportsRepeater.count>0 ? root.viewportsRepeater.itemAt(index).data.y*root.height : 20

            Label {
                anchors.right: parent.right
                anchors.bottom: parent.bottom
                anchors.rightMargin: 5
                anchors.bottomMargin: 5
                text: index===root.threeDViewportId ? '' : root.viewportToViewportName[index]
            }
            color: "transparent"
        }
    }

}
