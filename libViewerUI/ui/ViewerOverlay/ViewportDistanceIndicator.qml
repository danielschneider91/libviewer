import QtQuick
import QtQuick.Controls
import QtQuick.Shapes 1.15

Item {
    id: root
    property Repeater viewportsRepeater
    property int numberOfViewports
    property variant viewportToViewportName: { 0: 'Axial', 1: 'Coronal',2:'Sagittal',3:'' }
    property int threeDViewportId: 9
    property var cameraViewportWidths: [100 ,90,80,70]// in mm
    property variant viewportToSlicetypeMap: { 0: 'Axial', 1: 'Coronal',2:'Sagittal',3:'3D',4: 'Axial', 5: 'Coronal' }
    readonly property real percentageViewportRightOffset: 0.02
    readonly property real percentageViewportWidth: 0.35
    readonly property real yCoordPercentage:0.04// from the top
    readonly property real yExtentPercentage: 0.02

    function getRoundedCameraViewportWidth(actualViewportWidth) {
        var fractionViewportWidth = actualViewportWidth*root.percentageViewportWidth;
        var roundedViewportWidth = Math.floor(fractionViewportWidth/10)*10;
        return roundedViewportWidth;
    }
    function getPercentageOfViewportWidth(actualViewportWidth,width) {
        return width/actualViewportWidth;
    }

    Repeater {
        id: distanceIndicatorRepeater

        model:root.numberOfViewports
        Rectangle {
            id:rect
            visible: {
                var isVpEnabled =root.viewportsRepeater.count>0 ? root.viewportsRepeater.itemAt(index).isEnabled:false
                return root.viewportToSlicetypeMap[index]!=="3D" && isVpEnabled;
            }
            enabled: visible
            height: root.viewportsRepeater.count>0 ? root.viewportsRepeater.itemAt(index).data.height*root.height : 20
            width: root.viewportsRepeater.count>0 ? root.viewportsRepeater.itemAt(index).data.width*root.width : 20
            x: root.viewportsRepeater.count>0 ? root.viewportsRepeater.itemAt(index).data.x*root.width : 20
            y: root.viewportsRepeater.count>0 ? root.viewportsRepeater.itemAt(index).data.y*root.height : 20
            color: "transparent"
            property bool ready: {
                var isNotANumber = isNaN(root.cameraViewportWidths[index])
                var isZero = root.cameraViewportWidths[index]==0;
                var haveSameNumberOfValues = root.numberOfViewports==root.cameraViewportWidths.length
                var isAFiniteNumber = isFinite(root.cameraViewportWidths[index])
                var isReady = (!isNotANumber) && haveSameNumberOfValues && isAFiniteNumber && !isZero
                return isReady;
            }

            Shape {
                id: shape
                anchors.fill: parent
                ShapePath {
                    id:shapePath
                    strokeColor: "white"
                    strokeStyle: ShapePath.SolidLine
                    strokeWidth: 1
                    fillColor: "transparent"
                    startX: {if(rect.ready) {
                        var w = getRoundedCameraViewportWidth(root.cameraViewportWidths[index]);
                        var p= getPercentageOfViewportWidth(root.cameraViewportWidths[index],w);
                        if(isNaN(p)) {return 0}
                        var shift = (1-p)/2.0-root.percentageViewportRightOffset;
                        return shape.width*(1-p)/2.0+shift*shape.width;
                    } else {return 0}}
                    startY: {if(rect.ready) {
                        return shape.height*(root.yCoordPercentage+root.yExtentPercentage)
                    } else {return 0}}

                    PathLine {
                        x: {if(rect.ready) {
                            var w = getRoundedCameraViewportWidth(root.cameraViewportWidths[index]);
                            var p= getPercentageOfViewportWidth(root.cameraViewportWidths[index],w);
                            if(isNaN(p)) {return 0}
                            var shift = (1-p)/2.0-root.percentageViewportRightOffset;
                            return shape.width*(1-p)/2.0+shift*shape.width;
                        } else {return 0}}
                        y: {if(rect.ready) {
                            return shape.height*root.yCoordPercentage
                        } else {return 0}}
                    }

                    PathLine {
                        x: {if(rect.ready) {
                            var w = getRoundedCameraViewportWidth(root.cameraViewportWidths[index]);
                            var p= getPercentageOfViewportWidth(root.cameraViewportWidths[index],w);
                            if(isNaN(p)) {return 0}
                            var shift = (1-p)/2.0-root.percentageViewportRightOffset;
                            return shape.width-shape.width*(1-p)/2.0+shift*shape.width;
                        } else {return 0}}
                        y: {if(rect.ready) {
                            return shape.height*root.yCoordPercentage
                        } else {return 0}}
                    }
                    PathLine {
                        x: {if(rect.ready) {
                            var w = getRoundedCameraViewportWidth(root.cameraViewportWidths[index]);
                            var p= getPercentageOfViewportWidth(root.cameraViewportWidths[index],w);
                            if(isNaN(p)) {return 0}
                            var shift = (1-p)/2.0-root.percentageViewportRightOffset;
                            return shape.width-shape.width*(1-p)/2.0+shift*shape.width;
                        } else {return 0}}
                        y: {if(rect.ready) {
                            return shape.height*(root.yCoordPercentage+root.yExtentPercentage)
                        } else {return 0}}
                    }
                }
            }
            Rectangle {
                anchors.top: parent.top
                anchors.topMargin: 5
                height: root.viewportsRepeater.count>0 ? root.viewportsRepeater.itemAt(index).data.height*root.yCoordPercentage*root.height:20
                x: {if(rect.ready) {
                    var w = getRoundedCameraViewportWidth(root.cameraViewportWidths[index]);
                    if(isNaN(p)) {return 0}
                    var p= getPercentageOfViewportWidth(root.cameraViewportWidths[index],w);
                    var shift = (1-p)/2.0-root.percentageViewportRightOffset;
                    return shape.width*(1-p)/2.0+shift*shape.width;
                } else {return 0}}
                width: {if(rect.ready) {
                    var w = getRoundedCameraViewportWidth(root.cameraViewportWidths[index]);
                    var p= getPercentageOfViewportWidth(root.cameraViewportWidths[index],w);
                    if(isNaN(p)) {return 0}
                    var shift = (1-p)/2.0-root.percentageViewportRightOffset;
                    var xEnd= shape.width-shape.width*(1-p)/2.0+shift*shape.width;
                    return Math.abs(xEnd-x)
                } else {return 0}}

                color: "transparent"
                Label {
                    anchors.verticalCenter: parent.verticalCenter
                    anchors.horizontalCenter: parent.horizontalCenter

                    horizontalAlignment: Text.AlignHCenter
                    verticalAlignment: Text.AlignBottom
                    text: {
                        if(rect.ready) {
                            var w = getRoundedCameraViewportWidth(root.cameraViewportWidths[index]);
                            return w+ " mm"
                        } else {
                            return "NA";
                        }
                    }
                }
            }
        }
    }
}
