import QtQuick
import QtQuick.Controls

Rectangle {
    id:root

    height: parent.height
    width: parent.width
    x: parent.x
    y: parent.y
    color: "transparent"

    property real buttonSize: root.height/8.0
    property real viewerIndex:0

    property int expandedViewport:-1
    property alias distanceMeasurement: measurementButtons.distance

    property bool zoomInEnabled: true
    property bool zoomOutEnabled: true
    property bool moveUpEnabled: true
    property bool moveDownEnabled: true
    property bool windowWidthLevelEnabled: true
    property bool restartEnabled: true
    property bool fullscreenEnabled: true
    property bool volumeRenderingEnabled: true
    property bool rulerEnabled: true

    property var iconSources: ["qrc:/libViewerUI/ui/resources/icons/zoom_in.svg","qrc:/libViewerUI/ui/resources/icons/zoom_out.svg","qrc:/libViewerUI/ui/resources/icons/moveUp.svg","qrc:/libViewerUI/ui/resources/icons/moveDown.svg",
        "qrc:/libViewerUI/ui/resources/icons/brightness.svg","qrc:/libViewerUI/ui/resources/icons/restart.svg","qrc:/libViewerUI/ui/resources/icons/fullscreen.svg"]
    property var iconNormalScreenSize: "qrc:/libViewerUI/ui/resources/icons/normalscreen.svg"
    property var buttonEnabled: [root.zoomInEnabled,root.zoomOutEnabled,root.moveUpEnabled,root.moveDownEnabled,
        root.windowWidthLevelEnabled,root.restartEnabled,root.fullscreenEnabled]
    property var buttonCheckable: [0,0,0,0,1,0,1,1]

    onExpandedViewportChanged: {
        if(root.viewerIndex==root.expandedViewport) {
            viewerButtons.itemAt(iconSources.length-1).checked=true
        }
        else {
            viewerButtons.itemAt(iconSources.length-1).checked=false
        }
    }

    // signals
    signal fullscreenButtonClicked(real viewerIndex)
    signal nextCaudalSliceRequested(real viewerIndex)
    signal nextCranialSliceRequested(real viewerIndex)
    signal resetViewerRequested(real viewerIndex)
    signal adjustingWindowLevelAndWidthChanged(real viewerIndex, bool adjusting)
    signal zoomChanged(real viewerIndex, real inOrOut)
    signal volRenderingEnabledChanged(real viewerIndex, real checked)
    signal volRenderingLevelingEnabled(real viewerIndex, real checked)
    signal volRenderingTypeChanged(real viewerIndex, real type)
    signal volRenderingTransferFunctionTypeChanged(real viewerIndex, real type)
    signal rulerEnbldChanged(real viewerIndex, bool enabled)
    signal rulerReset()

    Component.onCompleted: {
        measurementButtons.measurementEnabled.connect(connectMeasEnabled)
        measurementButtons.resetMeasurement.connect(connectMeasReset)

        volumeRenderingButtons.volRenderingEnabled.connect(connectVolRenderingEnabled)
        volumeRenderingButtons.volRenderingWindowingEnabled.connect(connectVolRenderingWindowingEnabled)
        volumeRenderingButtons.volRenderingTypeChanged.connect(connectVolRenderingTypeChanged)
        volumeRenderingButtons.volRenderingTransferFunctionTypeChanged.connect(connectVolRenderingTransferFunctionTypeChanged)


    }
    function connectMeasEnabled(checked) {
        root.rulerEnbldChanged(root.viewerIndex,checked);
    }
    function connectMeasReset() {
        root.rulerReset();
    }
    function connectVolRenderingEnabled(checked) {
        root.volRenderingEnabledChanged(root.viewerIndex,checked);
    }
    function connectVolRenderingWindowingEnabled(checked) {
        root.volRenderingLevelingEnabled(root.viewerIndex,checked);
    }
    function connectVolRenderingTypeChanged(type) {
        root.volRenderingTypeChanged(root.viewerIndex,type);
    }
    function connectVolRenderingTransferFunctionTypeChanged(type) {
        root.volRenderingTransferFunctionTypeChanged(root.viewerIndex,type);
    }

    Column {
        id: columnRoot
        anchors.top: parent.top
        anchors.left: parent.left
        anchors.leftMargin: 5
        anchors.topMargin: 5


        Repeater {
            id:viewerButtons
            model: root.buttonEnabled.length
            Button {
                anchors.left: parent.left
                height: root.buttonSize
                width: root.buttonSize
                icon.source: {
                    if(index===iconSources.length-2) {
                        if(checked) {
                            return root.iconNormalScreenSize;
                        } else {
                            return root.iconSources[index]
                        }
                    } else {
                        return root.iconSources[index];
                    }
                }
                icon.width: root.buttonSize
                icon.height: root.buttonSize
                autoRepeat: true
                visible: root.buttonEnabled[index]
                checkable: root.buttonCheckable[index];
                onClicked: {
                    switch(index) {
                    case 0: root.zoomChanged(root.viewerIndex, 1.0); break;
                    case 1: root.zoomChanged(root.viewerIndex, -1.0); break;
                    case 2: root.nextCranialSliceRequested(root.viewerIndex); break;
                    case 3: root.nextCaudalSliceRequested(root.viewerIndex); break;
                    case 4: root.adjustingWindowLevelAndWidthChanged(root.viewerIndex,checked); break;
                    case 5: root.resetViewerRequested(root.viewerIndex); break;
                    case 6: {
                        if(checked) {
                            root.fullscreenButtonClicked(root.viewerIndex)
                        } else {
                            root.fullscreenButtonClicked(-1);
                        }
                    }
                    //case 7: root.rulerEnbldChanged(root.viewerIndex,checked); break;
                    //case 7: root.volRenderingEnabledChanged(root.viewerIndex,checked); break;
                    //case 8: root.volRenderingLevelingEnabled(root.viewerIndex,checked); break;
                    }
                }
            }
        }
        MeasurementButtons {
            id:measurementButtons
            enabled: root.rulerEnabled
            visible: root.rulerEnabled
            anchors.left: parent.left
            buttonSize:root.buttonSize
            height: root.buttonSize
            width: root.buttonSize*2
        }

        VolumeRenderingButtons {
            id:volumeRenderingButtons
            visible: root.volumeRenderingEnabled
            anchors.left: parent.left
            buttonSize:root.buttonSize
            height: root.buttonSize*3
            width: root.buttonSize*2
        }

    }
}
