import QtQuick
import QtQuick.Controls

Item {
    id: root
    property real buttonSize: 30
    property real distance: 0
    signal measurementEnabled(real checked)
    signal resetMeasurement()
    property var iconSources: ["qrc:/libViewerUI/ui/resources/icons/ruler.svg","qrc:/libViewerUI/ui/resources/icons/settings.svg"]

    Column {
        Row {
            Button {
                id:enablingButton
                checkable: true
                height: root.buttonSize
                width: root.buttonSize
                icon.source: root.iconSources[0]
                icon.width: root.buttonSize
                icon.height: root.buttonSize
                onCheckedChanged: {
                    root.measurementEnabled(checked)
                }
            }
            Column {
                spacing: 0
                Label {
                    id: distanceLabel
                    text: root.distance.toFixed(1)+" mm"
                    enabled: enablingButton.checked
                    visible: enablingButton.checked
                    height: root.buttonSize/2.0
                    width: root.buttonSize
                    verticalAlignment: Text.AlignVCenter
                    horizontalAlignment: Text.AlignHCenter
                }
                Button {
                    id: resetButton
                    text: "Reset"
                    //icon.source: root.iconSources[1]
                    //icon.width: root.buttonSize
                    //icon.height: root.buttonSize
                    enabled: enablingButton.checked
                    visible: enablingButton.checked
                    checkable: false
                    height: root.buttonSize/2.0
                    width: root.buttonSize
                    onClicked: {
                        root.resetMeasurement()
                    }
                }
            }
        }
    }
}
