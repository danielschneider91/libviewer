import QtQuick
import QtQuick.Controls

Item {
    id: root
    property int numberOfViewports: 4
    property Repeater viewportsRepeater
    property variant viewportToSlicetypeMap: { 0: 'Axial', 1: 'Coronal',2:'Sagittal',3:'3D' }
    readonly property Repeater frameRepeater: frameRepeater
    property var frameColors: ["#5a5a5a","#5a5a5a","#5a5a5a","#5a5a5a"];

    Repeater {
        id: frameRepeater
        model: root.numberOfViewports
        Rectangle {
            height: root.viewportsRepeater.count>0 ? root.viewportsRepeater.itemAt(index).data.height*root.height : 20
            width: root.viewportsRepeater.count>0 ? root.viewportsRepeater.itemAt(index).data.width*root.width : 20
            x: root.viewportsRepeater.count>0 ? root.viewportsRepeater.itemAt(index).data.x*root.width : 20
            y: root.viewportsRepeater.count>0 ? root.viewportsRepeater.itemAt(index).data.y*root.height : 20

            border.color: {
                var sliceType = root.viewportToSlicetypeMap[index]
                if(sliceType==='Axial') {
                    return root.frameColors[0]
                } else if(sliceType==='Coronal') {
                    return root.frameColors[1]
                } else if(sliceType==='Sagittal') {
                    return root.frameColors[2]
                } else if(sliceType==='3D') {
                    return root.frameColors[3]
                } else {
                    console.assert(false,'This cannot happen!')
                }
            }
            border.width: 2
            color: "transparent"
        }
    }

}
