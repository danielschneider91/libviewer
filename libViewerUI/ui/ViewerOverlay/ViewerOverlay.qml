import QtQuick
import QtQuick.Controls

import libViewer

Item {
    id: root
    anchors.fill: parent

    property int numberOfViewports: 4
    property int threeDViewportId: 9
    property int expandedViewport:-1
    property Repeater viewportsRepeater
    property variant viewportToSlicetypeMap: { 0: 'Axial', 1: 'Coronal',2:'Sagittal',3:'3D',4: 'Axial', 5: 'Coronal' }
    property variant viewportToViewportName: { 0: 'Axial', 1: 'Coronal',2:'Sagittal',3:'3D' }
    property real windowWidth: 1000
    property real windowLevel: 500
    property real volumeWindowWidth: 1000
    property real volumeWindowLevel: 500
    property real distanceMeasurement: 0
    property alias cameraViewportWidths: viewportDistanceIndicator.cameraViewportWidths
    property alias visibleColor: visibilityButton.visibleColor
    property alias transparentColor: visibilityButton.transparentColor
    property alias hiddenColor: visibilityButton.hiddenColor


    property alias frameRepeater: viewerFrames.frameRepeater
    property alias viewerFrameColors: viewerFrames.frameColors
    property alias viewerButtonsRepeater: viewerButtonsRepeater

    property var visibilityModel

    Component.onCompleted: {
        visibilityButton.visibilityChanged.connect(onVisibilityChanged)
    }

    function onVisibilityChanged(index) {// dont know why this works, visibilityModel is defined outside, but anyways leaving it. check here if it doesn work anymore.
        if(!root.visibilityModel) { return; }

        var vis = root.visibilityModel[index].visibility.visibility;
        switch(vis){
        case(VisibilityClass.VISIBLE): {
            root.visibilityModel[index].visibility.visibility = VisibilityClass.TRANSPARENNT;
            break;
        }
        case(VisibilityClass.TRANSPARENNT): {
            visibilityModel[index].visibility.visibility = VisibilityClass.HIDDEN;
            break;
        }
        case(VisibilityClass.HIDDEN): {
            visibilityModel[index].visibility.visibility = VisibilityClass.VISIBLE;
            break;
        }
        default:
            visibilityModel[index].visibility.visibility = VisibilityClass.VISIBLE;
        }
    }

    ViewerFrames {
        id: viewerFrames
        height: root.height
        width: root.width
        numberOfViewports: root.numberOfViewports
        viewportsRepeater: root.viewportsRepeater
        viewportToSlicetypeMap: root.viewportToSlicetypeMap
    }

    ViewportDistanceIndicator {
        id:viewportDistanceIndicator
        height: root.height
        width: root.width
        numberOfViewports: root.numberOfViewports
        viewportsRepeater: root.viewportsRepeater
        viewportToSlicetypeMap:root.viewportToSlicetypeMap
    }

    ViewerLabels {
        height: root.height
        width: root.width
        numberOfViewports: root.numberOfViewports
        viewportsRepeater: root.viewportsRepeater
        viewportToViewportName: root.viewportToViewportName
        threeDViewportId: root.threeDViewportId
    }

    Repeater {
        id: viewerButtonsRepeater
        //property var clrs: ["red","green","blue","white"]
        model:root.numberOfViewports
        SliceViewerButtonList {
            id:sliceViewerButtonListRect
            enabled: root.viewportsRepeater.count>0 ? (root.viewportsRepeater.itemAt(index).state===1 || root.viewportsRepeater.itemAt(index).state===2) : true
            height: root.viewportsRepeater.count>0 ? root.viewportsRepeater.itemAt(index).data.height*root.height : 20
            width: root.viewportsRepeater.count>0 ? root.viewportsRepeater.itemAt(index).data.width*root.width: 20
            x: root.viewportsRepeater.count>0 ? root.viewportsRepeater.itemAt(index).data.x*root.width : 20
            y: root.viewportsRepeater.count>0 ? root.viewportsRepeater.itemAt(index).data.y*root.height : 20
            buttonSize: Math.min(height,width)/8.0
            color: "transparent"

            viewerIndex: index
            expandedViewport: root.expandedViewport
            distanceMeasurement: index===root.threeDViewportId ? root.distanceMeasurement : 0

            zoomInEnabled: (index===0 || index===root.threeDViewportId)
            zoomOutEnabled: (index===0 || index===root.threeDViewportId)
            moveUpEnabled: (index!=root.threeDViewportId)
            moveDownEnabled: (index!=root.threeDViewportId)
            windowWidthLevelEnabled: (index===0)
            restartEnabled: (index===0 || index===root.threeDViewportId)
            fullscreenEnabled: true
            volumeRenderingEnabled: index===root.threeDViewportId
            rulerEnabled: index===root.threeDViewportId
        }
    }

    Label {
        id: windowLevelWidthLabel
        text: {
            var str1 = "Slice window level: "+root.windowLevel+", Slice window width: "+root.windowWidth+"\n";
            var str2 = "Volume window level: "+root.volumeWindowLevel+", Volume window width: "+root.volumeWindowWidth
            return str1.concat(str2)
        }
        anchors.left: parent.left
        anchors.bottom: parent.bottom
        anchors.margins: 5
        font.pixelSize: Math.max(parent.height/100,15)
        wrapMode: Label.Wrap
    }

    ItemVisibilityButton {
        id:visibilityButton
        visibilityModel: root.visibilityModel ? root.visibilityModel:null
        enabled: root.viewportsRepeater.count>0 ? (root.viewportsRepeater.itemAt(threeDViewportId).state===1 || root.viewportsRepeater.itemAt(threeDViewportId).state===2) : true
        visible: root.viewportsRepeater.count>0 ? (root.viewportsRepeater.itemAt(threeDViewportId).state===1 || root.viewportsRepeater.itemAt(threeDViewportId).state===2) : true
        width: parent.width/5.0
        height: parent.height
        anchors.bottom: parent.bottom
        anchors.right: parent.right
        //buttonSize: root.viewportsRepeater.count>0 ? root.viewportsRepeater.itemAt(threeDViewportId).data.height*root.height/8.0 : 20
        buttonSize: root.height/14.0
    }


}
